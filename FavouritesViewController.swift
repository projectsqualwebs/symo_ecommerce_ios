//
//  FavouritesViewController.swift
//  Symo New
//
//  Created by Apple on 01/04/21
//

import UIKit

class FavouritesViewController: UIViewController, Confirmation {
    //MARK: Delegate Functions
    func confirmationSelection(action: Int) {
        if (self.currentAlertModal == 1){
            if(action == 1){
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                myVC.redirectBack = true
                self.navigationController?.pushViewController(myVC, animated: true)
            }else{
                NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_TABBAR), object: nil,userInfo: ["home":true])
                self.dismiss(animated: true)
            }
        }else{
            if(action == 1){
                ActivityIndicator.show(view: self.view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_REMOVE_FROM_WISHLIST, method: .post, parameter: ["product_id":self.favouriteData[self.selectedIndex].wishlist_items?.id ?? 0], objectClass: SuccessResponse.self, requestCode: U_REMOVE_FROM_WISHLIST, userToken: nil) { (response) in
                    Singleton.shared.wishlistResponse = [WishlistResponse]()
                    ActivityIndicator.hide()
                    NavigationController.shared.getWishlistItem { (response) in
                        self.favouriteData = response
                        self.favouriteTable.reloadData()
                    }
                }
            }
        }
        
    }
    
    
    //MARK: IBOutlets
    @IBOutlet weak var pinkView: UIView!
    @IBOutlet weak var favouriteTable: UITableView!
    @IBOutlet weak var noFaovuriteView: UIView!
    @IBOutlet weak var menuIcon: UIImageView!
    @IBOutlet weak var menuHeight: NSLayoutConstraint!
    @IBOutlet weak var noDataLabel: DesignableUILabel!
    @IBOutlet weak var noDataDescription: DesignableUILabel!
    @IBOutlet weak var bagCount: DesignableUILabel!
    @IBOutlet weak var myFavouritesLabel: DesignableUILabel!
    
    
    var favouriteData = [WishlistResponse]()
    var selectedIndex = Int()
    var isBackButton = false
    var isPinkViewHidden = false
    var isRedirectionFromSideBar = false
    var currentAlertModal = 0
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.myFavouritesLabel.text = "My Favourites".localizableString(loc: Singleton.shared.language ?? "en")
        
        self.pinkView.isHidden = self.isPinkViewHidden
        self.view.backgroundColor = .white
        self.pinkView.backgroundColor = K_PINK_COLOR
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
        
        
        if(isPinkViewHidden){
            self.noDataLabel.text = "No Offers Found".localizableString(loc: Singleton.shared.language ?? "en")
            self.noDataDescription.text = "Products offers will appear here.".localizableString(loc: Singleton.shared.language ?? "en")
        }else {
            self.noDataLabel.text = "Wishlist is empty!".localizableString(loc: Singleton.shared.language ?? "en")
            self.noDataDescription.text = "Your favourite products will appear here.".localizableString(loc: Singleton.shared.language ?? "en")
            if(self.isBackButton){
                self.menuIcon.image = #imageLiteral(resourceName: "back_arrow@2x-2")
                self.menuHeight.constant = 25
            }else {
                self.menuIcon.image = #imageLiteral(resourceName: "menu_icon")
                self.menuHeight.constant = 30
            }
            NavigationController.shared.getWishlistItem { (response) in
                Singleton.shared.wishlistResponse = response
                self.favouriteData = response
                self.favouriteTable.reloadData()
            }
        }
        if(K_BAG_COUNT == 0){
            self.bagCount.isHidden = true
        }else {
            self.bagCount.text = "\(K_BAG_COUNT)"
            self.bagCount.isHidden = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if Singleton.shared.userDetail.id == nil {
            self.showPopup(title: "Alert".localizableString(loc: Singleton.shared.language ?? "en"), msg: "Login to continue".localizableString(loc: Singleton.shared.language ?? "en"))
        }
    }
    
        
    //MARK: Functions
    func showPopup(title: String, msg: String) {
        
        
        self.currentAlertModal = 1
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationPopupViewController") as! ConfirmationPopupViewController
        myVC.confirmationDelegate = self
        myVC.confirmationText = title.localizableString(loc: Singleton.shared.language ?? "en")
        myVC.detailText = msg.localizableString(loc: Singleton.shared.language ?? "en")
        myVC.secondButtonTitle = "Not now".localizableString(loc: Singleton.shared.language ?? "en")
        myVC.modalPresentationStyle = .overFullScreen
        self.present(myVC, animated: true, completion: nil)
        
    }
    
    
    
    //MARK: IBActions
    @IBAction func menuAction(_ sender: Any) {
        if(self.isBackButton){
            if isRedirectionFromSideBar == true {
                self.menu()
            }
            self.navigationController?.popViewController(animated: true)
        }else {
            self.menu()
        }
    }
    
    @IBAction func bagAction(_ sender: Any) {
        if(self.isBackButton){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ShoppingBucketViewController") as! ShoppingBucketViewController
            myVC.showBackButton = true
            self.navigationController?.pushViewController(myVC, animated: true)
        }else {
            self.tabBarController?.selectedIndex = 2
        }
    }
    
    
}

//MARK: Table View Delegate
extension FavouritesViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.noFaovuriteView.isHidden = self.favouriteData.count == 0 ? false : true
        return self.favouriteData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableCell") as! CategoryTableCell
        let val = self.favouriteData[indexPath.row]
        if((val.wishlist_items?.default_image ?? "").contains("http")){
            cell.shopImage.sd_setImage(with: URL(string: (val.wishlist_items?.default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }else {
            cell.shopImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (val.wishlist_items?.default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }
        
        if(isPinkViewHidden){
            cell.bagView.isHidden = true
            cell.cancelView.isHidden =  true
        }
        let tip: Double = Double(val.wishlist_items?.avg_rating ?? "0.00")!
        cell.ratingText.text = String(format: "%.1f",tip)
        
        cell.shopName.text = val.wishlist_items?.name
        
        let currentPrice = convertPriceInDouble(price: val.wishlist_items?.price)
        let discountPrice = val.wishlist_items?.discount ?? 0
        
        if (val.wishlist_items?.offer_qty ?? 0) > 0 && (val.wishlist_items?.offer_valid_till ?? 0) > Int(NSDate().timeIntervalSince1970) {
            
            cell.shopPrice.attributedText = self.handlePrice(price: "\(currentPrice)" , cut: "\(val.wishlist_items?.offer_price ?? 0.00)")
            
        } else if val.wishlist_items?.discount != nil && discountPrice != 0.00 && (discountPrice) != (currentPrice){
            cell.shopPrice.attributedText = self.handlePrice(price: "\(currentPrice)" , cut: "\(discountPrice)")
        } else {
            cell.shopPrice.text = "\(Singleton.shared.selectedCurrencySymbol)" + " \(currentPrice.addComma)"
        }
        
        // for adding product in bag(cart)
        cell.heartButton={
            NavigationController.shared.addToBag(controller: self, qty: 1, variationId: val.id ?? 0, productId:val.product_id ?? 0) {
                (val) in
                self.bagCount.text = "\(K_BAG_COUNT)"
                self.bagCount.isHidden = K_BAG_COUNT > 0 ? false : true
                Singleton.shared.showToast(msg:"Product added successfully".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 1)
            }
        }
        
        // for removing product from favourite
        cell.bagButton={
            self.currentAlertModal = 0
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationPopupViewController") as! ConfirmationPopupViewController
            myVC.confirmationDelegate = self
            myVC.secondTxt = true
            myVC.detailText = "Do you want to remove this item?".localizableString(loc: Singleton.shared.language ?? "en")
            myVC.confirmationText = "Remove item".localizableString(loc: Singleton.shared.language ?? "en")
            self.selectedIndex = indexPath.row
            myVC.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SingleProductViewController") as! SingleProductViewController
        myVC.selectedProduct = self.favouriteData[indexPath.row].wishlist_items ?? ProductResponse()
        myVC.selectedVarientId = self.favouriteData[indexPath.row].variation_id ?? 0
        myVC.subdomain = self.favouriteData[indexPath.row].wishlist_items?.vendor_details?.subdomain ?? ""
        if(myVC.subdomain != ""){
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
}
