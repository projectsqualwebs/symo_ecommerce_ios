//
//  CategoryViewController.swift
//  Symo New
//
//  Created by Apple on 06/04/21
//

import UIKit
import Cosmos
import SDWebImage
import ImageSlideshow
import CoreLocation
import MapKit
import CallKit

class CategoryViewController: UIViewController, Confirmation, CXCallObserverDelegate,PostReview {
    // delegate fucntions
    func addProductReview() {// new review added so fetch data again using delegate function
        self.getVendorDetail()
    }
    
    func confirmationSelection(action: Int) {
        if(action == 1){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            myVC.redirectBack = true
            self.navigationController?.pushViewController(myVC, animated: true)
        }else if (currentAlertModal == 1){
            NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_TABBAR), object: nil,userInfo: ["home":true])
            self.dismiss(animated: true)
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var shopProductsLabel: DesignableUILabel!
    @IBOutlet weak var viewProductButtom: CustomButton!
    @IBOutlet weak var nearbyShopCollection: UICollectionView!
    @IBOutlet weak var shopProduct: UICollectionView!
    @IBOutlet weak var shopImage: ImageView!
    @IBOutlet weak var vendorName: DesignableUILabel!
    @IBOutlet weak var vendorRating: CosmosView!
    @IBOutlet weak var totalReview: DesignableUILabel!
    @IBOutlet weak var mobileNumber: DesignableUILabel!
    @IBOutlet weak var address: DesignableUILabel!
    @IBOutlet weak var time: DesignableUILabel!
    @IBOutlet weak var vendorDescription: DesignableUILabel!
    @IBOutlet weak var fovouriteIcon: UIImageView!
    @IBOutlet weak var noProductLabel: DesignableUILabel!
    @IBOutlet weak var noMostSellingLabel: DesignableUILabel!
    @IBOutlet weak var noGalleryImage: DesignableUILabel!
    @IBOutlet weak var mostSellingCollection: UICollectionView!
    @IBOutlet weak var shopProductsViewAllButton: CustomButton!
    @IBOutlet weak var mostSellingLabel: DesignableUILabel!
    @IBOutlet weak var mostSellingViewAllButton: CustomButton!
    @IBOutlet weak var shopGalleryLabel: DesignableUILabel!
    @IBOutlet weak var openingClosingTimeLabel: UILabel!
    @IBOutlet weak var storeLogo: ImageView!
    @IBOutlet weak var storeMail: DesignableUILabel!
    @IBOutlet weak var storeDomain: DesignableUILabel!
    @IBOutlet weak var storeWebsiteStack: UIStackView!
    @IBOutlet weak var physicalOnlineStoreLabel: DesignableUILabel!
    @IBOutlet weak var storeAvailabilityTableView: ContentSizedTableView!
    @IBOutlet weak var availablityNotFoundLabel: DesignableUILabel!
    @IBOutlet weak var featuresCollectionView: UICollectionView!
    @IBOutlet weak var featureCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var availablityView: UIView!
    @IBOutlet weak var starImage: UIImageView!
    @IBOutlet weak var bookMarkImage: UIImageView!
    @IBOutlet weak var halfCircleView: View!
    @IBOutlet weak var descriptionView: View!
    @IBOutlet weak var descriptionTitleView: View!
    @IBOutlet weak var bannerViewheightConstraint: NSLayoutConstraint!
    
    static var bagCountDelegate : settingBagCount? = nil
    var vendorId = Int()
    var vendorDetail = ShopDetails()
    var productDetail = [ProductResponse]()
    let callObserver = CXCallObserver()
    var didDetectOutgoingCall = false
    var storeAvailablity = [StoreAvailability]()
    var featuresData = [FeatureData]()
    
    private var currentAlertModal = 0
    
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationController.shared.changeStatusBar(color: K_PINK_COLOR)
        self.view.backgroundColor = .white
        callObserver.setDelegate(self, queue: nil)
        self.halfCircleView.topCornorRound = self.halfCircleView.frame.size.width / 6
        didDetectOutgoingCall = false
        PostReviewViewController.postDelegate = self
        self.shopProductsLabel.text = "Shop Products".localizableString(loc: Singleton.shared.language ?? "en")
        self.shopProductsViewAllButton.setTitle("   View All   ".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.mostSellingLabel.text = "Most Selling".localizableString(loc: Singleton.shared.language ?? "en")
        self.mostSellingViewAllButton.setTitle("   View All   ".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.shopGalleryLabel.text = "Shop Gallery".localizableString(loc: Singleton.shared.language ?? "en")
        self.noProductLabel.text = "No Product Found".localizableString(loc: Singleton.shared.language ?? "en")
        self.noMostSellingLabel.text = "No Product Found".localizableString(loc: Singleton.shared.language ?? "en")
        self.getVendorDetail()
        self.callVisitCount(index: 1)
        if Singleton.shared.bookmarkedVendors.contains(vendorId){
            self.bookMarkImage.image = #imageLiteral(resourceName: "MicrosoftTeams-image")
        }else{
            self.bookMarkImage.image = #imageLiteral(resourceName: "bookmark")
        }
        self.view.isUserInteractionEnabled = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 5){
            if self.vendorDetail == nil{
                self.getVendorDetail()
            }
        }
    }
    
    
    //MARK: Functions
    //for adjusting banner height as per loaded image (dynamic image height)
    func adjustBannerheight(for image: UIImage) {
        let aspectRatio = (image.size.height) / (image.size.width)
        let width = self.view.frame.width
        let height = width * aspectRatio
        self.bannerViewheightConstraint.constant = height + 0.5  //0.5 for bottom line
        self.view.layoutIfNeeded()
    }
    
    
    func getVendorDetail(){
        ActivityIndicator.show(view: self.view)
        let url = U_BASE + U_GET_VENDOR_DETAIL + "?currency=\(Singleton.shared.selectedCurrency)"
        guard let  token = UserDefaults.standard.value(forKey: UD_ACCESS_TOKEN) as? String else {
            return
        }
        let param:[String:Any] = [
            "device_token":token,
            "business_id":vendorId,
            "lang":Singleton.shared.language
            
        ]
        SessionManager.shared.methodForApiCalling(url: url, method: .post, parameter: param, objectClass:GetVendorDetail.self, requestCode: url, userToken: nil) { (response) in
            self.vendorDetail = response.response
            self.productDetail = response.response.featured_products
            self.shopProduct.reloadData()
            self.mostSellingCollection.reloadData()
            self.storeAvailablity = response.response.availability ?? [StoreAvailability]()
            self.storeAvailabilityTableView.reloadData()
            self.view.isUserInteractionEnabled = true
            
            
            if self.vendorDetail.disabled_notification == 0 {
                self.starImage.image = UIImage(named: "bell-1")
            } else {
                self.starImage.image = UIImage(named: "notification_icon_2")
            }
            
            
            if self.vendorDetail.shop.store_type == 1 {
                self.physicalOnlineStoreLabel.text = "Physical Store".localizableString(loc: Singleton.shared.language ?? "en")
            } else if self.vendorDetail.shop.store_type == 2 {
                self.physicalOnlineStoreLabel.text = "Online Store".localizableString(loc: Singleton.shared.language ?? "en")
            }else if self.vendorDetail.shop.store_type == 3 {
                self.physicalOnlineStoreLabel.text = "Physical & Online Store".localizableString(loc: Singleton.shared.language ?? "en")
            }
            
            if((self.vendorDetail.shop.shop_image ?? self.vendorDetail.shop.avatar ?? "").contains("http")){
                self.shopImage.sd_setImage(with: URL(string: (self.vendorDetail.shop.background_image ?? self.vendorDetail.shop.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder")) { image, error, cache, url in
                    if let downloadedImage = image {
                        self.adjustBannerheight(for: downloadedImage)
                    }
                }
            }else {
                self.shopImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (self.vendorDetail.shop.background_image ?? self.vendorDetail.shop.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder")){ image, error, cache, url in
                    if let downloadedImage = image {
                        self.adjustBannerheight(for: downloadedImage)
                    }
                }
            }
            
            if((self.vendorDetail.shop.avatar ?? self.vendorDetail.shop.shop_image ?? "").contains("http")){
                self.storeLogo.sd_setImage(with: URL(string: (self.vendorDetail.shop.avatar ?? self.vendorDetail.shop.shop_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }else {
                self.storeLogo.sd_setImage(with: URL(string: U_IMAGE_STORE_BASE + (self.vendorDetail.shop.avatar ?? self.vendorDetail.shop.shop_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }
            
            self.featuresData = []
            self.featuresData.append(FeatureData(name: "Delivery", image: "blue_delivery"))
            if self.vendorDetail.shop.pickup_delivery_setting == 1 {
                self.featuresData.append(FeatureData(name: "Pickup", image: "rectangle.filled.and.hand.point.up.left"))
            }
            if self.vendorDetail.shop.cod_method == 1 {
                self.featuresData.append(FeatureData(name: "COD", image: "blue_price"))
            }
            if self.vendorDetail.shop.schedule_delivery == 1 {
                self.featuresData.append(FeatureData(name: "One day delivery", image: "blue_calendar"))
            }
            if self.vendorDetail.shop.one_day_delivery == 1 {
                self.featuresData.append(FeatureData(name: "Schedule delivery", image: "blue_calendar"))
            }
            self.featuresCollectionView.reloadData()
            
            self.storeDomain.text = self.vendorDetail.shop.website ?? ""
            if self.vendorDetail.shop.website == nil || self.vendorDetail.shop.website == "" {
                self.storeWebsiteStack.isHidden = true
            } else {
                self.storeWebsiteStack.isHidden = false
            }
            
//            if self.vendorDetail.shop.is_bookmark == 1 {
//                self.bookMarkImage.image = #imageLiteral(resourceName: "MicrosoftTeams-image")
//            }else {
//                self.bookMarkImage.image = #imageLiteral(resourceName: "bookmark")
//            }
            
            self.storeMail.text = self.vendorDetail.shop.email ?? ""
            self.vendorName.text = self.vendorDetail.shop.vendor_name
            self.mobileNumber.text = "+" + (self.vendorDetail.phoneCode?.country_code ?? "") + (self.vendorDetail.shop.phone ?? "")
            self.address.text =  (self.vendorDetail.shop.address ?? "")
            if((self.vendorDetail.shop.description ?? "") == "" || (self.vendorDetail.shop.description == "NULL") || self.vendorDetail.shop.description == nil){
                self.vendorDescription.text = "No description"
            }else {
                self.vendorDescription.text = self.vendorDetail.shop.description
                //                self.vendorDescription.isHidden = false
            }
            //            if self.vendorDetail.shop.today == nil {
            //                self.openingClosingTimeLabel.isHidden = true
            //            } else {
            //                self.openingClosingTimeLabel.isHidden = false
            //            }
            self.openingClosingTimeLabel.text = "Opening Time".localizableString(loc: Singleton.shared.language ?? "en") + " : " + "\(self.vendorDetail.shop.today?.open_time ?? "")" + " - " + "Closing time".localizableString(loc: Singleton.shared.language ?? "en") + " : " + "\(self.vendorDetail.shop.today?.close_time ?? "")"
            
            self.totalReview.text = "\(self.vendorDetail.reviews?.count ?? 0)" + " Reviews".localizableString(loc: Singleton.shared.language ?? "en")
            switch self.vendorDetail.shop.rating {
            case .string(let text):
                self.vendorRating.rating = Double(text) ?? 0
                break
            case .double(let text):
                break
                self.vendorRating.rating = Double(text)
            case .int(let text):
                self.vendorRating.rating = Double(text)
                break
            default:
                break
            }
            self.nearbyShopCollection.reloadData()
        }
        
        self.view.isUserInteractionEnabled = true
    }
    
    func callVisitCount(index:Int){
        
        ActivityIndicator.show(view: self.view)
        guard let  token = UserDefaults.standard.value(forKey: UD_ACCESS_TOKEN) as? String else {
            return
            
        }
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_VISIT_COUNT, method: .post,
                                                  parameter: ["device_token": token,"vendor_id":self.vendorId,"visit":index], objectClass: ErrorResponse.self, requestCode: U_VISIT_COUNT, userToken: UD_TOKEN) { (response) in
            self.view.isUserInteractionEnabled = true
            ActivityIndicator.hide()
        }
        
    }
    
    func addNotifObserver() {
        let selector = #selector(appDidBecomeActive)
        let notifName = UIApplication.didBecomeActiveNotification
        NotificationCenter.default.addObserver(self, selector: selector, name: notifName, object: nil)
    }
    
    @objc func appDidBecomeActive() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
            if !(self?.didDetectOutgoingCall ?? true) {
                print("Cancel button pressed")
            }
        }
    }
    
    func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall) {
        if call.isOutgoing && !didDetectOutgoingCall {
            didDetectOutgoingCall = true
            self.callVisitCount(index: 3)
            print("Call button pressed")
        }
    }
    
    func showConfirmationPopup(){
        self.view.isUserInteractionEnabled = true
        self.currentAlertModal = 1
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationPopupViewController") as! ConfirmationPopupViewController
        myVC.confirmationDelegate = self
        myVC.confirmationText = "Please login to proceed".localizableString(loc: Singleton.shared.language ?? "en")
        myVC.secondTxt = false
        myVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        CategoryViewController.bagCountDelegate?.setBagCount()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func reviewAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PostReviewViewController") as! PostReviewViewController
        myVC.productId = self.vendorId
        myVC.redirection = 2
        myVC.reviewData = self.vendorDetail.reviews ?? []
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func bookMarkAction(_ sender: Any) {
        if(Singleton.shared.userDetail.id != nil){
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_BOOKMARK_VENODR, method: .post,
                                                      parameter: ["vendor_id": self.vendorId], objectClass: SuccessResponse.self, requestCode: U_BOOKMARK_VENODR, userToken: UD_TOKEN) { (response) in
                if self.bookMarkImage.image == UIImage(named: "bookmark") {
                    self.bookMarkImage.image = #imageLiteral(resourceName: "MicrosoftTeams-image")
                    Singleton.shared.bookmarkedVendors.append(self.vendorId)
                }else {
                    self.bookMarkImage.image = #imageLiteral(resourceName: "bookmark")
                    Singleton.shared.bookmarkedVendors.removeAll { $0 == self.vendorId }
                }
                
                Singleton.shared.showToast(msg: response.message?.localizableString(loc: Singleton.shared.language ?? "en") ?? "", controller: self, type: 0)
                ActivityIndicator.hide()
            }
        } else {
            self.showPopup(title: "Alert".localizableString(loc: Singleton.shared.language ?? "en"), msg: "Login to continue".localizableString(loc: Singleton.shared.language ?? "en"))
        }
    }
    
    func showPopup(title: String, msg: String) {
        
        self.currentAlertModal = 1
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationPopupViewController") as! ConfirmationPopupViewController
        myVC.confirmationDelegate = self
        myVC.confirmationText = title.localizableString(loc: Singleton.shared.language ?? "en")
        myVC.detailText = msg.localizableString(loc: Singleton.shared.language ?? "en")
        myVC.secondButtonTitle = "Not now".localizableString(loc: Singleton.shared.language ?? "en")
        myVC.modalPresentationCapturesStatusBarAppearance = true
        myVC.modalPresentationStyle = .overFullScreen
        self.present(myVC, animated: true, completion: nil)
    }
    
    func showMapsOption(title: String, msg: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .actionSheet)
        let yesBtn = UIAlertAction(title:"Apple Map".localizableString(loc: Singleton.shared.language ?? "en"), style: .default) { (UIAlertAction) in
            let latitude:CLLocationDegrees =  Double(self.vendorDetail.shop.latitude ?? "0")!
            let longitude:CLLocationDegrees =  Double(self.vendorDetail.shop.longitude ?? "0")!
            let regionDistance:CLLocationDistance = 10000
            let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
            let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
            ]
            let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = "\(self.vendorDetail.shop.name ?? "")"
            self.callVisitCount(index: 2)
            mapItem.openInMaps(launchOptions: options)
        }
        let mapBtn = UIAlertAction(title:"Google Map".localizableString(loc: Singleton.shared.language ?? "en"), style: .default) { (UIAlertAction) in
            if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
                let name = self.vendorDetail.shop.name?.replacingOccurrences(of: " ", with: "+")
                let latLng = "\(self.vendorDetail.shop.latitude ?? "0"),\(self.vendorDetail.shop.latitude ?? "0")"
                let url = "comgooglemaps://?q=\(latLng)&center=\(latLng)"
                self.callVisitCount(index: 2)
                UIApplication.shared.openURL(NSURL(string: url)! as URL)
            } else {
                let appStore = URL(string: "https://itunes.apple.com/us/app/google-maps-navigation-transit/id585027354?mt=8#")
                UIApplication.shared.openURL(appStore!)
            }
        }
        let noBtn = UIAlertAction(title: "Cancel".localizableString(loc: Singleton.shared.language ?? "en"), style: .default){
            (UIAlertAction) in
            self.dismiss(animated: true)
        }
        
        alert.addAction(mapBtn)
        alert.addAction(yesBtn)
        alert.addAction(noBtn)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func favouriteStoreAction(_ sender: Any) {
        let param:[String:Any] = ["business_id":vendorId]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_SHOP_NOTIFICATION, method: .post, parameter: param, objectClass:SuccessResponse.self, requestCode: U_SHOP_NOTIFICATION, userToken: nil) { (response) in
            if((response.message ?? "").contains("enabled")){
                self.starImage.image = UIImage(named: "bell-1")
            } else {
                self.starImage.image = UIImage(named: "notification_icon_2")
            }
            
            Singleton.shared.showToast(msg: response.message?.localizableString(loc: Singleton.shared.language ?? "en") ?? "", controller: self, type: 1)
            
        }
        
        
    }
    
    @IBAction func whatsappAction(_ sender: Any) {
        let urlWhats = "whatsapp://send?phone=\(self.vendorDetail.phoneCode?.country_code ?? "971")\(self.vendorDetail.shop.whatsapp_number ?? self.vendorDetail.shop.phone ?? "")"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL){
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(whatsappURL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(whatsappURL)
                    }
                }
                else {
                    Singleton.shared.showToast(msg: "Install Whatsapp".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                }
            }
        }
    }
    
    
    @IBAction func viewProductAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ProductScreenViewController") as! ProductScreenViewController
        myVC.subdomain = self.vendorDetail.shop.subdomain ?? ""
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func viewAllShopAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ProductScreenViewController") as! ProductScreenViewController
        myVC.subdomain = self.vendorDetail.shop.subdomain ?? ""
        myVC.shopName = self.vendorDetail.shop.name ?? self.vendorDetail.shop.name_en ?? ""
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func markFavouriteAction(_ sender: Any) {
        if(Singleton.shared.userDetail.id != nil){
            if(self.fovouriteIcon.image == #imageLiteral(resourceName: "bookmark_icon@2x-2")){
                self.fovouriteIcon.image = #imageLiteral(resourceName: "bookmark_select")
                NavigationController.shared.addBookMarkVendor(id: self.vendorDetail.shop.vendor_id ?? 0, action: 1) {
                    
                }
            }else {
                NavigationController.shared.addBookMarkVendor(id: self.vendorDetail.shop.vendor_id ?? 0, action: 2) {
                    
                }
            }
        }else {
            self.showConfirmationPopup()
            
        }
    }
    
    @IBAction func callAction(_ sender: Any) {
        if let phoneCallURL = URL(string: "telprompt://\(self.vendorDetail.shop.phone ?? "")") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:]) {[weak self] success in
                    if success {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                            self?.addNotifObserver()
                        }
                    }
                    
                }
                
            }
        }
    }
    
    @IBAction func notificationAction(_ sender: Any) {
        self.showMapsOption(title: "Open in", msg: "Open \(self.vendorDetail.shop.name ?? "")'s location on")
    }
    
    
    @IBAction func shareAction(_ sender: Any) {
        self.share(self,K_SHARE_BUSINESS, self.vendorDetail, nil)
    }
    
    @IBAction func postAction(_ sender: Any) {
        if(Singleton.shared.userDetail.id != nil){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PostReviewViewController") as! PostReviewViewController
            myVC.productId = self.vendorDetail.shop.vendor_id ?? 0
            self.navigationController?.pushViewController(myVC, animated: true)
        }else {
            self.showConfirmationPopup()
        }
    }
    
    @IBAction func viewAllMostSellingAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ProductScreenViewController") as! ProductScreenViewController
        myVC.subdomain = self.vendorDetail.shop.subdomain ?? ""
        myVC.isMostSelling = true
        myVC.mostSellingData = self.vendorDetail.similarProducts ?? []
        self.navigationController?.pushViewController(myVC, animated: true)
        
    }
    
    @IBAction func exploreAllProductsAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ProductScreenViewController") as! ProductScreenViewController
        myVC.subdomain = self.vendorDetail.shop.subdomain ?? ""
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}

//MARK: Table View Delegate
extension CategoryViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.availablityView.isHidden = self.storeAvailablity.count > 0 ? false : true
        return self.storeAvailablity.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableCell") as! CategoryTableCell
        cell.shopName.text = "\(self.storeAvailablity[indexPath.row].day ?? "")"
        cell.shopDistance.text = "\(self.storeAvailablity[indexPath.row].open_time ?? "") - \(self.storeAvailablity[indexPath.row].close_time ?? "")"
        return cell
    }
}

//MARK: Collection View
extension CategoryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == nearbyShopCollection){
            self.noGalleryImage.text = "No Image(s) Found".localizableString(loc: Singleton.shared.language ?? "en")
            if((self.vendorDetail.gallery_images?.count ?? 0) == 0){
                self.noGalleryImage.isHidden = false
            }else {
                self.noGalleryImage.isHidden = true
            }
            return (self.vendorDetail.gallery_images?.count ?? 0)
        }else if(collectionView == shopProduct){
            if(self.productDetail.count == 0){
                self.noProductLabel.isHidden = false
            }else {
                self.noProductLabel.isHidden = true
            }
            return self.productDetail.count
        }else if(collectionView == mostSellingCollection){
            if(self.vendorDetail.similarProducts.count == 0){
                self.noMostSellingLabel.isHidden = false
            }else {
                self.noMostSellingLabel.isHidden = true
            }
            return self.vendorDetail.similarProducts.count
        }else if(collectionView == featuresCollectionView){
            if featuresData.count == 1 || featuresData.count == 2 {
                self.featureCollectionHeight.constant = 30
            } else if featuresData.count == 3 || featuresData.count == 4 {
                self.featureCollectionHeight.constant = 60
            } else if featuresData.count == 5 || featuresData.count == 6 {
                self.featureCollectionHeight.constant = 90
            } else {
                self.featureCollectionHeight.constant = 0
            }
            return self.featuresData.count
        }
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionCell", for: indexPath) as! CategoryCollectionCell
        
        
        if(collectionView == featuresCollectionView){
            
            cell.shopImage.image = UIImage(named: self.featuresData[indexPath.row].image ?? "")
            cell.shopName.text = (self.featuresData[indexPath.row].name ?? "").localizableString(loc: Singleton.shared.language ?? "en")
            
        }else if(collectionView == nearbyShopCollection){
            if((((self.vendorDetail.gallery_images?[indexPath.row].image ?? "").contains("http")))){
                cell.shopImage.sd_setImage(with: URL(string: (self.vendorDetail.gallery_images?[indexPath.row].image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }else {
                cell.shopImage.sd_setImage(with: URL(string:U_IMAGE_BASE + (self.vendorDetail.gallery_images?[indexPath.row].image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }
        }else if(collectionView == shopProduct){
            
            var currentPrice = convertPriceInDouble(price: productDetail[indexPath.row].price)
            //            var discountPrice = convertPriceInDouble(price: self.productDetail[indexPath.row].discounted_price)
            let discountPrice = self.productDetail[indexPath.row].discount ?? 0
            
            if((((self.productDetail[indexPath.row].default_image ?? "").contains("http")))){
                cell.shopImage.sd_setImage(with: URL(string: (self.productDetail[indexPath.row].default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }else {
                cell.shopImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (self.productDetail[indexPath.row].default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }
            cell.shopName.text = self.productDetail[indexPath.row].name
            
            if (self.productDetail[indexPath.row].offer_qty ?? 0) > 0 && (self.productDetail[indexPath.row].offer_valid_till ?? 0) > Int(NSDate().timeIntervalSince1970) {
                
                cell.shopPrice.attributedText = self.handlePrice(price: "\(currentPrice)", cut: "\(self.productDetail[indexPath.row].offer_price ?? 0.00)")
                
            } else if self.productDetail[indexPath.row].discount != nil && discountPrice != 0.00 && (discountPrice) != (currentPrice){
                cell.shopPrice.attributedText = self.handlePrice(price: "\(currentPrice)", cut: "\(discountPrice)")
            } else {
                cell.shopPrice.text = "\(Singleton.shared.selectedCurrencySymbol)" + " \(currentPrice.addComma)"
            }
            let tip: Double = Double(self.productDetail[indexPath.row].avg_rating ?? "0.00")!
            cell.shopRating.text = String(format: "%.1f", tip)
            
            if self.productDetail[indexPath.row].is_favorite == 1{
                cell.favouriteIcon.image = #imageLiteral(resourceName: "like")
                cell.favouriteIcon.changeTint(color: K_PINK_COLOR)
            } else {
                cell.favouriteIcon.image = #imageLiteral(resourceName: "heart_outline")
                cell.favouriteIcon.changeTint(color: K_PINK_COLOR)
            }
            
            // that is_favorite is now so did these..
            NavigationController.shared.getWishlistItem { (response) in
                for val in response{
                    if(val.product_id == self.productDetail[indexPath.row].id){
                        cell.favouriteIcon.image = #imageLiteral(resourceName: "like")
                        cell.favouriteIcon.changeTint(color: K_PINK_COLOR)
                    }
                }
            }
            
            cell.favouriteButton = {
                if(cell.favouriteIcon.image == #imageLiteral(resourceName: "heart_outline")){
                    NavigationController.shared.addRemoveProductAsFavourite(view: self.view, id: self.productDetail[indexPath.row].id ?? 0, variantId: 0, action: 1) { (val) in
                        self.productDetail[indexPath.row].is_favorite = 1
                        cell.favouriteIcon.image = #imageLiteral(resourceName: "like")
                        cell.favouriteIcon.changeTint(color: K_PINK_COLOR)
                        Singleton.shared.showToast(msg: val.localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 0)
                        Singleton.shared.wishlistResponse = []
                        //self.handleFavourite()
                    }
                }else {
                    NavigationController.shared.addRemoveProductAsFavourite(view: self.view, id: self.productDetail[indexPath.row].id ?? 0, variantId: 0, action: 2) { (val) in
                        self.productDetail[indexPath.row].is_favorite = 0
                        cell.favouriteIcon.image = #imageLiteral(resourceName: "heart_outline")
                        cell.favouriteIcon.changeTint(color: K_PINK_COLOR)
                        Singleton.shared.showToast(msg: val.localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 0)
                        Singleton.shared.wishlistResponse = []
                        // self.handleFavourite()
                    }
                }
                
            }
        }
        else if(collectionView == mostSellingCollection){
            if((((self.vendorDetail.similarProducts[indexPath.row].default_image ?? "").contains("http")))){
                cell.shopImage.sd_setImage(with: URL(string: (self.vendorDetail.similarProducts[indexPath.row].default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }else {
                cell.shopImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (self.vendorDetail.similarProducts[indexPath.row].default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }
            cell.shopName.text = self.vendorDetail.similarProducts[indexPath.row].name
            
            
            var currentPrice = convertPriceInDouble(price: vendorDetail.similarProducts[indexPath.row].price)
            //            var discountPrice = convertPriceInDouble(price: self.vendorDetail.similarProducts[indexPath.row].discounted_price)
            let discountPrice = self.vendorDetail.similarProducts[indexPath.row].discount ?? 0
            
            if (self.vendorDetail.similarProducts[indexPath.row].offer_qty ?? 0) > 0 && (self.vendorDetail.similarProducts[indexPath.row].offer_valid_till ?? 0) > Int(NSDate().timeIntervalSince1970) {
                
                cell.shopPrice.attributedText = self.handlePrice(price: "\(currentPrice)", cut: "\(self.vendorDetail.similarProducts[indexPath.row].offer_price ?? 0.00)")
                
            } else if self.vendorDetail.similarProducts[indexPath.row].discount != nil && discountPrice != 0.00 && (discountPrice) != (currentPrice){
                cell.shopPrice.attributedText = self.handlePrice(price: "\(currentPrice)", cut: "\(discountPrice)")
            } else {
                cell.shopPrice.text = "\(Singleton.shared.selectedCurrencySymbol)" + " \(currentPrice.addComma)"
            }
            
            let tip: Double = Double(self.vendorDetail.similarProducts[indexPath.row].avg_rating ?? "0.00")!
            cell.shopRating.text = String(format: "%.1f", tip)
            if self.vendorDetail.similarProducts[indexPath.row].is_favorite == 1{
                cell.favouriteIcon.image = #imageLiteral(resourceName: "like")
                cell.favouriteIcon.changeTint(color: K_PINK_COLOR)
            } else {
                cell.favouriteIcon.image = #imageLiteral(resourceName: "heart_outline")
                cell.favouriteIcon.changeTint(color: K_PINK_COLOR)
            }
            
            // that is_favorite is now so did these..
            NavigationController.shared.getWishlistItem { (response) in
                for val in response{
                    if(val.product_id == self.vendorDetail.similarProducts[indexPath.row].id){
                        cell.favouriteIcon.image = #imageLiteral(resourceName: "like")
                        cell.favouriteIcon.changeTint(color: K_PINK_COLOR)
                    }
                }
            }
            
            cell.favouriteButton = {
                if(cell.favouriteIcon.image == #imageLiteral(resourceName: "heart_outline")){
                    NavigationController.shared.addRemoveProductAsFavourite(view: self.view, id: self.vendorDetail.similarProducts[indexPath.row].id ?? 0, variantId: 0, action: 1) { (val) in
                        self.vendorDetail.similarProducts[indexPath.row].is_favorite = 1
                        cell.favouriteIcon.image = #imageLiteral(resourceName: "like")
                        cell.favouriteIcon.changeTint(color: K_PINK_COLOR)
                        Singleton.shared.showToast(msg: val.localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 0)
                        Singleton.shared.wishlistResponse = []
                    }
                }else {
                    NavigationController.shared.addRemoveProductAsFavourite(view: self.view, id: self.vendorDetail.similarProducts[indexPath.row].id ?? 0, variantId: 0, action: 2) { (val) in
                        self.vendorDetail.similarProducts[indexPath.row].is_favorite = 0
                        cell.favouriteIcon.image = #imageLiteral(resourceName: "heart_outline")
                        cell.favouriteIcon.changeTint(color: K_PINK_COLOR)
                        Singleton.shared.showToast(msg: val.localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 0)
                        Singleton.shared.wishlistResponse = []
                    }
                }
                
            }
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView == shopProduct){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SingleProductViewController") as! SingleProductViewController
            myVC.subdomain = self.vendorDetail.shop.subdomain ?? ""
            myVC.selectedProduct = self.productDetail[indexPath.row]
            self.navigationController?.pushViewController(myVC, animated: true)
        }else if(collectionView == mostSellingCollection){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SingleProductViewController") as! SingleProductViewController
            
            myVC.subdomain = self.vendorDetail.similarProducts[indexPath.row].vendor_details?.subdomain ?? ""
            myVC.selectedProduct = self.vendorDetail.similarProducts[indexPath.row]
            self.navigationController?.pushViewController(myVC, animated: true)
        }else if(collectionView == nearbyShopCollection){
            let imageSlider = ImageSlideshow()
            var input = [SDWebImageSource]()
            let currentImage = self.vendorDetail.gallery_images?[indexPath.row].image ?? ""
            input.append(SDWebImageSource(urlString: currentImage)!)
            for val in self.vendorDetail.gallery_images! {
                if val.image != currentImage {
                    input.append(SDWebImageSource(urlString:val.image ?? "")!)
                }
            }
            imageSlider.setImageInputs(input)
            let fullScreenController = imageSlider.presentFullScreenController(from: self)
            
            fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(collectionView == nearbyShopCollection){
            return CGSize(width: collectionView.frame.size.width/2.5-20, height: 100)
        }else if (collectionView == featuresCollectionView){
            return CGSize(width: collectionView.frame.size.width/2-20, height: 20)
        }else {
            return CGSize(width: collectionView.frame.size.width/2.5-20, height: 200)
        }
        return CGSize(width: collectionView.frame.size.width/2.5-20, height: 200)
    }
}


struct FeatureData : Codable {
    var name : String?
    var image : String?
}
