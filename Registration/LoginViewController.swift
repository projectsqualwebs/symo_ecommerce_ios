//
//  LoginViewController.swift
//  Symo New
//
//  Created by Apple on 30/03/21.
//

import UIKit
import SkyFloatingLabelTextField
import GoogleSignIn
import FBSDKLoginKit
import AuthenticationServices


class LoginViewController: UIViewController, Confirmation {
    
    
    func confirmationSelection(action: Int) {
        if(action == 1){
            var cartId = ""
            if(UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) != nil){
                cartId = UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) as! String
            }
            let param: [String:Any] = [
                "email":storedEmail,
                "flag": storedFlagId,
                "google_id":storedGoogleId ?? "",
                "facebook_id":storedFacebookId ?? "",
                "apple_id": storedAppleId ?? "",
                "firebase_token" : UserDefaults.standard.value(forKey: UD_ACCESS_TOKEN) as? String,
                "lang" : "en",
                "platform":2,
                "cart_id": cartId == "" ? nil : cartId
            ]
            ActivityIndicator.show(view: self.view)
            
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_UPDATE_SOCIAL_DETAIL, method: .post, parameter: param, objectClass: LoginResponse.self, requestCode: U_UPDATE_SOCIAL_DETAIL, userToken: nil) { response in
                Singleton.shared.userDetail = response.response
                UserDefaults.standard.setValue(response.response.access_token, forKey: UD_TOKEN)
                Singleton.shared.saveUserDetail(user: response.response)
                UserDefaults.standard.removeObject(forKey: UD_GUEST_CART_ID)
                ActivityIndicator.hide()
                let vNVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                self.navigationController?.pushViewController(vNVC, animated: true)
            }
        }
    }
    
    
    

    
    
    
    
    //MARK: IBOutlets
    @IBOutlet weak var loginLabel: DesignableUILabel!
    @IBOutlet weak var emailFIeld: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var passwordField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var forgotPassword: CustomButton!
    @IBOutlet weak var loginButton: CustomButton!
    @IBOutlet weak var orLabel: DesignableUILabel!
    @IBOutlet weak var noAccountLabel: DesignableUILabel!
    @IBOutlet weak var signupButton: CustomButton!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var pinkView: UIView!
    
    
    var redirectBack = false
    var googleData = GIDGoogleUser()
    var fbData = [String:Any]()
    var loginFrom = 1
    var appleData = [String:Any]()
    var isRedirectionFromSideBar = false
    
    var storedFlagId = 0
    var storedGoogleId = ""
    var storedFacebookId =  ""
    var storedAppleId = ""
    var storedEmail = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeView()
        let loginManager = LoginManager()
        loginManager.logOut()
        let helper = Helper()
        let textFields = [emailFIeld,  passwordField]
        for index in 0..<textFields.count {
            helper.setupFields(textField: textFields[index]! , index: index, form: K_LOGIN_FORM)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
    }
    
    func localizeView(){
        self.loginLabel.text = "Login".localizableString(loc: Singleton.shared.language ?? "en")
        self.emailFIeld.placeholder = "Email Address".localizableString(loc: Singleton.shared.language ?? "en")
        self.passwordField.placeholder = "Password".localizableString(loc: Singleton.shared.language ?? "en")
        self.forgotPassword.setTitle("Forgot Password?".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.loginButton.setTitle("Login".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.orLabel.text = "OR".localizableString(loc: Singleton.shared.language ?? "en")
        
        self.noAccountLabel.text = "Don't have an account?".localizableString(loc: Singleton.shared.language ?? "en")
        self.signupButton.setTitle("Sign Up".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
    }
    
    func callSocialLoginAPI(flafId: Int,gId: String?,fId: String?, appleId: String?,email: String?){
        ActivityIndicator.show(view: self.view)
        var cartId = ""
        if(UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) != nil){
            cartId = UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) as! String
        }
        let param = [
            "flag" : flafId, // 1 for facebook_id , 2 for google_id 3 for apple
            "lang" : "en",
            "google_id":gId ?? "",
            "facebook_id":fId ?? "",
            "apple_id": appleId ?? "",
            "firebase_token" : UserDefaults.standard.value(forKey: UD_ACCESS_TOKEN) as? String,
            "email":email,
            "platform": 2,
            "cart_id" : cartId == "" ? nil : cartId
        ] as? [String:Any]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_SOCIAL_MEDIA_LOGIN, method: .post, parameter: param, objectClass: LoginResponse.self, requestCode: U_SOCIAL_MEDIA_LOGIN, userToken: nil) { (response) in
            ActivityIndicator.hide()
            if(response.response.status_flag == 1){
                Singleton.shared.userDetail = response.response
                UserDefaults.standard.setValue(response.response.access_token, forKey: UD_TOKEN)
                Singleton.shared.saveUserDetail(user: response.response)
                ActivityIndicator.hide()
                if self.redirectBack == false {
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                    self.navigationController?.pushViewController(myVC, animated: true)
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
            }else if(response.response.status_flag == 2){
                if self.loginFrom != 3 {
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
                    if(self.loginFrom == 1){
                        myVC.fName = self.fbData["first_name"] as? String ?? ""
                        myVC.lName = self.fbData["last_name"] as? String ?? ""
                        myVC.userEmail = self.fbData["email"] as? String ?? ""
                        myVC.fbId = self.fbData["id"] as? String
                    }else if(self.loginFrom == 2){
                        var components = self.googleData.profile?.name.components(separatedBy: " ")
                        if((components != nil) && components!.count > 0){
                            let firstName = components!.removeFirst()
                            let lastName = components!.joined(separator: " ")
                            myVC.fName = firstName
                            myVC.lName = lastName
                            
                        }
                        myVC.googleId = self.googleData.userID
                        myVC.userEmail = self.googleData.profile?.email ?? ""
                    }
                    else if(self.loginFrom == 3){
                        myVC.fName = self.appleData["first_name"] as? String ?? ""
                        myVC.lName = self.appleData["last_name"] as? String ?? ""
                        myVC.userEmail = self.appleData["email"] as? String ?? ""
                        myVC.appleId = self.appleData["id"] as? String
                    }
                    self.navigationController?.pushViewController(myVC, animated: true)
                    Singleton.shared.showToast(msg: "User not found please enter details to signup".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                    
                }
                else {
                    self.registerWithApple()
                }
            }else if(response.response.status_flag == 3){
                
                self.storedFlagId = flafId
                self.storedGoogleId = gId ?? ""
                self.storedFacebookId = fId ?? ""
                self.storedAppleId = appleId ?? ""
                self.storedEmail = email ?? ""
                
                
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationPopupViewController") as! ConfirmationPopupViewController
                myVC.confirmationDelegate = self
                myVC.confirmationText = "Are you sure?".localizableString(loc: Singleton.shared.language ?? "en")
                myVC.detailText = "The email already exist, do you want to link account with existing email?".localizableString(loc: Singleton.shared.language ?? "en")
                myVC.firstButtonTitle = "Confirm".localizableString(loc: Singleton.shared.language ?? "en")
                myVC.secondButtonTitle = "Cancel".localizableString(loc: Singleton.shared.language ?? "en")
                myVC.modalPresentationStyle = .overFullScreen
                self.present(myVC, animated: true, completion: nil)
                
            }
            
        }
    }
    
    func getFacebookUserInfo(){
        let loginManager = LoginManager()
        loginManager.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
            if let error = error {
                print("Failed to login: \(error.localizedDescription)")
                return
            }
            guard let accessToken = AccessToken.current else {
                print("Failed to get access token")
                return
            }
            self.getFBUserData()
        }
    }
    
    func getFBUserData(){
        if((AccessToken.current) != nil){
            let request = GraphRequest(graphPath: "me", parameters: [:])
            request.start() { connection, result, error in
                if (error == nil){
                    print(result)
                    if let data = result as? [String:Any]{
                        self.fbData = data
                        self.callSocialLoginAPI(flafId: 1, gId: nil, fId: data["id"] as! String, appleId: nil, email: data["email"] as? String)
                    }
                }
            }
        }
    }
    
    
    func registerWithApple(){
        var cartId = ""
        if(UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) != nil){
            cartId = UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) as! String
        }
        var param:[String:Any] = [
            "name": (self.appleData["first_name"] as? String ?? "") + " " + (self.appleData["last_name"] as? String ?? ""),
            "email":self.appleData["email"] as? String,
            "password": "123456",
            "city_id": 4,
            "country_id" : Singleton.shared.selectedCountry.id,
            "country": Singleton.shared.selectedCountry.id ?? 0,
            "country_code": Singleton.shared.selectedCountry.country_code,
            "platform":2,
            "firebase_token" : UserDefaults.standard.value(forKey: UD_ACCESS_TOKEN) as? String,
            "apple_id": self.appleData["id"] as? String,
            "cart_id": cartId == "" ? nil : cartId
        ]
        
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_SIGNUP, method: .put, parameter: param, objectClass: LoginResponse.self, requestCode: U_SIGNUP, userToken: nil) { (response) in
            ActivityIndicator.hide()
            UserDefaults.standard.setValue(response.response.access_token ?? "", forKey: UD_TOKEN)
            Singleton.shared.saveUserDetail(user: response.response)
            Singleton.shared.userDetail = response.response
            UserDefaults.standard.removeObject(forKey: UD_GUEST_CART_ID)
            if self.redirectBack == false {
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                self.navigationController?.pushViewController(myVC, animated: true)
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    //MARK: IBActions
    @IBAction func forgotPasswordAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func loginAction(_ sender: Any) {
        if(self.emailFIeld.text!.isEmpty){
            Singleton.shared.showToast(msg: "Enter Email Address".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if !(self.isValidEmail(emailStr: self.emailFIeld.text ?? "")){
            Singleton.shared.showToast(msg: "Enter valid Email Address".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if(self.passwordField.text!.isEmpty){
            Singleton.shared.showToast(msg: "Enter Password".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else {
            var cartId = ""
            if(UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) != nil){
                cartId = UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) as! String
            }
            let param:[String:Any] = [
                "email":self.emailFIeld.text ?? "",
                "password":self.passwordField.text ?? "",
                "lang": "en",
                "firebase_token" : UserDefaults.standard.value(forKey: UD_ACCESS_TOKEN) as? String,
                "platform":2,
                "cart_id": cartId == "" ? nil : cartId
            ]
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_LOGIN
                                                      , method: .post, parameter: param, objectClass: LoginResponse.self, requestCode: U_LOGIN, userToken: nil) { (response) in
                ActivityIndicator.hide()
                Singleton.shared.showToast(msg: "Logged In Successfully".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 1)
                UserDefaults.standard.setValue(response.response.access_token ?? "", forKey: UD_TOKEN)
                Singleton.shared.userDetail = response.response
                UserDefaults.standard.removeObject(forKey: UD_GUEST_CART_ID)
                Singleton.shared.saveUserDetail(user: response.response)
                if self.redirectBack == false {
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                    self.navigationController?.pushViewController(myVC, animated: true)
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
                
            }
            
        }
    }
    
    @IBAction func signupAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func backAction(_ sender: Any) {
//        NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_TABBAR), object: nil,userInfo: ["home":true])
        if isRedirectionFromSideBar == true {
            self.menu()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction private func loginWithReadPermissions() {
        loginFrom = 1
        self.getFacebookUserInfo()
    }
    
    @IBAction func googleAction(_ sender: Any) {
        loginFrom = 2
        let signInConfig = GIDConfiguration(clientID: GOOGLE_CLIENT_ID)
        GIDSignIn.sharedInstance.signIn(withPresenting: self) { signInResult, error in
            guard let signInUser = GIDSignIn.sharedInstance.currentUser  else {
                
                return
            }
            
            self.googleData = signInUser
            self.callSocialLoginAPI(flafId: 2, gId: signInUser.userID, fId:nil, appleId: nil, email: signInUser.profile?.email)
        }
    }
    
    
    
    @IBAction func appleAction(_ sender: Any) {
        if #available(iOS 13.0, *) {
            loginFrom = 3
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.performRequests()
        }
    }
}

//MARK: For Apple login
extension LoginViewController: ASAuthorizationControllerDelegate{
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error.localizedDescription)
    }
    
    // Authorization Succeeded
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            
            // Get user data with Apple ID credentitial
            let userId = appleIDCredential.user
            let userFirstName = appleIDCredential.fullName?.givenName as? String
            let userLastName = appleIDCredential.fullName?.familyName as? String
            let userEmail = appleIDCredential.email
            if (userEmail == nil || userEmail == "" || userEmail == (UserDefaults.standard.value(forKey: UD_APPLE_EMAIL) as? String)) {
                self.appleData["email"] = UserDefaults.standard.value(forKey: UD_APPLE_EMAIL) as? String
                self.appleData["first_name"] = UserDefaults.standard.value(forKey: UD_APPLE_FIRST_NAME) as? String
                self.appleData["last_name"] = UserDefaults.standard.value(forKey: UD_APPLE_LAST_NAME) as? String
                self.appleData["id"] = UserDefaults.standard.value(forKey: UD_APPLE_ID) as? String
                self.callSocialLoginAPI(flafId: 3, gId: nil, fId: nil, appleId: userId, email: UserDefaults.standard.value(forKey: UD_APPLE_EMAIL) as? String)
            } else {
                self.appleData["email"] = userEmail
                self.appleData["first_name"] = userFirstName
                self.appleData["last_name"] = userLastName
                self.appleData["id"] = userId
                UserDefaults.standard.setValue(userEmail, forKey: UD_APPLE_EMAIL)
                UserDefaults.standard.setValue(userFirstName, forKey: UD_APPLE_FIRST_NAME)
                UserDefaults.standard.setValue(userLastName, forKey: UD_APPLE_LAST_NAME)
                UserDefaults.standard.setValue(userId, forKey: UD_APPLE_ID)
                self.callSocialLoginAPI(flafId: 3, gId: nil, fId: nil, appleId: userId, email: userEmail ?? "")
            }
            
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            // Get user data using an existing iCloud Keychain credential
            let appleUsername = passwordCredential.user
            let applePassword = passwordCredential.password
            // Write your code here
        }
    }
}
