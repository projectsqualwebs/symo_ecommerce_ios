//
//  ForgotPasswordViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 13/11/21.
//

import UIKit
import SkyFloatingLabelTextField

class ForgotPasswordViewController: UIViewController ,UITextFieldDelegate{
    //MARK: IBOutlets
    @IBOutlet weak var emailField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var forgotPasswordHeading: DesignableUILabel!
    @IBOutlet weak var sendEmailButton: CustomButton!
    
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
        self.emailField.delegate = self
        if Singleton.shared.language == "ur" || Singleton.shared.language == "ar" {
            self.emailField.textAlignment = .right
        } else {
            self.emailField.textAlignment = .left
        }
        self.forgotPasswordHeading.text = "Forgot Password".localizableString(loc: Singleton.shared.language ?? "en")
        self.sendEmailButton.setTitle("Send Email".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.emailField.placeholder = "Email Address".localizableString(loc: Singleton.shared.language ?? "en")
        emailField.iconType = .image
        let helper = Helper()
        let textFields = [emailField]
        for index in 0..<textFields.count {
            helper.setupFields(textField: textFields[index]! , index: index, form: K_FORGOT_FORM)
        }
        handleTextAlignmentForArabic(view: self.view)
    }
    
    //MARK: IBActions
    @IBAction func sendAction(_ sender: Any) {
        if(self.emailField.text!.isEmpty){
            Singleton.shared.showToast(msg: "Enter email address".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else {
            ActivityIndicator.show(view: self.view)
            let param:[String:Any] = [
                "email": self.emailField.text,
                "lang": Singleton.shared.language ?? "en"
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_FORGOT_PASSWORD, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_FORGOT_PASSWORD, userToken: nil) { response in
                ActivityIndicator.hide()
                Singleton.shared.showToast(msg: response.message?.localizableString(loc: Singleton.shared.language ?? "en") ?? "", controller: self, type: 0)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
