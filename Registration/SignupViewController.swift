//
//  SignupViewController.swift
//  Symo New
//
//  Created by Apple on 30/03/21
//

import UIKit
import SkyFloatingLabelTextField
import BEMCheckBox

class SignupViewController: UIViewController, SelectFromPicker, SelectDate {
    //MARK: Delegate functions
    //picker fucntion
    func selectedPickerData(val: String, pos: Int) {
        if (self.currentPicker == 1){
            Singleton.shared.selectedCountry = Singleton.shared.countryData[pos]
            self.countryField.text = val
            self.cityField.text = ""
            Singleton.shared.cityData = []
            self.countryCode.text = "+" + (Singleton.shared.selectedCountry.country_code ?? "")
            NavigationController.shared.getCities(countryId: Singleton.shared.selectedCountry.id ?? 0) { (val) in
            }
        }else if(self.currentPicker == 2){
            Singleton.shared.selectedCity = Singleton.shared.cityData[pos]
            let city = try? JSONEncoder().encode(Singleton.shared.cityData[pos])
            Singleton.shared.selectedCity = Singleton.shared.cityData[pos]
            UserDefaults.standard.setValue(city, forKey: UD_SELECTED_CITY)
            self.cityField.text = val
        }else if(self.currentPicker == 3){
            self.gender.text = val
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
            self.view.isUserInteractionEnabled = true
        }
    }
    
    // date picker function
    func selectedDate(timestamp: Date) {
        self.dobField.text = self.convertTimestampToDate(Int(timestamp.timeIntervalSince1970), to: "dd-MM-yy")
    }
    
    //MARK: IBOutlets
    
    @IBOutlet weak var signupLabel: DesignableUILabel!
    @IBOutlet weak var fullName: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var email: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var countryField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var countryCode: DesignableUILabel!
    @IBOutlet weak var contactNumber: DesignableUITextField!
    @IBOutlet weak var gender: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var dobField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var password: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var confirmPassword: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var cityField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var pinkView: UIView!
    @IBOutlet weak var accountType: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var registerButton: CustomButton!
    @IBOutlet weak var haveAccountLabel: DesignableUILabel!
    @IBOutlet weak var loginButton: CustomButton!
    @IBOutlet weak var termsConditionCheckBox: BEMCheckBox!
    @IBOutlet weak var bySigningUpLabel: UILabel!
    @IBOutlet weak var checkBoxImage: UIImageView!
    
    var currentPicker = 1
    var fName = String()
    var lName = String()
    var userEmail = String()
    var fbId: String?
    var googleId: String?
    var appleId: String?
    var isTermsSelected = false
    var isRedirectionFromSideBar = false
    var redirectBack = false
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeView()
        self.contactNumber.delegate = self
        if (Singleton.shared.language ?? "en") == "ur" || (Singleton.shared.language ?? "en") == "ar" {
            self.contactNumber.textAlignment = .right
        } else {
            self.contactNumber.textAlignment = .left
        }
        Singleton.shared.cityData = []
        if(fName != ""){
            self.fullName.text = self.fName +  " " + self.lName
        }
        
        if(userEmail != ""){
            self.email.text = self.userEmail 
        }
        //TextFields
        let helper = Helper()
        let textFields = [fullName, email, countryField, gender, dobField, password, confirmPassword, cityField, accountType]
        for index in 0..<textFields.count{
            helper.setupFields(textField: textFields[index]! , index: index, form: K_SIGNUP_FORM)
        }
        handleTextAlignmentForArabic(view: self.view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
    }
    
    //MARK: Functions
    func localizeView(){
        self.signupLabel.text = "Sign up".localizableString(loc: Singleton.shared.language ?? "en")
        self.fullName.placeholder = "Full Name".localizableString(loc: Singleton.shared.language ?? "en")
        self.email.placeholder = "Email Address".localizableString(loc: Singleton.shared.language ?? "en")
        self.countryField.placeholder = "Country".localizableString(loc: Singleton.shared.language ?? "en")
        self.contactNumber.placeholder = "Contact Number".localizableString(loc: Singleton.shared.language ?? "en")
        self.gender.placeholder = "Gender".localizableString(loc: Singleton.shared.language ?? "en")
        self.dobField.placeholder = "Date of Birth".localizableString(loc: Singleton.shared.language ?? "en")
        self.password.placeholder = "Create Password".localizableString(loc: Singleton.shared.language ?? "en")
        self.confirmPassword.placeholder = "Confirm Password".localizableString(loc: Singleton.shared.language ?? "en")
        self.cityField.placeholder = "City".localizableString(loc: Singleton.shared.language ?? "en")
        self.registerButton.setTitle("Register Now".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.loginButton.setTitle("Login".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.haveAccountLabel.text = "Already have an account?".localizableString(loc: Singleton.shared.language ?? "en")
        
    }
    
    //MARK: IBActions
    
    @IBAction func signupAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        if isRedirectionFromSideBar == true {
            self.menu()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countryAction(_ sender: Any) {
        currentPicker = 1
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        for val in Singleton.shared.countryData{
            myVC.pickerData.append(val.name_en ?? "")
        }
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func genderAction(_ sender: Any) {
        currentPicker = 3
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        myVC.pickerData = ["Male".localizableString(loc: Singleton.shared.language ?? "en"), "Female".localizableString(loc: Singleton.shared.language ?? "en")]
        myVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func cityAction(_ sender: Any) {
        currentPicker = 2
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        myVC.pickerData.append("All")
        for val in Singleton.shared.cityData{
            myVC.pickerData.append(val.name ?? "")
        }
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func dobAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        myVC.dateDelegate = self
        myVC.maxDate = Date()
        myVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    @IBAction func termsConditionCheckBoxAction(_ sender: BEMCheckBox) {
        if isTermsSelected == false {
            self.isTermsSelected = true
        } else {
            self.isTermsSelected = false
        }
        
    }
    @IBAction func goToTermsAndConditionsAction(_ sender: Any) {
        self.openUrl(urlStr: U_TERMS_CONDITION)
    }
    
    @IBAction func registerAction(_ sender: Any) {
        if(self.fullName.text!.isEmpty){
            Singleton.shared.showToast(msg: "Enter Full Name".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if(self.email.text!.isEmpty){
            Singleton.shared.showToast(msg: "Enter Email Address".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if !(self.isValidEmail(emailStr: self.email.text ?? "")){
            Singleton.shared.showToast(msg: "Enter valid Email Address".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if(self.countryField.text!.isEmpty){
            Singleton.shared.showToast(msg: "Select Country".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if(self.cityField.text!.isEmpty){
            Singleton.shared.showToast(msg: "Select City".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        } else if(self.contactNumber.text!.isEmpty){
            Singleton.shared.showToast(msg: "Enter Contact Number".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if(self.password.text!.isEmpty){
            Singleton.shared.showToast(msg: "Enter Password".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if(self.confirmPassword.text!.isEmpty){
            Singleton.shared.showToast(msg: "Enter confirm password".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        } else if (self.confirmPassword.text != self.password.text){
            Singleton.shared.showToast(msg: "Enter correct password".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        } else if (self.isTermsSelected == false){
            Singleton.shared.showToast(msg: "Please check the terms and conditions".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        } else {
            ActivityIndicator.show(view: self.view)
            var cartId = ""
            if(UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) != nil){
                cartId = UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) as! String
            }
            var param:[String:Any] = [
                "name": self.fullName.text,
                "email":self.email.text,
                "password": self.password.text,
                "phone" :self.contactNumber.text,
                "city_id": Singleton.shared.selectedCity.city_id,
                "country_id" : Singleton.shared.selectedCountry.id,
                "country": Singleton.shared.selectedCountry.id ?? 0,
                "country_code": self.countryCode.text,
                "platform":2,
                "firebase_token" : UserDefaults.standard.value(forKey: UD_ACCESS_TOKEN) as? String,
                "cart_id": cartId == "" ? nil : cartId
            ]
            if(self.googleId != nil){
                param["google_id"] = self.googleId ?? ""
            }
            if(self.fbId != nil){
                param["facebook_id"] = self.fbId ?? ""
            }
            if(self.appleId != nil){
                param["apple_id"] = self.appleId!
            }
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_SIGNUP, method: .put, parameter: param, objectClass: LoginResponse.self, requestCode: U_SIGNUP, userToken: nil) { (response) in
                ActivityIndicator.hide()
                Singleton.shared.showToast(msg: "User Registered Successfully".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 1)
                UserDefaults.standard.setValue(response.response.access_token ?? "", forKey: UD_TOKEN)
                Singleton.shared.saveUserDetail(user: response.response)
                UserDefaults.standard.removeObject(forKey: UD_GUEST_CART_ID)
                Singleton.shared.userDetail = response.response
                if self.redirectBack == false {
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                    self.navigationController?.pushViewController(myVC, animated: true)
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    @IBAction func checkBoxAction(_ sender: Any) {
        if isTermsSelected == false {
            self.isTermsSelected = true
            checkBoxImage.image = UIImage(named: "selected")
        } else {
            self.isTermsSelected = false
            checkBoxImage.image = UIImage(named: "unselected")
        }
    }
}

extension SignupViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == contactNumber){
            let text = (self.contactNumber.text ?? "") + string
            if(text.count <= 10){
                return true
            }else {
                return false
            }
        }
        return true
    }
}
