//
//  SplashScreenViewController.swift
//  Symo New
//
//  Created by Apple on 30/03/21.
//

protocol CustomUserActivityDelegate {
    func handleUserActivity()
}

import UIKit

class SplashScreenViewController: UIViewController {
    
    //MARK: IBOutlet
    @IBOutlet weak var containerView: UIView!
  
    var userActivityDelegate: CustomUserActivityDelegate? = nil
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.setValue(true, forKey: UD_FIRST_TIME)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.changeStatusBar(color: .black)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}

