//
//  SplashScreen1ViewController.swift
//  Symo New
//
//  Created by Apple on 30/03/21.
//

import UIKit

class SplashScreen1ViewController: UIViewController, UIGestureRecognizerDelegate {
    
    private var minSpaceFromTop: CGFloat = 100
    private var defaultGreaterThanHeight : CGFloat = 500
    
    //MARK: IBOutlet
    @IBOutlet weak var labelHeading: UILabel!
    @IBOutlet weak var labelDetails: UILabel!
    @IBOutlet weak var whyUsLabel: UILabel!
    @IBOutlet weak var freeDeliveryLabel: DesignableUILabel!
    @IBOutlet weak var exploreLabel: DesignableUILabel!
    @IBOutlet weak var joinSymo: DesignableUILabel!
    @IBOutlet weak var continueLabel: CustomButton!
    @IBOutlet weak var termsConditionsLabel: CustomButton!
    @IBOutlet weak var greatherThanContraint: NSLayoutConstraint!
    @IBOutlet weak var stackHideShow: UIStackView!
    @IBOutlet weak var swipeUpCard: UIView!
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    
    //MARK: View Cycle
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setSpaceFromTop()
        self.bottomHeight.constant = 0
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(self.wasDragged(gestureRecognizer:)))
                swipeUpCard.addGestureRecognizer(gesture)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(_:)))
        swipeUpCard.addGestureRecognizer(tapGesture)
        swipeUpCard.isUserInteractionEnabled = true
        gesture.delegate = self
        self.stackHideShow.isHidden = false
        NavigationController.shared.changeStatusBar(color: .black)
        self.labelHeading.text = "WELCOME"
        self.labelDetails.text = "Discover the best local and international luxury brands.".localizableString(loc: Singleton.shared.language ?? "en") + "\n" + "Be the first to buy latest arrivals.".localizableString(loc: Singleton.shared.language ?? "en")
        self.whyUsLabel.text = "WHY US?".localizableString(loc: Singleton.shared.language ?? "en")
        self.freeDeliveryLabel.text = "Free delivery".localizableString(loc: Singleton.shared.language ?? "en") + "\n" + "and return".localizableString(loc: Singleton.shared.language ?? "en")
        self.exploreLabel.text = "Explore over".localizableString(loc: Singleton.shared.language ?? "en") + "\n" + "50k products".localizableString(loc: Singleton.shared.language ?? "en")
        self.joinSymo.text = "Join symo today and get".localizableString(loc: Singleton.shared.language ?? "en") + "\n" + "best offers".localizableString(loc: Singleton.shared.language ?? "en")
        self.continueLabel.setTitle("CONTINUE".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.termsConditionsLabel.setTitle("*Terms & Condition".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        defaultGreaterThanHeight = self.greatherThanContraint.constant
    }
    
    @objc func wasDragged(gestureRecognizer: UIPanGestureRecognizer) {
        if gestureRecognizer.state == UIGestureRecognizer.State.began || gestureRecognizer.state == UIGestureRecognizer.State.changed {
            let translation = gestureRecognizer.translation(in: self.view)
            if(greatherThanContraint.constant + translation.y > minSpaceFromTop){
                if(greatherThanContraint.constant + translation.y < (UIApplication.shared.keyWindow?.safeAreaLayoutGuide.layoutFrame.size.height ?? 50) - 50){
                    greatherThanContraint.constant = greatherThanContraint.constant + translation.y
                    if( -(bottomHeight.constant + translation.y - 100) < 0){
                        bottomHeight.constant = -(bottomHeight.constant + translation.y - 100)
                    }
                }
            }else {
                self.swipeUpCard.isUserInteractionEnabled = true
            }
            gestureRecognizer.setTranslation(CGPoint(x: 0,y: 0), in: self.view)
        }  else if gestureRecognizer.state == .ended {
            // Snap to specific positions based on final position
            
            let middlePosition = (self.view.frame.height - 170) / 2
            
            if greatherThanContraint.constant < middlePosition {
                greatherThanContraint.constant = minSpaceFromTop
            } else {
                greatherThanContraint.constant = self.view.frame.height - 170
            }
            
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
            
            swipeUpCard.isUserInteractionEnabled = true
        }
    }
    
    @objc func handleTapGesture(_ gesture: UITapGestureRecognizer) {
        // Handle the tap gesture here
        print("View tapped!")
        UIView.animate(withDuration: 0.3){
            if self.greatherThanContraint.constant == self.minSpaceFromTop {
                self.greatherThanContraint.constant = self.view.frame.height - 170
            } else{
                self.greatherThanContraint.constant = self.minSpaceFromTop
            }
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK: Action
    @IBAction func skip(_ sender: UIButton) {
        self.openUrl(urlStr: U_TERMS_CONDITION)
        setCountry()
    }
    
    @IBAction func next(_ sender: UIButton) {
        let pageVC = self.parent as! PageViewController
        pageVC.nextController(index: 1, direction: .forward)
        setCountry()
    }
    
    @IBAction func setHeightAction(_ sender: Any) {
        if self.greatherThanContraint.constant == 100 {
            self.greatherThanContraint.constant = self.view.frame.height - 300
            self.stackHideShow.isHidden = true
        } else{
            self.greatherThanContraint.constant = 100
            self.stackHideShow.isHidden = false
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //first time set country..
    func setCountry(){
        if let country = UserDefaults.standard.string(forKey: UD_CURRENT_COUNTRY){
            print("Current country : \(country)")
            let lowercaseCountry = country.lowercased()
            if lowercaseCountry.contains("bahrain") {
                NavigationController.shared.changeCountry(pos: 1)
            } else {
                NavigationController.shared.changeCountry(pos: 0)
            }
        }
    }
    
    //for setting top constraint of content.
    private func setSpaceFromTop(){
        //screen size(safe height) - content data
        //screensize = total height - safe area height
        //content Data = 30+270+155 = 455, margin - 20+10+20+20 =70  = 525
        self.minSpaceFromTop = (Singleton.shared.screenData.safeHeight ?? 600) - 525
        self.greatherThanContraint.constant = self.minSpaceFromTop
        
    }
    
}
