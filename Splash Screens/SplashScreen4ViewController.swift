//
//  SplashScreen4ViewController.swift
//  Symo New
//
//  Created by Apple on 30/03/21.
//

import UIKit

class SplashScreen4ViewController: UIViewController, SelectFromPicker {
    func selectedPickerData(val: String, pos: Int) {
        
    }
    
    //MARK: IBOutlet
    @IBOutlet weak var buttongetStarted: UIButton!
    @IBOutlet weak var engLangView: View!
    @IBOutlet weak var arabicLangView: View!
    
    var langugage: String = "en"
    var arabicToggle: String = "false"
    var langData = [LanguageResponse]()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        arabicLangView.borderColor = .white
        engLangView.borderColor = .white
        NavigationController.shared.changeStatusBar(color: .black)
        NavigationController.shared.getLangByCountry(countryId: Singleton.shared.selectedCountry.id ?? 0) { response in
            self.langData = response
            
            for i in self.langData {
                if i.lang_name == "English" {
                    Singleton.shared.selectedLanguage = i.code ?? ""
                    Singleton.shared.saveAppLanguage(withLanguage: i.code ?? "")
                    UserDefaults.standard.setValue(Singleton.shared.selectedLanguage, forKey: K_APP_LANGUAGE)
                    self.engLangView.borderColor = K_PINK_COLOR
                }
            }
        }
    }
    
    //MARK: Action
    @IBAction func languageAction(_ sender: UIButton) {
        
        if self.langData.count != 0{
            let pos = sender.tag
            self.engLangView.borderColor = pos == 0 ? K_PINK_COLOR : .white
            self.arabicLangView.borderColor = pos == 1 ? K_PINK_COLOR: .white
            
            Singleton.shared.selectedLanguage = self.langData[pos].code ?? ""
            Singleton.shared.language = self.langData[pos].code ?? ""
            Singleton.shared.saveAppLanguage(withLanguage: self.langData[pos].code ?? "" )
            UserDefaults.standard.setValue(Singleton.shared.selectedLanguage, forKey: K_APP_LANGUAGE)
        }
    }
    
    
    
    @IBAction func getStarted(_ sender: UIButton) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
        UserDefaults.standard.setValue(false, forKey: UD_FIRST_TIME)
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    
}
