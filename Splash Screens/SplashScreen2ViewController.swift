//
//  SplashScreen2ViewController.swift
//  Symo New
//
//  Created by Apple on 30/03/21.
//
import UIKit

class SplashScreen2ViewController: UIViewController, UIGestureRecognizerDelegate {

    
    private var minSpaceFromTop: CGFloat = 100
    
    //MARK: IBOutlet
    @IBOutlet weak var labelHeading: UILabel!
    @IBOutlet weak var labelDetails: UILabel!
    @IBOutlet weak var notifyButton: CustomButton!
    @IBOutlet weak var notNowButton: CustomButton!
    @IBOutlet weak var imageHideShow: UIImageView!
    @IBOutlet weak var stackHideShow: UIStackView!
    @IBOutlet weak var heightConstarint: NSLayoutConstraint!
    @IBOutlet weak var swipeUpCard: UIView!
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    
    private var firstTime = true
    
    //MARK: View Cycle
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.setSpaceFromTop()
        self.bottomHeight.constant = 0
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(self.wasDragged(gestureRecognizer:)))
        swipeUpCard.addGestureRecognizer(gesture)
        swipeUpCard.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(_:)))
        swipeUpCard.addGestureRecognizer(tapGesture)
        gesture.delegate = self
        
        self.stackHideShow.isHidden = false
        self.imageHideShow.isHidden = false
        NavigationController.shared.changeStatusBar(color: .black)
        self.labelHeading.text = "GET NOTIFIED".localizableString(loc: Singleton.shared.language ?? "en")
        self.labelDetails.text = "Join symo now and get notified for latest deals, special offers on arrival.".localizableString(loc: Singleton.shared.language ?? "en")
        self.notifyButton.setTitle("YES, NOTIFY ME".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.notNowButton.setTitle("NOT RIGHT NOW".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
    }
    
    @objc func wasDragged(gestureRecognizer: UIPanGestureRecognizer) {
        if gestureRecognizer.state == UIGestureRecognizer.State.began || gestureRecognizer.state == UIGestureRecognizer.State.changed {
            let translation = gestureRecognizer.translation(in: self.view)
            if(heightConstarint.constant + translation.y > minSpaceFromTop){
                if(heightConstarint.constant + translation.y < (UIApplication.shared.keyWindow?.safeAreaLayoutGuide.layoutFrame.size.height ?? 50) - 50){
                    heightConstarint.constant = heightConstarint.constant + translation.y
                    if( -(bottomHeight.constant + translation.y - 100) < 0){
                    bottomHeight.constant = -(bottomHeight.constant + translation.y - 100)
                    }
                }
          }else {
              self.swipeUpCard.isUserInteractionEnabled = true
            }
            gestureRecognizer.setTranslation(CGPoint(x: 0,y: 0), in: self.view)
        }else if gestureRecognizer.state == .ended {
            // Snap to specific positions based on final position
            
            let middlePosition = (self.view.frame.height - 170) / 2
            
            if heightConstarint.constant < middlePosition {
                heightConstarint.constant = minSpaceFromTop
            } else {
                heightConstarint.constant = self.view.frame.height - 170
            }
            
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
            
            swipeUpCard.isUserInteractionEnabled = true
        }
    }
    
    
    @objc func handleTapGesture(_ gesture: UITapGestureRecognizer) {
        // Handle the tap gesture here
        print("View tapped!")
        UIView.animate(withDuration: 0.3){
            if self.heightConstarint.constant == self.minSpaceFromTop {
                self.heightConstarint.constant = self.view.frame.height - 170
            } else{
                self.heightConstarint.constant = self.minSpaceFromTop
            }
            self.view.layoutIfNeeded()
        }
    }

    //MARK: Action
    @IBAction func next(_ sender: UIButton) {
        let pageVC = self.parent as! PageViewController
        pageVC.nextController(index: 2, direction: .forward)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func heightAction(_ sender: Any) {
        if self.heightConstarint.constant == 100 {
            self.heightConstarint.constant = self.view.frame.height - 300
        } else {
            self.heightConstarint.constant = 100
        }
    }
    
    
    //for setting top constraint of content.
    private func setSpaceFromTop(){
        //screen size(safe height) - content data
        //screensize = total height - safe area height
        //content Data = 485, margin = 20+10+20 = 50 = 5
        self.minSpaceFromTop = (Singleton.shared.screenData.safeHeight ?? 600) - 535
        self.heightConstarint.constant = self.minSpaceFromTop
        
    }
}

