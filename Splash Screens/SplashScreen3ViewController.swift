//
//  SplashScreen3ViewController.swift
//  Symo New
//
//  Created by Apple on 30/03/21.
//

import UIKit
import SDWebImage

class SplashScreen3ViewController: UIViewController, SelectFromPicker {
    //MARK: Picker Delegate Method
    func selectedPickerData(val: String, pos: Int) {
        let city = try? JSONEncoder().encode(self.cityData[pos])
        Singleton.shared.selectedCity = self.cityData[pos]
        UserDefaults.standard.setValue(city, forKey: UD_SELECTED_CITY)
        
    }
    
    
    //MARK: IBOutlet
    @IBOutlet weak var labelDetails: UILabel!
    @IBOutlet weak var selectCountryLabel: DesignableUILabel!
    @IBOutlet weak var continueButton: CustomButton!
    
    @IBOutlet weak var uaeFlag: View!
    @IBOutlet weak var bahrainFlag: View!
    
    
    var countryData = [Countries]()
    var cityData = [Cities]()
    var currentPicker = 1
    private var firstTime = true
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationController.shared.changeStatusBar(color: .black)
        self.labelDetails.text = "Select your preference".localizableString(loc: Singleton.shared.language ?? "en")
        self.selectCountryLabel.text = "Select country".localizableString(loc: Singleton.shared.language ?? "en")
        self.continueButton.setTitle("CONTINUE".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
    }
    
    override func viewWillLayoutSubviews() {
        if firstTime{
            self.uaeFlag.borderColor = .clear
            self.bahrainFlag.borderColor = .clear
            Singleton.shared.selectedCountry = Countries()
            
            NavigationController.shared.getCountries { (response) in
                self.countryData = response
            }
        }
        firstTime = false
    }
    
    //MARK: Action
    @IBAction func next(_ sender: UIButton) {
        if(Singleton.shared.selectedCountry.id == nil){
            Singleton.shared.showToast(msg: "Select country".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if(Singleton.shared.selectedCity.name == nil){
            Singleton.shared.showToast(msg: "Select city".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else {
            let pageVC = self.parent as! PageViewController
            pageVC.nextController(index: 3, direction: .forward)
        }
    }
    
    @IBAction func countryAction(_ sender: UIButton) {
        if countryData.count != 0{
            selectCountry(pos: sender.tag)
        }
    }
    
    func selectCountry(pos: Int) {
        Singleton.shared.cityData = []
        self.cityData = []
        ActivityIndicator.show(view: self.view)
        Singleton.shared.selectedCountry = self.countryData[pos]
        let country = try? JSONEncoder().encode(Singleton.shared.selectedCountry)
        UserDefaults.standard.setValue(country, forKey: UD_SELECTED_COUNTRY)
        if Singleton.shared.selectedCountry.id == 7 {
            Singleton.shared.selectedCurrency = "aed"
            Singleton.shared.selectedCurrencySymbol = "AED"
            UserDefaults.standard.setValue("aed", forKey: UD_SELECTED_CURRENCY)
            self.bahrainFlag.borderColor = .clear
            self.uaeFlag.borderColor = .white
            
        } else {
            Singleton.shared.selectedCurrency = "bhd"
            Singleton.shared.selectedCurrencySymbol = "BHD"
            UserDefaults.standard.setValue("bhd", forKey: UD_SELECTED_CURRENCY)
            self.uaeFlag.borderColor = .clear
            self.bahrainFlag.borderColor = .white
        }
        ActivityIndicator.show(view: self.view)
        NavigationController.shared.getCities(countryId: self.countryData[pos].id ?? 0) { (val) in
            self.view.isUserInteractionEnabled = true
            for i in val {
                self.cityData.append(i)
            }
            if(self.cityData.count > 0){
                Singleton.shared.selectedCity = self.cityData[0]
                let city = try? JSONEncoder().encode(self.cityData[0])
                UserDefaults.standard.setValue(city, forKey: UD_SELECTED_CITY)
            }
            Singleton.shared.langData = []
            NavigationController.shared.getLangByCountry(countryId: Singleton.shared.selectedCountry.id ?? 0) { _ in
                
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
                myVC.pickerDelegate = self
                myVC.headingLabel = "Select City"
                for val in self.cityData {
                    myVC.pickerData.append(val.name ?? "")
                }
                myVC.modalPresentationStyle = .overFullScreen
                if(myVC.pickerData.count > 0){
                    self.navigationController?.present(myVC, animated: true, completion: nil)
                }
                
            }
            ActivityIndicator.hide()
        }
    }
}


class CountryCollectionCell: UICollectionViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var imageView: ImageView!
    @IBOutlet weak var nameLabel: DesignableUILabel!
    
}
