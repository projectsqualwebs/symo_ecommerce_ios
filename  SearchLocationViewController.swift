//
//  SearchLocationViewController.swift
//  Symo New
//
//  Created by Apple on 20/04/21.
//

import UIKit

class SearchLocationViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var locationTable: ContentSizedTableView!
    @IBOutlet weak var countrySearch: UITextField!
    @IBOutlet weak var nearbyLabel: DesignableUILabel!
    
    var countryData = [Countries]()
    var cityData = [Cities]()
    var currentData = 1
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nearbyLabel.text = "Nearby".localizableString(loc: Singleton.shared.language ?? "en")
        self.countrySearch.placeholder = "Search Country".localizableString(loc: Singleton.shared.language ?? "en")
        NavigationController.shared.getCountries { (val) in
            self.countryData = val
        }
        handleTextAlignmentForArabic(view: self.view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clearAction(_ sender: Any) {
    }
    
    
}

//MARK: View Cycle
extension SearchLocationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(currentData == 1){
            return self.countryData.count
        }else {
            return self.cityData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableCell") as!  MenuTableCell
        cell.menuLabel.text = currentData == 1 ? self.countryData[indexPath.row].name ?? self.countryData[indexPath.row].name_en : self.cityData[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(currentData == 1){
            Singleton.shared.cityData = []
            ActivityIndicator.show(view: self.view)
            let encode = try? JSONEncoder().encode(self.countryData[indexPath.row])
            Singleton.shared.selectedCountry = self.countryData[indexPath.row]
            NavigationController.shared.getCities(countryId: self.countryData[indexPath.row].id ?? 0) { (val) in
                self.view.isUserInteractionEnabled = true
                self.currentData = 2
                UserDefaults.standard.setValue(encode, forKey: UD_SELECTED_COUNTRY)
                self.countrySearch.placeholder = "Search City".localizableString(loc: Singleton.shared.language ?? "en")
                self.cityData = val
                self.locationTable.reloadData()
                ActivityIndicator.hide()
            }
        }else {
            let encode = try? JSONEncoder().encode(self.cityData[indexPath.row])
            UserDefaults.standard.setValue(encode, forKey: UD_SELECTED_CITY)
            Singleton.shared.selectedCity = self.cityData[indexPath.row]
            NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_CITY), object: nil, userInfo: ["callApi": "yes"])
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
}
