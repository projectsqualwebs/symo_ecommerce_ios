//
//  AddAddressViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 01/05/21
//

import UIKit
import MapKit
import GoogleMaps
import GooglePlaces
import SkyFloatingLabelTextField

protocol AddAddress {
    func addNewAddress()
}

class AddAddressViewController: UIViewController ,SelectFromPicker {
    
    //MARK: Picker delegate fucntion
    func selectedPickerData(val: String, pos: Int) {
        // 1 for country field
        if currentPicker == 1 {
            self.countryField.text = val
            self.selectedCountry = Singleton.shared.countryData[pos].id ?? 0
            selectedCountryCode = "+\(Singleton.shared.countryData[pos].country_code ?? "")"
            var number = mobileNumber.text ?? ""
            self.isCountrySelected = true
            number = number.replacingOccurrences(of: "+971 ", with: "")
            number = number.replacingOccurrences(of: "+973 ", with: "")
            self.mobileNumber.text = "+\(Singleton.shared.countryData[pos].country_code ?? "") " + number
            
            Singleton.shared.cityData = []
            self.cityFieldOptionArray = []
            self.cityField.text = ""
            if(Singleton.shared.countryData[pos].country_code == "971"){
                self.pincodeField.isHidden = true
            }else {
                self.pincodeField.isHidden = false
            }
            NavigationController.shared.getCities(countryId: self.selectedCountry) { (city) in
                for val in city{
                    if val.name != "All"{
                        self.cityFieldOptionArray.append(val.name ?? "")
                    }
                }
            }
            let camera = GMSCameraPosition.camera(withLatitude: Singleton.shared.countryData[pos].latitude ?? 0, longitude: Singleton.shared.countryData[pos].latitude ?? 0, zoom: Float(15))
            self.mapView.isMyLocationEnabled = true
            self.mapView.camera = camera
            self.myLocationMarker?.map = self.mapView
        } else if currentPicker == 2 { // 2 for city field
            self.cityField.text = val
            self.selectedCity = Singleton.shared.cityData[pos + 1].city_id ?? 0
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){ //We had did these because some time userInteraction off bydefault (currentky working fine by change in activity indicator.)
            self.view.isUserInteractionEnabled = true
        }
    }
    
    
    //MARK: IBOutlets
    

    @IBOutlet weak var firstName: SkyFloatingLabelTextField!
    @IBOutlet weak var lastField: SkyFloatingLabelTextField!
    @IBOutlet weak var mobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var addressField: SkyFloatingLabelTextField!
    @IBOutlet weak var pincodeField: SkyFloatingLabelTextField!
    @IBOutlet weak var countryField: SkyFloatingLabelTextField!
    @IBOutlet weak var cityField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var addNewAddressLabel: DesignableUILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var homeButton: CustomButton!
    @IBOutlet weak var officeButton: CustomButton!
    @IBOutlet weak var saveAddressButton: CustomButton!
    
    var isEditAddress = false
    var addressType = 1
    var selectedCity = Int()
    var selectedCountry = Int()
    var addressDelegate: AddAddress? = nil
    var myLocationMarker: GMSMarker?
    var addressData = Address()
    var latitude = CLLocationDegrees()
    var longitude = CLLocationDegrees()
    var address: CLPlacemark?
    var selectedCountryCode = String()
    var currentPicker = Int()
    var countryFieldOptionArray = [String]()
    var cityFieldOptionArray = [String]()
    var isCountrySelected = false
    
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pincodeField.delegate = self
        self.mobileNumber.delegate = self
        self.pincodeField.isHidden = true
        self.addNewAddressLabel.text = "Add New Address".localizableString(loc: Singleton.shared.language ?? "en")
        self.countryField.placeholder = "Country".localizableString(loc: Singleton.shared.language ?? "en")
        self.cityField.placeholder = "City".localizableString(loc: Singleton.shared.language ?? "en")
        self.firstName.placeholder = "First Name".localizableString(loc: Singleton.shared.language ?? "en")
        self.lastField.placeholder = "Last Name".localizableString(loc: Singleton.shared.language ?? "en")
        self.mobileNumber.placeholder = "Mobile Number".localizableString(loc: Singleton.shared.language ?? "en")
        self.addressField.placeholder = "Address".localizableString(loc: Singleton.shared.language ?? "en")
        self.pincodeField.placeholder = "Pincode".localizableString(loc: Singleton.shared.language ?? "en")
        
        self.countryField.font = UIFont(name:"Lato-Regular", size:15)!
        self.cityField.font = UIFont(name:"Lato-Regular", size:15)!
        self.firstName.font = UIFont(name:"Lato-Regular", size:15)!
        self.lastField.font = UIFont(name:"Lato-Regular", size:15)!
        self.mobileNumber.font = UIFont(name:"Lato-Regular", size:15)!
        self.addressField.font = UIFont(name:"Lato-Regular", size:15)!
        self.pincodeField.font = UIFont(name:"Lato-Regular", size:15)!

        self.mapView.settings.myLocationButton = true
        self.view.backgroundColor = .white
        
        NavigationController.shared.getCountries { (country) in
            for val in country{
                self.countryFieldOptionArray.append(val.name_en ?? "")
            }
        }
        if(self.isEditAddress){
            self.intializeView()
        }
        // for handling alignment right to left for arabic
        handleTextAlignmentForArabic(view: self.view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
    }
    
    func intializeView(){
        
        self.isCountrySelected = true
        self.selectedCountryCode =  Singleton.shared.countryData.first { $0.id == self.addressData.country_id ?? 0 }?.country_code ?? ""
        
        self.countryField.text = self.addressData.country_name
        self.selectedCountry = self.addressData.country_id ?? 0
        self.cityField.text = self.addressData.city_name
        self.selectedCity = self.addressData.city_id ?? 0
        self.firstName.text = self.addressData.firstname
        self.lastField.text = self.addressData.lastname
        self.mobileNumber.text = formatPhoneNumber(number: self.addressData.phone ?? "" , countryCode: selectedCountryCode)
        self.addressField.text = self.addressData.address
        self.pincodeField.text = self.addressData.postalcode != nil ? "\(self.addressData.postalcode!)" : ""
        Singleton.shared.cityData = []
        NavigationController.shared.getCities(countryId: self.selectedCountry) { (city) in
            for val in city{
                if val.name != "All"{
                    self.cityFieldOptionArray.append(val.name ?? val.name_en ?? "")
                }
            }
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addressTypeAction(_ sender: UIButton) {
        self.addressType = sender.tag
        if(sender.tag == 1){
            self.homeButton.setTitleColor(K_PINK_COLOR, for: .normal)
            self.officeButton.setTitleColor(.lightGray, for: .normal)
        }else if(sender.tag == 2){
            self.officeButton.setTitleColor(K_PINK_COLOR, for: .normal)
            self.homeButton.setTitleColor(.lightGray, for: .normal)
        }
    }
    
    @IBAction func googlePlacesAction(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue:UInt(GMSPlaceField.name.rawValue) |
                                                  UInt(GMSPlaceField.placeID.rawValue) |
                                                  UInt(GMSPlaceField.coordinate.rawValue) |
                                                  GMSPlaceField.addressComponents.rawValue |
                                                  GMSPlaceField.formattedAddress.rawValue)
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        if isCountrySelected {
            filter.countries = self.selectedCountry == 7 ? ["AE"] : ["BH"]
        }else{
            filter.countries = ["BH", "AE"]
        }
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        autocompleteController.modalPresentationStyle = .overFullScreen
        present(autocompleteController, animated: true, completion: nil)
    }
    
    //Save address details action (first check validation than call api)
    @IBAction func saveAddressAction(_ sender: Any) {
        if(self.countryField.text!.isEmpty){
            Singleton.shared.showToast(msg: "Select Country".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if(self.cityField.text!.isEmpty){
            Singleton.shared.showToast(msg: "Select City".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if(self.firstName.text!.isEmpty){
            Singleton.shared.showToast(msg: "Enter First Name".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if(self.lastField.text!.isEmpty){
            Singleton.shared.showToast(msg: "Enter Last Name".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if(self.mobileNumber.text!.isEmpty || self.mobileNumber.text?.count ?? 0 < 8){
            Singleton.shared.showToast(msg: "Enter Valid Mobile Number".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if(self.addressField.text!.isEmpty){
            Singleton.shared.showToast(msg: "Enter Address".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
       }else if (self.pincodeField.isHidden == false && self.pincodeField.text!.isEmpty){
            Singleton.shared.showToast(msg: "Enter Postal code".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else {
            ActivityIndicator.hide()
            var url = String()
            if(self.isEditAddress){
                url = U_BASE + U_UPDATE_ADDRESS + "\(self.addressData.id ?? 0)"
            }else {
                url = U_BASE + U_ADD_NEW_ADDRESS
            }
            let number = simplifyPhoneNumber(number: self.mobileNumber.text ?? "")
            let param:[String:Any] = [
                "firstname": self.firstName.text ?? "",
                "lastname" : self.lastField.text ?? "",
                "address" : self.addressField.text ?? "",
                "city_id" : self.selectedCity,
                "country_id" : self.selectedCountry,
                "phone" : number
            ]
            
            SessionManager.shared.methodForApiCalling(url: url + "?lang=\(Singleton.shared.language ?? "en")", method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: url, userToken: nil) { (response) in
                self.addressDelegate?.addNewAddress()
                
                if(self.navigationController != nil){
                    self.navigationController?.popViewController(animated: true)
                }else {
                    self.dismiss(animated: true, completion: nil)
                }
                Singleton.shared.showToast(msg: self.isEditAddress ? "Address edited successfully".localizableString(loc: Singleton.shared.language ?? "en"):"Address saved successfully".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 1)
            }
        }
    }
    
    
    @IBAction func countryAction(_ sender: Any) {
        self.currentPicker = 1
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        myVC.pickerData = self.countryFieldOptionArray
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func cityAction(_ sender: Any) {
        self.currentPicker = 2
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        myVC.pickerData = self.cityFieldOptionArray
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    
}

//MARK: Textfield Delegate
extension AddAddressViewController : UITextFieldDelegate ,UITextDropDelegate{
    //For handling textfield entered data
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.pincodeField {
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
            }
            
            if (textField.text?.count ?? 0) < 7 {
                return true
            } else {
                return false
            }
        } else if textField == self.mobileNumber {
            
            if textField.text == "" && isCountrySelected{
                textField.text = selectedCountryCode
                
            }
            
            let currentCharacterCount = textField.text?.count ?? 0
            let max = isCountrySelected ? 17 : 12
            let pattern = isCountrySelected ? "+xxx xxx-xxx-xxxx" : "xxx-xxx-xxxx"
            
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            
            textField.text = textField.text?.applyPatternOnNumbers(pattern: pattern, replacmentCharacter: "x")
            return newLength <= max
        }
        return true
    }
    
}


//MARK: Map Delegate
extension AddAddressViewController: GMSAutocompleteViewControllerDelegate ,GMSMapViewDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        dismiss(animated: true, completion: nil)
        self.addressField.text = place.formattedAddress
        self.zoomToLocation(val: place.coordinate, address: place.formattedAddress)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func zoomToLocation(val:CLLocationCoordinate2D, address: String?){
        self.mapView.clear()
        self.latitude = val.latitude
        self.longitude = val.longitude
        myLocationMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude))
        myLocationMarker?.map = self.mapView
        let center = CLLocationCoordinate2D(latitude: val.latitude, longitude: val.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))
        
        mapView.camera = GMSCameraPosition.camera(withLatitude: self.latitude, longitude: self.longitude, zoom: Float(15))
        self.mapView.isMyLocationEnabled = true
        
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: val.latitude, longitude: val.longitude), preferredLocale: nil) { (response, error) in
            if((response?.count ?? 0) > 0){
                self.address = response?[0]
                if(address != nil){
                    self.addressField.text = (response?[0].thoroughfare ?? "") + (response?[0].subLocality ?? "") + (response?[0].locality ?? "")
                    self.addressField.text = (self.addressField.text ?? "") + (response?[0].country ?? "")
                    
                    if let zipCode = response?[0].postalCode {
                        self.pincodeField.text = zipCode
                    }
                }
            }
        }
    }
    
}
