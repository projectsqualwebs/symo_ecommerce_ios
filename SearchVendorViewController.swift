//
//  SearchVendorViewController.swift
//  Symo New
//
//  Created by Apple on 01/04/21
//

import UIKit
import Alamofire
import SkeletonView

var dataType : Int? = nil
var SHOW_LOADING_FLITER = false

protocol settingBagCount {
    func setBagCount()
}

var resetView = true

class SearchVendorViewController: UIViewController, SortData ,settingBagCount,reloadSearchResult, HandleBlurView, SelectFromPicker{
    //MARK: Delegate methods
    //for value from picker view
    func selectedPickerData(val: String, pos: Int) {
        if self.currentPicker == 1 {
            if pos == 0 {
                K_SELECTED_SORTBY = 1
            } else if pos == 1{
                K_SELECTED_SORTBY = 3
            } else if pos == 2{
                K_SELECTED_SORTBY = 4
            }
            self.sortShopList(option: K_SELECTED_SORTBY)
        } else if self.currentPicker == 2 {
            K_SELECTED_SORTBY = pos
            self.sortProductList(option: pos)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func showHideBlurView(type:Int) {
        if type == 1 {
            self.blurView.isHidden = true
        } else if type == 2{
            self.blurView.isHidden = false
        }
    }
    
    func sortShopList(option: Int) {
        self.searchField.text = ""
        self.getShopData(text: "")
    }
    
    func reloadLocationTabel(searchText: String) {
        if(resetView){
            self.menuImage.image = #imageLiteral(resourceName: "menu_icon")
            self.selectedCountry.setTitle("Explore".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
            self.initView()
            self.vendorTable.isHidden = true
            self.productCollection.isHidden = true
            self.selectionView.isHidden = false
            self.containerView.isHidden = true
            self.searchBarView.isHidden = true
        }
    }
    
    
    func setBagCount(){
        if(K_BAG_COUNT == 0){
            self.bagCount.isHidden = true
        }else {
            self.bagCount.text = "\(K_BAG_COUNT)"
            self.bagCount.isHidden = false
        }
    }
    
    // for loading more stores and products
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let height = scrollView.frame.size.height
        let threshold: CGFloat = 10 // Adjust this value as needed
        
        // Check if the user is near the bottom of the scroll view
        if offsetY >= contentHeight - height - threshold {
            if scrollView == vendorTable {
                // Check if vendorTable has data to load more
                guard !isLoadingList && storeData.count != 0 && lastPage > currentPage else { return }
                
                isLoadingList = true
                currentPage += 1
                self.getShopData(text: "")
            } else if scrollView == productCollection && selectedView == 2 {
                // Check if productCollection has data to load more
                guard !isLoadingList && productData.count != 0 && lastPage > currentPage else { return }
                
                isLoadingList = true
                currentPage += 1
                self.getProductData(text: "")
            }
        }
    }

    
    
    
    func sortProductList(option: Int) {
        switch option {
        case 0:
            self.sortType = "distance"
            break
        case 1:
            self.sortType = "offer"
            break
        case 2:
            self.sortType = "rating"
            break
        case 3:
            self.sortType = "reviews"
            break
        case 4:
            self.sortType = "category"
            break
        default:
            break
        }
        self.getProductData(text: "")
        
        self.searchField.text = " "
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var pinkView: UIView!
    @IBOutlet weak var vendorTable: UITableView!
    @IBOutlet weak var selectedCountry: CustomButton!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var menuImageHeight: NSLayoutConstraint!
    @IBOutlet weak var bagCount: DesignableUILabel!
    @IBOutlet weak var selectionView: UIView!
    @IBOutlet weak var productCollection: UICollectionView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var searchBarView: UIView!
    @IBOutlet weak var shopByStoreButton: CustomButton!
    @IBOutlet weak var shopByCategoryButton: CustomButton!
    @IBOutlet weak var shopByProductsButton: CustomButton!
    @IBOutlet weak var noDataLabel: DesignableUILabel!
    @IBOutlet weak var sortbyView: View!
    @IBOutlet weak var fiiterbyView: View!
    @IBOutlet weak var shopByLocationLabel: CustomButton!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var loadMoreButton: CustomButton!
    @IBOutlet weak var gridView: View!
    @IBOutlet weak var gridImage: UIImageView!
    
    var shopByCategoryData = [ShopByCategoryResponse]()
    var filterCategoryData = [ShopByCategoryResponse]()
    var storeData = [StoreData]()
    var productData  = [ProductResponse]()
    var showBackOption = false
    var categoryId =  Set<Int>()
    var subcategoryId = Set<Int>()
    var sortType = "distance"
    var selectedView = Int()
    var currentPage : Int = 1
    var isLoadingList: Bool = false {
        didSet(oldValue) {
            if oldValue != isLoadingList {
                updateFooterVisibility()
            }
        }
    }
    var currentPicker = Int()
    var isGridViewSelected = false
    var lastPage = 1
    var dataFetching = true
    
    static var showMapBlurView : HandleBlurView? = nil
    static var searchDelegate : reloadSearchResult? = nil
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchBarView.isHidden = true //initially hide that view
        localizeView()
        handleTextAlignmentForArabic(view: self.view)
        FilterScreenViewController.handleBlurView = self
        self.blurView.addBlurEffect()
        TabbarViewController.reloadDelegate = self
        MapViewController.resetDelegate = self
        self.vendorTable.estimatedRowHeight = 350
        self.vendorTable.rowHeight = UITableView.automaticDimension
        self.view.backgroundColor = .white
        //  self.pinkView.backgroundColor = .white
        self.searchField.delegate = self
        CategoryViewController.bagCountDelegate = self
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_REDIRECTION_FROM_HOME), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleHomeRedirection(_:)), name: NSNotification.Name(N_REDIRECTION_FROM_HOME), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleFilter(_:)), name: NSNotification.Name(N_FILTER_PRODUCT), object: nil)

        setupCollectionViewFooter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showSkeleton()
        dataFetching = true
        self.loadMoreButton.isHidden = true
        self.blurView.isHidden = true
        self.gridImage.image = UIImage(named: self.isGridViewSelected ? "grid-1" : "list")
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
        if(resetView){
            self.menuImage.image = #imageLiteral(resourceName: "menu_icon")
            self.selectedCountry.setTitle("Explore".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
            self.initView()
            setBagCount()
            self.shopByCategoryData = []
            self.filterCategoryData = []
            self.vendorTable.reloadData()
            self.productCollection.reloadData()
            self.currentPage = 1
            self.noDataLabel.isHidden = true
            if dataType != nil {
                self.handleApiCalling()
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        setBagCount()
        if SHOW_LOADING_FLITER{ //loading was not showing when come back after filter
//            ActivityIndicator.show(view: self.view)
            dataFetching = true
            self.noDataLabel.isHidden = true
            self.showSkeleton()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if dataFetching{ //skeleton view was not properly showing in table view sometime mean selected view 3
            if selectedView == 3{
                hideSkeleton()
            }
            showSkeleton()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if(resetView){
            self.menuImage.image = #imageLiteral(resourceName: "menu_icon")
            self.initView()
            self.vendorTable.isHidden = false
            self.productCollection.isHidden = true
            self.selectionView.isHidden = false
            self.containerView.isHidden = true
            self.searchBarView.isHidden = true
            currentPage = 1
            self.productData = []
            productCollection.removeOldData()
            self.productCollection.reloadData()

        }else{
            if(self.menuImage.image == #imageLiteral(resourceName: "back_arrow@2x-2")){
                resetView = false
            } else {
                resetView = true
            }
        }
        
    }
    
    //MARK: Functions
    
    func showSkeleton(){
        productCollection.isSkeletonable = true
        vendorTable.isSkeletonable = true
        productCollection.showAnimatedGradientSkeleton()
        vendorTable.showAnimatedGradientSkeleton()
    }
    
    func hideSkeleton(){
        productCollection.hideSkeleton()
        vendorTable.hideSkeleton()
    }
    
    private func setupTableViewFooter() {
        let footerView = LoadingFooterTableView(frame: CGRect(x: 0, y: 0, width: vendorTable.frame.width, height: 60))
        vendorTable.tableFooterView = footerView
    }
    
    private func setupCollectionViewFooter() {
        productCollection.register(LoadingFooterCollectionView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: LoadingFooterCollectionView.identifier)
    }
    
    private func updateFooterVisibility() {
        if !isLoadingList {
            vendorTable.tableFooterView = UIView(frame: .zero)
            vendorTable.tableFooterView?.isHidden = true
        } else {
            setupTableViewFooter()
            vendorTable.tableFooterView?.isHidden = false
        }
        productCollection.reloadSections(IndexSet(integer: 0))
    }
    
    
    @objc func handleFilter(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_FILTER_PRODUCT), object: nil)
        //clear data when filter applied
        self.productData = []
        self.productCollection.reloadData()
        SHOW_LOADING_FLITER = true
        
        self.categoryId = selectedCategory
        self.subcategoryId = selectedSubCategory
        self.handleViewSelection(fromHome: false)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleFilter(_:)), name: NSNotification.Name(N_FILTER_PRODUCT), object: nil)
    }
    
    func localizeView(){
        self.searchField.placeholder = "Search by name".localizableString(loc: Singleton.shared.language ?? "en")
        self.shopByCategoryButton.setTitle("Shop by category".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.shopByProductsButton.setTitle("Shop by products".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.shopByStoreButton.setTitle("Shop by stores".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.shopByLocationLabel.setTitle("Shop by location".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
    }
    
    func initView(){
        sortType = "distance"
        self.categoryId = Set<Int>()
        self.subcategoryId = Set<Int>()
        self.searchField.text = ""
        self.filterCategoryData = []
        self.searchField.resignFirstResponder()
        selectedCategory = Set<Int>()
        selectedSubCategory = Set<Int>()
        selectedPriceMin = 0
        selectedPriceMax = 10000
        selectedDistance = Int()
        offerSelected = 0
        selectedStar = nil
        nearMe = nil
        K_SELECTED_SORTBY = 0
        FILTER_SUBDOMAIN = ""
        currentFilterParam = [String:Set<Int>]()
        storeType = nil
        if dataType == nil {
            productType = nil
        }
    }
    
    @objc func handleHomeRedirection(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_REDIRECTION_FROM_HOME), object: nil)
        self.selectionView.isHidden = true
        if let data = notif.userInfo?["selectedView"] as? Int{
            if let cat = notif.userInfo?["categoryId"] as? Int{
                self.categoryId.insert(cat)
            }
            self.selectedView = data
            self.handleViewSelection(fromHome:true)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleHomeRedirection(_:)), name: NSNotification.Name(N_REDIRECTION_FROM_HOME), object: nil)
    }
    
    func handleViewSelection(fromHome: Bool){
        self.currentPage = 1
        self.searchBarView.isHidden = false
        if(self.selectedView == 1){
            self.selectedCountry.setTitle("All categories".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
            self.vendorTable.isHidden = true
            self.fiiterbyView.isHidden = true
            self.sortbyView.isHidden = true
            self.productCollection.isHidden = false
            self.containerView.isHidden = true
            self.productCollection.performBatchUpdates(nil, completion: nil)
            self.getVendorByCategory()
            self.gridView.isHidden = true
            productType = nil
        }else if(self.selectedView == 2){
            self.categoryId = Set<Int>()
            self.subcategoryId = Set<Int>()
            self.fiiterbyView.isHidden = false
            self.selectedCountry.setTitle("Explore products".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
            self.vendorTable.isHidden = true
            self.productCollection.isHidden = false
            self.containerView.isHidden = false
            self.containerView.isHidden = true
            self.productCollection.performBatchUpdates(nil, completion: nil)
            self.gridView.isHidden = true
            self.getProductData(text: "")
        }else if(self.selectedView == 3){
            self.subcategoryId = Set<Int>()
            self.selectedCountry.setTitle("Explore stores".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
            self.vendorTable.isHidden = false
            self.sortbyView.isHidden = false
            self.fiiterbyView.isHidden = false
            self.productCollection.isHidden = true
            self.containerView.isHidden = true
            self.gridView.isHidden = false
            productType = nil
            self.getShopData(text: "")
        }else if(self.selectedView == 4){
            self.noDataLabel.isHidden = true
            self.categoryId = Set<Int>()
            self.subcategoryId = Set<Int>()
            self.fiiterbyView.isHidden = false
            self.gridView.isHidden = true
            self.selectedCountry.setTitle("Store locations".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
            self.vendorTable.isHidden = true
            self.productCollection.isHidden = true
            self.sortbyView.isHidden = false
            self.containerView.isHidden = false
            resetView = false
            productType = nil
        }
    }
    
    func handleApiCalling(){
        self.selectedView  = dataType ?? 1
        if(self.selectedView == 2){
            self.sortbyView.isHidden = false
        }
        self.shopByCategoryData = []
        self.selectionView.isHidden = true
        self.handleViewSelection(fromHome: false)
        dataType = nil
    }
    
    func getVendorByCategory(){
        let url = U_BASE + "shop-by-category?lang=\(Singleton.shared.language ?? "en")&country_id=\(Singleton.shared.selectedCountry.id ?? 0)&city_id=\(Singleton.shared.selectedCity.city_id ?? 0)"
        SessionManager.shared.methodForApiCalling(url: url, method: .get, parameter: nil, objectClass: GetShopByCategory.self, requestCode: U_SEARCH_VENDOR_NAME, userToken: nil, reloadData: false) { (response) in
            self.hideSkeleton()
            self.shopByCategoryData = response.response
            self.filterCategoryData = response.response
            self.view.isUserInteractionEnabled = true
            self.productCollection.reloadData()
            SHOW_LOADING_FLITER = false
            self.dataFetching = false
        }
    }
    
    func getShopData(text: String){
        let param:[String:Any] = [
            "lat":Singleton.shared.userLocation.coordinate.latitude,
            "long" : Singleton.shared.userLocation.coordinate.longitude,
            "lang": Singleton.shared.language ?? "en",
            "category" : Array(categoryId),// category id,
            "sub_category" : Array(selectedSubCategory),// subcategory id
            "keyword" : text, // search with the shop name
            "distance": selectedDistance == 0 ? "":selectedDistance,// possible values 0-5 km -> 5, 5-10 -> 10, 10-15 -> 15, more than 16 -> 16
            "city_id": Singleton.shared.selectedCity.city_id,
            "country_id" : Singleton.shared.selectedCountry.id,
            "store_type" : storeType,
            "nearby_me" : nearMe,
            "sort_by" : K_SELECTED_SORTBY,
            "filter":[
                "color": Array(currentFilterParam["Color"] ?? []),
                "price": [selectedPriceMin,selectedPriceMax],
                "size": Array(currentFilterParam["Size"] ?? []),
                "rating": selectedStar ?? 0
            ]
        ]
        
        
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_SEARCH_STORES + "\(self.currentPage)", method: .post, parameter: param, objectClass: GetStore.self, requestCode: U_SEARCH_STORES, userToken: nil, reloadData: false) { response in
            
            self.hideSkeleton()
            self.lastPage = response.response.last_page ?? 0
            self.isLoadingList = false
            
            if(self.storeData.count == 0 || self.currentPage == 1 ){
                self.storeData = response.response.data
            } else if response.response.data.count > 0{
                for val in response.response.data {
                    self.storeData.append(val)
                }
            }
            if(response.response.data.count == 0){
                if self.currentPage > 1 {
                    self.currentPage -= 1
                }
            }
            self.view.isUserInteractionEnabled = true
            self.vendorTable.reloadData()
            SHOW_LOADING_FLITER = false
            self.dataFetching = false
        }
    }
    
    func getProductData(text: String){
        let param:[String:Any] = [
            "lang": Singleton.shared.language ?? "en",
            "category" : Array(selectedCategory),// category id,
            "sub_category" :Array(selectedSubCategory),// subcategory id
            "keyword" : text, // search with the shop name
            "offers" : offerSelected == 0 ? "":offerSelected, // if 1 then only offers products will return in the response
            "search_for": productType ?? 0,//0 - products , 1 - services , 2 - all products and services
            "sort_by": K_SELECTED_SORTBY + 1,  // 1 ASC, 2 DESC
            "city_id": Singleton.shared.selectedCity.city_id,
            "country_id" : Singleton.shared.selectedCountry.id,
            "store_type" : storeType,
            "filter":[
                "color": Array(currentFilterParam["Color"] ?? []),
                "price": [selectedPriceMin,selectedPriceMax],
                "size": Array(currentFilterParam["Size"] ?? []),
                "rating": selectedStar ?? 0
            ]
            
        ]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_SEARCH_PRODUCTS + "\(self.currentPage)" + "&currency=\(Singleton.shared.selectedCurrency)", method: .post, parameter: param, objectClass: GetProduct.self, requestCode: U_SEARCH_PRODUCTS, userToken: nil, reloadData: false) { response in
            
            self.lastPage = response.response.business_products.last_page ?? 0
            self.isLoadingList = false
            self.hideSkeleton()
            
            if(self.productData.count == 0 || self.currentPage == 1 ){
                self.productData = response.response.business_products.data
                
            } else if response.response.business_products.data.count > 0{
                for val in response.response.business_products.data {
                    self.productData.append(val)
                }
            }
            if(response.response.business_products.data.count == 0){
                if self.currentPage > 1 {
                    self.currentPage -= 1
                }
            }
            self.view.isUserInteractionEnabled = true
            self.productCollection.reloadData()
            SHOW_LOADING_FLITER = false
            self.dataFetching = false
        }
    }
    
    
    
    
    //MARK: IBActions
    @IBAction func menuAction(_ sender: Any) {
        if(self.menuImage.image == #imageLiteral(resourceName: "back_arrow@2x-2")){
            self.selectedView = 1
            self.selectedCountry.setTitle("All categories".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
            self.vendorTable.isHidden = true
            self.sortbyView.isHidden = true
            self.fiiterbyView.isHidden = true
            self.productCollection.isHidden = false
            self.containerView.isHidden = true
            self.noDataLabel.isHidden = true
            self.gridView.isHidden = true
            self.storeData = []
            self.menuImage.image = #imageLiteral(resourceName: "menu_icon")
        }else {
            self.menu()
        }
    }
    
    @IBAction func nearbyAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        resetView = false
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func selectCountryAction(_ sender: Any) {
        self.selectedCountry.setTitle("Explore".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.initView()
        self.vendorTable.isHidden = true
        self.productCollection.isHidden = true
        self.selectionView.isHidden = false
        self.containerView.isHidden = true
        self.searchBarView.isHidden = true
        resetView = true
    }
    
    @IBAction func gridAction(_ sender: Any) {
        self.isGridViewSelected = !self.isGridViewSelected
        self.gridImage.image = UIImage(named: self.isGridViewSelected ? "grid-1" : "list")
        self.vendorTable.reloadData()
    }
    
    
    
    @IBAction func sortbyAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.headingLabel = "Sort by"
        myVC.pickerDelegate = self
        resetView = false
        if self.selectedView == 2 {
            myVC.pickerData = ["Most popular","Price low to high","Price high to low"]
            currentPicker = 2
        } else {
            myVC.pickerData = ["Latest","A-Z","Z-A"]
            currentPicker = 1
        }
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func filterAction(_ sender: Any) {
        self.blurView.isHidden = false
        SearchVendorViewController.showMapBlurView?.showHideBlurView(type: 2)
        self.openFilter(view:self.selectedView)
    }
    
    @IBAction func selectionAction(_ sender: UIButton) {
        self.selectedView  = sender.tag
        
        if(self.selectedView == 2){
            self.sortbyView.isHidden = false
            productType = 0
        }
        self.shopByCategoryData = []
        self.productCollection.reloadData()
        self.selectionView.isHidden = true
        self.handleViewSelection(fromHome: false)
    }
    
    @IBAction func bagAction(_ sender: Any) {
        self.tabBarController?.selectedIndex = 2
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        if(self.selectedView == 1){
            self.shopByCategoryData = []
            self.shopByCategoryData = self.filterCategoryData.filter{
                return ($0.name ?? "").lowercased().contains(self.searchField.text ?? "")
            }
            self.productCollection.reloadData()
        }else if(self.selectedView == 2){
            self.getProductData(text: self.searchField.text ?? "")
        }else if(self.selectedView == 3){
            self.getShopData(text: self.searchField.text ?? "")
        }else if selectedView == 4 {
            SearchVendorViewController.searchDelegate?.reloadLocationTabel(searchText: self.searchField.text ?? "")
        }
    }
    
    @IBAction func loadMoreAction(_ sender: Any) {
        //no more use of these ibaction
    }
}

//MARK: Textfield Delegate
extension SearchVendorViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text =  (self.searchField.text ?? "") + string
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                if text.count == 1 {
                    if self.selectedView == 1 {
                        self.shopByCategoryData = self.filterCategoryData
                    } else if self.selectedView == 2 {
                        self.getProductData(text: "")
                    }else if self.selectedView == 3 {
                        self.getShopData(text: "")
                    } else if self.selectedView == 4 {
                        SearchVendorViewController.searchDelegate?.reloadLocationTabel(searchText: "")
                    }
                }
            }
        }
        self.productCollection.reloadData()
        
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(self.selectedView == 1){
            self.shopByCategoryData = []
            self.shopByCategoryData = self.filterCategoryData.filter{
                return ($0.name ?? "").lowercased().contains(self.searchField.text ?? "")
            }
            self.productCollection.reloadData()
        }else if(self.selectedView == 2){
            self.getProductData(text: self.searchField.text ?? "")
        }else if(self.selectedView == 3){
            self.getShopData(text: self.searchField.text ?? "")
        }else if selectedView == 4 {
            SearchVendorViewController.searchDelegate?.reloadLocationTabel(searchText: self.searchField.text ?? "")
        }
    }
}

//MARK: Table View delegate
extension SearchVendorViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.selectedView == 3){
            self.noDataLabel.text = "No Stores Found".localizableString(loc: Singleton.shared.language ?? "en")
            if(self.storeData.count == 0 && !dataFetching){
                self.noDataLabel.isHidden = false
            }else {
                self.noDataLabel.isHidden = true
            }
            return self.storeData.count
        }else {
            if(self.searchField.text!.isEmpty){
                return 0
            }else {
                return 0//self.shopByCategoryData.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = isGridViewSelected ? tableView.dequeueReusableCell(withIdentifier: "CategoryTableCell") as! CategoryTableCell  : tableView.dequeueReusableCell(withIdentifier: "CategoryTableCell2") as! CategoryTableCell
        
        if(self.selectedView == 3){
            let val = self.storeData[indexPath.row]
            
            cell.shopName.text = val.name
            cell.totalReview.text = "\(val.ratings_count ?? 0) " + "reviews".localizableString(loc: Singleton.shared.language ?? "en")
            cell.ratingView.rating = Double(val.ratings ?? "0")!
            
            if(self.isGridViewSelected){
                if((val.avatar ?? val.shop_image  ?? "").contains("http")){
                    cell.shopImage.sd_setImage(with: URL(string: (  val.avatar ?? val.shop_image  ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
                }else {
                    cell.shopImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (val.shop_image ?? val.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
                }
                
                
                if self.storeData[indexPath.row].is_bookmark == 1 {
                    cell.bookMarkIcon.image = #imageLiteral(resourceName: "bookmark_select")
                }else {
                    cell.bookMarkIcon.image = #imageLiteral(resourceName: "bookmark_icon@2x-2")
                }
                
                cell.bookMarkView.isHidden = Singleton.shared.userDetail.id == nil ? true : false
                cell.deliveryLocalizeLabel.text = "Delivery".localizableString(loc: Singleton.shared.language ?? "en")
                cell.viewButton.setTitle("View".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
                cell.freeDeliveryStack.isHidden = false
                cell.pickupStack.isHidden = true
                cell.codStack.isHidden = true
                cell.scheduleDeliveryStack.isHidden = true
                cell.oneDayDelivery.isHidden = true
                
                var count = 1
                var dta = ["Delivery"]
                
                if val.pickup_delivery_setting == 1 {
                    dta.append("Pickup")
                    count += 1
                }
                
                if val.one_day_delivery == 1 {
                    count += 1
                    dta.append("One day delivery")
                }
                
                if val.cod_method == 1 {
                    count += 1
                    dta.append("COD")
                }
                
                if val.schedule_delivery == 1{
                    count += 1
                    dta.append("Schedule delivery")
                }
                
                
                if count < 5 {
                    if count >= 2 {
                        cell.pickupStack.isHidden = false
                        switch dta[1] {
                            
                        case "Pickup" :
                            cell.secondViewIcon.image = UIImage(named: "rectangle.filled.and.hand.point.up.left")
                            cell.pickupLocalizeLabel.text = "Pickup"
                            break
                            
                        case "COD" :
                            cell.secondViewIcon.image = UIImage(named: "blue_price")
                            cell.pickupLocalizeLabel.text = "COD"
                            break
                            
                        case "Schedule delivery" :
                            cell.secondViewIcon.image = UIImage(named: "blue_calendar")
                            cell.pickupLocalizeLabel.text = "Schedule delivery"
                            break
                            
                        case "One day delivery" :
                            cell.secondViewIcon.image = UIImage(named: "blue_calendar")
                            cell.pickupLocalizeLabel.text = "One day delivery"
                            break
                            
                        default:
                            break
                            
                        }
                    }
                    
                    if count >= 3 {
                        cell.codStack.isHidden = false
                        switch dta[2] {
                            
                        case "Pickup" :
                            cell.thirdViewIcon.image = UIImage(named: "rectangle.filled.and.hand.point.up.left")
                            cell.codLocalizeLabel.text = "Pickup"
                            break
                            
                        case "COD" :
                            cell.thirdViewIcon.image = UIImage(named: "blue_price")
                            cell.codLocalizeLabel.text = "COD"
                            break
                            
                        case "Schedule delivery" :
                            cell.thirdViewIcon.image = UIImage(named: "blue_calendar")
                            cell.codLocalizeLabel.text = "Schedule delivery"
                            break
                            
                        case "One day delivery" :
                            cell.thirdViewIcon.image = UIImage(named: "blue_calendar")
                            cell.codLocalizeLabel.text = "One day delivery"
                            break
                            
                        default:
                            break
                            
                        }
                    }
                    
                    if count >= 4 {
                        cell.oneDayDelivery.isHidden = false
                        switch dta[3] {
                            
                        case "Pickup" :
                            cell.fourthViewIcon.image = UIImage(named: "rectangle.filled.and.hand.point.up.left")
                            cell.oneDayDeliveryLocalize.text = "Pickup"
                            break
                            
                        case "COD" :
                            cell.fourthViewIcon.image = UIImage(named: "blue_price")
                            cell.oneDayDeliveryLocalize.text = "COD"
                            break
                            
                        case "Schedule delivery" :
                            cell.fourthViewIcon.image = UIImage(named: "blue_calendar")
                            cell.oneDayDeliveryLocalize.text = "Schedule delivery"
                            break
                            
                        case "One day delivery" :
                            cell.fourthViewIcon.image = UIImage(named: "blue_calendar")
                            cell.oneDayDeliveryLocalize.text = "One day delivery"
                            break
                            
                        default:
                            break
                            
                        }
                    }
                } else {
                    cell.freeDeliveryStack.isHidden = false
                    cell.pickupStack.isHidden = false
                    cell.codStack.isHidden = false
                    cell.scheduleDeliveryStack.isHidden = false
                    cell.oneDayDelivery.isHidden = false
                }
                
                
                cell.shopAddress.text = val.address
                cell.shopDistance.text = "\(String(format:"%.2f", val.distance ?? 0))" + " kms".localizableString(loc: Singleton.shared.language ?? "en")
            } else {
                if((val.avatar ?? val.shop_image ?? "").contains("http")){
                    cell.shopImage.sd_setImage(with: URL(string: (val.avatar ?? val.shop_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
                }else {
                    cell.shopImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (val.avatar ?? val.shop_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
                }
            }
            
            cell.bookmarkButton = {
                ActivityIndicator.show(view: self.view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_BOOKMARK_VENODR, method: .post,
                                                          parameter: ["vendor_id": self.storeData[indexPath.row].id], objectClass: SuccessResponse.self, requestCode: U_BOOKMARK_VENODR, userToken: UD_TOKEN) { (response) in
                    if cell.bookMarkIcon.image == UIImage(named: "bookmark_icon@2x-2") {
                        cell.bookMarkIcon.image = #imageLiteral(resourceName: "bookmark_select")
                        Singleton.shared.bookmarkedVendors.append(self.storeData[indexPath.row].id ?? 0)
                    }else {
                        cell.bookMarkIcon.image = #imageLiteral(resourceName: "bookmark_icon@2x-2")
                        Singleton.shared.bookmarkedVendors.removeAll { $0 == self.storeData[indexPath.row].id }
                    }
                    Singleton.shared.showToast(msg: response.message?.localizableString(loc: Singleton.shared.language ?? "en") ?? "", controller: self, type: 0)
                    ActivityIndicator.hide()
                }
            }
            
            
            cell.bagButton = {
                resetView = false
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
                myVC.vendorId = self.storeData[indexPath.row].id ?? 0
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        resetView = false
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
        if(self.selectedView == 1){
            
        }else if(self.selectedView == 2){
            
        }else if(self.selectedView == 3){
            myVC.vendorId = self.storeData[indexPath.row].id ?? 0
        }else if(self.selectedView == 4){
            
        }else {
            
        }
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isGridViewSelected {
            return UITableView.automaticDimension // Use default automatic sizing
        } else {
            return 100 // Height for list view
        }
    }
    
}

//MARK: Collection View Delegate
extension SearchVendorViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(self.selectedView == 1){
            self.noDataLabel.text = "No Products Found".localizableString(loc: Singleton.shared.language ?? "en")
            
            let data = self.shopByCategoryData
            
            if(data.count == 0 && !dataFetching){
                self.noDataLabel.isHidden = false
            }else {
                self.noDataLabel.isHidden = true
            }
            return data.count
        } else if(self.selectedView == 2){
            self.noDataLabel.text = "No Products Found".localizableString(loc: Singleton.shared.language ?? "en")
            if(self.productData.count == 0 && !dataFetching){
                self.noDataLabel.isHidden = false
            }else {
                self.noDataLabel.isHidden = true
            }
            return self.productData.count
        }else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(self.selectedView == 1){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionCell3", for: indexPath) as! CategoryCollectionCell
            
            let val = self.shopByCategoryData[indexPath.row]
            
            if((val.category_avatar ?? "").contains("http")){
                cell.shopImage.sd_setImage(with: URL(string: (val.category_avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }else {
                cell.shopImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (val.category_avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }
            cell.shopName.text = val.name
            cell.shopRating.text = "Products ".localizableString(loc: Singleton.shared.language ?? "en") + "\(val.product_count ?? 0)"
            cell.shopDistance.text = "Shops ".localizableString(loc: Singleton.shared.language ?? "en") + "\(val.shop_count ?? 0)"
            return cell
        }else if(self.selectedView == 2){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionCell", for: indexPath) as! CategoryCollectionCell
            let val = self.productData[indexPath.row]
            
            if((val.default_image ?? "").contains("http")){
                cell.shopImage.sd_setImage(with: URL(string: (val.default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }else {
                cell.shopImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (val.default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }
            
            var count = Int()
            switch val.qty {
            case .string(let text):
                count = Int(text ?? "0")!
                break
            case .int(let text):
                count = Int(text)
                break
            default:
                break
            }
            
            if val.stock_status == 1 {
                if count < 10 && count > 0 {
                    cell.onlyLeftView.isHidden = false
                    cell.onlyLeftLabel.text = "Only \(count) left"
                } else {
                    cell.onlyLeftView.isHidden = true
                }
            } else {
                cell.onlyLeftView.isHidden = false
                cell.onlyLeftLabel.text = "Out of stock"
            }
            
            cell.onlyLeftView.clipsToBounds = false
            cell.onlyLeftView.layer.masksToBounds = false
            cell.onlyLeftView.layer.cornerRadius = 4
            cell.onlyLeftView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
            
            cell.shopName.text = val.name
            
            var currentPrice = convertPriceInDouble(price: val.price)
            let discountPrice = convertPriceInDouble(price: val.discounted_price)
            //            let discountPrice = val.discount ?? 0
            
            
            if (val.offer_qty ?? 0) > 0 && (val.offer_valid_till ?? 0) > Int(NSDate().timeIntervalSince1970) {
                
                cell.shopPrice.attributedText = self.handlePrice(price: "\(currentPrice)" , cut: "\(val.offer_price ?? 0.00)")
                
            } else if (discountPrice != 0.00) && ((discountPrice) != (currentPrice)){  //0.00
                
                cell.shopPrice.attributedText = self.handlePrice(price: "\(currentPrice)" , cut: "\(discountPrice)")
            } else {
                cell.shopPrice.text = "\(Singleton.shared.selectedCurrencySymbol) " + "\(currentPrice.addComma)"
            }
            
            cell.shopDistance.text = "by \(val.vendor_details?.name ?? "")"
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionCell", for: indexPath) as! CategoryCollectionCell
            return cell
        }
    }
    
    //for setting size of each cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(self.selectedView == 1){
            return CGSize(width: collectionView.frame.size.width, height:70)
        }else{
            return CGSize(width: (collectionView.frame.size.width/2)-15, height:(collectionView.frame.size.width/2) + 85)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(self.selectedView == 1){
            self.currentPage = 1
            self.selectedView = 3
            self.menuImage.image = #imageLiteral(resourceName: "back_arrow@2x-2")
            self.storeData = []
            self.categoryId = []
            self.categoryId.insert(self.shopByCategoryData[indexPath.row].id ?? 0)
            self.handleViewSelection(fromHome: false)
        }else {
            if(self.selectedView == 2){
                resetView = false
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SingleProductViewController") as! SingleProductViewController
                myVC.selectedProduct = self.productData[indexPath.row]
                myVC.subdomain = self.productData[indexPath.row].vendor_details?.subdomain ?? ""
                if(myVC.subdomain != ""){
                    self.navigationController?.pushViewController(myVC, animated: true)
                }
            }else if(self.selectedView == 3){
                resetView = false
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
                if(self.selectedView == 1){
                    
                }else if(self.selectedView == 2){
                    
                }else if(self.selectedView == 3){
                    myVC.vendorId = self.storeData[indexPath.row].id ?? 0
                }else if(self.selectedView == 4){
                    
                }else {
                    
                }
                self.navigationController?.pushViewController(myVC, animated: true)
            }else if(self.selectedView == 4){
                resetView = false
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
                resetView = false
                if(self.selectedView == 1){
                    
                }else if(self.selectedView == 2){
                    
                }else if(self.selectedView == 3){
                    myVC.vendorId = self.storeData[indexPath.row].id ?? 0
                }else if(self.selectedView == 4){
                    
                }else {
                    
                }
                
                self.navigationController?.pushViewController(myVC, animated: true)
            }else {
                
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionFooter {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LoadingFooterCollectionView", for: indexPath) as! LoadingFooterCollectionView
            footerView.isHidden = !isLoadingList
            return footerView
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        let showFooter = isLoadingList || productData.count % 20 == 0
        return CGSize(width: collectionView.frame.width, height: showFooter ? 60 : 10)
    }
}


extension SearchVendorViewController: SkeletonCollectionViewDataSource {
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return (selectedView == 1) ? "CategoryCollectionCell3" : "CategoryCollectionCell"
    }
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (selectedView == 1) ? 10 : 6
    }
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell? {
        let reuseIdentifier = (selectedView == 1) ? "CategoryCollectionCell3" : "CategoryCollectionCell"
        
        let cell = skeletonView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? CategoryCollectionCell
        
        return cell
    }
    
}

extension SearchVendorViewController : SkeletonTableViewDataSource{
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return  isGridViewSelected ? "CategoryTableCell" : "CategoryTableCell2"
    }
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isGridViewSelected ? 2 : 8
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, skeletonCellForRowAt indexPath: IndexPath) -> UITableViewCell? {
        let cell = skeletonView.dequeueReusableCell(withIdentifier: isGridViewSelected ? "CategoryTableCell" : "CategoryTableCell2", for: indexPath) as? CategoryTableCell
        return cell
    }
}



//MARK: Loading Footer View for table
class LoadingFooterTableView: UIView {
    let activityIndicator = UIActivityIndicatorView(style: .large)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView() {
        backgroundColor = UIColor.clear
        activityIndicator.color = K_PINK_COLOR
        activityIndicator.startAnimating()
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(activityIndicator)
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
    }
}

//MARK: Loading Footer View for table
class LoadingFooterCollectionView: UICollectionReusableView {
    static let identifier = "LoadingFooterCollectionView"
    
    let activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .large)
        indicator.color = K_PINK_COLOR
        indicator.translatesAutoresizingMaskIntoConstraints = false
        return indicator
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView() {
        backgroundColor = UIColor.clear
        addSubview(activityIndicator)
        
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
        
        activityIndicator.startAnimating()
    }
}
