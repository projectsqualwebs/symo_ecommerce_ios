
# SYMO

SYMO is an iOS app for users in the UAE and Bahrain, available in English and Arabic. It lets users buy products for delivery or pickup and allows vendors to sell their products easily.


## Features

- Localization English and Arabic language supported 
- Supported in ios 13 and more
- Product search functionality with filters.
- Cart management and checkout process.



## Tech Stack

**Swift:** Version 5

**UIKit:** For building the user interface.

**Firebase Cloud Messaging (FCM):** For push notifications.

**Alamofire:** For networking.

**CocoaPods:** Dependency manager for third-party libraries.

**Map:** Google Maps



## Environment Variables

Important URL and API

BASE_URL = "https://gosymo.com/api/" // beta also available

IMAGE_BASE_URL = "https://gosymo.com/storage/app/public/"

GOOGLE_PLACE_API = "AIzaSyDEKWYKnm_ZHSH0p7FP3Y08Ol3fDiQr78E" //from kapil sir account


## Setup Instruction

Clone the repository:
git clone https://dev_qualwebs@bitbucket.org/dev_qualwebs/

Install CocoaPods dependencies:
cd symo-ecommerce-ios
pod install

Open Symo\ New.xcodeproj in Xcode.
Build and run the app on a simulator or device.



## Dashboard Accounts

**Firebase:** "simon.qualwebs@gmail.com , g.qualwebs@gmail.com"

**Bitbucket:** "dev_qualwebs"

**GoogleMap:** "kapil sir acount"

**Apple Account:** "qualwebs"
