//
//  MyOrderViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 01/05/21.
//

import UIKit

class MyOrderViewController: UIViewController, SortData, SelectFromPicker {
    //picker delegate methof
    func selectedPickerData(val: String, pos: Int) {
        K_SELECTED_SORTBY = pos
        self.sortProductList(option: pos)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func sortShopList(option: Int) {
    }
    
    
    func sortProductList(option: Int) {
        self.selectedIndex = sortbyData[option].id
        self.getMyOrder()
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var productListLabel: DesignableUILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var orderTable: UITableView!
    @IBOutlet weak var noOrderLabel: DesignableUILabel!
    @IBOutlet weak var bagCount: DesignableUILabel!
    @IBOutlet weak var sortLabel: DesignableUILabel!
    
    let data = ["All", "Delivery", "Pickup"]
    var selectedIndex : Int? = nil
    var selectedCell = 0
    var selectedPreference = 7
    var orderData = MyOrderResponse()
    var orderStatusData = ["Pending", "Accepted", "Ready For Delivery", "Out For Delivery", "Delivered/Pickedup", "Return Initiated", "Return Approved", "Return Rejected", "Product Pickedup", "Refund Initiated","Refunded", "Cancelled","Order Rejected","Exchange Requested"]
  
    // Create an array of OrderStatus objects
    var sortbyData: [OrderStatus] = [
        OrderStatus(id: nil, orderStatus: "All"),
        OrderStatus(id: 0, orderStatus: "Pending"),
        OrderStatus(id: 1, orderStatus: "Accepted"),
        OrderStatus(id: 4, orderStatus: "Delivered/Pickedup"),
        OrderStatus(id: 11, orderStatus: "Cancelled"),
        OrderStatus(id: 12, orderStatus: "Order Rejected"),
    ]
    var isRedirectionFromSideBar = false
    let refreshControl = UIRefreshControl()
    
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.productListLabel.text = "My Orders".localizableString(loc: Singleton.shared.language ?? "en")
        self.sortLabel.text = "Filter".localizableString(loc: Singleton.shared.language ?? "en")
        self.view.backgroundColor = .white
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        orderTable.refreshControl = refreshControl
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
        if(K_BAG_COUNT == 0){
            self.bagCount.isHidden = true
        }else {
            self.bagCount.text = "\(K_BAG_COUNT)"
            self.bagCount.isHidden = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.getMyOrder()
    }
    
    
    //MARK: Functions
    func getMyOrder(){
        ActivityIndicator.show(view: self.view)
        let param:[String:Any] = [
            "lang": Singleton.shared.language,
            "status": self.selectedIndex ?? nil ,
            "preference" : self.selectedPreference,
            "per_page": 10
            
        ]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_MY_ORDER + "?currency=\(Singleton.shared.selectedCurrency)", method: .post, parameter: param, objectClass: GetMyOrders.self, requestCode: U_GET_MY_ORDER, userToken: nil) { (response) in
            self.orderData = response.response
            self.orderTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions
    @IBAction func sortAction(_ sender: Any) {
        K_SELECTED_SORTBY = self.selectedIndex ?? 0
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        myVC.pickerData = sortbyData.compactMap { $0.orderStatus }
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        if isRedirectionFromSideBar == true {
            self.menu()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func bagAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ShoppingBucketViewController") as! ShoppingBucketViewController
        myVC.showBackButton = true
        
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @objc func refresh() {
        K_RELOAD_DATA = true // for reload data from API
        self.getMyOrder()
        refreshControl.endRefreshing()
        K_RELOAD_DATA = false
    }
    
    
}


//MARK: Table View
extension MyOrderViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if((self.orderData.data?.count ?? 0) == 0){
            self.noOrderLabel.isHidden = false
        }else{
            self.noOrderLabel.isHidden = true
        }
        return self.orderData.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orderData.data?[section].order_items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableCell") as! CategoryTableCell
        let val = self.orderData.data?[indexPath.section].order_items?[indexPath.row]
        cell.shopName.text = val?.Product?.name
        cell.orderId.text = "Order ID: ".localizableString(loc: Singleton.shared.language ?? "en") + "#SYMO000\(self.orderData.data?[indexPath.section].id ?? 0)"
        
        let stamp = self.orderData.data?[indexPath.section].created_at ?? ""
        let sliced = stamp.prefix(10)
        
        cell.orderDate.text = "Placed on: ".localizableString(loc: Singleton.shared.language ?? "en") + "\(String(sliced))"
        if((val?.Product?.default_image ?? "").contains("http")){
            cell.shopImage.sd_setImage(with: URL(string: (val?.Product?.default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }else{
            cell.shopImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (val?.Product?.default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }
        
        
        if let status = orderData.data?[indexPath.section].status {
            var statusText: String
            
            switch status {
                case 7, 11, 12, 13:
                    statusText = "🔴 " + self.orderStatusData[status].localizableString(loc: Singleton.shared.language ?? "en")
                case 4, 10:
                    statusText = "🟢 " + self.orderStatusData[status].localizableString(loc: Singleton.shared.language ?? "en")
                default:
                    statusText = "🟡 " + self.orderStatusData[status].localizableString(loc: Singleton.shared.language ?? "en")
            }
            
            cell.ratingText.text = statusText
        }
        
        cell.shopPrice.text = "\(Singleton.shared.selectedCurrencySymbol) \(self.orderData.data?[indexPath.section].currency_total?.order_total ?? "")"
        
        let variation = val?.VariationValues ?? []
        cell.shopAddress.text = ""
        for i in variation{
            if((i.attribute_text == "Color") || (i.attribute_text == "Ram") || (i.attribute_text == "Internal Storage") || (i.attribute_text == "Size")){
                let att = (i.attribute_text ?? "") + " | " + (i.value_text ?? "")
                cell.shopAddress.text = (((cell.shopAddress.text ?? "") == "") ? ((cell.shopAddress.text ?? "")):(cell.shopAddress.text ?? "") + ", ") + att
            }
        }
        cell.cartItemCount.text = "\(val?.qty ?? 0)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
        myVC.orderId = self.orderData.data?[indexPath.section].id ?? 0
        myVC.orderStatus = self.selectedIndex ?? 0
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}

//MARK: Collection Delegate
extension MyOrderViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionCell", for: indexPath) as! CategoryCollectionCell
        if(self.selectedCell == indexPath.row){
            cell.backView.backgroundColor = K_PINK_COLOR
            cell.shopName.textColor = .white
        }else {
            cell.backView.backgroundColor = K_OFF_WHITE_COLOR
            cell.shopName.textColor = .black
        }
        
        cell.shopName.text = data[indexPath.row].localizableString(loc: Singleton.shared.language ?? "en")
        
        return cell
    }
    
    //For select item in collection View
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedCell = indexPath.item
        self.selectedIndex = nil
        if indexPath.item == 0{
            self.selectedPreference = 7
        } else if indexPath.item == 1 {
            self.selectedPreference = 0
        } else if indexPath.item == 2 {
            self.selectedPreference = 1
        }
        
        self.collectionView.reloadData()
        self.getMyOrder()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let label = UILabel(frame: CGRect.zero)
        label.text = data[indexPath.row].localizableString(loc: Singleton.shared.language ?? "en")
        label.sizeToFit()
        return CGSize(width: label.frame.width + 40, height: 50)
    }
}
