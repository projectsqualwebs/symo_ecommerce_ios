//
//  ConfirmationPopupViewController.swift
//  Symo New
//
//  Created by Apple on 06/04/21.
//

import UIKit

protocol Confirmation{
    func confirmationSelection(action: Int)
}


class ConfirmationPopupViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var confirmationLabel: UILabel!
    @IBOutlet weak var secondText: UILabel!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    
    
    var confirmationDelegate: Confirmation? = nil
    var secondTxt = true
    var confirmationText = String()
    var detailText = String()
    var firstButtonTitle = ""
    var secondButtonTitle = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(secondTxt){
            self.secondText.isHidden = false
            self.okButton.setTitle("YES".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
            self.cancelButton.setTitle("NO".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        }else {
            self.secondText.isHidden = true
        }
        
        if firstButtonTitle != "" {
            self.okButton.setTitle(firstButtonTitle.localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        }
        if secondButtonTitle != "" {
            self.cancelButton.setTitle(secondButtonTitle.localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        }
        
        
        self.confirmationLabel.text = self.confirmationText.localizableString(loc: Singleton.shared.language ?? "en")
        self.secondText.text = self.detailText.localizableString(loc: Singleton.shared.language ?? "en")
    }
    
    //MARK: IBAction
    
    @IBAction func okAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        self.confirmationDelegate?.confirmationSelection(action: 1)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        self.confirmationDelegate?.confirmationSelection(action: 2)
       
    }
    
    @IBAction func confirmOkAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        self.confirmationDelegate?.confirmationSelection(action: 1)
        
    }
    
}
