//
//  CheckoutViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 01/05/21
//

import UIKit
import iOSDropDown
import SkyFloatingLabelTextField
import WebKit
import GoogleMaps
import GooglePlaces

class CheckoutViewController: UIViewController, AddAddress, SelectDate, WKNavigationDelegate ,SelectFromPicker {
    //MARK: Delegate functions
    func selectedPickerData(val: String, pos: Int) {
        if currentPicker == 1 {
            self.deliveryMethod.text = val
            if(val == "Pickup"){
                self.deliveryMethodView.isHidden = true
                self.orderPreferenceView.isHidden = false
                self.selectedDeliveryOrPickup = 1
                self.setOrderPrefernce()
            }else {
                self.deliveryMethodView.isHidden = false
                self.orderPreferenceView.isHidden = true
                self.selectedDeliveryOrPickup = 0
                self.setOrderPrefernce()
            }
            self.ccAvenueUrl = nil
        } else if currentPicker == 2 {
            self.paymentPreference.text = val
            if self.paymentPreferenceOptionArray[pos] == "Debit/Credit Card" {
                self.paymentMethod = 0
            } else if self.paymentPreferenceOptionArray[pos] == "Cash on Delivery"{
                self.paymentMethod = 1
            } else if self.paymentPreferenceOptionArray[pos] == "Bank Transfer"{
                self.paymentMethod = 2
            }
            self.ccAvenueUrl = nil
        } else if currentPicker == 3 {
            self.countryField.text = val
            self.selectedCountry = Singleton.shared.countryData[pos].id ?? 0
            self.mobileNumberField.text = "+" + (Singleton.shared.countryData[pos].country_code ?? "")
            Singleton.shared.cityData = []
            self.cityFieldOptionArray = []
            self.cityField.text = ""
            NavigationController.shared.getCities(countryId: self.selectedCountry) { (city) in
                for val in city{
                    self.cityFieldOptionArray.append(val.name ?? "")
                }
            }
        } else if currentPicker == 4 {
            self.cityField.text = val
            self.selectedCity = Singleton.shared.cityData[pos].city_id ?? 0
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
            self.view.isUserInteractionEnabled = true
        }
    }
    
    
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        ActivityIndicator.hide()
        guard let url = webView.url?.absoluteString else {
            
            return
        }
        if(url.contains("https://beta.gosymo.com/app-payment-handle-success/")){
            self.popupView.isHidden = true
            self.ccAvenueUrl = nil
            self.redirectToHome()
        }else if(url.contains("https://beta.gosymo.com/app-payment-handle-aborted/")){
            self.popupView.isHidden = true
            Singleton.shared.showToast(msg: "CC Avenue payment aborted".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if(url.contains("https://beta.gosymo.com/app-payment-handle-failure/")){
            self.popupView.isHidden = true
            Singleton.shared.showToast(msg: "CC Avenue payment failed".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
            
        }
        
    }
    
    func selectedDate(timestamp: Date) {
        self.selectedTime = Int(timestamp.timeIntervalSince1970)
        if(self.isSerivceDate){
            self.serviceDate = Int(timestamp.timeIntervalSince1970)
            self.serviceDateField.text = self.convertTimestampToDate(self.selectedTime, to: "dd/MM/yyyy") // h:mm a
        }else {
            self.orderPreference.text = self.convertTimestampToDate(self.selectedTime, to: "dd/MM/yyyy") //h:mm a
        }
        
    }
    
    func addNewAddress() {
        Singleton.shared.savedAddress = []
        NavigationController.shared.getSavedAddress { (response) in
            self.addressData = response
            self.addressTable.reloadData()
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var orderPreference: DesignableUITextField!
    @IBOutlet weak var addressTable: UITableView!
    
    @IBOutlet weak var fullNameField: DesignableUITextField!
    @IBOutlet weak var mobileNumberField: DesignableUITextField!
    @IBOutlet weak var emailField: DesignableUITextField!
    @IBOutlet weak var countryField: DesignableUITextField!
    @IBOutlet weak var cityField: DesignableUITextField!
    
    @IBOutlet weak var addressField: DesignableUITextField!
    @IBOutlet weak var postalCodeField: DesignableUITextField!
    @IBOutlet weak var cartTable: ContentSizedTableView!
    @IBOutlet weak var itemTotal: DesignableUILabel!
    @IBOutlet weak var deliveryCharge: DesignableUILabel!
    @IBOutlet weak var orderTax: DesignableUILabel!
    @IBOutlet weak var totalAmount: DesignableUILabel!
    @IBOutlet weak var deliveryNote: DesignableUITextField!
    @IBOutlet weak var popupView: UIView!
    
    @IBOutlet weak var deliveryMethodView: UIStackView!
    @IBOutlet weak var orderPreferenceView: UIStackView!
    @IBOutlet weak var chargesLabel: DesignableUILabel!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var deliveryMethod: DesignableUITextField!
    @IBOutlet weak var paymentPreference: DesignableUITextField!
    @IBOutlet weak var billingStack: UIStackView!
    @IBOutlet weak var defaultBillingImage: UIImageView!
    @IBOutlet weak var orderPrefView: UIView!
    @IBOutlet weak var deliveryMethoTable: ContentSizedTableView!
    @IBOutlet weak var methoDescription: DesignableUILabel!
    @IBOutlet weak var serviceView: UIStackView!
    @IBOutlet weak var servicePrefernce: DesignableUITextField!
    @IBOutlet weak var servicePreferenceLabel: DesignableUILabel!
    @IBOutlet weak var serviceDateField: DesignableUITextField!
    
    @IBOutlet weak var cancelButton: CustomButton!
    @IBOutlet weak var payNowLabel: DesignableUILabel!
    @IBOutlet weak var addNewAddressLabel: DesignableUILabel!
    @IBOutlet weak var placeYourOrderButton: CustomButton!
    @IBOutlet weak var keepSocialDistancingLabel: DesignableUILabel!
    @IBOutlet weak var leaveYourOrderAtDoorStepLabel: DesignableUILabel!
    @IBOutlet weak var orderPreferencesLabel: DesignableUILabel!
    @IBOutlet weak var serviceDateLabel: DesignableUILabel!
    @IBOutlet weak var scheduleDeliveryLabel: DesignableUILabel!
    @IBOutlet weak var selectDateLabel: DesignableUILabel!
    @IBOutlet weak var anotherAddNewAddressLabel: DesignableUILabel!
    @IBOutlet weak var addNewAddressButton: UIButton!
    @IBOutlet weak var selectDeliveryAddressForLabel: DesignableUILabel!
    @IBOutlet weak var billingAddressLabel: DesignableUILabel!
    @IBOutlet weak var sameAsdefaultLabel: DesignableUILabel!
    @IBOutlet weak var billingFullName: DesignableUILabel!
    @IBOutlet weak var billingcontactNumberLabel: DesignableUILabel!
    @IBOutlet weak var billibgEmailLabel: DesignableUILabel!
    @IBOutlet weak var billingCountry: DesignableUILabel!
    @IBOutlet weak var billingCityLabel: DesignableUILabel!
    @IBOutlet weak var billingAddress: DesignableUILabel!
    @IBOutlet weak var billingPostalCode: DesignableUILabel!
    @IBOutlet weak var orderBucketLabel: DesignableUILabel!
    @IBOutlet weak var selectDeliveryAddressForItemLabel: DesignableUILabel!
    @IBOutlet weak var billingDetailssLabel: DesignableUILabel!
    @IBOutlet weak var itemTotalLabel: DesignableUILabel!
    @IBOutlet weak var itemDeliveryfee: DesignableUILabel!
    @IBOutlet weak var itemTax: DesignableUILabel!
    @IBOutlet weak var itemTotalAmount: DesignableUILabel!
    @IBOutlet weak var discountLocalizeLabel: DesignableUILabel!
    @IBOutlet weak var discountAmountLabel: DesignableUILabel!
    @IBOutlet weak var hideShowAddressView: UIView!
    @IBOutlet weak var hideShowBillingAddress: UIView!
    @IBOutlet weak var doYouHaveCouponCodeLabel: DesignableUILabel!
    @IBOutlet weak var applyButtonLocalize: UIButton!
    @IBOutlet weak var couponCodeTextField: DesignableUITextField!
    @IBOutlet weak var couponCodeHideShowView: View!
    @IBOutlet weak var couponCodeStackView: UIStackView!
    @IBOutlet weak var couponCodeDiscountedPrice: DesignableUILabel!
    @IBOutlet weak var discountStackView: UIStackView!
    
    var addressData = [Address]()
    var selectedAddress = Address()
    var selectedCity = Int()
    var selectedCountry = Int()
    var selectedTime = 0
    var pickupDeliverySetting = PickupDeliveryResponse()
    var selectedMethod = DeliveryMethod()
    var paymentMethod : Int? = nil
    var ccAvenueUrl: String?
    var isSerivceDate = false
    var serviceDate = Int()
    var selectedDeliveryMethod : Int? = nil
    var allDeliveryMethod = [DeliveryMethod]()
    var selectedDeliveryOrPickup = 0
    var couponCodeAppliedData = CouponCode()
    var deliveryChargeAmount = String()
    var deliveryMethodOptionArray = [String]()
    var paymentPreferenceOptionArray = [String]()
    var servicePrefernceOptionArray = [String]()
    var countryFieldOptionArray = [String]()
    var cityFieldOptionArray = [String]()
    var currentPicker = Int()
    
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
        self.postalCodeField.delegate = self
        
        if Singleton.shared.language == "ur" || Singleton.shared.language == "ar" {
            self.couponCodeTextField.textAlignment = .right
        } else {
            self.couponCodeTextField.textAlignment = .left
        }
        
        self.methoDescription.text = "*For regular deliveries date selection will be allowed after 4 days of order however if possible vendor will deliver sooner.".localizableString(loc: Singleton.shared.language ?? "en")
        self.couponCodeTextField.placeholder = "Enter code here".localizableString(loc: Singleton.shared.language ?? "en")
        self.doYouHaveCouponCodeLabel.text = "Do you have a coupon code?".localizableString(loc: Singleton.shared.language ?? "en")
        self.applyButtonLocalize.setTitle("Apply".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.cancelButton.setTitle("Cancel".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.payNowLabel.text = "Pay Now".localizableString(loc: Singleton.shared.language ?? "en")
        self.addNewAddressLabel.text = "Checkout".localizableString(loc: Singleton.shared.language ?? "en")
        self.placeYourOrderButton.setTitle("Proceed to pay".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.keepSocialDistancingLabel.text = "Keep social distancing".localizableString(loc: Singleton.shared.language ?? "en")
        self.leaveYourOrderAtDoorStepLabel.text = "Leave Your Order At Door Step".localizableString(loc: Singleton.shared.language ?? "en")
        self.orderPreferencesLabel.text = "Order preference".localizableString(loc: Singleton.shared.language ?? "en")
        self.deliveryMethod.placeholder = "Delivery Method".localizableString(loc: Singleton.shared.language ?? "en")
        self.scheduleDeliveryLabel.text = "Schedule Delivery".localizableString(loc: Singleton.shared.language ?? "en")
        self.selectDateLabel.text = "Select Date".localizableString(loc: Singleton.shared.language ?? "en")
        self.anotherAddNewAddressLabel.text = "Address".localizableString(loc: Singleton.shared.language ?? "en")
        self.addNewAddressButton.setTitle("+ Add new address".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        
        self.billingAddressLabel.text = "Billing Address".localizableString(loc: Singleton.shared.language ?? "en")
        self.sameAsdefaultLabel.text = "Same as shipping address".localizableString(loc: Singleton.shared.language ?? "en")
        self.billingFullName.text = "Full Name".localizableString(loc: Singleton.shared.language ?? "en")
        self.billingcontactNumberLabel.text = "Contact No.".localizableString(loc: Singleton.shared.language ?? "en")
        self.billibgEmailLabel.text = "Email".localizableString(loc: Singleton.shared.language ?? "en")
        self.billingCountry.text = "Country".localizableString(loc: Singleton.shared.language ?? "en")
        self.billingCityLabel.text = "City".localizableString(loc: Singleton.shared.language ?? "en")
        self.billingAddress.text = "Address".localizableString(loc: Singleton.shared.language ?? "en")
        self.billingPostalCode.text = "Postal Code".localizableString(loc: Singleton.shared.language ?? "en")
        self.orderBucketLabel.text = "Order Bucket".localizableString(loc: Singleton.shared.language ?? "en")
        self.selectDeliveryAddressForItemLabel.text = "".localizableString(loc: Singleton.shared.language ?? "en") //Select delivery address for item
        self.billingDetailssLabel.text = "Billing details".localizableString(loc: Singleton.shared.language ?? "en")
        self.itemTotalLabel.text = "Item total".localizableString(loc: Singleton.shared.language ?? "en")
        self.itemDeliveryfee.text = "Delivery fee".localizableString(loc: Singleton.shared.language ?? "en")
        self.itemTax.text = "Tax".localizableString(loc: Singleton.shared.language ?? "en")
        self.itemTotalAmount.text = "Total amount".localizableString(loc: Singleton.shared.language ?? "en")
        self.servicePrefernce.placeholder = "Service preference".localizableString(loc: Singleton.shared.language ?? "en")
        self.serviceDateField.placeholder = "Service Date".localizableString(loc: Singleton.shared.language ?? "en")
        self.deliveryNote.placeholder = "Add a note of delivery address".localizableString(loc: Singleton.shared.language ?? "en")
        self.fullNameField.placeholder = "Full Name".localizableString(loc: Singleton.shared.language ?? "en")
        self.mobileNumberField.placeholder = "Enter valid mobile number".localizableString(loc: Singleton.shared.language ?? "en")
        self.emailField.placeholder = "Email Address".localizableString(loc: Singleton.shared.language ?? "en")
        self.countryField.placeholder = "Country".localizableString(loc: Singleton.shared.language ?? "en")
        self.cityField.placeholder = "City".localizableString(loc: Singleton.shared.language ?? "en")
        self.addressField.placeholder = "Enter address here".localizableString(loc: Singleton.shared.language ?? "en")
        self.postalCodeField.placeholder = "Postal Code".localizableString(loc: Singleton.shared.language ?? "en")
        //        self.methoDescription.isHidden = true
        self.view.backgroundColor  = .white
        NavigationController.shared.getCartItem(view: self.view) { (val) in
            self.initializeView()
        }
        self.getPickupDeliverySetting()
        self.deliveryMethodOptionArray = ["Delivery"]
        self.ccAvenueUrl = nil
        handleTextAlignmentForArabic(view: self.view)
    }
    
    //MARK: Functions
    //Initialize View
    func initializeView(){
        
        NavigationController.shared.getCountries { (country) in
            for val in country{
                self.countryFieldOptionArray.append(val.name ?? val.name_en ?? "")
            }
        }
        
        NavigationController.shared.getSavedAddress { (response) in
            //            self.addressData = response
            if(response.count > 0){
                self.selectDeliveryAddressForLabel.text = "Select Delivery Address".localizableString(loc: Singleton.shared.language ?? "en")
                //                self.selectedAddress = response[0]
                for add in response {
                    if add.is_default_address == 1 {
                        self.selectedAddress = add
                    }
                }
                self.defaultBillingImage.image = #imageLiteral(resourceName: "selected")
                self.billingStack.isHidden = true
            }else {
                self.selectDeliveryAddressForLabel.text = "".localizableString(loc: Singleton.shared.language ?? "en")
                self.defaultBillingImage.image = #imageLiteral(resourceName: "unselected")
                self.billingStack.isHidden = false
            }
            self.addressData = response
            self.addressTable.reloadData()
        }
        
        self.setBillingAmountDetails()
        
    }
    
    func setOrderPrefernce(){
        var path = String()
        if self.selectedDeliveryOrPickup == 0 {
            path = "/" + String(self.selectedDeliveryOrPickup) + "/" + String(self.selectedDeliveryMethod ?? 0)
        } else {
            path = "/" + String(self.selectedDeliveryOrPickup) + "/" + "0"
        }
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_SET_ORDER_PREFERENCE + path + "?currency=\(Singleton.shared.selectedCurrency)", method: .get, parameter: nil, objectClass: SetOrderPreferenceResponse.self, requestCode: U_SET_ORDER_PREFERENCE, userToken: UD_TOKEN) { (response) in
            let val = response.response?.cart?.currency_total
            self.deliveryChargeAmount = val?.shipping_amount ?? "0"
            self.chargesLabel.text = "Charges: ".localizableString(loc: Singleton.shared.language ?? "en") + "\(Singleton.shared.selectedCurrencySymbol)" + " \(val?.shipping_amount ?? "0")"
            self.deliveryCharge.text = "\(Singleton.shared.selectedCurrencySymbol)" + " \(val?.shipping_amount?.addComma ?? "0.00")"
            self.itemTotal.text = "\(Singleton.shared.selectedCurrencySymbol) " + (val?.sub_total?.addComma ?? "0.00")
            self.orderTax.text = "\(Singleton.shared.selectedCurrencySymbol) " + (val?.total_tax?.addComma ?? "0.00")
            self.totalAmount.text = "\(Singleton.shared.selectedCurrencySymbol) " + (val?.order_total?.addComma ?? "0.00")
        }
    }
    
    func setBillingAmountDetails(){
        if Singleton.shared.cartData.cart?.coupon_applied == "0" || Singleton.shared.cartData.cart?.coupon_code == nil {
            self.couponCodeStackView.isHidden = true
            self.couponCodeTextField.text = ""
        } else {
            self.couponCodeStackView.isHidden = false
            self.couponCodeDiscountedPrice.text = "- \(Singleton.shared.selectedCurrencySymbol) " + "\(Singleton.shared.cartData.cart?.currency_total?.coupon_discount_amount ?? "0")"
            self.couponCodeTextField.text = Singleton.shared.cartData.cart?.coupon_code ?? ""
        }
        
        self.discountAmountLabel.text = "- \(Singleton.shared.selectedCurrencySymbol) " + "\(Singleton.shared.cartData.cart?.currency_total?.discount_amount ?? "0")"
        self.itemTotal.text = "\(Singleton.shared.selectedCurrencySymbol) " + (Singleton.shared.cartData.cart?.currency_total?.sub_total ?? "")
        self.orderTax.text = "\(Singleton.shared.selectedCurrencySymbol) " + (Singleton.shared.cartData.cart?.currency_total?.total_tax ?? "")
        self.deliveryCharge.text = "\(Singleton.shared.selectedCurrencySymbol) " + (Singleton.shared.cartData.cart?.currency_total?.shipping_amount ?? "")
        self.totalAmount.text = "\(Singleton.shared.selectedCurrencySymbol) " + "\(Singleton.shared.cartData.cart?.currency_total?.order_total ?? "")"
    }
    
    func getPickupDeliverySetting(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_PICKUP_DELIVERY_SETTING + "?lang=\(Singleton.shared.language ?? "en")", method: .get, parameter: nil, objectClass: GetPickupDelivery.self, requestCode: U_GET_PICKUP_DELIVERY_SETTING, userToken: nil) { (response) in
            self.pickupDeliverySetting = response.response
            
            
            for i in 0..<(self.pickupDeliverySetting.delivery_methods?.count ?? 0){
                if self.pickupDeliverySetting.delivery_methods?[i].status == 1 {
                    self.allDeliveryMethod.append(self.pickupDeliverySetting.delivery_methods?[i] ?? DeliveryMethod())
                }
            }
            
            if self.allDeliveryMethod.count > 0 {
                self.selectedMethod = self.allDeliveryMethod[0]
                self.selectedDeliveryMethod = self.allDeliveryMethod[0].id
                self.setOrderPrefernce()
            }
            
            var paymentMethodArray = [String]()
            
            if self.pickupDeliverySetting.payment_methods?.cc_method != nil {
                paymentMethodArray.append("Debit/Credit Card")
            }
            
            if self.pickupDeliverySetting.payment_methods?.cod_method != nil {
                paymentMethodArray.append("Cash on Delivery")
            }
            
            if self.pickupDeliverySetting.payment_methods?.bank_transfer_method != nil {
                paymentMethodArray.append("Bank Transfer")
            }
            
            self.paymentPreferenceOptionArray = paymentMethodArray
            
            if(self.pickupDeliverySetting.order_preference == 0 || self.pickupDeliverySetting.order_preference == 2){
                self.deliveryMethoTable.reloadData()
                if(self.pickupDeliverySetting.order_preference == 2){
                    self.deliveryMethodOptionArray.append("Pickup")
                    self.orderPrefView.isHidden = false
                }else {
                    self.orderPrefView.isHidden = true
                }
                self.orderPreferenceView.isHidden = true
                self.deliveryMethodView.isHidden = false
            }else {
                self.orderPrefView.isHidden = true
                self.deliveryMethodOptionArray = []
                self.deliveryMethod.text = "Pickup".localizableString(loc: Singleton.shared.language ?? "en")
                self.deliveryMethodOptionArray.append("Pickup")
                self.deliveryMethodView.isHidden = true
                self.selectedDeliveryOrPickup = 1
                self.orderPreferenceView.isHidden = false
            }
            
            if(Singleton.shared.cartData.cart?.serviceable_cart == 1){
                self.serviceView.isHidden = false
                self.orderPreferenceView.isHidden = true
                self.orderPrefView.isHidden = true
                self.deliveryMethodView.isHidden = true
                if(self.pickupDeliverySetting.service_available_at == 1){
                    self.servicePrefernce.isHidden = true
                    self.servicePreferenceLabel.text = "Service preference: At Home".localizableString(loc: Singleton.shared.language ?? "en")
                }else if(self.pickupDeliverySetting.service_available_at == 2){
                    self.servicePreferenceLabel.text = "Service preference: At store".localizableString(loc: Singleton.shared.language ?? "en")
                    self.servicePrefernce.isHidden = true
                }else if(self.pickupDeliverySetting.service_available_at == 3){
                    self.servicePrefernce.isHidden = false
                    self.servicePreferenceLabel.text = "Service preference".localizableString(loc: Singleton.shared.language ?? "en")
                    self.servicePrefernceOptionArray = ["At Home", "At Store"]
                }
            }else {
                self.serviceView.isHidden = true
            }
            
            ActivityIndicator.hide()
        }
    }
    
    
    
    //MARK: IBAction
    @IBAction func addNewAddress(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
        myVC.addressDelegate = self
        self.navigationController?.pushViewController(myVC, animated: true)
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func placeOrderAction(_ sender: Any) {
        if self.selectedDeliveryOrPickup == nil {
            Singleton.shared.showToast(msg: "Please select delivery or pickup".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
            return
        } else if self.selectedDeliveryOrPickup == 0 {
            if( self.selectedDeliveryMethod == nil){
                Singleton.shared.showToast(msg: "Please select delivery method".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                return
            }else if(self.pickupDeliverySetting.order_preference == 1 && self.selectedTime == 0){
                Singleton.shared.showToast(msg: "Select pickup Date".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                return
            }
            else if self.paymentMethod == nil {
                Singleton.shared.showToast(msg: "Select payment method".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                return
            }
            
            if(self.defaultBillingImage.image == #imageLiteral(resourceName: "unselected") && self.fullNameField.text!.isEmpty){
                Singleton.shared.showToast(msg: "Enter Billing name".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                return
            }else if(self.defaultBillingImage.image == #imageLiteral(resourceName: "unselected") && self.mobileNumberField.text!.isEmpty){
                Singleton.shared.showToast(msg: "Enter Billing contact number".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                return
            }else if(self.defaultBillingImage.image == #imageLiteral(resourceName: "unselected") && self.emailField.text!.isEmpty){
                Singleton.shared.showToast(msg: "Enter Billing email".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                return
            }else if(self.defaultBillingImage.image == #imageLiteral(resourceName: "unselected") && self.addressField.text!.isEmpty){
                Singleton.shared.showToast(msg: "Enter Billing address".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                return
            }else if(self.defaultBillingImage.image == #imageLiteral(resourceName: "unselected") && self.selectedCity == 0){
                Singleton.shared.showToast(msg: "Select Billing city".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                return
            }else if(self.defaultBillingImage.image == #imageLiteral(resourceName: "unselected") && self.selectedCountry == 0){
                Singleton.shared.showToast(msg: "Select Billing country".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                return
            }else if(self.defaultBillingImage.image == #imageLiteral(resourceName: "unselected") && self.postalCodeField.text!.isEmpty){
                Singleton.shared.showToast(msg: "Enter Billing postal code".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                return
            }
            
        } else if selectedDeliveryOrPickup == 1 {
            if self.orderPreference.text == "" {
                Singleton.shared.showToast(msg: "Select pickup Date".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                return
            } else if self.paymentMethod == nil {
                Singleton.shared.showToast(msg: "Select payment method".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                return
            } else if(self.defaultBillingImage.image == #imageLiteral(resourceName: "unselected") && self.fullNameField.text!.isEmpty){
                Singleton.shared.showToast(msg: "Enter Billing name".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                return
            }else if(self.defaultBillingImage.image == #imageLiteral(resourceName: "unselected") && self.mobileNumberField.text!.isEmpty){
                Singleton.shared.showToast(msg: "Enter Billing contact number".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                return
            }else if(self.defaultBillingImage.image == #imageLiteral(resourceName: "unselected") && self.emailField.text!.isEmpty){
                Singleton.shared.showToast(msg: "Enter Billing email".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                return
            }else if(self.defaultBillingImage.image == #imageLiteral(resourceName: "unselected") && self.addressField.text!.isEmpty){
                Singleton.shared.showToast(msg: "Enter Billing address".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                return
            }else if(self.defaultBillingImage.image == #imageLiteral(resourceName: "unselected") && self.selectedCity == 0){
                Singleton.shared.showToast(msg: "Select Billing city".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                return
            }else if(self.defaultBillingImage.image == #imageLiteral(resourceName: "unselected") && self.selectedCountry == 0){
                Singleton.shared.showToast(msg: "Select Billing country".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                return
            }else if(self.defaultBillingImage.image == #imageLiteral(resourceName: "unselected") && self.postalCodeField.text!.isEmpty){
                Singleton.shared.showToast(msg: "Enter Billing postal code".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                return
            }
            
        } else  if let url = self.ccAvenueUrl {
            self.webView.load(URLRequest(url: URL(string: url)!))
            self.popupView.isHidden = false
            self.webView.navigationDelegate = self
            return
        }
        ActivityIndicator.show(view: self.view)
        var param = [String:Any]()
        
        if self.selectedDeliveryOrPickup == 0 {
            param = [
                "email" : self.emailField.text != ""  ?  self.emailField.text ?? "":Singleton.shared.userDetail.email ?? "",
                "billing_name" : self.fullNameField.text != "" ?  self.fullNameField.text:((self.selectedAddress.firstname ?? "") + " " + ( self.selectedAddress.lastname ?? "")),
                "billing_phone" : (self.mobileNumberField.text != "" ? self.mobileNumberField.text ?? "":self.selectedAddress.phone ?? "").replacingOccurrences(of: "+", with: ""),
                "billing_address" : self.addressField.text != "" ? self.addressField.text ?? "":self.selectedAddress.address ?? "",
                "billing_city" : self.selectedCity != 0 ? self.selectedCity:self.selectedAddress.city_id ?? 0,
                "billing_country" : self.selectedCountry != 0 ? self.selectedCountry:self.selectedAddress.country_id ?? 0,
                "billing_postcode" : self.postalCodeField.text != "" ? self.postalCodeField.text ?? "":self.selectedAddress.postalcode ?? "00000",
                "shipping_name" : self.fullNameField.text != "" ?  self.fullNameField.text:((self.selectedAddress.firstname ?? "") + " " + ( self.selectedAddress.lastname ?? "")),
                "shipping_phone" : (self.mobileNumberField.text != "" ? self.mobileNumberField.text ?? "":self.selectedAddress.phone ?? "").replacingOccurrences(of: "+", with: ""),
                "shipping_address" : self.addressField.text != "" ? self.addressField.text ?? "":self.selectedAddress.address ?? "",
                "shipping_city" : self.selectedCity != 0 ? self.selectedCity:self.selectedAddress.city_id ?? 0,
                "shipping_country" : self.selectedCountry != 0 ? self.selectedCountry:self.selectedAddress.country_id ?? 0,
                "shipping_postcode" :self.postalCodeField.text != "" ? self.postalCodeField.text ?? "":self.selectedAddress.postalcode ?? "00000",
                "order_preference" : self.selectedDeliveryOrPickup,
                "delivery_method": selectedDeliveryMethod,
                "delivery_date": self.selectedTime,
                "payment_method":self.paymentMethod,
                "ios_placeorder": 1,
                "service_preference": self.servicePrefernce.text == "At Home" ? 2:1,
                "service_date":self.serviceDate,
                "order_notes" : self.deliveryNote.text
            ]
        } else if self.selectedDeliveryOrPickup == 1{
            param = [
                "email" : self.emailField.text != ""  ?  self.emailField.text ?? "":Singleton.shared.userDetail.email ?? "",
                "billing_name" : self.fullNameField.text != "" ?  self.fullNameField.text:((self.selectedAddress.firstname ?? "") + " " + ( self.selectedAddress.lastname ?? "")),
                "billing_phone" : (self.mobileNumberField.text != "" ? self.mobileNumberField.text ?? "":self.selectedAddress.phone ?? "").replacingOccurrences(of: "+", with: ""),
                "billing_address" : self.addressField.text != "" ? self.addressField.text ?? "":self.selectedAddress.address ?? "",
                "billing_city" : self.selectedCity != 0 ? self.selectedCity:self.selectedAddress.city_id ?? 0,
                "billing_country" : self.selectedCountry != 0 ? self.selectedCountry:self.selectedAddress.country_id ?? 0,
                "billing_postcode" : self.postalCodeField.text != "" ? self.postalCodeField.text ?? "":self.selectedAddress.postalcode ?? "00000",
                "order_preference" : self.selectedDeliveryOrPickup,
                "delivery_method": selectedDeliveryMethod,
                "delivery_date": self.selectedTime,
                "payment_method":self.paymentMethod,
                "ios_placeorder": 1,
                "service_preference": self.servicePrefernce.text == "At Home" ? 2:1,
                "service_date":self.serviceDate,
                "shipping_name" : self.fullNameField.text != "" ?  self.fullNameField.text:((self.selectedAddress.firstname ?? "") + " " + ( self.selectedAddress.lastname ?? "")),
                "shipping_phone" : (self.mobileNumberField.text != "" ? self.mobileNumberField.text ?? "":self.selectedAddress.phone ?? "").replacingOccurrences(of: "+", with: ""),
                "shipping_address" : self.addressField.text != "" ? self.addressField.text ?? "":self.selectedAddress.address ?? "",
                "shipping_city" : self.selectedCity != 0 ? self.selectedCity:self.selectedAddress.city_id ?? 0,
                "shipping_country" : self.selectedCountry != 0 ? self.selectedCountry:self.selectedAddress.country_id ?? 0,
                "shipping_postcode" :self.postalCodeField.text != "" ? self.postalCodeField.text ?? "":self.selectedAddress.postalcode ?? "00000",
                "order_notes" : self.deliveryNote.text
            ]
        }
        
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_PLACE_ORDER, method: .post, parameter: param, objectClass: PlaceOrder.self, requestCode: U_PLACE_ORDER, userToken: nil) { (response) in
            ActivityIndicator.hide()
            switch response.status {
            case .string(let text):
                Singleton.shared.showToast(msg: (response.message ?? "").localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 0)
                break
            case .int(let text):
                if(self.paymentMethod == 0){
                    ActivityIndicator.show(view: self.view)
                    self.ccAvenueUrl =  response.response?.redirect_url ?? ""
                    self.webView.load(URLRequest(url: URL(string: response.response?.redirect_url ?? "")!))
                    self.webView.navigationDelegate = self
                    self.popupView.isHidden = false
                }else {
                    self.popupView.isHidden = true
                    self.redirectToHome()
                    break
                }
            default:
                break
            }
        }
    }
    
    
    func redirectToHome(){
        K_BAG_COUNT = 0
        UserDefaults.standard.removeObject(forKey: UD_BAG_COUNT)
        let myVC  = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
        self.navigationController?.pushViewController(myVC, animated: true)
        Singleton.shared.cartData = GetCartResponse()
        Singleton.shared.showToast(msg: "Order placed successfully".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 1)
        
    }
    
    @IBAction func billingGoogleAction(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        let fields: GMSPlaceField = GMSPlaceField(rawValue:UInt(GMSPlaceField.name.rawValue) |
                                                  UInt(GMSPlaceField.placeID.rawValue) |
                                                  UInt(GMSPlaceField.coordinate.rawValue) |
                                                  GMSPlaceField.addressComponents.rawValue |
                                                  GMSPlaceField.formattedAddress.rawValue)
        autocompleteController.placeFields = fields
        let filter = GMSAutocompleteFilter()
       filter.countries = ["BH", "AE"]
        
        autocompleteController.autocompleteFilter = filter
        autocompleteController.modalPresentationStyle = .overFullScreen
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func removeCouponCodeAction(_ sender: Any) {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_REMOVE_COUPON + "?currency=\(Singleton.shared.selectedCurrency)", method: .get, parameter: nil, objectClass: GetCouponCodeResponse.self, requestCode: U_REMOVE_COUPON, userToken: UD_TOKEN) { (response) in
            self.couponCodeStackView.isHidden = true
            self.couponCodeAppliedData = response.response ?? CouponCode()
            Singleton.shared.cartData.cart = response.response?.cart ?? CartResponse()
            self.discountAmountLabel.text = "- \(Singleton.shared.selectedCurrencySymbol) " + "\(self.couponCodeAppliedData.cart?.currency_total?.discount_amount ?? "")"
            self.itemTotal.text = "\(Singleton.shared.selectedCurrencySymbol) " + (self.couponCodeAppliedData.cart?.currency_total?.sub_total ?? "")
            self.orderTax.text = "\(Singleton.shared.selectedCurrencySymbol) " + (self.couponCodeAppliedData.cart?.currency_total?.total_tax ?? "")
            let total = Double(self.couponCodeAppliedData.cart?.currency_total?.order_total ?? "0")!
            self.totalAmount.text = "\(Singleton.shared.selectedCurrencySymbol) " + "\(total)"
            self.couponCodeTextField.text = ""
            Singleton.shared.showToast(msg: "Coupon code removed successfully".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 1)
            ActivityIndicator.hide()
        }
    }
    
    //Apply coupon code action and also validate
    @IBAction func applyCouponCodeAction(_ sender: Any) {
        self.couponCodeTextField.resignFirstResponder()
        if self.couponCodeTextField.text != "" {
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_APPLY_COUPON + "?currency=\(Singleton.shared.selectedCurrency)", method: .post, parameter: ["coupon_code" : self.couponCodeTextField.text], objectClass: GetCouponCodeResponse.self, requestCode: U_APPLY_COUPON, userToken: UD_TOKEN) { (response) in
                Singleton.shared.cartData.cart = response.response?.cart ?? CartResponse()
                self.couponCodeAppliedData = response.response ?? CouponCode()
                self.couponCodeStackView.isHidden = false
                self.discountAmountLabel.text = "- \(Singleton.shared.selectedCurrencySymbol) " + "\(self.couponCodeAppliedData.cart?.currency_total?.discount_amount ?? "0")"
                self.couponCodeDiscountedPrice.text = "- \(Singleton.shared.selectedCurrencySymbol) " + "\(self.couponCodeAppliedData.cart?.currency_total?.coupon_discount_amount ?? "")"
                self.itemTotal.text = "\(Singleton.shared.selectedCurrencySymbol) " + (self.couponCodeAppliedData.cart?.currency_total?.sub_total ?? "")
                self.orderTax.text = "\(Singleton.shared.selectedCurrencySymbol) " + (self.couponCodeAppliedData.cart?.currency_total?.total_tax ?? "")
                let total = Double(self.couponCodeAppliedData.cart?.currency_total?.order_total ?? "0")!
                self.totalAmount.text = "\(Singleton.shared.selectedCurrencySymbol) " + "\(total)"
                Singleton.shared.showToast(msg: "Coupon code applied successfully".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 1)
                ActivityIndicator.hide()
            }
        } else {
            Singleton.shared.showToast(msg: "Enter coupon code first".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.popupView.isHidden = true
    }
    
    @IBAction func orderPreferenceAction(_ sender: UIButton){
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        myVC.modalPresentationStyle = .overFullScreen
        myVC.pickerMode = .date
        myVC.dateDelegate = self
        myVC.headingText = "Pick Date".localizableString(loc: Singleton.shared.language ?? "en")
        if(sender.tag == 1){
            var dayComponent    = DateComponents()
            dayComponent.day    = Int(self.pickupDeliverySetting.expected_pickup_days ?? "0")
            let theCalendar     = Calendar.current
            let nextDate        = theCalendar.date(byAdding: dayComponent, to: Date())
            myVC.minDate = nextDate
            self.isSerivceDate = true
        }else {
            myVC.orderPreference = self.pickupDeliverySetting.order_preference ?? 0
            if((self.pickupDeliverySetting.order_preference ?? 0) == 1){
                myVC.minDate = Date()
            }else if(((self.pickupDeliverySetting.order_preference ?? 0) == 0) || ((self.pickupDeliverySetting.order_preference ?? 0) == 2)){
                var dayComponent    = DateComponents()
                dayComponent.day    = 4
                let theCalendar     = Calendar.current
                let nextDate        = theCalendar.date(byAdding: dayComponent, to: Date())
                myVC.minDate = nextDate
            }
        }
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
    
    
    @IBAction func defaultBillingAction(_ sender: Any) {
        if(self.defaultBillingImage.image == #imageLiteral(resourceName: "unselected")){
            if self.addressData.count > 0 {
                self.defaultBillingImage.image = #imageLiteral(resourceName: "selected")
                self.billingStack.isHidden = true
            } else {
                Singleton.shared.showToast(msg: "Please enter address first".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
            }
        }else {
            self.defaultBillingImage.image = #imageLiteral(resourceName: "unselected")
            self.billingStack.isHidden = false
        }
    }
    
    
    //for delivery type picker view open
    @IBAction func deliveryMethodDropDownAction(_ sender: Any) {
        self.currentPicker = 1
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        myVC.pickerData = self.deliveryMethodOptionArray
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    //open pickerview for payment type
    @IBAction func paymentPreferenceDropAction(_ sender: Any) {
        self.currentPicker = 2
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        myVC.pickerData = self.paymentPreferenceOptionArray
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func servicePrefernceDropAction(_ sender: Any) {
    }
    
    //open country picker view
    @IBAction func countryAction(_ sender: Any) {
        self.currentPicker = 3
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        myVC.pickerData = self.countryFieldOptionArray
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    //open city picker view
    @IBAction func cityAction(_ sender: Any) {
        self.currentPicker = 4
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        myVC.pickerData = self.cityFieldOptionArray
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
}

//MARK: Table Delegate
extension CheckoutViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == self.cartTable){
            return Singleton.shared.cartData.cart?.cart_items?.count ?? 0
        }else if(tableView == self.deliveryMethoTable){
            return self.allDeliveryMethod.count
        }else {
            return self.addressData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == self.cartTable){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableCell", for: indexPath) as! CartTableCell
            let val = Singleton.shared.cartData.cart?.cart_items?[indexPath.row]
            cell.item = val
            if((val?.Product?.default_image ?? "").contains("http")){
                cell.productImage.sd_setImage(with: URL(string: (val?.Product?.default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }else {
                cell.productImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (val?.Product?.default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }
            cell.productName.text = (val?.Product?.name ?? "") + " x \(val?.qty ?? 0)"
            
            cell.productPrice.text = "\(Singleton.shared.selectedCurrencySymbol) \(convertPriceInDouble(price: val?.price).addComma)"
            
            cell.countLabel.text = "\(val?.qty ?? 0)"
            
            return cell
        }else if(tableView == self.deliveryMethoTable){
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddressTableCell", for: indexPath) as! AddressTableCell
            let val = self.allDeliveryMethod[indexPath.row]
            
            cell.street.text =  val.title
            cell.address.text = "\(Singleton.shared.selectedCurrencySymbol) \(val.delivery_charges ?? "0")"
            
            if((val.title ?? "") == (self.selectedMethod.title ?? "")){
                cell.addressSelectionimage.image = #imageLiteral(resourceName: "selected")
                if(val.title == "Regular Delivery"){
                    //                    self.orderPreferenceView.isHidden = false
                    //                    self.orderPreference.text = ""
                    //                    self.methoDescription.text = "Want to schedule the delivery".localizableString(loc: Singleton.shared.language ?? "en")
                }else {
                    self.orderPreferenceView.isHidden = true
                    self.orderPreference.text = ""
                    //                    self.methoDescription.text = "*For regular deliveries date selection will be allowed after 4 days of order however if possible vendor will deliver sooner.".localizableString(loc: Singleton.shared.language ?? "en")
                }
            }else {
                cell.addressSelectionimage.image = #imageLiteral(resourceName: "unselected")
            }
            cell.defaultAddressbutton={
                self.selectedMethod = val
                
                switch self.selectedMethod.id {
                    
                case 1:
                    self.selectedDeliveryMethod = 1
                    break
                case 2:
                    self.selectedDeliveryMethod = 2
                    break
                case 3:
                    self.selectedDeliveryMethod = 3
                    break
                case 4:
                    self.selectedDeliveryMethod = 4
                    break
                default:
                    break
                }
                
                self.setOrderPrefernce()
                self.deliveryMethoTable.reloadData()
            }
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddressTableCell", for: indexPath) as! AddressTableCell
            let val = self.addressData[indexPath.row]
            if((val.id ?? 0) == (self.selectedAddress.id ?? 0)){
                cell.addressSelectionimage.image = #imageLiteral(resourceName: "selected")
            }else {
                cell.addressSelectionimage.image = #imageLiteral(resourceName: "unselected")
            }
            cell.street.text =  (val.firstname ?? "") + (val.lastname ?? "")
            cell.address.text = val.address
            
            cell.defaultAddressbutton={
                let add = self.addressData[indexPath.row]
                self.selectedAddress = Address(vendor_name: add.vendor_name, subdomain: add.subdomain, long: add.long, lat: add.lat, vendor_id: add.vendor_id, distance: add.distance, image: add.image, city_id: add.city_id, country_id: add.country_id, id: add.id, user_id: add.user_id, firstname: add.firstname, lastname: add.lastname, address: add.address, postalcode: add.postalcode, phone: add.phone, is_default_address: add.is_default_address, phone_code: add.phone_code, country_name: add.country_name, city_name: add.city_name)
                self.addressTable.reloadData()
            }
            return cell
        }
    }
}


//MARK: Textfield Delegate
extension CheckoutViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.postalCodeField {
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
            }
            
            if (textField.text?.count ?? 0) < 7 {
                return true
            } else {
                return false
            }
        }
        
        return true
    }
}

//MARK: Map Delegate
extension CheckoutViewController: GMSAutocompleteViewControllerDelegate ,GMSMapViewDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        dismiss(animated: true, completion: nil)
        self.addressField.text = place.formattedAddress
        self.zoomToLocation(val: place.coordinate, address: place.formattedAddress)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func zoomToLocation(val:CLLocationCoordinate2D, address: String?){
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: val.latitude, longitude: val.longitude), preferredLocale: nil) { (response, error) in
            var add = String()
            if((response?.count ?? 0) > 0){
                if(address != nil){
                    self.addressField
                        .text = (response?[0].thoroughfare ?? "") + (response?[0].subLocality ?? "") + (response?[0].locality ?? "")
                    self.addressField.text = (self.addressField.text ?? "") + (response?[0].locality ?? "")
                    self.addressField.text = (self.addressField.text ?? "") + (response?[0].country ?? "")
                }
            }
        }
    }
    
}


//MARK: Table Cell
class CartTableCell : UITableViewCell{
    
    
    var item : CartItems?
    
    @IBOutlet weak var productImage: ImageView!
    @IBOutlet weak var productName: DesignableUILabel!
    @IBOutlet weak var productPrice: DesignableUILabel!
    @IBOutlet weak var countLabel: DesignableUILabel!
    
    
    
    @IBAction func minusButtonAction(_ sender: Any) {
        
        if item?.qty ?? 0 <= 0 {
            return
        }
        updateQuantity(addValue : -1)
    }
    
    @IBAction func plusButtonAction(_ sender: Any) {
        
        updateQuantity(addValue : 1)
    }
    
    
    // for quantity update of a product.
    func updateQuantity(addValue : Int){
        countLabel.text = ("\((item?.qty ?? 0) + addValue)")
        
        guard let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController,
              let mainScreen = navigationController.topViewController as? CheckoutViewController else {
            print("error")
            return
        }
        
        ActivityIndicator.show(view: mainScreen.view )
        
        var url = String()
        
        if(Singleton.shared.userDetail.id != nil){
            if(countLabel.text == "0"){
                url = U_BASE + U_DELETE_CART_ITEM
            }else {
                url = U_BASE + U_UPDATE_CART
            }
            
            SessionManager.shared.methodForApiCalling(url: url + "?lang\(Singleton.shared.language ?? "en")", method: .post, parameter: ["id": item?.id ?? 0, "qty": countLabel.text ?? ""], objectClass: SuccessResponse.self, requestCode: U_UPDATE_CART, userToken: nil) { (response) in
                Singleton.shared.cartData = GetCartResponse()
                ActivityIndicator.hide()
                NavigationController.shared.getCartItem(view: mainScreen.view) { (response) in
                    
                    if(Singleton.shared.cartData.cart?.cart_items == nil ){
                        UserDefaults.standard.removeObject(forKey: UD_BAG_COUNT)
                        K_BAG_COUNT = 0
                        reloadBucket = true
                        mainScreen.navigationController?.popViewController(animated: true)
                    }else{
                        mainScreen.cartTable.reloadData()
                        mainScreen.setBillingAmountDetails()
                    }
                }
            }
        }
    }
}
