//
//  ShoppingBucketViewController.swift
//  Symo New
//
//  Created by Apple on 01/04/21
//

import UIKit

var reloadBucket = false

class ShoppingBucketViewController: UIViewController, Confirmation{
    
    
    //MARK: Delegate Functions
    func confirmationSelection(action: Int) {
        if (self.currentAlertModal == 1){
            if(action == 1){
                
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                myVC.redirectBack = true
                self.navigationController?.pushViewController(myVC, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
                    self.movetoCheckout = true
                }
                
            }
        }else{
            if(action == 1){
                self.shouldReload = true
                ActivityIndicator.show(view: self.view)
                if(Singleton.shared.userDetail.id != nil){
                    SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_CART, method: .post, parameter: ["cart_id":Singleton.shared.cartData.cart?.id ?? 0, "lang": Singleton.shared.language ?? "en"], objectClass: SuccessResponse.self, requestCode: U_DELETE_CART, userToken: nil) { (response) in
                        UserDefaults.standard.removeObject(forKey: UD_BAG_COUNT)
                        self.getCartData()
                        ActivityIndicator.hide()
                    }
                } else {
                    var cartId = ""
                    if(UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) != nil){
                        cartId = UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) as! String
                    }
                    SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_GUEST_CART + "\(cartId)" , method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_DELETE_GUEST_CART, userToken: nil) { (response) in
                        UserDefaults.standard.removeObject(forKey: UD_BAG_COUNT)
                        self.getGuestCartData()
                        ActivityIndicator.hide()
                    }
                }
            }
        }
        
    }
    
    
    
    
    //MARK: IBOutlets
    @IBOutlet weak var pinkView: UIView!
    @IBOutlet weak var cartTable: UITableView!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var buyButton: CustomButton!
    @IBOutlet weak var menuHeight: NSLayoutConstraint!
    @IBOutlet weak var shoppingBucketLabel: DesignableUILabel!
    @IBOutlet weak var cartEmptyHeadingLabel: DesignableUILabel!
    @IBOutlet weak var cartEmptyLabel: DesignableUILabel!
    @IBOutlet weak var proceedToCheckoutButton: CustomButton!
    @IBOutlet weak var clearButton: UIButton!
    
    static var manageDelegate : ManageCartButton? = nil
    var cartData = GetCartResponse()
    var showBackButton = false
    var shouldReload = false
    var manageButtonArray = [Int]()
    var currentAlertModal = 0
    var movetoCheckout = false
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.noDataView.isHidden = true
        self.shoppingBucketLabel.text = "Shopping Bucket".localizableString(loc: Singleton.shared.language ?? "en")
        self.cartEmptyHeadingLabel.text = "Cart is empty!".localizableString(loc: Singleton.shared.language ?? "en")
        self.cartEmptyLabel.text = "Cart is empty!".localizableString(loc: Singleton.shared.language ?? "en")
        self.buyButton.setTitle("Proceed to Checkout".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.clearButton.setTitle("Clear".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        
        self.cartTable.estimatedRowHeight = 150
        self.cartTable.rowHeight = UITableView.automaticDimension
        self.view.backgroundColor = .white
        self.pinkView.backgroundColor = K_PINK_COLOR
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
        self.clearButton.isHidden = true
        if Singleton.shared.userDetail.id == nil {
            
            self.getGuestCartData()
            
        } else {
            self.getCartData()
        }
        
        self.buyButton.isHidden = true
        if(self.showBackButton){
            self.menuImage.image = #imageLiteral(resourceName: "back_arrow@2x-2")
            self.menuHeight.constant = 25
        }else {
            self.menuImage.image = #imageLiteral(resourceName: "menu_icon")
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        // added these because bag was not reload when pop back from child screen
        if reloadBucket{
            self.cartData = Singleton.shared.cartData
            self.manageButtonArray = []
            for i in 0..<(self.cartData.cart?.cart_items?.count ?? 0){
                self.manageButtonArray.append(self.cartData.cart?.cart_items?[i].product_id ?? 0)
            }
            
            if (self.cartData.cart?.cart_items?.count ?? 0) > 0 {
                self.noDataView.isHidden = true
            } else {
                self.noDataView.isHidden = false
            }
            
            self.cartTable.reloadData()
            reloadBucket = false
        }
        
        if (movetoCheckout){
            movetoCheckout = false
            self.buyAction(self)
        }
    }
    
    //MARK: Functions
    func getGuestCartData(){
        K_BAG_COUNT = 0
        var cartId = ""
        if(UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) != nil){
            cartId = UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) as! String
        }
        if(cartId != nil && cartId != ""){
            Singleton.shared.cartData = GetCartResponse()
            ActivityIndicator.show(view: view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_GUEST_CART +  "\(cartId)" + "?currency=\(Singleton.shared.selectedCurrency)&lang=\(Singleton.shared.language ?? "en")", method: .get, parameter: nil, objectClass: GetCartData.self, requestCode: U_GET_GUEST_CART, userToken: nil) { (response) in
                Singleton.shared.cartData = response.response
                self.cartData = response.response
                self.manageButtonArray = []
                for i in 0..<(self.cartData.cart?.cart_items?.count ?? 0){
                    self.manageButtonArray.append(self.cartData.cart?.cart_items?[i].product_id ?? 0)
                }
                
                if (self.cartData.cart?.cart_items?.count ?? 0) > 0 {
                    self.noDataView.isHidden = true
                } else {
                    self.noDataView.isHidden = false
                }
                
                self.cartTable.reloadData()
                ActivityIndicator.hide()
                ActivityIndicator.hide()
            }
        } else {
            self.noDataView.isHidden = false
        }
    }
    
    func getCartData(){
        K_BAG_COUNT = 0
        Singleton.shared.cartData = GetCartResponse()
        if(Singleton.shared.userDetail.id != nil){
            NavigationController.shared.getCartItem(view: self.view) { (response) in
                
                self.cartData = response
                self.manageButtonArray = []
                for i in 0..<(self.cartData.cart?.cart_items?.count ?? 0){
                    self.manageButtonArray.append(self.cartData.cart?.cart_items?[i].product_id ?? 0)
                }
                
                if (self.cartData.cart?.cart_items?.count ?? 0) > 0 {
                    self.noDataView.isHidden = true
                } else {
                    self.noDataView.isHidden = false
                }
                
                self.cartTable.reloadData()
            }
            ActivityIndicator.hide()
            self.buyButton.isHidden = false
        }
    }
    
    func showPopup(title: String, msg: String) {
        
        currentAlertModal = 1
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationPopupViewController") as! ConfirmationPopupViewController
        myVC.confirmationDelegate = self
        myVC.confirmationText = title.localizableString(loc: Singleton.shared.language ?? "en")
        myVC.detailText = msg.localizableString(loc: Singleton.shared.language ?? "en")
        myVC.secondButtonTitle = "Not now".localizableString(loc: Singleton.shared.language ?? "en")
        myVC.modalPresentationStyle = .overFullScreen
        self.present(myVC, animated: true, completion: nil)
        
    }
    
    func showClearPopup(title: String, msg: String) {
        currentAlertModal = 0
        
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationPopupViewController") as! ConfirmationPopupViewController
        myVC.confirmationDelegate = self
        myVC.confirmationText = title.localizableString(loc: Singleton.shared.language ?? "en")
        myVC.detailText = msg.localizableString(loc: Singleton.shared.language ?? "en")
        myVC.secondButtonTitle = "Not now".localizableString(loc: Singleton.shared.language ?? "en")
        myVC.modalPresentationStyle = .overFullScreen
        self.present(myVC, animated: true, completion: nil)

    }
    
    
    //MARK: IBActions
    @IBAction func menuAction(_ sender: Any) {
        if(showBackButton){
            ShoppingBucketViewController.manageDelegate?.manageButton(idArray: self.manageButtonArray,isReload: self.shouldReload)
            self.navigationController?.popViewController(animated: true)
        }else{
            self.menu()
        }
    }
    
    @IBAction func clearCartAction(_ sender: Any) {
        self.showClearPopup(title: "Clear Cart".localizableString(loc: Singleton.shared.language ?? "en"), msg: "Do you really want to clear this cart?".localizableString(loc: Singleton.shared.language ?? "en"))
    }
    
    @IBAction func buyAction(_ sender: Any) {
        if(Singleton.shared.userDetail.id != nil){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CheckoutViewController") as! CheckoutViewController
            self.navigationController?.pushViewController(myVC, animated: true)
        } else {
            self.showPopup(title: "Alert!", msg: "Please login to proceed")
        }
    }
}

//MARK: Table Delegate
extension ShoppingBucketViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SingleProductViewController") as! SingleProductViewController
        myVC.selectedProduct = self.cartData.cart?.cart_items?[indexPath.row].Product ?? ProductResponse()
        myVC.subdomain = self.cartData.cart?.cart_items?[indexPath.row].Product?.vendor_details?.subdomain ?? ""
        if(myVC.subdomain != ""){
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if((self.cartData.cart?.cart_items?.count ?? 0) == 0){
            buyButton.isHidden = true
            self.clearButton.isHidden = true
        }else {
            buyButton.isHidden = false
            self.clearButton.isHidden = false
        }
        return self.cartData.cart?.cart_items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableCell") as! CategoryTableCell
        let val = self.cartData.cart?.cart_items?[indexPath.row]
        if((val?.Product?.default_image ?? "").contains("http")){
            cell.shopImage.sd_setImage(with: URL(string: (val?.Product?.default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }else {
            cell.shopImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (val?.Product?.default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }
        
        
        let variation = val?.VariationValues ?? []
        cell.orderDate.text = ""
        for i in variation{
            if((i.attribute_text == "Color") || (i.attribute_text == "Ram") || (i.attribute_text == "Internal Storage") || (i.attribute_text == "Size")){
                let addAttribute = "\(i.attribute_text ?? "")" + " | " + (i.value_text ?? "")
                
                cell.orderDate.text = (((cell.orderDate.text ?? "") == "") ? ((cell.orderDate.text ?? "")):(cell.orderDate.text ?? "") + ", ") + addAttribute
            }
        }
        
        cell.shopName.text = val?.Product?.name
        let discountPrice = convertPriceInDouble(price: val?.discounted_price)
        //        let discountPrice = val?.Product?.discount ?? 0
        let cartPrice = convertPriceInDouble(price: val?.price)
        let tip: Double = Double(val?.Product?.avg_rating ?? "0.00")!
        cell.ratingText.text = String(format: "%.1f",tip)
        
        if (val?.Product?.offer_qty ?? 0) > 0 && (val?.Product?.offer_valid_till ?? 0) > Int(NSDate().timeIntervalSince1970) {
            
            cell.shopPrice.attributedText = self.handlePrice(price: "\(cartPrice)" , cut: "\(val?.Product?.offer_price ?? 0.00)")
            
        } else if (discountPrice != 0.00) && ((discountPrice) != (cartPrice)){  //0.00
            
            cell.shopPrice.attributedText = self.handlePrice(price: "\(cartPrice)" , cut: "\(discountPrice)")
        } else {
            cell.shopPrice.text = "\(Singleton.shared.selectedCurrencySymbol) " + "\(cartPrice.addComma)"
        }
        cell.cartItemCount.text = "\(val?.qty ?? 0)"
        
        cell.heartButton={
            cell.cartItemCount.text = "\((val?.qty ?? 0) + 1)"
            ActivityIndicator.show(view: self.view)
            if(Singleton.shared.userDetail.id != nil){
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_UPDATE_CART + "?lang\(Singleton.shared.language ?? "en")", method: .post, parameter: ["id": val?.id ?? 0, "qty": cell.cartItemCount.text ?? ""], objectClass: SuccessResponse.self, requestCode: U_UPDATE_CART, userToken: nil) { (response) in
                    Singleton.shared.cartData = GetCartResponse()
                    ActivityIndicator.hide()
                    self.getCartData()
                }
            } else {
                var cartId = ""
                if(UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) != nil){
                    cartId = UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) as! String
                }
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_UPDATE_GUEST_CART + "\(cartId)", method: .post, parameter: ["id": val?.id ?? 0, "qty": cell.cartItemCount.text ?? "","currency":Singleton.shared.selectedCurrency], objectClass: SuccessResponse.self, requestCode: U_UPDATE_GUEST_CART, userToken: nil) { (response) in
                    Singleton.shared.cartData = GetCartResponse()
                    ActivityIndicator.hide()
                    self.getGuestCartData()
                }
            }
            
        }
        
        cell.bagButton={
            if(Int(cell.cartItemCount.text ?? "0")! > 0){
                cell.cartItemCount.text = "\((val?.qty ?? 0) - 1)"
                var url = String()
                if(Singleton.shared.userDetail.id != nil){
                    if(cell.cartItemCount.text == "0"){
                        url = U_BASE + U_DELETE_CART_ITEM
                    }else {
                        url = U_BASE + U_UPDATE_CART
                    }
                } else {
                    if(cell.cartItemCount.text == "0"){
                        url = U_BASE + U_DELETE_GUEST_CART_ITEM
                    }else {
                        url = U_BASE + U_UPDATE_GUEST_CART
                    }
                }
                
                ActivityIndicator.show(view: self.view)
                if(Singleton.shared.userDetail.id != nil){
                    SessionManager.shared.methodForApiCalling(url: url + "?lang\(Singleton.shared.language ?? "en")", method: .post, parameter: ["id": val?.id ?? 0, "qty": ((cell.cartItemCount.text  ?? "") == "0") ? "1":(cell.cartItemCount.text  ?? "")], objectClass: SuccessResponse.self, requestCode: url, userToken: nil) { (response) in
                        Singleton.shared.cartData = GetCartResponse()
                        
                        self.shouldReload = true
                        
                        self.getCartData()
                        ActivityIndicator.hide()
                    }
                } else {
                    var cartId = ""
                    if(UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) != nil){
                        cartId = UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) as! String
                    }
                    SessionManager.shared.methodForApiCalling(url: url + "\(cartId)", method: .post, parameter: ["id": val?.id ?? 0, "qty": ((cell.cartItemCount.text  ?? "") == "0") ? "1":(cell.cartItemCount.text  ?? ""),"currency":Singleton.shared.selectedCurrency], objectClass: SuccessResponse.self, requestCode: url, userToken: nil) { (response) in
                        Singleton.shared.cartData = GetCartResponse()
                        
                        self.shouldReload = true
                        
                        self.getGuestCartData()
                        ActivityIndicator.hide()
                    }
                }
                
            }
        }
        return cell
    }
}
