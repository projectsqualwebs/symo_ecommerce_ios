//
//  AddressViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 01/05/21
//

import UIKit

class AddressViewController: UIViewController, AddAddress, Confirmation {
    
    
    //delegate method for new address is added and reload table
    func addNewAddress() {
        Singleton.shared.savedAddress = []
        NavigationController.shared.getSavedAddress { (response) in
            self.addressData = response
            self.addressTable.reloadData()
        }
    }
    
    //delegate method for alert modal
    func confirmationSelection(action: Int) {
        if(action == 1){
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_ADDRESS + "\(deleteId ?? 0)", method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_DELETE_ADDRESS, userToken: nil) { (response) in
                Singleton.shared.showToast(msg: "Address deleted successfully".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 1)
                self.addNewAddress()
            }
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var addressTable: UITableView!
    @IBOutlet weak var shippingAddressLabel: DesignableUILabel!
    @IBOutlet weak var addNewAddressButton: CustomButton!
    @IBOutlet weak var noDataLabel: UILabel!
    
    
    var addressData = [Address]()
    var isRedirectionFromSideBar = false
    private var deleteId = 0
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.shippingAddressLabel.text = "Shipping Address".localizableString(loc: Singleton.shared.language ?? "en")
        self.addNewAddressButton.setTitle("Add New Address".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.view.backgroundColor  = .white
        NavigationController.shared.getSavedAddress { (response) in
            self.addressData = response
            self.addressTable.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
    }
    
    func showDeletePopup(id:Int) {
        self.deleteId = id
        
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationPopupViewController") as! ConfirmationPopupViewController
        myVC.confirmationDelegate = self
        myVC.confirmationText = "Are you sure?".localizableString(loc: Singleton.shared.language ?? "en")
        myVC.detailText = "Do you want to delete this address?".localizableString(loc: Singleton.shared.language ?? "en")
        myVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(myVC, animated: true, completion: nil)
        
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        if isRedirectionFromSideBar == true {
            self.menu()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addAddressAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
        myVC.addressDelegate = self
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
}

//MARK: Table View Delegate
extension AddressViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.noDataLabel.isHidden = self.addressData.count > 0 ? true : false
        return self.addressData.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressTableCell", for: indexPath) as! AddressTableCell
        cell.addressType.text = "Home".localizableString(loc: Singleton.shared.language ?? "en")
        cell.editLocalizeButton.setTitle("Edit".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        cell.primaryAddressLabel.text = "Primary Address".localizableString(loc: Singleton.shared.language ?? "en")
        let val = self.addressData[indexPath.row]
        cell.addressType.text =  (val.firstname ?? "") + " " + (val.lastname ?? "")
        cell.address.text = val.address
        
        let selectedCountryCode = Singleton.shared.countryData.first { $0.id ==  val.country_id ?? 0 }?.country_code ?? ""
        cell.contactNumber.text =   formatPhoneNumber(number: val.phone ?? "" , countryCode: selectedCountryCode)
        
        if(val.is_default_address == 1){
            cell.addressSelectionimage.image = #imageLiteral(resourceName: "Ellipse 3 copy")
        }else {
            cell.addressSelectionimage.image = #imageLiteral(resourceName: "Ellipse 3 copy 2")
        }
        
        cell.editButton={
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
            myVC.addressDelegate = self
            myVC.isEditAddress = true
            myVC.addressData = val
            self.navigationController?.pushViewController(myVC, animated: true)
        }
        
        cell.deleteButton={
            self.showDeletePopup(id: val.id ?? 0)
        }
        
        cell.defaultAddressbutton={
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_MARK_DEFAULT_ADDRESS + "\(val.id ?? 0)", method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_MARK_DEFAULT_ADDRESS, userToken: nil) { (response) in
                Singleton.shared.showToast(msg: "Default address changed Successfully".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 1)
                self.addNewAddress()
            }
        }
        
        return cell
    }
}


//MARK: Table View Cell
class AddressTableCell: UITableViewCell{
    //MARK:IBOutlets
    @IBOutlet weak var street: DesignableUILabel!
    @IBOutlet weak var address: DesignableUILabel!
    @IBOutlet weak var contactNumber: DesignableUILabel!
    @IBOutlet weak var addressType: DesignableUILabel!
    @IBOutlet weak var addressSelectionimage: UIImageView!
    @IBOutlet weak var editLocalizeButton: UIButton!
    @IBOutlet weak var primaryAddressLabel: DesignableUILabel!
    @IBOutlet weak var storeImage: ImageView!
    
    var defaultAddressbutton: (()-> Void)? = nil
    var editButton: (()-> Void)? = nil
    var deleteButton: (()-> Void)? = nil
    
    //MARK: IBActions
    @IBAction func addressAction(_ sender: Any) {
        if let defaultAddressbutton = self.defaultAddressbutton{
            defaultAddressbutton()
        }
    }
    
    @IBAction func editAction(_ sender: Any) {
        if let editButton = self.editButton{
            editButton()
        }
    }
    
    @IBAction func deleteAction(_ sender: Any) {
        if let deleteButton = self.deleteButton{
            deleteButton()
        }
    }
    
    
}
