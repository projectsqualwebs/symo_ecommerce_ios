//
//  SubCategoryViewController.swift
//  Symo New
//
//  Created by Apple on 17/04/21.
//

import UIKit

class SubCategoryViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var pinkView: UIView!
    @IBOutlet weak var noDataLabel: DesignableUILabel!
    @IBOutlet weak var subCategoryCollection: UICollectionView!
    @IBOutlet weak var categoryLabel: CustomButton!
    @IBOutlet weak var nearByLabel: DesignableUILabel!
    @IBOutlet weak var searchField: UITextField!
    
    var categoryId = Set<Int>()
    var subCategoryData = [CategoryResponse]()
    var showCategory = false
    var categoryData = [CategoryResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.categoryLabel.setTitle("Categories".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.noDataLabel.text = "No vendors found.".localizableString(loc: Singleton.shared.language ?? "en")
        self.searchField.placeholder = "Search by name".localizableString(loc: Singleton.shared.language ?? "en")
        
        self.view.backgroundColor = .white
        self.pinkView.backgroundColor = .white
        if(self.showCategory){
            if let layout = subCategoryCollection.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .vertical
            }
            self.subCategoryCollection.reloadData()
        }else{
            self.getSubcategoryData()
        }
        handleTextAlignmentForArabic(view: self.view)
    }
    
    func getSubcategoryData(){
        ActivityIndicator.show(view: self.view)
        let url = U_BASE + "user/\(self.categoryId)/sub_category" + pageLimitWithLang + (Singleton.shared.language ?? "en")
        SessionManager.shared.methodForApiCalling(url: url, method: .get, parameter: nil, objectClass: GetCategory.self, requestCode: url, userToken: nil) { (response) in
            ActivityIndicator.hide()
            self.subCategoryData = response.response
            self.subCategoryCollection.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK: Collection view
extension SubCategoryViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(self.showCategory){
            self.noDataLabel.isHidden = true
            return self.categoryData.count
        }else {
            if(self.subCategoryData.count == 0){
                self.noDataLabel.isHidden = false
            }else {
                self.noDataLabel.isHidden = true
            }
            return self.subCategoryData.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(self.showCategory){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionCell2", for: indexPath) as! CategoryCollectionCell
            let val = self.categoryData[indexPath.row]
            if((val.category_avatar ?? "").contains("http")){
                cell.shopImage.sd_setImage(with: URL(string: (val.category_avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }else {
                cell.shopImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (val.category_avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }
            cell.shopName.text = val.category_name
            cell.shopRating.text = "Product ".localizableString(loc: Singleton.shared.language ?? "en") + "\(val.subcategory_count ?? 0)"
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionCell", for: indexPath) as! CategoryCollectionCell
            let val = self.subCategoryData[indexPath.row]
            if((val.sub_category_avatar ?? "").contains("http")){
                cell.shopImage.sd_setImage(with: URL(string: (val.sub_category_avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }else {
                cell.shopImage.sd_setImage(with: URL(string:U_SUBCATEGORY_IMAGE_BASE + (val.sub_category_avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }
            cell.shopName.text = val.sub_category_name
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(self.showCategory){
            return CGSize(width: collectionView.frame.size.width, height:80)
        }else {
            return CGSize(width: (collectionView.frame.size.width/2)-15, height:150)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(self.showCategory){
            self.showCategory = false
            self.categoryId.insert(self.categoryData[indexPath.row].category_id ?? 0)
            self.getSubcategoryData()
        }else {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchVendorViewController") as! SearchVendorViewController
            myVC.showBackOption = true
            myVC.categoryId = self.categoryId
            myVC.subcategoryId.insert(self.subCategoryData[indexPath.row].sub_category_id ?? 0)
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
}

