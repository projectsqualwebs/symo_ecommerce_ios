//
//  ProfileViewController.swift
//  Symo New
//
//  Created by Apple on 01/04/21
//

import UIKit

class ProfileViewController: UIViewController, SelectDate {
    //date picker method
    func selectedDate(timestamp: Date) {
        dobTimestamp = Int(timestamp.timeIntervalSince1970)
        self.dob.text = self.convertTimestampToDate(Int(timestamp.timeIntervalSince1970), to: "dd-MM-yyyy")
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var pinkView: UIView!
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var fullName: DesignableUITextField!
    @IBOutlet weak var femaleButton: CustomButton!
    @IBOutlet weak var maleButton: CustomButton!
    @IBOutlet weak var dob: DesignableUITextField!
    @IBOutlet weak var contactNumber: DesignableUITextField!
    @IBOutlet weak var emailAddress: DesignableUITextField!
    
    @IBOutlet weak var updateInfoButton: CustomButton!
    @IBOutlet weak var emailIdLabel: DesignableUILabel!
    @IBOutlet weak var contactNumberLabel: DesignableUILabel!
    @IBOutlet weak var dateOfBirthLabel: DesignableUILabel!
    @IBOutlet weak var genderLabel: DesignableUILabel!
    @IBOutlet weak var fulleNameLabel: DesignableUILabel!
    @IBOutlet weak var profileLabel: DesignableUILabel!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var popupId: UILabel!
    @IBOutlet weak var popupNUmber: UILabel!
    @IBOutlet weak var popupEmail2: UILabel!
    @IBOutlet weak var namePlaceholder: UILabel!
    @IBOutlet weak var emailPlaceholder: UILabel!
    @IBOutlet weak var phoneNumberPlaceholder: UILabel!
    @IBOutlet weak var membershipId: UILabel!
    @IBOutlet weak var membershipCardButton: CustomButton!
    @IBOutlet weak var popupName: DesignableUILabel!
    @IBOutlet weak var popupEmail: DesignableUILabel!
    @IBOutlet weak var popupNumber: DesignableUILabel!
    @IBOutlet weak var popupMemberId: DesignableUILabel!
    @IBOutlet weak var symoNumber: DesignableUILabel!
    @IBOutlet weak var countryCodeLabel: DesignableUILabel!
    
    var gender = Int()
    var dobTimestamp = Int()
    var imagePath  = ""
    var imageName = ""
    let picker = UIImagePickerController()
    var isRedirectionFromSideBar = false
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        localizeView()
        self.countryCodeLabel.text = "+\(Singleton.shared.selectedCountry.country_code ?? "")"
        self.view.backgroundColor = .white
        self.pinkView.backgroundColor = K_PINK_COLOR
        self.emailAddress.isUserInteractionEnabled = false
        self.picker.delegate = self
        self.intializeView()
        handleTextAlignmentForArabic(view: self.view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
    }
    
    
    //MARK: Functions
    func localizeView(){
        self.membershipCardButton.setTitle("Membership card".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.profileLabel.text = "Profile".localizableString(loc: Singleton.shared.language ?? "en")
        self.fulleNameLabel.text = "Full Name".localizableString(loc: Singleton.shared.language ?? "en")
        self.fullName.placeholder = "Full Name".localizableString(loc: Singleton.shared.language ?? "en")
        self.genderLabel.text = "Gender".localizableString(loc: Singleton.shared.language ?? "en")
        self.maleButton.setTitle("Male".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.femaleButton.setTitle("Female".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.dateOfBirthLabel.text = "Date of Birth".localizableString(loc: Singleton.shared.language ?? "en")
        self.contactNumberLabel.text = "Contact No.".localizableString(loc: Singleton.shared.language ?? "en")
        self.contactNumber.placeholder = "Enter phone number".localizableString(loc: Singleton.shared.language ?? "en")
        self.emailIdLabel.text = "Email Id".localizableString(loc: Singleton.shared.language ?? "en")
        self.emailAddress.placeholder = "Enter email".localizableString(loc: Singleton.shared.language ?? "en")
        self.updateInfoButton.setTitle("Update Informations".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        
        self.popupName.text = "Name:".localizableString(loc: Singleton.shared.language ?? "en")
        self.popupEmail.text = "Email:".localizableString(loc: Singleton.shared.language ?? "en")
        self.popupNumber.text = "Phone number:".localizableString(loc: Singleton.shared.language ?? "en")
        self.popupMemberId.text = "Membership Id:".localizableString(loc: Singleton.shared.language ?? "en")
    }
    
    
    func intializeView(){
        if(Singleton.shared.userDetail.id != nil){
            self.fullName.text = Singleton.shared.userDetail.name
            self.emailAddress.text = Singleton.shared.userDetail.email
            self.contactNumber.text =  (Singleton.shared.userDetail.phone ?? "") //"+" + (Singleton.shared.selectedCountry.country_code ?? "") +
            self.membershipId.text = Singleton.shared.userDetail.membership_id
            self.popupNUmber.text = "+\(Singleton.shared.selectedCountry.country_code ?? "")  \(Singleton.shared.userDetail.phone ?? "")"
            self.emailPlaceholder.text = Singleton.shared.userDetail.email
            self.namePlaceholder.text = Singleton.shared.userDetail.name
            if((Singleton.shared.userDetail.avatar ?? "").contains("http")){
                self.userImage.sd_setImage(with: URL(string: Singleton.shared.userDetail.avatar ?? ""), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }else {
                self.userImage.sd_setImage(with: URL(string: U_IMAGE_STORE_BASE + (Singleton.shared.userDetail.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }
            
            self.symoNumber.text = "+971554595173"
        }
    }
    
    
    //MARK: IBActions
    @IBAction func menuAction(_ sender: Any) {
        if self.isRedirectionFromSideBar == true {
            self.menu()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func uploadImageAction(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image".localizableString(loc: Singleton.shared.language ?? "en"), message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera".localizableString(loc: Singleton.shared.language ?? "en"), style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery".localizableString(loc: Singleton.shared.language ?? "en"), style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel".localizableString(loc: Singleton.shared.language ?? "en"), style: .cancel, handler: nil))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender as! UIView
            popoverController.sourceRect = (sender as AnyObject).bounds
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func updateInfoAction(_ sender: Any) {
        if(self.fullName.text!.isEmpty){
            Singleton.shared.showToast(msg: "Enter your Name".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if(self.contactNumber.text!.isEmpty){
            Singleton.shared.showToast(msg: "Enter Contact Number".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if(self.emailAddress.text!.isEmpty){
            Singleton.shared.showToast(msg: "Enter Email Address".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if !(self.isValidEmail(emailStr: self.emailAddress.text ?? "")){
            Singleton.shared.showToast(msg: "Enter valid Email Address".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else {
            ActivityIndicator.show(view: self.view)
            let param:[String:Any]=[
                "access_token": Singleton.shared.userDetail.access_token ?? 0,
                "name": self.fullName.text,
                "city": Singleton.shared.userDetail.city_id,
                "country": Singleton.shared.userDetail.country_id ?? 0,
                "current_password": "",
                "new_password": "",
                "confirm_password": "",
                "email": self.emailAddress.text ,
                "phone":self.contactNumber.text
            ]
            
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_UPDATE_PROFILE, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_UPDATE_PROFILE, userToken: nil) { (response) in
                Singleton.shared.userDetail.name = self.fullName.text ?? ""
                Singleton.shared.saveUserDetail(user: Singleton.shared.userDetail)
                Singleton.shared.showToast(msg: "Profile updated successfully".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 1)
                ActivityIndicator.hide()
            }
        }
    }
    
    
    @IBAction func genderAction(_ sender: UIButton) {
        if(sender.tag == 1){
            self.maleButton.setTitleColor(K_PINK_COLOR, for: .normal)
            self.femaleButton.setTitleColor(.lightGray, for: .normal)
            self.gender = 1
        }else if(sender.tag == 2){
            self.femaleButton.setTitleColor(K_PINK_COLOR, for: .normal)
            self.maleButton.setTitleColor(.lightGray, for: .normal)
            self.gender = 2
        }
    }
    
    @IBAction func dobAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        myVC.modalPresentationStyle = .overFullScreen
        myVC.maxDate = Date()
        myVC.dateDelegate = self
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func dismissBtPressed(_ sender: Any) {
        self.popupView.isHidden = true
    }
    
    @IBAction func membershipAction(_ sender: Any) {
        self.popupView.isHidden = false
    }
}

//MARK: Image Picker
extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else { return }
        
        if let selectedImageName = ((info[UIImagePickerController.InfoKey.referenceURL] as? NSURL)?.lastPathComponent) {
            
            self.imageName = selectedImageName
        }else {
            self.imageName = "image.jpg"
        }
        let imageData:NSData = selectedImage.pngData()! as NSData
        
        //save in user default
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        if let cropImage = selectedImage as? UIImage {
            let data = cropImage.jpegData(compressionQuality: 0.2) as Data?
            let params = [
                "avatar": data!,
                
            ] as [String : Any]
            // HTTP Request for upload Picture
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.makeMultipartRequest(url: U_BASE + U_UPLAOD_IMAGE, fileData: data, param: params, fileName: imageName) { (response) in
                
                self.userImage.image = cropImage
                self.imagePath = (response as! UploadImage).response?.avatar ?? ""
                Singleton.shared.userDetail.avatar = self.imagePath
                Singleton.shared.saveUserDetail(user: Singleton.shared.userDetail)
                ActivityIndicator.hide()
            }
        }
        picker.dismiss(animated: true)
    }
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
        else{
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationPopupViewController") as! ConfirmationPopupViewController
            myVC.confirmationText = "Warning".localizableString(loc: Singleton.shared.language ?? "en")
            myVC.detailText = "You don't have camera".localizableString(loc: Singleton.shared.language ?? "en")
            myVC.secondTxt = false
            myVC.modalPresentationStyle = .overFullScreen
            self.present(myVC, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
    }
    
    func changeColor(string:String,colorString: String,color: UIColor,field:UILabel){
        let main_string = string
        var string_to_color = colorString
        var range = (main_string as NSString).range(of: string_to_color)
        
        let attribute = NSMutableAttributedString.init(string: main_string)
        if let font = UIFont(name: "Lato-SemiBold", size: 20) {
            let fontAttributes = [NSAttributedString.Key.font: font]
            let size = (string_to_color as NSString).size(withAttributes: fontAttributes)
        }
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: color , range: range)
        field.attributedText = attribute
    }
    
}
