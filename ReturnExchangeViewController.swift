//
//  ReturnExchangeViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 19/10/21.
//

import UIKit

class ReturnExchangeViewController: UIViewController, UITextViewDelegate {
    
    // for textview delegate methods
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(self.textView.text == "Write you review here".localizableString(loc: Singleton.shared.language ?? "en")){
            self.textView.text = ""
            self.textView.textColor = .black
        }else {
            self.textView.textColor = .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(self.textView.text == ""){
            self.textView.text = "Write you review here".localizableString(loc: Singleton.shared.language ?? "en")
            self.textView.textColor = .lightGray
        }else {
            self.textView.textColor = .black
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var imageOne: UIImageView!
    @IBOutlet weak var imageTwo: UIImageView!
    @IBOutlet weak var imageThree: UIImageView!
    @IBOutlet weak var imageFour: UIImageView!
    @IBOutlet weak var imageFive: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var returnButton: CustomButton!
    @IBOutlet weak var requestReturnHeadingLabel: DesignableUILabel!
    @IBOutlet weak var reasonForReturnLabel: DesignableUILabel!
    @IBOutlet weak var damagedProductLabel: DesignableUILabel!
    @IBOutlet weak var noLongerRequiredLabel: DesignableUILabel!
    @IBOutlet weak var placedWrongOrderLabel: DesignableUILabel!
    @IBOutlet weak var gotBetterDeal: DesignableUILabel!
    @IBOutlet weak var reasonNotListedLabel: DesignableUILabel!
    @IBOutlet weak var explainYourReasonLabel: DesignableUILabel!
    @IBOutlet weak var uploadPhotosLabel: DesignableUILabel!
    @IBOutlet weak var addPhotosLabel: UILabel!
    @IBOutlet weak var cameraIcon: UIImageView!
    
    
    var orderData = CartItems()
    var imagePath  = ""
    var imageName = ""
    let picker = UIImagePickerController()
    var selectedReason = 0
    var currentAction = 1
    static var returnDelegate : returnExchangeStatus? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
        self.requestReturnHeadingLabel.text = "Request return".localizableString(loc: Singleton.shared.language ?? "en")
        self.reasonForReturnLabel.text = "Reason for return".localizableString(loc: Singleton.shared.language ?? "en")
        self.damagedProductLabel.text = "Damaged product".localizableString(loc: Singleton.shared.language ?? "en")
        self.noLongerRequiredLabel.text = "No longer required".localizableString(loc: Singleton.shared.language ?? "en")
        self.placedWrongOrderLabel.text = "Placed wrong order".localizableString(loc: Singleton.shared.language ?? "en")
        self.gotBetterDeal.text = "Got better deal".localizableString(loc: Singleton.shared.language ?? "en")
        self.reasonNotListedLabel.text = "Reason not listed".localizableString(loc: Singleton.shared.language ?? "en")
        self.explainYourReasonLabel.text = "Explain your reason".localizableString(loc: Singleton.shared.language ?? "en")
        self.uploadPhotosLabel.text = "Upload photos".localizableString(loc: Singleton.shared.language ?? "en")
        self.addPhotosLabel.text = "add photos".localizableString(loc: Singleton.shared.language ?? "en")
        self.textView.delegate = self
        self.textView.text = "Write you review here".localizableString(loc: Singleton.shared.language ?? "en")
        self.textView.textColor = .lightGray
        self.picker.delegate = self
        self.returnButton.setTitle(currentAction == 1 ? "Return".localizableString(loc: Singleton.shared.language ?? "en"):"Exchange".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        
        self.cameraIcon.isHidden = false
        self.addPhotosLabel.isHidden = false
        self.itemImage.isHidden = true
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func reasonAction(_ sender: UIButton) {
        selectedReason = sender.tag
        if(sender.tag == 1){
            self.imageOne.image = UIImage(named: "Ellipse 3 copy")
            self.imageTwo.image = UIImage(named: "Ellipse 3 copy 2")
            self.imageThree.image = UIImage(named: "Ellipse 3 copy 2")
            self.imageFour.image = UIImage(named: "Ellipse 3 copy 2")
            self.imageFive.image = UIImage(named: "Ellipse 3 copy 2")
        }else if(sender.tag == 2){
            self.imageTwo.image = UIImage(named: "Ellipse 3 copy")
            self.imageOne.image = UIImage(named: "Ellipse 3 copy 2")
            self.imageThree.image = UIImage(named: "Ellipse 3 copy 2")
            self.imageFour.image = UIImage(named: "Ellipse 3 copy 2")
            self.imageFive.image = UIImage(named: "Ellipse 3 copy 2")
        }else if(sender.tag == 3){
            self.imageThree.image = UIImage(named: "Ellipse 3 copy")
            self.imageTwo.image = UIImage(named: "Ellipse 3 copy 2")
            self.imageOne.image = UIImage(named: "Ellipse 3 copy 2")
            self.imageFour.image = UIImage(named: "Ellipse 3 copy 2")
            self.imageFive.image = UIImage(named: "Ellipse 3 copy 2")
        }else if(sender.tag == 4){
            self.imageFour.image = UIImage(named: "Ellipse 3 copy")
            self.imageTwo.image = UIImage(named: "Ellipse 3 copy 2")
            self.imageThree.image = UIImage(named: "Ellipse 3 copy 2")
            self.imageOne.image = UIImage(named: "Ellipse 3 copy 2")
            self.imageFive.image = UIImage(named: "Ellipse 3 copy 2")
        }else if(sender.tag == 5){
            self.imageFive.image = UIImage(named: "Ellipse 3 copy")
            self.imageTwo.image = UIImage(named: "Ellipse 3 copy 2")
            self.imageThree.image = UIImage(named: "Ellipse 3 copy 2")
            self.imageFour.image = UIImage(named: "Ellipse 3 copy 2")
            self.imageOne.image = UIImage(named: "Ellipse 3 copy 2")
        }
    }
    
    @IBAction func uploadImageAction(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image".localizableString(loc: Singleton.shared.language ?? "en"), message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera".localizableString(loc: Singleton.shared.language ?? "en"), style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery".localizableString(loc: Singleton.shared.language ?? "en"), style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel".localizableString(loc: Singleton.shared.language ?? "en"), style: .cancel, handler: nil))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender as! UIView
            popoverController.sourceRect = (sender as AnyObject).bounds
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func returnAction(_ sender: UIButton) {
        if(self.selectedReason == 0){
            Singleton.shared.showToast(msg: "Select reason".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if(self.textView.text == "Write you review here".localizableString(loc: Singleton.shared.language ?? "en")){
            Singleton.shared.showToast(msg: "Enter reason".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if(self.imagePath == ""){
            Singleton.shared.showToast(msg: "Select image".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else {
            let param:[String:Any] = [
                "lang": Singleton.shared.language,
                "reason" : self.selectedReason,
                "comment" : self.textView.text,
                "item_img" :self.imagePath,
                "return_item": self.orderData.id
            ]
            var url = String()
            // base url as per return and exchange.
            if(self.currentAction == 1){
                url = U_BASE + U_RETURN_REQUEST + "\(self.orderData.order_id ?? 0)"
            }else if(self.currentAction == 2){
                url = U_BASE + U_EXCHANGE_REQUEST + "\(self.orderData.order_id ?? 0)"
            }
            
            SessionManager.shared.methodForApiCalling(url: url, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: url, userToken: nil) { response in
                ActivityIndicator.hide()
                if self.currentAction == 1 {
                    ReturnExchangeViewController.returnDelegate?.reloadStatus(ind: 5)
                } else {
                    ReturnExchangeViewController.returnDelegate?.reloadStatus(ind: 13)
                }
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}


//MARK: Image Picker
extension ReturnExchangeViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate  {

    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else { return }
        
        if let selectedImageName = ((info[UIImagePickerController.InfoKey.referenceURL] as? NSURL)?.lastPathComponent) {
            
            self.imageName = selectedImageName
        }else {
            self.imageName = "image.jpg"
        }
        let imageData:NSData = selectedImage.pngData()! as NSData
        
        //save in user default
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        if let cropImage = selectedImage as? UIImage {
            let data = cropImage.jpegData(compressionQuality: 0.2) as Data?
            let params = [
                "avatar": data!,
                
            ] as [String : Any]
            // HTTP Request for upload Picture
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.makeMultipartRequest(url: U_BASE + U_UPLAOD_IMAGE, fileData: data, param: params, fileName: imageName) { (response) in
                
                self.itemImage.image = cropImage
                self.imagePath = (response as! UploadImage).response?.avatar ?? ""
                self.cameraIcon.isHidden = true
                self.addPhotosLabel.isHidden = true
                self.itemImage.isHidden = false
                
                ActivityIndicator.hide()
            }
        }
        picker.dismiss(animated: true)
    }
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
        else{
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationPopupViewController") as! ConfirmationPopupViewController
            myVC.confirmationText = "Warning".localizableString(loc: Singleton.shared.language ?? "en")
            myVC.detailText = "You don't have camera".localizableString(loc: Singleton.shared.language ?? "en")
            myVC.secondTxt = false
            myVC.modalPresentationStyle = .overFullScreen
            self.present(myVC, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
    }
}
