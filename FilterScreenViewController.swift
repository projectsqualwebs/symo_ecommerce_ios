//
//  FilterScreenViewController.swift
//  Symo New
//
//  Created by Apple on 07/04/21.
//

import UIKit
import SkyFloatingLabelTextField
import RangeSeekSlider

var FILTER_SUBDOMAIN = ""
var currentFilterParam = [String:Set<Int>]()
var selectedCategory = Set<Int>()
var selectedSubCategory = Set<Int>()
var selectedPriceMin = 0
var selectedPriceMax = 10000
var selectedDistance : Int? = nil
var offerSelected = 0
var selectedStar: Int?
var productType : Int? = nil
var storeType : Int? = nil
var nearMe : Int? = nil


class FilterScreenViewController: UIViewController, SelectFromPicker {
    func selectedPickerData(val: String, pos: Int) {
        if(currentPicker == 1){
            self.categoryField.text = val
            self.shopSubcategoryData = []
        }else {
            self.subcategoryField.text = val
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
            self.view.isUserInteractionEnabled = true
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var filterTable: UITableView!
    @IBOutlet weak var subcategoryField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var categoryField: SkyFloatingLabelTextFieldWithIcon!
    
    @IBOutlet weak var filterHeadingLabel: DesignableUILabel!
    @IBOutlet weak var configureLabel: DesignableUILabel!
    @IBOutlet weak var applyFilterButton: CustomButton!
    @IBOutlet weak var priceLabel: DesignableUILabel!
    @IBOutlet weak var lowToHighLabel: DesignableUILabel!
    @IBOutlet weak var highToLowLabel: DesignableUILabel!
    @IBOutlet weak var distanceLabel: DesignableUILabel!
    @IBOutlet weak var zeroFiveMilesLabel: DesignableUILabel!
    @IBOutlet weak var sixTenMilesLabel: DesignableUILabel!
    @IBOutlet weak var elevenFifteenMiles: DesignableUILabel!
    @IBOutlet weak var sixteenAboveLabel: DesignableUILabel!
    @IBOutlet weak var ratingsLabel: DesignableUILabel!
    @IBOutlet weak var fiveAboveLabel: DesignableUILabel!
    @IBOutlet weak var fourAboveLabel: DesignableUILabel!
    @IBOutlet weak var threeAboveLabel: DesignableUILabel!
    @IBOutlet weak var twoAboveLabel: DesignableUILabel!
    @IBOutlet weak var oneAboveLabel: DesignableUILabel!
    @IBOutlet weak var showOnlyOffersLabel: DesignableUILabel!
    @IBOutlet weak var lowToHighImage: UIImageView!
    @IBOutlet weak var highToLowImage: UIImageView!
    @IBOutlet weak var fiveMileImage: UIImageView!
    @IBOutlet weak var tenMileImage: UIImageView!
    @IBOutlet weak var elevenMileImage: UIImageView!
    @IBOutlet weak var sixteenMileImage: UIImageView!
    @IBOutlet weak var oneStarImage: UIImageView!
    @IBOutlet weak var twoStarImage: UIImageView!
    @IBOutlet weak var threeStarImage: UIImageView!
    @IBOutlet weak var fourStarImage: UIImageView!
    @IBOutlet weak var fiveStarImage: UIImageView!
    @IBOutlet weak var offerImage: UIImageView!
    @IBOutlet weak var priceSlider: RangeSeekSlider!
    @IBOutlet weak var searchProductImage: UIImageView!
    @IBOutlet weak var serviceProductImage: UIImageView!
    @IBOutlet weak var bothProductImage: UIImageView!
    @IBOutlet weak var searchForStackView: UIStackView!
    @IBOutlet weak var searchForLabel: DesignableUILabel!
    @IBOutlet weak var productsLabel: DesignableUILabel!
    @IBOutlet weak var servicesLabel: DesignableUILabel!
    @IBOutlet weak var bothLabel: DesignableUILabel!
    @IBOutlet weak var pricerangeLabel: DesignableUILabel!
    @IBOutlet weak var showOnlyOffersView: UIView!
    
    @IBOutlet weak var priceStackView: UIStackView!
    @IBOutlet weak var distanceStackView: UIStackView!
    @IBOutlet weak var storeTypeStackView: UIStackView!
    @IBOutlet weak var physicalStoreImage: UIImageView!
    @IBOutlet weak var onlineStoreImage: UIImageView!
    @IBOutlet weak var bothStoreImage: UIImageView!
    @IBOutlet weak var nearMeView: UIView!
    @IBOutlet weak var nearMeIcon: UIImageView!
    @IBOutlet weak var showShopsNearMeLabel: DesignableUILabel!
    @IBOutlet weak var physicalStoreLabel: DesignableUILabel!
    @IBOutlet weak var onlineStoreLabel: DesignableUILabel!
    @IBOutlet weak var bothStoreLabel: DesignableUILabel!
    
    
    var filterData = [FilterForShopResponse]()
    var shopByCategoryData = [ShopByCategoryResponse]()
    var shopSubcategoryData = [ShopByCategoryResponse]()
    var currentPicker = 1
    var selectedView:Int?
    static var handleBlurView : HandleBlurView? = nil
    static var shopProductDelegate : HandleShopProductFilter? = nil
    
    //MARK: View Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if productType == nil{
            self.nearMeView.isHidden = false
            self.showOnlyOffersView.isHidden = true
            self.searchForStackView.isHidden = true
            self.storeTypeStackView.isHidden = false
        } else {
            self.nearMeView.isHidden = true
            self.showOnlyOffersView.isHidden = false
            self.searchForStackView.isHidden = false
            self.storeTypeStackView.isHidden = true
        }
        //        || self.selectedView == 10
        if(self.selectedView == 3 || self.selectedView == 4) {
            self.priceStackView.isHidden = true
            self.distanceStackView.isHidden = false
        }else {
            self.priceStackView.isHidden = false
            self.distanceStackView.isHidden = true
        }
        
        
        priceSlider.delegate = self
        self.pricerangeLabel.text = "Price Range".localizableString(loc: Singleton.shared.language ?? "en")
        self.searchForLabel.text = "Search For".localizableString(loc: Singleton.shared.language ?? "en")
        self.productsLabel.text = "Products".localizableString(loc: Singleton.shared.language ?? "en")
        self.servicesLabel.text = "Services".localizableString(loc: Singleton.shared.language ?? "en")
        self.bothLabel.text = "Both".localizableString(loc: Singleton.shared.language ?? "en")
        
        self.filterHeadingLabel.text = "Filter".localizableString(loc: Singleton.shared.language ?? "en")
        self.configureLabel.text = "Configure your search preference".localizableString(loc: Singleton.shared.language ?? "en")
        self.applyFilterButton.setTitle("Apply filter".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.categoryField.placeholder = "Select Category".localizableString(loc: Singleton.shared.language ?? "en")
        self.subcategoryField.placeholder = "Select SubCategory".localizableString(loc: Singleton.shared.language ?? "en")
        self.priceLabel.text = "Price".localizableString(loc: Singleton.shared.language ?? "en")
        self.lowToHighLabel.text = "Low to high".localizableString(loc: Singleton.shared.language ?? "en")
        self.highToLowLabel.text = "High to low".localizableString(loc: Singleton.shared.language ?? "en")
        self.distanceLabel.text = "Distance".localizableString(loc: Singleton.shared.language ?? "en")
        self.zeroFiveMilesLabel.text = "0-5".localizableString(loc: Singleton.shared.language ?? "en") + " miles".localizableString(loc: Singleton.shared.language ?? "en")
        self.sixTenMilesLabel.text = "6-10".localizableString(loc: Singleton.shared.language ?? "en") + " miles".localizableString(loc: Singleton.shared.language ?? "en")
        self.elevenFifteenMiles.text = "11-15".localizableString(loc: Singleton.shared.language ?? "en") + " miles".localizableString(loc: Singleton.shared.language ?? "en")
        self.sixteenAboveLabel.text = "16 miles to above".localizableString(loc: Singleton.shared.language ?? "en")
        self.ratingsLabel.text = "Ratings".localizableString(loc: Singleton.shared.language ?? "en")
        self.fiveAboveLabel.text = "& above".localizableString(loc: Singleton.shared.language ?? "en")
        self.fourAboveLabel.text = "& above".localizableString(loc: Singleton.shared.language ?? "en")
        self.threeAboveLabel.text = "& above".localizableString(loc: Singleton.shared.language ?? "en")
        self.twoAboveLabel.text = "& above".localizableString(loc: Singleton.shared.language ?? "en")
        self.oneAboveLabel.text = "& above".localizableString(loc: Singleton.shared.language ?? "en")
        self.showOnlyOffersLabel.text = "Show only offers".localizableString(loc: Singleton.shared.language ?? "en")
        
        self.handleProductType(type: productType ?? 0)
        
        if let sel = selectedDistance {
            self.handleDistance(tag: sel)
        }
        
        if let star = selectedStar {
            handleStar(tag: star)
        }
        
        if let store = storeType {
            handleStoreType(type: store)
        }
        
        if(offerSelected == 0){
            offerSelected = 0
            self.offerImage.image = UIImage(named: "unselected")
            self.showOnlyOffersLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
        }else if offerSelected == 1{
            offerSelected = 1
            self.offerImage.image = UIImage(named: "selected")
            self.showOnlyOffersLabel.textColor = .black
        }
        
        if(nearMe == 0){
            nearMe = 0
            self.nearMeIcon.image = UIImage(named: "unselected")
            self.showShopsNearMeLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
        }else if nearMe == 1{
            nearMe = 1
            self.nearMeIcon.image = UIImage(named: "selected")
            self.showShopsNearMeLabel.textColor = .black
        }
        
        
        self.getVendorByCategory()
        if !Array(selectedCategory).isEmpty {
            getSubcategoryData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        FilterScreenViewController.handleBlurView?.showHideBlurView(type: 1)
        K_FILTER_REDIRECTION = 1
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        FilterScreenViewController.handleBlurView?.showHideBlurView(type: 1)
    }
    
    //MARK: Functions
    func getVendorByCategory(){
        if Singleton.shared.shopByCategoryData.isEmpty {
            let url = U_BASE + "shop-by-category?lang=\(Singleton.shared.language ?? "en")&country_id=\(Singleton.shared.selectedCountry.id ?? 0)&city_id=\(Singleton.shared.selectedCity.city_id ?? 0)"
            SessionManager.shared.methodForApiCalling(url: url, method: .get, parameter: nil, objectClass: GetShopByCategory.self, requestCode: U_SEARCH_VENDOR_NAME, userToken: nil, reloadData: false) { (response) in
                self.shopByCategoryData = []
                //            for val in response.response {
                //                self.shopByCategoryData.append(val)
                //            }
                self.shopByCategoryData = response.response
                Singleton.shared.shopByCategoryData = self.shopByCategoryData
                self.view.isUserInteractionEnabled = true
                self.filterTable.reloadData()
            }
        } else {
            self.shopByCategoryData = Singleton.shared.shopByCategoryData
        }
    }
    
    func getSubcategoryData(){
        //selectedCategory
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_SUBCATEGORIES, method: .post, parameter: ["category_id": Array(selectedCategory) ,"lang" : Singleton.shared.language], objectClass: GetSubcategory.self, requestCode: U_GET_SUBCATEGORIES, userToken: nil, reloadData: false) { (response) in
            self.shopSubcategoryData = response.response
            self.view.isUserInteractionEnabled = true
            self.filterTable.reloadData()
        }
    }
    
    func getFilterData(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_FILTER_SHOP , method: .post, parameter: ["category":Array(selectedCategory)], objectClass: GetFilterForShop.self, requestCode: U_GET_FILTER_SHOP, userToken: nil) { (response) in
            self.filterData = response.response
            self.filterTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    func handleDistance(tag: Int){
        if(tag == 5){
            self.zeroFiveMilesLabel.textColor = .black
            self.sixTenMilesLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.elevenFifteenMiles.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.sixteenAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.fiveMileImage.image = UIImage(named: "selected")
            self.tenMileImage.image = UIImage(named: "unselected")
            self.elevenMileImage.image = UIImage(named: "unselected")
            self.sixteenMileImage.image = UIImage(named: "unselected")
        }else if(tag == 10){
            self.zeroFiveMilesLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.sixTenMilesLabel.textColor = .black
            self.elevenFifteenMiles.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.sixteenAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.fiveMileImage.image = UIImage(named: "unselected")
            self.tenMileImage.image = UIImage(named: "selected")
            self.elevenMileImage.image = UIImage(named: "unselected")
            self.sixteenMileImage.image = UIImage(named: "unselected")
        }else if(tag == 15){
            self.zeroFiveMilesLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.sixTenMilesLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.elevenFifteenMiles.textColor = .black
            self.sixteenAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.fiveMileImage.image = UIImage(named: "unselected")
            self.tenMileImage.image = UIImage(named: "unselected")
            self.elevenMileImage.image = UIImage(named: "selected")
            self.sixteenMileImage.image = UIImage(named: "unselected")
        }else if(tag == 16){
            self.zeroFiveMilesLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.sixTenMilesLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.elevenFifteenMiles.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.sixteenAboveLabel.textColor = .black
            self.fiveMileImage.image = UIImage(named: "unselected")
            self.tenMileImage.image = UIImage(named: "unselected")
            self.elevenMileImage.image = UIImage(named: "unselected")
            self.sixteenMileImage.image = UIImage(named: "selected")
        }
    }
    
    func handleStar(tag: Int){
        if(tag == 0){
            self.oneAboveLabel.textColor = .black
            self.twoAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.threeAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.fourAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.fiveAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.oneStarImage.image = UIImage(named: "unselected")
            self.twoStarImage.image = UIImage(named: "unselected")
            self.threeStarImage.image = UIImage(named: "unselected")
            self.fourStarImage.image = UIImage(named: "unselected")
            self.fiveStarImage.image = UIImage(named: "selected")
        }else if(tag == 1){
            self.oneAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.twoAboveLabel.textColor = .black
            self.threeAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.fourAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.fiveAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.oneStarImage.image = UIImage(named: "unselected")
            self.twoStarImage.image = UIImage(named: "unselected")
            self.threeStarImage.image = UIImage(named: "unselected")
            self.fourStarImage.image = UIImage(named: "selected")
            self.fiveStarImage.image = UIImage(named: "unselected")
        }else if(tag == 2){
            self.oneAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.twoAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.threeAboveLabel.textColor = .black
            self.fourAboveLabel.textColor =  UIColor.init(hexString: "848484", alpha: 1)
            self.fiveAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.oneStarImage.image = UIImage(named: "unselected")
            self.twoStarImage.image = UIImage(named: "unselected")
            self.threeStarImage.image = UIImage(named: "selected")
            self.fourStarImage.image = UIImage(named: "unselected")
            self.fiveStarImage.image = UIImage(named: "unselected")
        }else if(tag == 3){
            self.oneAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.twoAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.threeAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.fourAboveLabel.textColor =  .black
            self.fiveAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.oneStarImage.image = UIImage(named: "unselected")
            self.twoStarImage.image = UIImage(named: "selected")
            self.threeStarImage.image = UIImage(named: "unselected")
            self.fourStarImage.image = UIImage(named: "unselected")
            self.fiveStarImage.image = UIImage(named: "unselected")
        }else if(tag == 4){
            self.oneAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.twoAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.threeAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.fourAboveLabel.textColor =  UIColor.init(hexString: "848484", alpha: 1)
            self.fiveAboveLabel.textColor = .black
            self.oneStarImage.image = UIImage(named: "selected")
            self.twoStarImage.image = UIImage(named: "unselected")
            self.threeStarImage.image = UIImage(named: "unselected")
            self.fourStarImage.image = UIImage(named: "unselected")
            self.fiveStarImage.image = UIImage(named: "unselected")
        }
    }
    
    
    func handleProductType(type : Int){
        if(type == 0){
            self.productsLabel.textColor = .black
            self.servicesLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.bothLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.searchProductImage.image = UIImage(named: "selected")
            self.serviceProductImage.image = UIImage(named: "unselected")
            self.bothProductImage.image = UIImage(named: "unselected")
            
        }else if(type == 1){
            self.productsLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.servicesLabel.textColor = .black
            self.bothLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.searchProductImage.image = UIImage(named: "unselected")
            self.serviceProductImage.image = UIImage(named: "selected")
            self.bothProductImage.image = UIImage(named: "unselected")
            
        }else if(type == 2){
            self.productsLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.servicesLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.bothLabel.textColor = .black
            self.searchProductImage.image = UIImage(named: "unselected")
            self.serviceProductImage.image = UIImage(named: "unselected")
            self.bothProductImage.image = UIImage(named: "selected")
        }
    }
    
    func handleStoreType(type : Int){
        if(type == 1){
            self.physicalStoreLabel.textColor = .black
            self.onlineStoreLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.bothStoreLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.physicalStoreImage.image = UIImage(named: "selected")
            self.onlineStoreImage.image = UIImage(named: "unselected")
            self.bothStoreImage.image = UIImage(named: "unselected")
            
        }else if(type == 2){
            self.physicalStoreLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.onlineStoreLabel.textColor = .black
            self.bothStoreLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.physicalStoreImage.image = UIImage(named: "unselected")
            self.onlineStoreImage.image = UIImage(named: "selected")
            self.bothStoreImage.image = UIImage(named: "unselected")
            
        }else if(type == 3){
            self.physicalStoreLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.onlineStoreLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            self.bothStoreLabel.textColor = .black
            self.physicalStoreImage.image = UIImage(named: "unselected")
            self.onlineStoreImage.image = UIImage(named: "unselected")
            self.bothStoreImage.image = UIImage(named: "selected")
        }
    }
    
    @IBAction func nearMeAction(_ sender: Any) {
        self.fiveMileImage.image = UIImage(named: "unselected")
        self.tenMileImage.image = UIImage(named: "unselected")
        self.elevenMileImage.image = UIImage(named: "unselected")
        self.sixteenMileImage.image = UIImage(named: "unselected")
        self.zeroFiveMilesLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
        self.sixTenMilesLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
        self.elevenFifteenMiles.textColor = UIColor.init(hexString: "848484", alpha: 1)
        self.sixteenAboveLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
        selectedDistance = Int()
        if(self.nearMeIcon.image == UIImage(named: "selected")){
            nearMe = 0
            self.nearMeIcon.image = UIImage(named: "unselected")
            self.showShopsNearMeLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
        }else {
            nearMe = 1
            self.nearMeIcon.image = UIImage(named: "selected")
            self.showShopsNearMeLabel.textColor = .black
        }
    }
    
    
    @IBAction func storeTypeAction(_ sender: UIButton) {
        storeType = sender.tag
        self.handleStoreType(type: sender.tag)
    }
    
    
    //MARK: IBAction
    @IBAction func dismissAction(_ sender: Any) {
        K_FILTER_REDIRECTION = 1
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func applyFilterAction(_ sender: Any) {
        selectedPriceMin = Int(priceSlider.selectedMinValue)
        selectedPriceMax = Int(priceSlider.selectedMaxValue )
        
        if(self.selectedView != 10){
            NotificationCenter.default.post(name: NSNotification.Name(N_FILTER_PRODUCT), object: nil, userInfo:currentFilterParam)
        } else {
            FilterScreenViewController.shopProductDelegate?.handleFilter(filterData: currentFilterParam)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func categoryAction(_ sender: UIButton) {
        currentPicker = sender.tag
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        if(currentPicker == 2){
            for val in self.shopSubcategoryData{
                myVC.pickerData.append(val.name ?? "")
            }
            myVC.modalPresentationStyle = .overFullScreen
            if(myVC.pickerData.count > 0){
                self.navigationController?.present(myVC, animated: true, completion: nil)
                
            }
        }else {
            for val in self.shopByCategoryData{
                myVC.pickerData.append(val.name ?? "")
            }
            myVC.modalPresentationStyle = .overFullScreen
            if(myVC.pickerData.count > 0){
                self.navigationController?.present(myVC, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func priceAction(_ sender: UIButton) {
        if(sender.tag == 1){
            self.lowToHighImage.image = UIImage(named: "selected")
            self.highToLowImage.image = UIImage(named: "unselected")
        }else {
            self.highToLowImage.image = UIImage(named: "selected")
            self.lowToHighImage.image = UIImage(named: "unselected")
        }
    }
    
    @IBAction func offerAction(_ sender: UIButton) {
        if(self.offerImage.image == UIImage(named: "selected")){
            offerSelected = 0
            self.offerImage.image = UIImage(named: "unselected")
            self.showOnlyOffersLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
        }else {
            offerSelected = 1
            self.offerImage.image = UIImage(named: "selected")
            self.showOnlyOffersLabel.textColor = .black
        }
        
    }
    
    @IBAction func distanceAction(_ sender: UIButton) {
        nearMe = nil
        self.nearMeIcon.image = UIImage(named: "unselected")
        self.showShopsNearMeLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
        selectedDistance = sender.tag
        self.handleDistance(tag: sender.tag)
    }
    
    @IBAction func startAction(_ sender: UIButton) {
        selectedStar =  sender.tag
        handleStar(tag: sender.tag)
    }
    
    @IBAction func searchForAction(_ sender: UIButton) {
        if sender.tag == 20 {
            productType = 0
        } else if sender.tag == 21 {
            productType = 1
        } else if sender.tag == 22 {
            productType = 2
        }
        self.handleProductType(type : productType ?? 0)
    }
    
    @IBAction func resetAction(_ sender: Any) {
        currentFilterParam = [String:Set<Int>]()
        selectedCategory = Set<Int>()
        selectedSubCategory = Set<Int>()
        selectedPriceMin = 0
        selectedPriceMax = 10000
        selectedDistance = Int()
        offerSelected = 0
        selectedStar = nil
        nearMe = nil
        K_SELECTED_SORTBY = 0
        FILTER_SUBDOMAIN = ""
        currentFilterParam = [String:Set<Int>]()
        storeType = nil
        if dataType == nil {
            productType = nil
        }
        
        self.filterTable.reloadData()
        NotificationCenter.default.post(name: NSNotification.Name(N_FILTER_PRODUCT), object: nil, userInfo:currentFilterParam)
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

//MARK: Table View Delegate
extension FilterScreenViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if(self.shopSubcategoryData.count > 0){
            return self.filterData.count + 2
        } else {
            return self.filterData.count + 1
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(self.shopSubcategoryData.count > 0){
            if(section == 0){
                return "Category".localizableString(loc: Singleton.shared.language ?? "en")
            }else if(section == 1) {
                return "Subcategory".localizableString(loc: Singleton.shared.language ?? "en")
            }else {
                return self.filterData[section-2].attribute.name_en ?? self.filterData[section-2].attribute.name_en ?? ""
            }
        } else {
            if(section == 0){
                return "Category".localizableString(loc: Singleton.shared.language ?? "en")
            }else {
                return self.filterData[section-1].attribute.name_en ?? self.filterData[section-1].attribute.name_en ?? ""
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.shopSubcategoryData.count > 0){
            if(section == 0){
                return self.shopByCategoryData.count
            }else if(section == 1) {
                return self.shopSubcategoryData.count
            }else {
                return self.filterData[section-2].attribute_values.count
            }
        } else {
            if(section == 0){
                return self.shopByCategoryData.count
            }else {
                return self.filterData[section-1].attribute_values.count
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableCell") as! MenuTableCell
        if(indexPath.section == 0){
            if selectedCategory.contains(self.shopByCategoryData[indexPath.row].id ?? 0){
                cell.menuImage.image = UIImage(named: "selected")
                cell.menuLabel.textColor = .black
            } else {
                cell.menuImage.image = UIImage(named: "unselected")
                cell.menuLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            }
            
            cell.menuLabel.text = self.shopByCategoryData[indexPath.row].name ?? ""
        }else if(indexPath.section == 1 && self.shopSubcategoryData.count > 0){
            if selectedSubCategory.contains(self.shopSubcategoryData[indexPath.row].id ?? 0) {
                cell.menuImage.image = UIImage(named: "selected")
                cell.menuLabel.textColor = .black
            } else {
                cell.menuImage.image = UIImage(named: "unselected")
                cell.menuLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            }
            cell.menuLabel.text = self.shopSubcategoryData[indexPath.row].name ?? ""
        } else {
            let price = "\(self.filterData[self.shopSubcategoryData.count > 0 ? indexPath.section-2 : indexPath.section-1].attribute_values[indexPath.row].min ?? 0)" + "-" +  "\(self.filterData[self.shopSubcategoryData.count > 0 ? indexPath.section-2 : indexPath.section-1].attribute_values[indexPath.row].max ?? 0)"
            if((self.filterData[self.shopSubcategoryData.count > 0 ? indexPath.section-2 : indexPath.section-1].attribute.name_en ?? "") == "Price".localizableString(loc: Singleton.shared.language ?? "en")){
                cell.menuLabel.text =  price
            }else {
                cell.menuLabel.text = self.filterData[self.shopSubcategoryData.count > 0 ? indexPath.section-2 : indexPath.section-1].attribute_values[indexPath.row].value
            }
            let key = currentFilterParam[self.filterData[self.shopSubcategoryData.count > 0 ? indexPath.section-2 : indexPath.section-1].attribute.name_en ?? ""] ?? Set<Int>()
            if(key.count != 0){
                for index in key {
                    if(self.filterData[self.shopSubcategoryData.count > 0 ? indexPath.section-2 : indexPath.section-1].attribute_values[indexPath.row].id == index){
                        cell.menuImage.image = #imageLiteral(resourceName: "selected")
                        cell.menuLabel.textColor = .black
                        print("key is", key)
                        print("Index is", key)
                        print("Index is", key)
                        print("Attribute value", self.filterData[self.shopSubcategoryData.count > 0 ? indexPath.section-2 : indexPath.section-1].attribute_values[indexPath.row])
                        print("currentId is",self.filterData[self.shopSubcategoryData.count > 0 ? indexPath.section-2 : indexPath.section-1].attribute_values[indexPath.row].id)
                    }else {
                        cell.menuImage.image = #imageLiteral(resourceName: "unselected")
                        cell.menuLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
                    }
                }
            }else {
                cell.menuImage.image = #imageLiteral(resourceName: "unselected")
                cell.menuLabel.textColor = UIColor.init(hexString: "848484", alpha: 1)
            }
            
            for val in currentFilterParam{
                
                if((val.key == (self.filterData[self.shopSubcategoryData.count > 0 ? indexPath.section-2 : indexPath.section-1].attribute.name_en ?? ""))) {
                    for index in val.value{
                        if((index == self.filterData[self.shopSubcategoryData.count > 0 ? indexPath.section-2 : indexPath.section-1].attribute_values[indexPath.row].id)){
                            cell.menuImage.image = #imageLiteral(resourceName: "selected")
                            cell.menuLabel.textColor = .black
                        }else {
                            
                        }
                    }
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            
            if K_FILTER_REDIRECTION != 1 {
                selectedCategory = []
            }
            if selectedCategory.contains(self.shopByCategoryData[indexPath.row].id ?? 0){
                selectedCategory.remove(self.shopByCategoryData[indexPath.row].id ?? 0)
            } else {
                selectedCategory.insert(self.shopByCategoryData[indexPath.row].id ?? 0)
            }
            
            currentFilterParam["category_id"] = selectedCategory
            if(self.selectedView != 4 && self.selectedView != 3){
                if selectedCategory.count > 0 {
                    self.getFilterData()
                }
            }
            if selectedCategory.count > 0 {
                self.getSubcategoryData()
            } else if selectedCategory.count == 0 {
                self.shopSubcategoryData = []
                selectedSubCategory = Set<Int>()
            }
        }else if indexPath.section == 1 && self.shopSubcategoryData.count > 0 {
            
            if K_FILTER_REDIRECTION != 1 {
                selectedSubCategory = []
            }
            
            if selectedSubCategory.contains(self.shopSubcategoryData[indexPath.row].id ?? 0){
                selectedSubCategory.remove(self.shopSubcategoryData[indexPath.row].id ?? 0)
            } else {
                selectedSubCategory.insert(self.shopSubcategoryData[indexPath.row].id ?? 0)
                currentFilterParam["sub_category_id"] = selectedSubCategory
            }
        } else {
            var a  = currentFilterParam[self.filterData[self.shopSubcategoryData.count > 0 ? indexPath.section-2 : indexPath.section-1].attribute.name_en ?? ""] ?? Set<Int>()
            if(a.contains(self.filterData[self.shopSubcategoryData.count > 0 ? indexPath.section-2 : indexPath.section-1].attribute_values[indexPath.row].id ?? 0)){
                a.remove(self.filterData[self.shopSubcategoryData.count > 0 ? indexPath.section-2 : indexPath.section-1].attribute_values[indexPath.row].id ?? 0)
            }else {
                a.insert(self.filterData[self.shopSubcategoryData.count > 0 ? indexPath.section-2 : indexPath.section-1].attribute_values[indexPath.row].id ?? 0)
            }
            currentFilterParam[self.filterData[self.shopSubcategoryData.count > 0 ? indexPath.section-2 : indexPath.section-1].attribute.name_en ?? ""] = a
        }
        
        self.filterTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont(name: "Lato-Medium", size: 17)
        header.textLabel?.textColor = .black
    }
}

extension FilterScreenViewController: RangeSeekSliderDelegate {
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        
        print("Standard slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
        print(slider.selectedMinValue)
        print(slider.selectedMaxValue)
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
    }
}
