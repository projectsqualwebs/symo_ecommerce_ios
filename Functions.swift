//
//  Functions.swift
//  Symo New
//
//  Created by Qualwebs on 29/02/24
//

import Foundation

//MARK: Convert to Double
func convertPriceInDouble(price : QuantumValue?) ->  Double {
    var currentPrice : Double = 0

    switch price {
    case .string(let text):
        currentPrice = Double(text) ?? 0.0
    case .double(let value):
        currentPrice = value
    case .int(let value):
        currentPrice = Double(value)
    default:
        break
    }
    
    return currentPrice
}




//MARK: Generate CacheKey for API's
func generateCacheKey(url: String, parameters: [String: Any]?) -> String {
    guard let params = parameters else {
        return url
    }
    // Sort the parameters dictionary based on keys
    let sortedParams = params.mapValues { value -> Any in
        if let nestedDictionary = value as? [String: Any] {
            return generateCacheKey(url: "", parameters: nestedDictionary)
        } else {
            return value
        }
    }.sorted { $0.key < $1.key }
    // Convert the sorted parameters to a string representation
    let paramString = sortedParams.map { "\($0.key)\($0.value)" }.joined(separator: "")
    let charactersToRemove: Set<Character> = ["!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "_", "+", "=", "[", "]", "{", "}", ";", ":", ",", "<", ">", ".", "/", "?", "|", "\"", "'", "~", "`", " ", "\t", "\n", "\r"]

    let finalString = String(paramString.filter { !charactersToRemove.contains($0) })
    return "\(url)?\(finalString)"
}



func simplifyPhoneNumber (number: String) -> String {
    var number = number
    
    number = number.replacingOccurrences(of: "+971", with: "")
    number = number.replacingOccurrences(of: "+973", with: "")
    number = number.replacingOccurrences(of: "(", with: "")
    number = number.replacingOccurrences(of: ")", with: "")
    number = number.replacingOccurrences(of: "-", with: "")
    number = number.replacingOccurrences(of: " ", with: "")
    
    return number
}


func formatPhoneNumber(number: String, countryCode: String) -> String {
    guard let _ = Int(number) else {
        return number
    }
    
    let firstPart = number.prefix(3)
    let middlePart = number[number.index(number.startIndex, offsetBy: 3)..<number.index(number.startIndex, offsetBy: 6)]
    let lastPart = number[number.index(number.startIndex, offsetBy: 6)...]
    
    let formattedNumber = "\(firstPart)-\(middlePart)-\(lastPart)"
    
    if countryCode == ""{
        return formattedNumber
    }
    
    let finalNumber = "+\(countryCode) \(formattedNumber)"
    return finalNumber
}
