//
//  HomeViewController.swift
//  Symo New
//
//  Created by Apple on 01/04/21
//

import UIKit
import ImageSlideshow
import SDWebImage
import Cosmos
import SkeletonView

class HomeViewController: UIViewController ,SelectedCategories{
    func reloadSearchByCollection() {
        self.getDefaultCategories()
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var imageSlider: ImageSlideshow!
    @IBOutlet weak var storeOfferSlider: ImageSlideshow!
    @IBOutlet weak var pinkView: UIView!
    @IBOutlet weak var selectedCountry: CustomButton!
    @IBOutlet weak var noOfferLabel: DesignableUILabel!
    @IBOutlet weak var bagCount: DesignableUILabel!
    @IBOutlet weak var symoOffersHeadingLabel: DesignableUILabel!
    @IBOutlet weak var trendingServicesLabel: DesignableUILabel!
    @IBOutlet weak var trendingViewAllButton: CustomButton!
    @IBOutlet weak var seasonalOfferView: UIView!
    @IBOutlet weak var symoOfferView: UIView!
    @IBOutlet weak var trendingServiceCollection: UICollectionView!
    @IBOutlet weak var searchByCollection: UICollectionView!
    @IBOutlet weak var searchByCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var CategoryCollectionHIdeShowView: UIView!
    @IBOutlet weak var searchByLabel: DesignableUILabel!
    @IBOutlet weak var nearByUnderLineLabel: CustomButton!
    @IBOutlet weak var productsLabel: DesignableUILabel!
    @IBOutlet weak var categoriesLabel: DesignableUILabel!
    @IBOutlet weak var storeLabel: DesignableUILabel!
    @IBOutlet weak var locationLabel: DesignableUILabel!
    @IBOutlet weak var categoriesHeadingLabel: DesignableUILabel!
    @IBOutlet weak var customizeUnderlineLabel: DesignableUILabel!
    @IBOutlet weak var symoOfferLabel: DesignableUILabel!
    @IBOutlet weak var symoOfferButton: CustomButton!
    @IBOutlet weak var noTrendingServiceDataLabel: UILabel!
    @IBOutlet weak var noCategoryDataLabel: UILabel!
    
    
    var shopnewTrendingData = NewTrendingResponse()
    var allCategoryData = [CategoryShowcase]()
    var homePageData = SeasonalOffersResponse()
    var shopByCategoryData = [ShopByCategoryResponse]()
    var preselectedCategory = [ShopByCategoryResponse]()
    var bannerRedirectId : Int?
    var shopRedirectId : Int?
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        SelectCategoryViewController.categoryDelegate = self
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue]
        let underlineAttributedString = NSAttributedString(string: "Near by".localizableString(loc: Singleton.shared.language ?? "en"), attributes: underlineAttribute)
        let underlineAttributedString2 = NSAttributedString(string: "Customize".localizableString(loc: Singleton.shared.language ?? "en"), attributes: underlineAttribute)
        let underlineAttributedString3 = NSAttributedString(string: "View all".localizableString(loc: Singleton.shared.language ?? "en"), attributes: underlineAttribute)
        
        self.nearByUnderLineLabel.setAttributedTitle(underlineAttributedString, for: .normal)
        self.nearByUnderLineLabel.setTitleColor(K_PINK_COLOR, for: .normal)
        self.trendingViewAllButton.setAttributedTitle(underlineAttributedString, for: .normal)
        self.trendingViewAllButton.setTitleColor(K_PINK_COLOR, for: .normal)
        
        
        self.searchByLabel.text = "Search by".localizableString(loc: Singleton.shared.language ?? "en")
        self.categoriesLabel.text = "Categories".localizableString(loc: Singleton.shared.language ?? "en")
        self.storeLabel.text = "Stores".localizableString(loc: Singleton.shared.language ?? "en")
        self.locationLabel.text = "Location".localizableString(loc: Singleton.shared.language ?? "en")
        self.categoriesHeadingLabel.text = "Categories".localizableString(loc: Singleton.shared.language ?? "en")
        self.customizeUnderlineLabel.attributedText = underlineAttributedString2
        self.customizeUnderlineLabel.textColor = K_PINK_COLOR
        self.noOfferLabel.text = "No Offers Found".localizableString(loc: Singleton.shared.language ?? "en")
        self.symoOffersHeadingLabel.text = "Symo offers".localizableString(loc: Singleton.shared.language ?? "en")
        self.trendingServicesLabel.text = "Trending Services".localizableString(loc: Singleton.shared.language ?? "en")
        
        

        self.view.backgroundColor = .white
        self.pinkView.backgroundColor = .white
        self.getFilterCategory()
        self.getVendorByCategory()
        self.handleAPICalling()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_HANDLE_CITY), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.setDefault(_:)), name: NSNotification.Name(N_HANDLE_CITY), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleBagCountUpdate), name: NSNotification.Name(N_UPDATE_BAG), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
        self.view.isUserInteractionEnabled = true
        if(K_BAG_COUNT == 0){
            self.bagCount.isHidden = true
        }else {
            self.bagCount.text = "\(K_BAG_COUNT)"
            self.bagCount.isHidden = false
        }
        dataType = nil
        self.trendingServiceCollection.reloadData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
    }
    
    //MARK: Functions
    @objc func setDefault(_ notif: Notification){
        if let callAPI = notif.userInfo?["callApi"] as? String{
            Singleton.shared.seasonalOffer = SeasonalOffersResponse()
            Singleton.shared.newTrendingBusiness = NewTrendingResponse()
            Singleton.shared.allCategoryData = []
            self.getVendorByCategory()
            self.handleAPICalling()
        }
        self.selectedCountry.setTitle(Singleton.shared.selectedCity.name ?? "", for: .normal)
    }
    
    //for handling count of product in cart
    @objc func handleBagCountUpdate(){
        if(K_BAG_COUNT == 0){
            self.bagCount.isHidden = true
        }else {
            self.bagCount.text = "\(K_BAG_COUNT)"
            self.bagCount.isHidden = false
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_UPDATE_BAG), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleBagCountUpdate), name: NSNotification.Name(N_UPDATE_BAG), object: nil)
    }
    
    @objc func didTap1() {
        if self.bannerRedirectId != 0 && self.bannerRedirectId != nil {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
            myVC.vendorId = self.bannerRedirectId ?? 0
            self.navigationController?.pushViewController(myVC, animated: true)
        } else {
            self.tabBarController?.selectedIndex = 1
        }
    }
    
    @objc func didTap2() {
        self.tabBarController?.selectedIndex = 1
    }
    
    @objc func didTap3() {
        self.tabBarController?.selectedIndex = 1
    }
    
    //for image slider
    func handleSlider(imageSlider:ImageSlideshow,input:[SDWebImageSource]){
        imageSlider.slideshowInterval = 3.0
        imageSlider.draggingEnabled = true
        imageSlider.setImageInputs(input)
        imageSlider.pageIndicatorPosition = .init(horizontal: .center, vertical: .bottom)
        imageSlider.contentScaleMode = UIViewContentMode.scaleToFill
        imageSlider.delegate = self
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTap1))
        let recognizer2 = UITapGestureRecognizer(target: self, action: #selector(self.didTap2))
        let recognizer3 = UITapGestureRecognizer(target: self, action: #selector(self.didTap3))
        if(imageSlider == imageSlider){
            imageSlider.addGestureRecognizer(recognizer)
        }else if(imageSlider == storeOfferSlider){
            storeOfferSlider.addGestureRecognizer(recognizer2)
        }
    }
    
    //fetching the selected categories
    func getDefaultCategories() {
        do {
            if let preselectedData = UserDefaults.standard.data(forKey: UD_SELECTED_CATEGORIES) {
                let preselected = try JSONDecoder().decode([ShopByCategoryResponse].self, from: preselectedData)
                if preselected.count > 0 {
                    // Assuming 'shopByCategoryData' is your source data for updates
                    // Make sure 'shopByCategoryData' is defined and contains the necessary properties
                    
                    // Update 'pre' names based on 'shopByCategoryData' using map
                    var updatedPreselected = preselected.map { preProduct in
                        // Check if there is a matching product in 'shopByCategoryData'
                        if let updatedProduct = shopByCategoryData.first(where: { $0.id == preProduct.id }) {
                            // If a matching product is found, update the 'name' property
                            var updatedPreProduct = preProduct
                            updatedPreProduct.name = updatedProduct.name
                            return updatedPreProduct
                        } else {
                            // If no matching product is found, keep 'preProduct' unchanged
                            return preProduct
                        }
                    }
                    
                    // Filter 'preselected' to get only selected categories
                    self.preselectedCategory = updatedPreselected.filter { $0.isSelected == true }
                }
            }
            
            // Reload the collection view after updating the data source
            self.searchByCollection.reloadData()
        } catch {
            print("Error decoding or updating categories:", error.localizedDescription)
        }
    }
    
    
    func getFilterCategory(){
        Singleton.shared.shopByCategoryData = []
        let url = U_BASE + "shop-by-category?lang=\(Singleton.shared.language ?? "en")"
        SessionManager.shared.methodForApiCalling(url: url, method: .get, parameter: nil, objectClass: GetShopByCategory.self, requestCode: U_SEARCH_VENDOR_NAME, userToken: nil, reloadData: false) { (response) in
            //            for val in response.response {
            //                Singleton.shared.shopByCategoryData.append(val)
            //            }
            Singleton.shared.shopByCategoryData = response.response
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func getVendorByCategory(){
        let url = U_BASE + "shop-by-category?lang=\(Singleton.shared.language ?? "en")"
        SessionManager.shared.methodForApiCalling(url: url, method: .get, parameter: nil, objectClass: GetShopByCategory.self, requestCode: U_SEARCH_VENDOR_NAME, userToken: nil) { (response) in
            self.shopByCategoryData = response.response
            if let preselected = UserDefaults.standard.value(forKey: UD_SELECTED_CATEGORIES) {
                if let pre = try? JSONDecoder().decode([ShopByCategoryResponse].self, from: preselected as! Data) as? [ShopByCategoryResponse]{
                    if pre.count == 0 {
                        self.preselectedCategory = response.response
                        for i in 0..<self.preselectedCategory.count {
                            self.preselectedCategory[i].isSelected = true
                        }
                        let categoriesSelected = try? JSONEncoder().encode(self.preselectedCategory)
                        UserDefaults.standard.set(categoriesSelected, forKey: UD_SELECTED_CATEGORIES)
                    }
                }
            }else{
                self.preselectedCategory = response.response
                for i in 0..<self.preselectedCategory.count {
                    self.preselectedCategory[i].isSelected = true
                }
                let categoriesSelected = try? JSONEncoder().encode(self.preselectedCategory)
                UserDefaults.standard.set(categoriesSelected, forKey: UD_SELECTED_CATEGORIES)
            }
            
            
            self.getDefaultCategories()
        }
    }
    
    func handleAPICalling(){
        NavigationController.shared.getAllSeasonalOffer(view: self.view) { (response) in
            self.homePageData = response
            if((response.banners.count) > 0){
                var data = response.banners
                //+ response.shops
                let input = data.map{
                    val in
                    return SDWebImageSource(urlString:val.image ?? "")!
                }
                self.handleSlider(imageSlider: self.imageSlider,input:input)
                self.noOfferLabel.isHidden = true
                self.seasonalOfferView.isHidden = false
            }else {
                self.seasonalOfferView.isHidden = true
                self.noOfferLabel.isHidden = false
            }
            
            if((response.header_offers.count) > 0){
                var data = response.header_offers
                //+ response.shops
                self.symoOfferLabel.setAttributedHtmlText(html: data[0].heading ?? "")
                self.symoOfferLabel.fontSize = 15
                self.symoOfferLabel.textColor = UIColor.white
                self.symoOfferLabel.textAlignment = .center
                self.symoOfferButton.setTitle(data[0].button_text ?? "", for: .normal)
                
                let input = data.map{
                    val in
                    return SDWebImageSource(urlString:val.image ?? "")!
                }
                self.handleSlider(imageSlider: self.storeOfferSlider,input:input)
                self.symoOfferView.isHidden = false
            }else {
                self.symoOfferView.isHidden = true
            }
            
            self.trendingServiceCollection.reloadData()
            self.allCategoryData = response.category_showcase
        }
    }
    
    //MARK: IBActions
    @IBAction func menuAction(_ sender: Any) {
        self.menu()
    }
    
    @IBAction func nearbyAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func selectCountryAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchLocationViewController") as! SearchLocationViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func searchByNameAction(_ sender: Any) {
        self.tabBarController?.selectedIndex = 1
    }
    
    @IBAction func shopNowSymoOfferAction(_ sender: Any) {
        
        if self.bannerRedirectId != 0 && self.bannerRedirectId != nil {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
            myVC.vendorId = self.shopRedirectId ?? 0
            self.navigationController?.pushViewController(myVC, animated: true)
        } else {
            self.tabBarController?.selectedIndex = 1
        }
    }
    
    
    // customize categories present list of categories
    @IBAction func customizeCategories(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectCategoryViewController") as! SelectCategoryViewController
        myVC.modalPresentationStyle = .overFullScreen
        var someCategories = [ShopByCategoryResponse]()
        do{
            if let preselected = UserDefaults.standard.value(forKey: UD_SELECTED_CATEGORIES) {
                if let pre = try! JSONDecoder().decode([ShopByCategoryResponse].self, from: preselected as! Data) as? [ShopByCategoryResponse]{
                    
                    // these is done for localization handle (when user defaults storendata in different language and current language is different.)
                    // Update 'pre' names based on 'shopByCategoryData' using map
                    var updatedPreselected = pre.map { preProduct in
                        // Check if there is a matching product in 'shopByCategoryData'
                        if let updatedProduct = shopByCategoryData.first(where: { $0.id == preProduct.id }) {
                            // If a matching product is found, update the 'name' property
                            var updatedPreProduct = preProduct
                            updatedPreProduct.name = updatedProduct.name
                            return updatedPreProduct
                        } else {
                            // If no matching product is found, keep 'preProduct' unchanged
                            return preProduct
                        }
                    }
                    someCategories = updatedPreselected
                }
            }
        }catch {
            print("error saving details")
        }
        
        
        myVC.subcategories = self.preselectedCategory.count > 0 ? someCategories : self.shopByCategoryData
        self.present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func viewAllShopAction(_ sender: UIButton) {
        if sender.tag == 1 {
            dataType = 1
        } else if sender.tag == 2 {
            productType = 1
            dataType = 2
        } else if sender.tag == 3 {
            dataType = 4
        }
        
        self.tabBarController?.selectedIndex = 1
    }
    
    
    @IBAction func bagAction(_ sender: Any) {
        self.tabBarController?.selectedIndex = 2
    }
    
    
    @IBAction func searchByAction(_ sender: UIButton) {
        if sender.tag == 10 {
            productType = 0
            dataType = 2
        } else if sender.tag == 11 {
            dataType = 1
        }  else if sender.tag == 12 {
            dataType = 3
        }  else if sender.tag == 13 {
            dataType = 4
        }
        self.tabBarController?.selectedIndex = 1
    }
}

//MARK: Image slider Delegate
extension HomeViewController: ImageSlideshowDelegate {
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        var data = self.homePageData.header_offers
        
        if imageSlideshow == self.storeOfferSlider {
            if data.count > 0 {
                self.symoOfferLabel.setAttributedHtmlText(html: data[page].heading ?? "")
                self.symoOfferLabel.fontSize = 15
                self.symoOfferLabel.textColor = UIColor.white
                self.symoOfferLabel.textAlignment = .center
                self.symoOfferButton.setTitle(data[page].button_text ?? "", for: .normal)
                self.shopRedirectId = data[page].vendor_id ?? 0
            }
        } else {
            if page < self.homePageData.banners.count {
                self.bannerRedirectId = self.homePageData.banners[page].vendor_id ?? 0
            }
        }
    }
}

//MARK: Collection view
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == trendingServiceCollection){
            self.noTrendingServiceDataLabel.isHidden = self.homePageData.trending_services.count > 0 ? true : false
            return self.homePageData.trending_services.count
        }
        else if (collectionView == searchByCollection) {
            
            if self.preselectedCategory.count > 0 {
                if(self.view.frame.width > 400){
                    let count = ceil(Double(Double(self.preselectedCategory.count) / 5.0))
                    self.searchByCollectionHeight.constant = CGFloat(count * 120)
                } else {
                    let count = ceil(Double(Double(self.preselectedCategory.count) / 4.0))
                    self.searchByCollectionHeight.constant = CGFloat(count * 120)
                }
                noCategoryDataLabel.isHidden = true
                
            } else {
                
                self.searchByCollectionHeight.constant = 150
                noCategoryDataLabel.isHidden = false
            }
            
            if self.preselectedCategory.count == 0 {
                return 0
            } else {
                return self.preselectedCategory.count
            }
        }else  {
            return self.allCategoryData.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionCell", for: indexPath) as! CategoryCollectionCell
        if(collectionView == trendingServiceCollection) {
            let val = self.homePageData.trending_services[indexPath.row]
            if((val.product?.default_image ?? "").contains("http")){
                cell.shopImage.sd_setImage(with: URL(string: (val.product?.default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }else {
                cell.shopImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (val.product?.default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }
            
            cell.shopName.text = val.name ?? ""
            let currentPrice = convertPriceInDouble(price: val.product?.price )
            //            var discountPrice = convertPriceInDouble(price: val.product?.discounted_price )
            let discountPrice = val.product?.discount
            
            if (val.product?.offer_qty ?? 0) > 0 && (val.product?.offer_valid_till ?? 0) > Int(NSDate().timeIntervalSince1970) {
                cell.shopPrice.attributedText = self.handlePrice(price: "\(currentPrice)" , cut: "\(val.product?.offer_price ?? 0.00)")
            } else if (val.product?.discount != nil) && (discountPrice != 0.00) && (discountPrice != Double(currentPrice)){
                cell.shopPrice.attributedText = self.handlePrice(price: "\(currentPrice)" , cut: "\(discountPrice ?? 0)")
            } else {
                cell.shopPrice.text = "\(Singleton.shared.selectedCurrencySymbol) \(currentPrice.addComma)"
            }
        } else if (collectionView == searchByCollection) {
            if self.preselectedCategory.count > 0 {
                if preselectedCategory[indexPath.row].isSelected == true {
                    if((preselectedCategory[indexPath.row].category_avatar ?? "").contains("http")){
                        cell.shopImage.sd_setImage(with: URL(string: (preselectedCategory[indexPath.row].category_avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
                    }else {
                        cell.shopImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (preselectedCategory[indexPath.row].category_avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
                    }
                    cell.shopName.text = preselectedCategory[indexPath.row].name
                }
            } else {
                let val = self.shopByCategoryData[indexPath.row]
                if((val.category_avatar ?? "").contains("http")){
                    cell.shopImage.sd_setImage(with: URL(string: (val.category_avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
                }else {
                    cell.shopImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (val.category_avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
                }
                cell.shopName.text = val.name ?? ""
            }
            
            return cell
        }else {
            let val = self.homePageData.top_stores[indexPath.row]
            if((val.image ?? "").contains("http")){
                cell.shopImage.sd_setImage(with: URL(string: (val.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }else {
                cell.shopImage.sd_setImage(with: URL(string:U_IMAGE_STORE_BASE + (val.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }
            cell.shopName.text = val.name
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView == trendingServiceCollection){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SingleProductViewController") as! SingleProductViewController
            myVC.selectedProduct = self.homePageData.trending_services[indexPath.row].product ?? ProductResponse()
            myVC.subdomain = self.homePageData.trending_services[indexPath.row].product?.vendorDetails?.subdomain ?? ""
            self.navigationController?.pushViewController(myVC, animated: true)
        } else if(collectionView == searchByCollection){
            self.tabBarController?.selectedIndex = 1
            if self.preselectedCategory.count == 0 {
                NotificationCenter.default.post(name: NSNotification.Name(N_REDIRECTION_FROM_HOME), object: nil, userInfo: ["selectedView":3, "categoryId": self.shopByCategoryData[indexPath.row].id ?? 0])
            } else {
                NotificationCenter.default.post(name: NSNotification.Name(N_REDIRECTION_FROM_HOME), object: nil, userInfo: ["selectedView":3, "categoryId": self.preselectedCategory[indexPath.row].id ?? 0])
            }
        }
        else {
            self.tabBarController?.selectedIndex = 1
            NotificationCenter.default.post(name: NSNotification.Name(N_REDIRECTION_FROM_HOME), object: nil, userInfo: ["selectedView":3, "categoryId": self.allCategoryData[indexPath.row].category_selection[0].category_id ?? 0])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (collectionView == searchByCollection){
            return CGSize(width: 70, height: 105)
        } else if collectionView == trendingServiceCollection {
            return CGSize(width: (self.trendingServiceCollection.frame.width/2)-10, height: 180)
        }
        return CGSize(width: 120, height: 180)
    }
    
}

//MARK: Collection Cell
class CategoryCollectionCell: UICollectionViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var shopImage: ImageView!
    @IBOutlet weak var shopName: DesignableUILabel!
    @IBOutlet weak var shopDistance: DesignableUILabel!
    @IBOutlet weak var shopPrice: DesignableUILabel!
    @IBOutlet weak var shopRating: DesignableUILabel!
    @IBOutlet weak var backView: View!
    @IBOutlet weak var favouriteIcon: UIImageView!
    @IBOutlet weak var viewButton: UIButton!
    @IBOutlet weak var onlyLeftView: UIView!
    @IBOutlet weak var onlyLeftLabel: UILabel!
    @IBOutlet weak var storeName: DesignableUILabel!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    
    var favouriteButton:(()->Void)? = nil
    var view :(()->Void)? = nil
    
    @IBAction func favouriteAction(_ sender: Any) {
        if let favouriteButton = self.favouriteButton {
            favouriteButton()
        }
    }
    
    @IBAction func viewAction(_ sender: Any) {
        if let view = self.view {
            view()
        }
    }
    
}

//MARK: Table Cell
class CategoryTableCell: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var shopImage: ImageView!
    @IBOutlet weak var shopName: DesignableUILabel!
    @IBOutlet weak var shopDistance: DesignableUILabel!
    @IBOutlet weak var shopAddress: DesignableUILabel!
    @IBOutlet weak var shopTime: DesignableUILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var totalReview: DesignableUILabel!
    @IBOutlet weak var shopPrice: DesignableUILabel!
    @IBOutlet weak var ratingText: DesignableUILabel!
    @IBOutlet weak var heartIcon: UIImageView!
    @IBOutlet weak var cartItemCount: DesignableUILabel!
    @IBOutlet weak var bagView: View!
    @IBOutlet weak var cancelView: View!
    @IBOutlet weak var freeDeliveryStack: UIStackView!
    @IBOutlet weak var scheduleDeliveryStack: UIStackView!
    @IBOutlet weak var codStack: UIStackView!
    @IBOutlet weak var oneDayDelivery: UIStackView!
    
    @IBOutlet weak var pickupStack: UIStackView!
    @IBOutlet weak var returnExchangeStack: UIStackView!
    @IBOutlet weak var returnButton: CustomButton!
    @IBOutlet weak var exchangeButton: CustomButton!
    
    @IBOutlet weak var orderId: DesignableUILabel!
    @IBOutlet weak var orderDate: DesignableUILabel!
    
    @IBOutlet weak var deliveryLocalizeLabel: DesignableUILabel!
    @IBOutlet weak var pickupLocalizeLabel: DesignableUILabel!
    @IBOutlet weak var codLocalizeLabel: DesignableUILabel!
    @IBOutlet weak var oneDayDeliveryLocalize: DesignableUILabel!
    @IBOutlet weak var scheduleDeliveryLocalize: DesignableUILabel!
    @IBOutlet weak var viewButton: CustomButton!
    @IBOutlet weak var bookMarkIcon: UIImageView!
    @IBOutlet weak var bookMarkView: View!
    @IBOutlet weak var minusIcon: UIImageView!
    @IBOutlet weak var secondViewIcon: UIImageView!
    @IBOutlet weak var thirdViewIcon: UIImageView!
    @IBOutlet weak var fourthViewIcon: UIImageView!
    @IBOutlet weak var fifthViewIcon: UIImageView!
    
    
    var heartButton:(()-> Void)? = nil
    var bagButton:(()->Void)? = nil
    var bookmarkButton:(()->Void)? = nil
    
    //MARK: IBActions
    @IBAction func heartAction(_ sender: Any) {
        if let heartButton = self.heartButton {
            heartButton()
        }
    }
    
    @IBAction func bagAction(_ sender: Any) {
        if let bagButton = self.bagButton {
            bagButton()
        }
        
    }
    
    @IBAction func bookMarkAction(_ sender: Any) {
        if let bookmarkButton = self.bookmarkButton {
            bookmarkButton()
        }
    }
    
}
