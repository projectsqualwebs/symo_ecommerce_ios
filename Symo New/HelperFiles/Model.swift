//
//  Model.swift
//  Hey Hala
//
//  Created by qw on 09/01/21
//

import Foundation

struct ErrorResponse: Codable{
    var message: String?
  	  var status: Int?
}

struct SuccessResponse: Codable{
    var message: String?
    var status: QuantumValue?
}

enum QuantumValue: Codable {
   
    func encode(to encoder: Encoder) throws {
        
    }
    

    case bool(Bool), string(String), int(Int), double(Double)

    init(from decoder: Decoder) throws {
        if let bool = try? decoder.singleValueContainer().decode(Bool.self) {
            self = .bool(bool)
            return
        }

        if let string = try? decoder.singleValueContainer().decode(String.self) {
            self = .string(string)
            return
        }
        
        if let int = try? decoder.singleValueContainer().decode(Int.self) {
            self = .int(int)
            return
        }
        
        if let double = try? decoder.singleValueContainer().decode(Double.self) {
            self = .double(double)
            return
        }
        
        throw QuantumError.missingValue
    }

    enum QuantumError:Error {
        case missingValue
    }
}

struct LoginResponse: Codable {
    var status: Int?
    var response = UserDetail()
    var error: String?
}
    
struct UserDetail: Codable{
    var id: Int?
    var email: String?
    var name: String?
    var phone: String?
    var avatar: String?
    var city: String?
    var otp: Int?
    var lat: Double?
    var long: Double?
    var is_verified: Int?
    var role_id: Int?
    var access_token: String?
    var gcm_token: String?
    var notification: String?
    var social_login: String?
    var social_id: String?
    var city_id: Int?
    var country_id: Int?
    var flag_deleted: Int?
    var logout: Int?
    var seen: Int?
    var email_verified: Int?
    var city_image: String?
    var user_type: Int?
    var manage_business: Int?
    var membership_id: String?
    var google_id: String?
    var facebook_id: String?
    var apple_id: String?
    var insta_id: String?
    var status_flag: Int?
}

struct GetCountry: Codable {
    var response = CountryResponse()
}

struct CountryResponse: Codable {
    var countries = [Countries]()
    var selected_country: Int?
    var selected_city: Int?
}

struct Countries: Codable {
    var id: Int?
    var name : String?
    var name_en: String?
    var name_ar: String?
    var country_code: String?
    var country_images: String?
    var latitude: Double?
    var longitude: Double?
    var country_id: Int?
    var name_ur: String?
    var name_es: String?
    var name_pt: String?
    var name_zh: String?
}

struct GetCities: Codable {
    var response = CityResponse()
    
}

struct CityResponse: Codable {
    var cities = [Cities]()
    var country = Countries()
    var categories = [CategoryResponse]()
    var salesperson = [SalesPerson]()
}

struct SalesPerson: Codable {
    var id: Int?
    var name: String?
    var email: String?
}

struct Cities: Codable {
    var name_en: String?
    var name: String?
    var name_ar: String?
    var city_images: String?
    var country_id: Int?
    var city_id: Int?
}

struct GetNewTrending: Codable {
    var status: Int?
    var response = NewTrendingResponse()
}

struct NewTrendingResponse: Codable {
    var trending_business = [BusinessDetail]()
    var new_business = [BusinessDetail]()
}

struct BusinessDetail: Codable{
    var id: Int?
    var vendor_id: Int?
    var user_id: Int?
    var name: String?
    var name_en: String?
    var review:String?
    var is_approved: Int?
    var report: Int?
    var report_seen: Int?
    var seen: Int?
    var last_updated_by: Int?
    var vendor_name: String?
    var phone: String?
    var premium_plan_id: Int?
    var mark_popular: Int?
    var avatar: String?
    var address: String?
    var description: String?
    var image: String?
}

struct GetCategory: Codable {
    var status: Int?
    var response = [CategoryResponse]()
}

struct CategoryResponse: Codable {
    var id: Int?
    var image: String?
    var name_en: String?
    var name: String?
    var name_ar: String?
    var category_image: String?
    var category_id: Int?
    var category_name: String?
    var category_avatar: String?
    var subcategory_count: Int?
    var sub_category_id: Int?
    var sub_category_name: String?
    var sub_category_avatar: String?
    var name_es: String?
    var name_pt: String?
    var name_zh: String?
    var name_ur: String?
}

struct GetSeasonalOffer: Codable {
  //  var status: Int?
    var response = SeasonalOffersResponse()
    
}

struct SeasonalOffersResponse: Codable {
    var shops = [SeasonalOffers]()
    var banners = [SeasonalOffers]()
    var header_offers = [SeasonalOffers]()
    var category_showcase = [CategoryShowcase]()
    var offer_products = [OfferProducts]()
    var footer_offers = [SeasonalOffers]()
    var trending_services = [CartItems]()
    var top_stores = [BusinessDetail]()
}

struct OfferProducts: Codable {
    var id: Int?
    var product_id: Int?
    var variation_id: Int?
    var price: Double?
    var valid_till: Int?
    var offer_text: String?
    var usd_price: Double?
    var pound_price: Double?
    var franc_price: Double?
    var products = ProductResponse()
}

struct CategoryShowcase: Codable {
    var id: Int?
    var heading_en: String?
    var heading_ar: String?
    var heading_es: String?
    var heading_pt: String?
    var heading_zh: String?
    var heading_ur: String?
    var heading_fr: String?
    var image: String?
    var category_id: Int?
    var category_selection = [CategorySelection]()
}

struct CategorySelection: Codable {
    var id: Int?
    var category_showcase_id: Int?
    var category_id: Int?
    var category = CategoryResponse()
}

struct SeasonalOffers: Codable {
    var heading: String?
    var button_text: String?
    var image: String?
    var vendor_id : Int?
}

struct GetShopProducts: Codable {
    var status: Int?
    var response = ShopProductResponse()
}

struct ShopProductResponse: Codable {
    var business_products = Business_products()
    var most_selling = [ProductResponse]()
    var total_count: Int?
    var categories = [CategoryResponse]()
}

struct Business_products : Codable {
    var current_page : Int?
    var data = [ProductResponse]()
    var first_page_url : String?
    var from : Int?
    var last_page : Int?
    var last_page_url : String?
    var next_page_url : String?
    var path : String?
    var per_page : Int?
    var prev_page_url : String?
    var to : Int?
    var total : Int?
}

struct GetSingleProduct: Codable {
    var status: Int?
    var response = [SingleProductResponse]()
}

struct SingleProductResponse: Codable {
    var business_product = ProductResponse()
    var similar_products = [ProductResponse]()
    var product_variant = [ProductVariant]()
    var all_reviews = [Reviews]()
    var starRatings = StarRating()
//    var stock_status : Int?
}

struct StarRating: Codable {
    var fivestar: Int?
    var fourstar: Int?
    var threestar: Int?
    var twostar: Int?
    var onestar: Int?
    var avgRating: AverageRating?
}

struct AverageRating: Codable {
    var rating: QuantumValue?
}

struct ProductResponse: Codable {
    var id: Int?
    var name_en: String?
    var product_type: Int?
    var name_ar: String?
    var name_ur: String?
    var name_es: String?
    var name_pt: String?
    var name_zh: String?
    var sku: String?
    var slug: String?
    var subdomain: String?
    var price: QuantumValue?
//    var tax: Double?
    var discount: Double?
    var business_id: Int?
    var category_id: Int?
    var subcategory_id: Int?
    var keyboard: String?
    var default_image: String?
    var description: String?
    var is_customizable: Int?
    var is_returnable: Int?
    var is_exchangable: Int?
    var returnable_within_days: Int?
    var responsibility: Int?
    var status: Int?
    var stock_status: Int?
    var is_deleted: Int?
    var is_featured: Int?
    var qty: QuantumValue?
    var is_bookmark : Int?
    var exchangable_within_days: Int?
    var parent_product_id:Int?
    var parent_product_slug: String?
    var category_image: String?
    var variation_id: Int?
    var name: String?
    var reviews: Int?
    //var rating: QuantumValue?
    var avg_rating : String?
    var product_category : CategoryResponse?
    var product_images:[ProductImage]?
    var is_favourite: Bool?
    var is_favorite : Int?
    var vendor_details: VendorDetail?
    var vendorDetails : VendorDetail?
    var offer_valid_till : Int?
    var discounted_price : QuantumValue?
    var offer_qty : Int?
    var offer_price : Double?
    var vendor : VendorDetail?
}

struct ProductImage: Codable {
    var id: Int?
    var product_id: Int?
    var image: String?
}

struct ProductVariant: Codable {
    var id: Int?
    var variation_id : Int?
    var product_id: Int?
    var business_id: Int?
    var VariationValues = [Variations]()
}

struct Variations: Codable {
    var id: Int?
    var product_id: Int?
    var attribute_id: Int?
    var value: String?
    var variation_id: Int?
    var value_text: String?
    var attribute_text: String?
}

struct GetVendorDetail: Codable {
    var status: Int?
    var response = ShopDetails()
}

struct ShopDetails: Codable {
    var shop = VendorDetail()
    var featured_products = [ProductResponse]()
    var ratings: Double?
    var ratings_count: Int?
    var bookmark: Int?
    var addresses: [Address]?
    var gallery_images:[Address]?
    var reviews : [Reviews]?
    var offerProducts : [Offers]?
    var similarProducts = [ProductResponse]()
    var availability : [StoreAvailability]?
    var disabled_notification : Int?
    var phoneCode : PhoneCode?
}

struct PhoneCode : Codable {
    var id : Int?
    var country_code : String?
    var name_en : String?
    var country_id : Int?
    var country_images : String?
}


struct StoreAvailability : Codable {
    var day : String?
    var open_time : String?
    var close_time : String?
}

struct VendorDetail: Codable {
    var id: Int?
    var user_id: Int?
    var name_en: String?
    var name: String?
    var name_ar: String?
    var latitude: String?
    var longitude: String?
    var name_slug: String?
    var vendor_id: Int?
    var subdomain: String?
    var vendor_name: String?
    var meta_title: String?
    var phone: String?
    var phone2: String?
    var phone3: String?
    var whatsapp_number: String?
    var address: String?
    var rating: QuantumValue?
    var avatar: String?
    var shop_image : String?
    var background_image: String?
    var description: String?
    var is_verified: Int?
    var is_bookmark : Int?
    var website : String?
    var city_id: Int?
    var country_id: Int?
    var city_name: String?
    var distance: Double?
    var notify: Int?
    var today : Today?
    var email : String?
    var store_type : Int?
    var visit_count : Int?
    var pickup_delivery_setting: Int?
    var schedule_delivery: Int?
    var one_day_delivery: Int?
    var cod_method: Int?
}

struct Today : Codable {
    var id : Int?
    var vendor_id : Int?
    var day : String?
    var open_time : String?
    var close_time : String?
}

struct Offers: Codable {
    var id: Int?
    var vendor_id: Int?
    var item: String?
    var item_ar: String?
    var discount: String?
    var date: Int?
    var start_date: Int?
    var end_date: Int?
    var description: String?
    var description_ar:String?
    var status: Int?
    var request_for_seasional_offers: Int?
    var request_for_update: Int?
    var avatar: String?
    var terms_en: String?
    var terms_ar: String?
    var name_en: String?
    var name: String?
    var template: String?
}

struct Reviews: Codable {
    var id: Int?
    var product_id: Int?
    var comment: String?
    var status: Int?
    var created_at:String?

    var review: String?
    var rating: QuantumValue?
    var is_approved: Int?
    var report: Int?
    var report_seen: Int?
    var seen: Int?
    var avatar: String?
    var name: String?
    var user: UserDetail?
}

struct GetAddress: Codable {
    var status: Int?
    var response = [Address]()
}

struct Address: Codable {
    var vendor_name: String?
    var subdomain: String?
    var long: Double?
    var lat: Double?
    var vendor_id: Int?
    var distance: Double?
    var image: String?
    var city_id: Int?
    var country_id: Int?
    var id: Int?
    var user_id: Int?
    var firstname: String?
    var lastname: String?
    var address: String?
    var postalcode: Int?
    var phone: String?
    var is_default_address: Int?
    var phone_code: String?
    var country_name: String?
    var city_name: String?
}

//MARK: Map
struct MapResponse: Codable {
    var status: Bool
    var response = [Address]()
   // let error: ErrorResponse?
}

struct GetFilterForShop: Codable {
    var status: Int?
    var response = [FilterForShopResponse]()
}

struct FilterForShopResponse: Codable {
    var id: Int?
    var category_id: Int?
    var attribute_values = [AttributeOption]()
    var attribute = AttributeOption()
}

struct AttributeOption: Codable {
    var id: Int?
    var name_en: String?
    var name: String?
    var value: String?
    var min: Int?
    var max: Int?
}

struct UploadImageResponse: Codable {
    var success: String?
    var avatar: String?
    var img_src: String?
    var img: String?
}

struct GetCartData: Codable {
    var status: Int?
    var response = GetCartResponse()
    var message: String?
}



struct  GetCartResponse: Codable {
    var profile : [UserDetail]?
    var cart: CartResponse?
    var cart_items:[CartItems]?
    var id: Int?
    var user_id: Int?
    var business_id: Int?
    var cart_id: String?
    var total_tax: String?
    var order_total: String?
    var discount_amount: String?
    var sub_total: String?
    var shipping_amount: String?
    var coupon_applied: String?
}

struct CurrencyTotal: Codable {
    var order_total: String?
    var total_tax: String?
    var discount_amount: String?
    var sub_total: String?
    var shipping_amount: String?
    var coupon_discount_amount: String?
    var order_subtotal : String?
    var order_tax : String?
    var order_shipping : String?
}


struct CartResponse: Codable {
    var id : Int?
    var user_id : Int?
    var business_id : Int?
    var cart_id : String?
    var total_tax : String?
    var order_total : String?
    var discount_amount : String?
    var sub_total : String?
    var shipping_amount : String?
    var coupon_applied : String?
    var coupon_id : Int?
    var coupon_code : String?
    var coupon_discount_amount : String?
    var coupon_discount_type : Int?
    var created_at : String?
    var updated_at : String?
    var delivery_method : Int?
    var serviceable_cart : Int?
    var usd_total_tax : String?
    var franc_total_tax : String?
    var pound_total_tax : String?
    var usd_order_total : String?
    var franc_order_total : String?
    var pound_order_total : String?
    var usd_discount_amount : String?
    var franc_discount_amount : String?
    var pound_discount_amount : String?
    var usd_sub_total : String?
    var franc_sub_total : String?
    var pound_sub_total : String?
    var usd_shipping_amount : String?
    var franc_shipping_amount : String?
    var pound_shipping_amount : String?
    var currency_total : CurrencyTotal?
    var cart_items : [CartItems]?
}

struct CartItems: Codable {
    var id: Int?
    var order_id: Int?
    var business_id: Int?
    var cart_list_id: Int?
    var product_id: Int?
    var qty: Int?
    var item_name: String?
    var name: String?
    var item_price: Double?
    var price: QuantumValue?
   // var price: String?
    var variation_id: Int?
    var tax: Int?
    //var discount: Double?
  //  var discount: String?
    var Product: ProductResponse?
    var product: ProductResponse?
    var is_favorite: Int?
    var VariationValues:[Variations]?
  //new
    var created_at : String?
    var updated_at : String?
    var service_date : String?
//    var usd_price : String?
//    var franc_price : String?
//    var pound_price : String?
    var discounted_price : QuantumValue?
//    var usd_discounted_price : String?
//    var franc_discounted_price : String?
//    var pound_discounted_price : String?
}

struct UploadImage: Codable {
    var response:UploadImageResponse?
}

struct GetWishlist: Codable {
    var status: Int?
    var response:[WishlistResponse]?
}

struct WishlistResponse: Codable {
    var id: Int?
    var user_id: Int?
    var product_id: Int?
    var business_id: Int?
    var variation_id : Int?
    var subdomain: String?
    var wishlist_items:ProductResponse?
}

struct GetMyOrders: Codable {
    var status: Int?
    var response = MyOrderResponse()
}

struct MyOrderResponse: Codable {
    var currentPage: Int?
    var data:[OrderResponse]?
}

struct OrderResponse: Codable {
    var id: Int?
    var user_id: Int?
    var business_id: Int?
    var cart_id: String?
    var order_subtotal:String?
    var order_total:String?
    var order_tax: String?
    var order_discount: String?
    var order_shipping: String?
    var coupon_code: String?
    var status: Int?
    var created_at: String?
    var payment_status: Int?
    var payment_method: Int?
    var user_preference: Int?
    var delivery_date: String?
    var delivery_status: Int?
    var difference: Int?
    var eligible_return_date: Int?
    var eligible_exchange_date: Int?
    var admin_commission_amount: String?
    var service_preference: Int?
    var service_date: String?
    var coupon_discount_amount :String?
    var order_notes : String?
    var currency_total : CurrencyTotal?
    var order_address: OrderAddress?
    var order_items: [CartItems]?
    var order_timeline : [Order_timeline]?
    var vendor_id : Int?
    
}

struct OrderAddress: Codable {
    var id: Int?
    var order_id: Int?
    var email: String?
    var billing_name: String?
    var billing_phone: String?
    var billing_address: String?
    var billing_city: String?
    var billing_country: Int?
    var billing_postcode: String?
    var shipping_name: String?
    var shipping_phone: String?
    var shipping_address: String?
    var shipping_city: String?
    var shipping_country: Int?
    var shipping_postcode: String?
}

struct GetCardData: Codable {
    var response = [CardData]()
    
}

struct CardData: Codable {
    
}

struct CheckBookMarkStatus: Codable {
    var status: Int?
    var response : CheckBookMark?
}

struct CheckBookMark: Codable {
    var response : BookmarkStatus?
    var count: Int?
}

struct BookmarkStatus: Codable {
    var id: Int?
    var user_id: Int?
    var vendor_id: Int?
    var bookmark: Int?
}

struct GetPickupDelivery: Codable {
    var status: Int?
    var message : String?
    var response = PickupDeliveryResponse()
    var pagination_limit : String?
}

struct PickupDeliveryResponse: Codable {
    var order_preference: Int?
    var schedule_delivery: Int?
    var expected_pickup_days: String?
    var provide_service: Int?
    var service_available_at: Int?
    var payment_methods : Payment_methods?
    var delivery_methods:[DeliveryMethod]?
}

struct Payment_methods : Codable {
    var cc_method : Int?
    var cod_method : Int?
    var bank_transfer_method : Int?
}


struct DeliveryMethod: Codable {
    var id: Int?
    var title: String?
    var status: Int?
    var delivery_charges: String?
}

struct PlaceOrder: Codable {
    var status: QuantumValue?
    var response: PlaceOrderResponse?
    var message: String?
}

struct PlaceOrderResponse: Codable {
    var redirect_url: String?
}

//MARK: Offers
struct GetOffers: Codable {
    var response = GetOfferResponse()
    var message: String?
    var status: Int?
}

struct GetOfferResponse: Codable {
    var offer_services = OffersResponse()
}

struct OffersResponse: Codable {
    var current_page: Int?
    var data = [ProductResponse]()
}


struct GetShopByCategory: Codable {
    var response = [ShopByCategoryResponse]()
    var status: Int?
}

struct GetSubcategory: Codable {
   // var response = Subcategory()
    var response = [ShopByCategoryResponse]()
}

struct Subcategory: Codable {
    var subcategories =  [ShopByCategoryResponse]()
}

struct ShopByCategoryResponse: Codable {
    var id: Int?
    var name_en: String?
    var name: String?
    var category_avatar: String?
    var category_image: String?
    var product_count: Int?
    var shop_count: Int?
    var isSelected : Bool?
}

struct GetStore: Codable{
    var response = StoreResponse()
    var message: String?
    var status: Int?
    
}

struct StoreResponse: Codable {
    var current_page: Int?
    var data = [StoreData]()
    var first_page_url: String?
    var from: Int?
    var last_page: Int?
    var last_page_url: String?
}

struct StoreData: Codable {
    var id: Int?
    var user_id: Int?
    var business_id: Int?
    var item_name: String?
    var item_price: String?
    var name_slug: String?
    var name_en: String?
    var name: String?
    var address: String?
    var open_time: Int?
    var close_time: Int?
    var phone: String?
    var avatar: String?
    var premium_plan_id: Int?
    var plan_expiry: Int?
    var shop_image: String?
    var city_name: String?
    var pickup_delivery_setting: Int?
    var schedule_delivery: Int?
    var one_day_delivery: Int?
    var cod_method: Int?
    var latitude: String?
    var longitude: String?
    var offers: Int?
    var rating: String?
    var distance: Double?
    var ratings: String?
    var ratings_count: Int?
    var today:TodayTimings?
    var premium_plans: premium_plans?
    var claim_business: Int?
    var is_bookmark : Int?
    //get-deals down
    var offer_id : Int?
    var offer_discount : String?
    var offer_start_date : Int?
    var offer_end_date : Int?
    var offer_description : String?
    var offer_terms : String?
    var offer_image : String?
}

struct TodayTimings: Codable {
    var id: Int?
    var vendor_id: Int?
    var day: String?
    var open_time: String?
    var close_time: String?
}

struct premium_plans: Codable {
    var id: Int?
    var user_id: Int?
    var is_premium: Int?
    var premium_plan_id: Int?
    var plan_type: String?
    var plan_duration_type: Int?
    var plan_upgrade_date: Int?
    var plan_expiry: Int?
    var is_banner_promotion: Int?
    var banner_count: Int?
    var is_banner: Int?
    var banner_start_date: Int?
    var banner_end_date: Int?
    var final_banner: String?
    var total_free_banner: Int?
    var notification_count: Int?
    var notification_end_date: Int?
    var offer_count: Int?
    var total_business: Int?
    var total_products: Int?
    var amount: String?
    var is_free_trail: Int?
}

struct GetProduct: Codable {
    var response = GetProductResponse()
    var message: String?
    var status: Int?
}

struct GetProductData: Codable {
    var data = [ProductResponse]()
    var current_page: Int?
    var last_page : Int?
}

struct GetProductResponse: Codable {
    var total_products: Int?
    var business_products = GetProductData()
}

struct GetLanguages: Codable {
    var country : Countries?
    var country_lang : [LanguageResponse]?
}

struct LanguageResponse: Codable {
    var id: Int?
    var country_id: Int?
    var language_id: Int?
    var code: String?
    var lang_name: String?
}

struct GetPDFresponse : Codable {
    var status : Int?
    var message : String?
    var pagination_limit : String?
    var response : Pdfresponse?
}

struct Pdfresponse : Codable {
    var status : Int?
    var pdf_url : String?
    var message : String?
}

struct GetCouponCodeResponse : Codable {
    var response : CouponCode?
    var message : String?
    var status : Int?
    var pagination_limit : String?
}

struct CouponCode : Codable {
    var profile : [Profile]?
    var cart : CartResponse?
}

struct Profile : Codable {
    var user_id : Int?
    var name : String?
    var email : String?
    var phone : String?
    var city : String?
    var city_id : Int?
    var password : String?
    var avatar : String?
}

struct SetOrderPreferenceResponse : Codable {
    var response : SetPreference?
    var message : String?
    var status : Int?
    var pagination_limit : String?
}

struct SetPreference : Codable {
    var profile : [Profile]?
    var cart : CartResponse?
}

struct GetCurrencyResponse : Codable {
    var response : GetCurrency?
    var message : String?
    var status : Int?
}

struct GetCurrency : Codable {
    var currency : Currency?
}

struct Currency : Codable {
    var aed : String?
    var usd : String?
    var pound : String?
    var euro : String?
}

struct GetNotificationResponse : Codable {
    var response : NotificationResponse?
    var message : String?
}

struct NotificationResponse : Codable {
    var current_page : Int?
    var data : [NotificationData]?
    var first_page_url : String?
    var from : Int?
    var last_page : Int?
    var last_page_url : String?
    var next_page_url : String?
    var path : String?
    var per_page : Int?
    var prev_page_url : String?
    var to : Int?
    var total : Int?
}

struct NotificationData : Codable {
    var id : Int?
    var user_id : Int?
    var order_id : Int?
    var message : String?
    var created_at : String?
    var updated_at : String?
    var message_ar : String?
    var status : Int?
}

struct GetSingleOrderResponse : Codable {
    var response : OrderResponse?
    var message : String?
}

struct Order_timeline : Codable {
    var id : Int?
    var order_id : Int?
    var type : Int?
    var status : Int?
    var created_at : String?
    var updated_at : String?
}

struct GetProductVariationResponse : Codable {
    var response : ProductVariantResponse?
    var message : String?
    var status : Int?
    var pagination_limit : String?
}

struct ProductVariantResponse : Codable {
    var stock_status : Int?
    var price : Double?
    var usd_price : String?
    var pound_price : String?
    var franc_price : String?
    var discounted_price : Double?
    var discounted_usd_price : Double?
    var discounted_pound_price : Double?
    var discounted_franc_price : Double?
    var discount : Double?
    var currency : String?
    var images : [Images]?
    var videos : [Videos]?
}

struct Images : Codable {
    var id : Int?
    var product_id : Int?
    var variation_id : Int?
    var image : String?
}

struct Videos : Codable {
    var id : Int?
    var product_id : Int?
    var video_url : String?
}

struct GetContactDetailsResponse : Codable {
    var response : ContactDetails?
    var message : String?
    var status : Int?
    var pagination_limit : String?
}

struct ContactDetails : Codable {
    var id : Int?
    var country_id : Int?
    var address : String?
    var phone : String?
    var email : String?
    var facebook : String?
    var twitter : String?
    var linkedin : String?
    var instagram : String?
    var whatsapp : String?
    var latitude : String?
    var longitude : String?
    var created_at : String?
    var updated_at : String?
}

struct GetDealsResponse : Codable {
    let response : DealsResponse?
    let message : String?
    let status : Int?
}

struct DealsResponse : Codable {
    let deals : [Deals]?
    let offers : DealOffers?
}

struct Deals : Codable {
    let id : Int?
    let country_id : Int?
    let image : String?
    let created_at : String?
    let updated_at : String?
    let vendor_id : Int?
    let vendor_details : VendorDetail?
}

struct DealOffers : Codable {
    let current_page : Int?
    let data : [StoreData]?
    let first_page_url : String?
    let from : Int?
    let last_page : Int?
    let last_page_url : String?
    let links : [Links]?
    let next_page_url : String?
    let path : String?
    let per_page : Int?
    let prev_page_url : String?
    let to : Int?
    let total : Int?
}

struct Links : Codable {
    let url : String?
    let label : String?
    let active : Bool?
}


struct OrderStatus{
    var id: Int?
    var orderStatus: String?
}


struct ScreenDetail{
    var screenHeight: CGFloat?
    var screenWidth: CGFloat?
    var topSafeArea: CGFloat?
    var bottomSafeArea: CGFloat?
    var safeHeight: CGFloat?
}
