//
//  ActivityIndicator.swift
//  Hey Hala
//
//  Created by qw on 14/01/21.
//

import UIKit

class ActivityIndicator
{
    static var overlayView = UIView()
    static var activityIndicator = UIActivityIndicatorView()
    
    static func show(view: UIView) {
        DispatchQueue.main.async {
            if let window = UIApplication.shared.keyWindow {
                self.overlayView.frame = window.bounds
                self.overlayView.backgroundColor = UIColor.black.withAlphaComponent(0.1)
                window.addSubview(self.overlayView)
                
                activityIndicator.center = window.center
                activityIndicator.hidesWhenStopped = true
                activityIndicator.startAnimating()
                activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
                activityIndicator.color = K_PINK_COLOR
                self.overlayView.addSubview(self.activityIndicator)
            }
        }
    }
    
    static func showPosition(view: UIView, center: CGPoint) {
        DispatchQueue.main.async {
            self.overlayView = view
            self.overlayView.isUserInteractionEnabled = false
            activityIndicator.center = center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.startAnimating()
            activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
            activityIndicator.color = K_PINK_COLOR
            view.addSubview(activityIndicator)
        }
    }
    
    static func hide() {
        DispatchQueue.main.async {
            overlayView.isUserInteractionEnabled = true
            self.overlayView.removeFromSuperview()
            activityIndicator.stopAnimating()
        }
    }
}
