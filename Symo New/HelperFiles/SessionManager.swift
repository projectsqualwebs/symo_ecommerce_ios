//
//  SessionManager.swift
//  Hey hala
//
//  Created by qw on 14/01/21.
//

import UIKit
import Alamofire

var K_RELOAD_DATA = false // MARK it as true when you want to refresh data, for example, using UIRefreshControl. Set it to false when done.

class SessionManager: NSObject {
    
    static var shared = SessionManager()
    
    var createWallet: Bool = true

    func methodForApiCalling<T: Codable>(url: String, method: HTTPMethod, parameter: Parameters?, objectClass: T.Type, requestCode: String, userToken: String?, reloadData : Bool = true, completionHandler: @escaping (T) -> Void) {
        
        //for store cache of api making its unique key with parameter
        let cacheKey = generateCacheKey(url: url, parameters: parameter)
        let defaultURL = "https://defaulturl.com"
        var cacheURL: URL = URL(string: defaultURL)! // default for null case

        if let url = URL(string: cacheKey) {
            cacheURL = url
            print("cacheURL \(cacheURL)")
        } else {
            print("Error: Unable to create URL from cacheKey")
        }
        
        
        
        //IF DONOT WANT TO RELOAD DATA pass reloadData = false...
        if !reloadData && cacheURL.absoluteString != defaultURL && !K_RELOAD_DATA{
            if let cachedResponse = URLCache.shared.cachedResponse(for: URLRequest(url: cacheURL)) {
                print("📀🛠️📀  Using cached response for \(cacheURL)")
                if let data = cachedResponse.data as? Data {
                    if let object = self.convertDataToObject(response: data, T.self) {
                        // Introduce a minor delay before completing
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            completionHandler(object)
                            print("cached data : \(object)")
                            ActivityIndicator.hide()
                        }
                        return
                    }
                }
            }
        }

        
        // if city selected is "All" & it's cityId is nil we have to remove city id from parameters
        var customUrl = url
        if customUrl.contains("&city_id=nil"){
            customUrl = customUrl.replacingOccurrences(of: "&city_id=nil", with: "")
        }
        
        if customUrl.contains("&city_id=0"){
            customUrl = customUrl.replacingOccurrences(of: "&city_id=0", with: "")
        }
        if customUrl.contains("&country_id=0"){
            customUrl = customUrl.replacingOccurrences(of: "&country_id=0", with: "")
        }
        
        var par = parameter
        let cityId = parameter?["city_id"] as? Int
        let distance = parameter?["distance"] as? Int
        let nearbyme = parameter?["nearby_me"] as? Int
        let storeType = parameter?["store_type"] as? Int
        
        if storeType == nil {
            par?.removeValue(forKey: "store_type")
        }
        
        if nearbyme == nil {
            par?.removeValue(forKey: "nearby_me")
        }
        
        if distance == nil {
            par?.removeValue(forKey: "distance")
        }
        
        if cityId == nil  || cityId == 0{
            par?.removeValue(forKey: "city_id")
        }
        
        print("URL: \(customUrl)")
        print("METHOD: \(method)")
        print("PARAMETERS: \(String(describing: par))")
        print("TOKEN: \(String(describing: getHeader(reqCode: requestCode, userToken: userToken)))")
        AF.request(customUrl, method: method, parameters: par, encoding: JSONEncoding.default, headers:  getHeader(reqCode: requestCode, userToken: userToken), interceptor: nil).responseString { (dataResponse) in
            let statusCode = dataResponse.response?.statusCode
            print("statusCode: ",dataResponse.response?.statusCode)
            print("dataResponse: \(dataResponse)")
            
            switch dataResponse.result {
            case .success(_):
                var object:T?
                if(statusCode != 400){
                    object = self.convertDataToObject(response: dataResponse.data, T.self)
                }else if(statusCode == 200){
                    object = self.convertDataToObject(response: dataResponse.data, T.self)
                }
                let errorObject = self.convertDataToObject(response: dataResponse.data, ErrorResponse.self)
                if (statusCode == 200 || statusCode == 201) && object != nil{
                    
            
                    //Saved API data in cache.
                    if let data = dataResponse.data {
                        let cachedResponse = CachedURLResponse(response: dataResponse.response ?? URLResponse(), data: data)
                        URLCache.shared.storeCachedResponse(cachedResponse, for: URLRequest(url: cacheURL))
                    }
                    else {
                        print("🔴Error: Unable to create URL from cacheKey")
                        // Handle the error or return from the function as needed
                    }
                    
                             
                    completionHandler(object!)
                } else if statusCode == 404  {
                    NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": (errorObject?.message ?? "").localizableString(loc: Singleton.shared.language ?? "en"),"type":2])
                    
                }else if statusCode == 400{
                    if((requestCode == U_ADD_TO_CART || requestCode == U_GUEST_ADD_CART) && errorObject?.status == 401){
                        NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_ADD_TO_BAG), object: nil)
                    }else {
                        if(errorObject?.message != "" || errorObject?.message != nil){
                            NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": (errorObject?.message ?? "").localizableString(loc: Singleton.shared.language ?? "en"),"type":2])
                        }
                    }
                } else {
                    
                    
                    
                    //NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": (errorObject?.message ?? "").localizableString(loc: Singleton.shared.language ?? "en")])
                    
                    
                }
                ActivityIndicator.hide()
                break
            case .failure(_):
                ActivityIndicator.hide()
                let error = dataResponse.error?.localizedDescription
                if error == "The Internet connection appears to be offline." {
                    //Showing error message on alert
                    if(error != "" || error != nil){
                    }
                    
                    NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": (error ?? "").localizableString(loc: Singleton.shared.language ?? "en"),"type":2])
                } else {
                    // NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": (error ?? "").localizableString(loc: Singleton.shared.language ?? "en")])
                    
                }
                break
            }
        }
    }
    
    
    func makeMultipartRequest(url: String, fileData: Data?, param: [String:Any], fileName: String, completionHandler: @escaping (Any) -> Void) {
        AF.upload(multipartFormData: { multipartFormData in
            
            for (key, value) in param {
                if let temp = value as? String {
                    multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                }
                if let temp = value as? Int {
                    multipartFormData.append("\(temp)".data(using: .utf8)!, withName: key)
                }
                if let temp = value as? NSArray {
                    temp.forEach({ element in
                        let keyObj = key + "[]"
                        if let string = element as? String {
                            multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                        } else
                        if let num = element as? Int {
                            let value = "\(num)"
                            multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                        }
                    })
                }
            }
            if let data = fileData{
                multipartFormData.append(data, withName: "file", fileName: "\(Date.init().timeIntervalSince1970).png", mimeType: "image/png")
            }
            
            if let data = fileData{
                multipartFormData.append(data, withName: "avatar", fileName: "\(Date.init().timeIntervalSince1970).png", mimeType: "image/png")
            }
        },
                  to: url, method: .post , headers: getHeader(reqCode: U_REVIEW_IMAGE_UPLOAD, userToken: nil))
        .responseJSON(completionHandler: { (response) in
            
            print(response)
            
            if let err = response.error{
                print(err)
                // onError?(err)
                return
            }
            print("Succesfully uploaded")
            
            let json = response.data
            
            if (json != nil)
            {
                let jsonObject = self.convertDataToObject(response: response.data, UploadImage.self)
                completionHandler(jsonObject)
            }
        })
    }
    
    private func convertDataToObject<T: Codable>(response inData: Data?, _ object: T.Type) -> T? {
        
        if let data = inData {
            do {
                let decoder = JSONDecoder()
                let decoded = try decoder.decode(T.self, from: data)
                return decoded
            } catch {
                print(error)
            }
        }
        return nil
    }
    
    
    func getHeader(reqCode: String, userToken: String?) -> HTTPHeaders? {
        var token = Singleton.shared.userDetail
        if (token.id != nil){
            if(token == nil){
                return nil
            }else {
                return ["Authorization": "Bearer " + "\(token.access_token ?? "")"]
            }
        } else {
            return nil
        }
    }
}
