//
//  Extensions.swift
//  Hey hala
//
//  Created by qw on 23/12/20.

import UIKit
import CoreLocation
import SkyFloatingLabelTextField
import SideMenu

extension UIViewController {
    
    func showErrorMsg(msg: String,controller: UIViewController){
        if let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as? NotAvailableViewController{
            myVC.heading = msg
            myVC.modalPresentationStyle = .overFullScreen
            if(msg != ""){
                self.present(myVC, animated: false, completion: nil)
            }
        }
    }
    
    
    func addTransition(direction: CATransitionSubtype,controller: UIViewController) {
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = direction
        controller.navigationController?.view.layer.add(transition, forKey: kCATransition)
    }
    
    func calculateTimeDifference(date1: Int, date2: Int) -> Int{
        let d1 = Date(timeIntervalSince1970: TimeInterval(date1))
        let d2 = Date(timeIntervalSince1970: TimeInterval(date2))
        let diff = d2.timeIntervalSince(d1)
        return Int(diff)
    }
    
    
    func convertTimestampToDate(_ timestamp: Int, to format: String) -> String {
        var myVal = Int()
        var intValue:Int64 = 10000000000
        if(timestamp/Int(truncatingIfNeeded: intValue) == 0){
            myVal = timestamp
        }else {
            myVal = timestamp/1000
        }
        let date = Date(timeIntervalSince1970: TimeInterval(myVal))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
    
    func formattedDateFromString(dateString: String, withFormat format: String) -> String? {

        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "dd-MM-YYYY"

        if let date = inputFormatter.date(from: dateString) {

            let outputFormatter = DateFormatter()
          outputFormatter.dateFormat = format

            return outputFormatter.string(from: date)
        }

        return nil
    }
    
    func openUrl(urlStr: String) {
        guard let url = URL(string: urlStr) else {
            print("Invalid URL: \(urlStr)")
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            }
        } else {
            hasPermission = false
        }
        
        return hasPermission
    }
    
    func callNumber(phoneNumber:String) {
        if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    application.openURL(phoneCallURL as URL)
                }
            }
        }
    }
    
    @objc func menu() {
        SideMenuManager.default.menuFadeStatusBar = false
        let sideMenuController = storyboard?.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
        SideMenuManager.defaultManager.menuWidth = self.view.frame.width
        
        SideMenuManager.defaultManager.menuLeftNavigationController = UISideMenuNavigationController(rootViewController: sideMenuController)
        SideMenuManager.defaultManager.menuLeftNavigationController?.setNavigationBarHidden(true, animated: false)
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    @objc func openFilter(view: Int) {
        SideMenuManager.default.menuFadeStatusBar = false
        let sideMenuController = storyboard?.instantiateViewController(withIdentifier: "FilterScreenViewController") as! FilterScreenViewController
        sideMenuController.selectedView = view
        SideMenuManager.defaultManager.menuWidth = self.view.frame.width
        SideMenuManager.defaultManager.menuWidth = self.view.frame.width * 0.75
        SideMenuManager.default.menuEnableSwipeGestures = false
        SideMenuManager.defaultManager.menuRightNavigationController = UISideMenuNavigationController(rootViewController: sideMenuController)
        SideMenuManager.defaultManager.menuRightNavigationController?.setNavigationBarHidden(true, animated: false)
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    
    func share(_ sender: Any, _ value: Int, _ vendor: ShopDetails?, _ offerId: Int?) { //, _ vendorName: String?
        //        let textToShare = "\(NSLocalizedString("Checkout the products of", tableName: nil, bundle: bundle!, value: "", comment: "")) \(vendorDetail!.vendorName!) \(NSLocalizedString("on SYMO app.", tableName: nil, bundle: bundle!, value: "", comment: ""))"
        //            URL(string:"symo://vendor-id?\(vendorDetail!.vendorId!)")
        
        var urlString: String = ""
        var text: String?
        if value == K_SHARE_APP {
            text = "SYMO by GOSYMO E-MARKETING & DIGITAL SOLUTION FZ LLE"
            urlString = U_SHARE_APP_LINK
        } else if value == K_SHARE_BUSINESS {
            //urlString = "https://gosymo.com/user/shop/\(vendorName)/\(vendorId!)"
            urlString = "https://gosymo.com/user/shop/\(vendor?.shop.subdomain ?? "")/\(vendor?.shop.vendor_id ?? 0)"
        } else if value == K_SHARE_OFFER {
            urlString = "https://gosymo.com/user/shop/\(vendor?.shop.subdomain ?? "")/\(vendor?.shop.id ?? 0)?offer=\(offerId!)"
        }
        let url = URL(string: urlString)
        
        let objectsToShare = [text, url] as [Any]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        
        //New Excluded Activities Code
        activityVC.excludedActivityTypes = []
        activityVC.popoverPresentationController?.sourceView = sender as? UIView
        self.present(activityVC, animated: true, completion: nil)
    }
    
    
    func handlePrice(price:String,cut: String) -> NSMutableAttributedString {
        var finalString = NSMutableAttributedString()
        
        if let priceDouble = Double(price), let cutDouble = Double(cut) {
            let formattedPrice = String(format: "%.2f", priceDouble)
            let formattedCut = String(format: "%.2f", cutDouble)
            
            let attributeString = NSMutableAttributedString(string: formattedPrice)
            let strikeThroughRange = NSMakeRange(0, attributeString.length)
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: strikeThroughRange)
            
            finalString.append(NSAttributedString(string: "\(Singleton.shared.selectedCurrencySymbol) \(formattedCut) "))
            finalString.append(attributeString)
        } else {
            finalString = NSMutableAttributedString(string: "Invalid price or discount")
        }
        
        return finalString
    }
}

extension String {
    func applyPatternOnNumbers(pattern: String, replacmentCharacter: Character) -> String {
        var pureNumber = self.replacingOccurrences( of: "[^0-9]", with: "", options: .regularExpression)
        for index in 0 ..< pattern.count {
            guard index < pureNumber.count else { return pureNumber }
            let stringIndex = String.Index(encodedOffset: index)
            let patternCharacter = pattern[stringIndex]
            guard patternCharacter != replacmentCharacter else { continue }
            pureNumber.insert(patternCharacter, at: stringIndex)
        }
        return pureNumber
    }
    
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    
    var camelcaseString: String {
        let source = self
        if source.contains(" ") {
            let splitString = source.components(separatedBy: " ")
            var camelCaseStr: String = ""
            for str in splitString {
                let first = str.capitalized
                camelCaseStr += "\(first) "
            }
            return camelCaseStr
        } else {
            return source.capitalized
        }
    }
}

extension UIImageView {
    func changeTint(color: UIColor) {
        let templateImage =  self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
    
    func imageResized(to size: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { _ in
            draw(CGRect(origin: .zero, size: size))
        }
    }
}

final class ContentSizedTableView: UITableView {
    override var contentSize:CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
    
}

extension UIPageControl {
    
    func customPageControl(dotFillColor:UIColor, dotBorderColor:UIColor, dotBorderWidth:CGFloat) {
        for (pageIndex, dotView) in self.subviews.enumerated() {
            dotView.frame.size = CGSize(width: 15, height: 5)
            if self.currentPage == pageIndex {
                dotView.backgroundColor = dotFillColor
            }else{
                dotView.backgroundColor = .clear
            }
        }
    }
}

extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}

extension String {
    func localizableString(loc : String) -> String {
        let path = Bundle.main.path(forResource: loc.lowercased(), ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}

extension String {
    
    var utf8Data: Data {
        return Data(utf8)
    }
    
    var attributedHtmlString: NSAttributedString? {
        do {
            return try NSAttributedString(data: utf8Data, options: [
              .documentType: NSAttributedString.DocumentType.html,
              .characterEncoding: String.Encoding.utf8.rawValue
            ],
            documentAttributes: nil)
        } catch {
            print("Error:", error)
            return nil
        }
    }
}

extension UILabel {
   func setAttributedHtmlText(html: String){
      if let attributedText = html.attributedHtmlString {
          self.attributedText = attributedText
      }
   }
}

public extension String {
    func setColor(_ color: UIColor, ofSubstring substring: String) -> NSMutableAttributedString {
        let range = (self as NSString).range(of: substring)
        let attributedString = NSMutableAttributedString(string: self)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        return attributedString
    }
}

extension UIView {
func roundedCorners(corners : UIRectCorner, radius : CGFloat) {
    let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
    let mask = CAShapeLayer()
    mask.path = path.cgPath
    layer.mask = mask
}
    
        func addBlurEffect()
        {
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.bounds

            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
            self.addSubview(blurEffectView)
        }
}

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}


// Extension to set the language for the main bundle
extension Bundle {
    static func setLanguage(_ languageCode: String) {
        var onceToken : Int = 0
        if(onceToken == 0){
            object_setClass(Bundle.main, PrivateBundle.self)
        }
        onceToken = 1
        objc_setAssociatedObject(Bundle.main, &asscoiatedLanguageBundle ,Bundle(path: Bundle.main.path(forResource: languageCode, ofType: "lproj") ?? "") , .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
}

private var asscoiatedLanguageBundle:Character = "0"

class PrivateBundle : Bundle{
    override func localizedString(forKey key: String, value: String?, table tableName: String?) -> String {
        let bundle : Bundle? = objc_getAssociatedObject(self, &asscoiatedLanguageBundle) as? Bundle
        return (bundle != nil) ? (bundle!.localizedString(forKey: key, value: value, table: tableName)) : super.localizedString(forKey: key, value: value, table: tableName)
    }
}



//To handle Arabic text ALigment..
extension UIViewController{
    // Loop through all subviews recursively
    func handleTextAlignmentForArabic(view: UIView) {
        if Singleton.shared.language == "ar" {
            for subview in view.subviews {
                if let textField = subview as? UITextField {
                    textField.textAlignment = .right
                } else if let textView = subview as? UITextView {
                    textView.textAlignment = .right
                } else if let label = subview as? UILabel{
                    if label.textAlignment == .right{
                        label.textAlignment = .left
                    }else if label.textAlignment == .left{
                        label.textAlignment = .right
                    }
                }
                
                // Recursively call the function for subviews of the current subview
                handleTextAlignmentForArabic(view: subview)
            }
        }
    }
}



//for underline under text/button
extension NSAttributedString {
    static func attributedTextWithUnderline(text: String, color: UIColor, fontSize: CGFloat = 12, fontFamily: UIFont? = nil) -> NSAttributedString {
        
        var font = UIFont.systemFont(ofSize: fontSize)
        if fontFamily != nil{
            font = fontFamily!
        }
        
        let attributes: [NSAttributedString.Key: Any] = [
            .underlineStyle: NSUnderlineStyle.single.rawValue,
            .font: font,
            .foregroundColor: color
        ]
        return NSAttributedString(string: text, attributes: attributes)
    }
}



extension Double{
    // for display value upto 2 digit decimal and with comma
    var addComma : String {
        if let number = Double(self) as? Double {
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .decimal
            numberFormatter.minimumFractionDigits = 2
            numberFormatter.maximumFractionDigits = 2
            numberFormatter.roundingMode = .halfUp // Add rounding mode here
            
            if let formattedNumber = numberFormatter.string(for: number) {
                return formattedNumber
            }
        }
        
        return "0.0000"
        
    }
}


extension String {
    // for display value upto 2 digit decimal and with comma
    var addComma : String {
        if let number = Double(self) {
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .decimal
            numberFormatter.minimumFractionDigits = 2
            numberFormatter.maximumFractionDigits = 2
            numberFormatter.roundingMode = .halfUp // Add rounding mode here
            
            if let formattedNumber = numberFormatter.string(for: number) {
                return formattedNumber
            }
        }
        return self
    }
}


extension UICollectionView {
    func removeOldData() {
        // Remove all visible cells
        for cell in self.visibleCells {
            cell.removeFromSuperview()
        }

        // Remove all visible supplementary views (headers and footers)
        for kind in [UICollectionView.elementKindSectionHeader, UICollectionView.elementKindSectionFooter] {
            let visibleSupplementaryViews = self.visibleSupplementaryViews(ofKind: kind)
            for view in visibleSupplementaryViews {
                view.removeFromSuperview()
            }
        }
    }
}
