//
//  Constants.swift
//  Hey Hala
//
//  Created by qw on 23/12/20.

import UIKit

//MARK: COLORS
var K_BLUE_COLOR = UIColor(red: 55/255, green: 193/255, blue: 240/255, alpha: 1)//#37C1F0
//var K_PINK_COLOR = UIColor(red: 228/255, green: 28/255, blue: 100/255, alpha: 1)//#E41C64
var K_YELLOW_COLOR = UIColor(red: 242/255, green: 207/255, blue: 31/255, alpha: 1)//#f2cf1f
var K_LIGHT_PINK_COLOR = UIColor(red: 238/255, green: 92/255, blue: 134/255, alpha: 1) //#EE5C86
var K_BLACK_COLOR = UIColor(red: 61/255, green: 61/255, blue: 61/255, alpha: 1)//#3d3d3dff
var K_OFF_WHITE_COLOR = UIColor(red: 236/255, green: 236/255, blue: 236/255, alpha: 1)//#ececec
//var K_STATUS_BAR_PINK = UIColor(red: 226/255, green: 4/255, blue: 89/255, alpha: 1)//##E20459

var K_PINK_COLOR : UIColor = UIColor(named: "K_RED_COlOR") ?? .systemRed
var K_STATUS_BAR_PINK : UIColor = UIColor(named: "K_RED_COlOR") ?? .systemRed

//let U_BASE = "https://beta.gosymo.com/api/"
//let U_PRODUCT_IMAGE_BASE =  "https://beta.gosymo.com/storage/app/public/product_image/"
//let U_SUBCATEGORY_IMAGE_BASE  = "https://beta.gosymo.com/assets/images/subCategory_avatar/"
//let U_IMAGE_BASE = "https://beta.gosymo.com/storage/app/public/"
//let U_IMAGE_STORE_BASE = "https://beta.gosymo.com/assets/images/vendor_profile_avatar/"


//MARK: URL
let U_BASE = "https://gosymo.com/api/"
let U_PRODUCT_IMAGE_BASE =  "https://gosymo.com/storage/app/public/product_image/"
let U_SUBCATEGORY_IMAGE_BASE  = "https://gosymo.com/assets/images/subCategory_avatar/"
let U_IMAGE_BASE = "https://gosymo.com/storage/app/public/"
let U_IMAGE_STORE_BASE = "https://gosymo.com/assets/images/vendor_profile_avatar/"

let U_REVIEW_IMAGE_UPLOAD = "user/review-image-upload"
let U_UPDATE_PROFILE = "user/v2/update/profile"
let U_UPLAOD_IMAGE = "user/update/avatar"

 let latitude = "&lat="
 let optionalLat = "?lat="
 let lang = "&lang="
 let optionalLang = "?lang="
 let page = "&page="
 let limit = "&limit=50"
 let pageLimitWithLang = "?page=1&limit=50&lang="
 let limitWithLang = "?limit=50&lang="

let GOOGLE_CLIENT_ID = "26688619210-1j8pmljaq7mjc5n3lrsptji3u6f9tt4t.apps.googleusercontent.com"
var GOOGLE_API_KEY = "AIzaSyDEKWYKnm_ZHSH0p7FP3Y08Ol3fDiQr78E"


let U_LOGIN = "user/login"
let U_SOCIAL_MEDIA_LOGIN = "user/social-login"
let U_SIGNUP = "user/signup"
let U_GET_COUNTRY = "user/get-country"
let U_GET_CITY = "user/get-cities-by-country/"
let U_GET_LANGUAGE = "get-languages/"
let U_UPDATE_SOCIAL_DETAIL = "user/update-social-detail"
let U_FORGOT_PASSWORD = "user/forget-password"
let U_GET_CURRENCY = "get-currency"
let U_GET_VENDOR_BY_CITY = "vendor-search"
let U_GET_NEW_TRENDING_BUSINESS = "get-new-and-trending-business"
let U_GET_VENDOR_DETAIL = "user/shop"
let U_SEARCH_VENDOR_NAME = "user/search"
let U_GET_SHOP_PRODUCT = "shop-products/"
let U_GET_SINGLE_PRODUCT = "single-product/"
let U_DOWNLOAD_PDF = "user/download-pdf-invoice/"
let U_GET_NOTIFICATION = "user/get-notifications"
let U_GET_SINGLE_ORDER_DETAIL = "user/single-order/"
let U_GET_VARIARANT_STOCK = "product-variant-stock-status"
let U_RETURN_REQUEST = "user/submit-return-request/"
let U_EXCHANGE_REQUEST = "user/submit-exchange-request/"
let U_VISIT_COUNT = "user/visit_count"
let U_POST_SHOP_REVIEW = "user/add/review"
let U_GET_CONTACT_DETAILS = "contact-details"
let U_POST_QUERY = "contact-us"
let U_GET_DEALS = "get-deals"
let U_SHOP_NOTIFICATION = "user/shop-notification-alert"

let U_SEARCH_STORES = "search-stores?page="
let U_SEARCH_PRODUCTS = "search-products?page="
// U_GET_SUBCATEGORIES = "https://gosymo.com/user/get_subcategories"
let U_GET_SUBCATEGORIES = "get-subcategories"

let U_ADD_TO_WISHLIST = "user/add-to-wishlist"
let U_REMOVE_FROM_WISHLIST = "user/remove-from-wishlist"
let U_GET_WISHLIST = "user/wishlist"

let U_GET_ALL_CATEGORY = "user/category"
let U_GET_SHOP_BY_CATEGORY = "user/search/category"

let U_GET_SEASONAL_OFFER = "user/home_page_data"

let U_GET_FILTER_SHOP = "get-attribute-by-category"

let U_GET_PICKUP_DELIVERY_SETTING = "user/get-pickup-delivery-setting"
//let U_BOOKMARK_VENODR = "user/profile/bookmark/status?vendor_id="
let U_BOOKMARK_VENODR = "user/bookmark-vendor"
let U_GET_BOOKMARK_SHOPS = "user/get-bookmark-vendors"
//let U_CHECK_BOOKMARK = "user/profile/check-bookmark-status/"

let U_SUBMIT_REVIEW = "user/submit-review"

let U_SET_ORDER_PREFERENCE = "user/set-order-preference"
let U_ADD_TO_CART = "user/add-to-cart"
let U_GET_CART = "user/get-cart"
let U_GUEST_ADD_CART = "guest-add-to-cart"
let U_GET_GUEST_CART = "get-cart/"
let U_UPDATE_CART = "user/update-cart"
let U_UPDATE_GUEST_CART = "update-guest-cart/"
let U_DELETE_CART_ITEM = "user/delete-cart-item"
let U_DELETE_CART = "user/delete-cart"
let U_DELETE_GUEST_CART = "guest-delete-cart/"
let U_DELETE_GUEST_CART_ITEM = "delete-cart-item/"

let U_CONTACT_US = "https://gosymo.com/contact-us"
let U_URL_FAQ = "https://gosymo.com/faqs"
let U_PRIVACY_POLICY = "https://gosymo.com/privacy-policy"
let U_TERMS_CONDITION = "https://gosymo.com/terms-and-conditions"
let U_DISCLAIMER =  "https://gosymo.com/disclaimer"

let U_GET_MY_ORDER = "user/my-orders"

let U_GET_ADDRESS = "user/address-list"
let U_ADD_NEW_ADDRESS = "user/add-new-address"
let U_MARK_DEFAULT_ADDRESS = "user/make-default-address/"
let U_UPDATE_ADDRESS = "user/update-address/"
let U_DELETE_ADDRESS = "user/delete-address/"

let U_APPLY_COUPON = "user/apply-coupon"
let U_REMOVE_COUPON = "user/remove-coupon"
let U_PLACE_ORDER = "user/place-order"
let U_CANCEL_ORDER = "user/cancel-order/"
let U_GET_OFFERS_CATEGORY = "get-offers"

let U_BECOME_VENDOR = "vendor/signup"
let U_BECOME_VENDOR_EXISTING_USER = "user/become-a-vendor"


var U_SHARE_APP_LINK = "https://itunes.apple.com/in/app/symo/id1596220154?mt=8"

//MARK: Constant Keys
//manage filter
var K_FILTER_REDIRECTION = 1

//Share keys
let K_SHARE_APP = 1
let K_SHARE_BUSINESS = 2
let K_SHARE_OFFER = 3

let K_APP_LANGUAGE = "K_APP_LANGUAGE"
let K_LOGIN_FORM = "K_LOGIN_FORM"
let K_SIGNUP_FORM = "K_SIGNUP_FORM"
let K_FORGOT_FORM = "K_FORGOT_FORM"
var K_SELECTED_SORTBY = 0
var K_BAG_COUNT =  0
var K_CURRENT_COUNTRY = ""

//MARK: User Defaults
let UD_USER_DETAIl = "userDetail"
let UD_TOKEN = "user_token"
let UD_SELECTED_COUNTRY = "UD_SELECTED_COUNTRY"
let UD_SELECTED_CITY = "UD_SELECTED_CITY"
let UD_BAG_COUNT = "UD_BAG_COUNT"
let UD_ACCESS_TOKEN = "UD_ACCESS_TOKEN"
let UD_DEVICE_TOKEN = "UD_DEVICE_TOKEN"
var UD_FIRST_TIME = "UD_FIRST_TIME"
var UD_APPLE_EMAIL = "UD_APPLE_EMAIL"
var UD_APPLE_FIRST_NAME = "UD_APPLE_FIRST_NAME"
var UD_APPLE_LAST_NAME = "UD_APPLE_LAST_NAME"
var UD_APPLE_ID = "UD_APPLE_ID"
var UD_SELECTED_CATEGORIES = "UD_SELECTED_CATEGORIES"
var UD_SELECTED_CURRENCY = "UD_SELECTED_CURRENCY"
let UD_GUEST_CART_ID = "UD_GUEST_CART_ID"
var UD_CURRENT_COUNTRY = "UD_CURRENT_COUNTRY"

//MARK: Notifications
let N_SHOW_ERROR_MESSAGE = "N_SHOW_ERROR_MESSAGE"
let N_HANDLE_LOCATION_PERMISSION = "N_HANDLE_LOCATION_PERMISSION"
let N_HANDLE_CITY = "N_HANDLE_CITY"
let N_HANDLE_TABBAR = "N_HANDLE_TABBAR"
let N_FILTER_PRODUCT = "N_FILTER_PRODUCT"
let N_REDIRECTION_FROM_HOME = "N_REDIRECTION_FROM_HOME"
let N_HANDLE_ADD_TO_BAG = "N_HANDLE_ADD_TO_BAG"
let N_NOTIFICATION_DATA  = "N_NOTIFICATION_DATA"
let N_UPDATE_BAG = "N_UPDATE_BAG"
