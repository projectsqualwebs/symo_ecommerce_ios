//
//  NavigationController.swift
//  Hey Hala
//
//  Created by qw on 01/01/21.
//

import UIKit
import CoreLocation


class NavigationController: UIViewController, Confirmation {
    func confirmationSelection(action: Int) {
        if (action == 1){
            self.emptyCartItem(cartId: Singleton.shared.cartData.cart?.id ?? 0, navigationFromOrder: false)
        }
    }
    
    //MARK: IBOutlets
    
    //Location MAP
    var onUserLocationReceived: ((_ userLocation: CLLocation) -> Void)? = nil
    
    static let shared = NavigationController()
    lazy var locationManager = CLLocationManager()
    let location = Singleton.shared.userLocation
    var controller = UIViewController()
    var param = [String:Any]()
    static var manageDelegate : ManageCartButton? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
    }
    
    func registerEventListenerForLocation(withBlock block: @escaping (_ location: CLLocation) -> Void) {
        onUserLocationReceived = block
    }
    
    func getUserCurrentLocation() {
        initLocationManager()
    }
    
    func initLocationManager() {
        locationManager = CLLocationManager()
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func goToHomeVC(controller:UIViewController){
        let myVC = controller.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        controller.navigationController?.pushViewController(myVC, animated: true)
    }
    
    //For changing status bar color
    func changeStatusBar(color : UIColor) {
        var statusBar1 =  UIView()
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.backgroundColor = color
        navigationController?.navigationBar.barStyle = .default
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        if #available(iOS 13.0, *) {
            if let keyWindow = UIApplication.shared.windows.first {
                let safeAreaTopHeight = keyWindow.safeAreaInsets.top
                let screenWidth = UIScreen.main.bounds.width
                statusBar1.frame = CGRect(x: 0, y: 0, width: screenWidth, height: safeAreaTopHeight)
            }else{
                statusBar1.frame = UIApplication.shared.keyWindow?.windowScene?.statusBarManager!.statusBarFrame ?? CGRect()
            }
            statusBar1.backgroundColor = color
            UIApplication.shared.keyWindow?.addSubview(statusBar1)
        } else {
            statusBar1 = UIApplication.shared.value(forKey: "statusBar") as! UIView
            statusBar1.backgroundColor = color
        }
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
    }
    
    func getNewAndTrending(view: UIView,completionHandler: @escaping (NewTrendingResponse) -> Void){
        if(Singleton.shared.newTrendingBusiness.new_business.count == 0 && Singleton.shared.newTrendingBusiness.trending_business.count == 0){
            ActivityIndicator.show(view: view)
            let param:[String:Any] = [
                "lang": Singleton.shared.language ?? "en",
                "city_id": Singleton.shared.selectedCity.city_id ?? "4",
                "country_id" : Singleton.shared.selectedCountry.id,
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_NEW_TRENDING_BUSINESS + "?currency=\(Singleton.shared.selectedCurrency)", method: .post, parameter: param, objectClass: GetNewTrending.self, requestCode: U_GET_NEW_TRENDING_BUSINESS, userToken: nil) { (response) in
                Singleton.shared.newTrendingBusiness = response.response
                completionHandler(response.response)
                ActivityIndicator.hide()
            }
        }else {
            completionHandler(Singleton.shared.newTrendingBusiness)
        }
    }
    
    func getShopByCategory(view: UIView,completionHandler: @escaping (NewTrendingResponse) -> Void){
        if(Singleton.shared.newTrendingBusiness.new_business.count == 0 && Singleton.shared.newTrendingBusiness.trending_business.count == 0){
            ActivityIndicator.show(view: view)
            let param:[String:Any] = [
                "lang": Singleton.shared.language ?? "en",
                "city_id": Singleton.shared.selectedCity.city_id ?? "4",
                "country_id" : Singleton.shared.selectedCountry.id,
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_NEW_TRENDING_BUSINESS + "?currency=\(Singleton.shared.selectedCurrency)&lang=\(Singleton.shared.language ?? "en")", method: .post, parameter: param, objectClass: GetNewTrending.self, requestCode: U_GET_NEW_TRENDING_BUSINESS, userToken: nil) { (response) in
                Singleton.shared.newTrendingBusiness = response.response
                completionHandler(response.response)
                ActivityIndicator.hide()
            }
        }else {
            completionHandler(Singleton.shared.newTrendingBusiness)
        }
    }
    
    //for fetching bookmark vendor
    func getAllBookMarkStore(){
        let param:[String:Any] = [
            "lat":Singleton.shared.userLocation.coordinate.latitude,
            "long" : Singleton.shared.userLocation.coordinate.longitude,
            "lang": Singleton.shared.language ?? "en",
            "country_id" : Singleton.shared.selectedCountry.id,
        ]
        //fetching the bookmark shops
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_BOOKMARK_SHOPS, method: .post, parameter: param, objectClass: GetStore.self, requestCode: U_GET_BOOKMARK_SHOPS, userToken: nil) { response in
            for store in response.response.data{
                if  !Singleton.shared.bookmarkedVendors.contains(store.id ?? 0){
                    Singleton.shared.bookmarkedVendors.append(store.id ?? 0)
                }
            }
        }
    }
    
    func getAllCategory(view: UIView,completionHandler: @escaping ([CategoryResponse]) -> Void){
        if(Singleton.shared.allCategoryData.count == 0){
            ActivityIndicator.show(view: view)
            let url = U_BASE + U_GET_ALL_CATEGORY + pageLimitWithLang + (Singleton.shared.language ?? "en")
            SessionManager.shared.methodForApiCalling(url: url, method: .get, parameter: nil, objectClass: GetCategory.self, requestCode: U_GET_ALL_CATEGORY, userToken: nil) { (response) in
                Singleton.shared.allCategoryData = response.response
                completionHandler(response.response)
                ActivityIndicator.hide()
            }
        }else {
            completionHandler(Singleton.shared.allCategoryData)
        }
    }
    
    func getAllSeasonalOffer(view: UIView,completionHandler: @escaping (SeasonalOffersResponse) -> Void){
        if((Singleton.shared.seasonalOffer.banners.count ?? 0) == 0){
            ActivityIndicator.show(view: view)
            
            let url = U_BASE + U_GET_SEASONAL_OFFER + "?lang=\(Singleton.shared.language ?? "en")&lat=\(Singleton.shared.userLocation.coordinate.latitude)&long=\(Singleton.shared.userLocation.coordinate.longitude)&city_id=\(Singleton.shared.selectedCity.city_id ?? 0)&currency=\(Singleton.shared.selectedCurrency)&country_id=\(Singleton.shared.selectedCountry.id ?? 0)"
            SessionManager.shared.methodForApiCalling(url: url, method: .get, parameter: nil, objectClass: GetSeasonalOffer.self, requestCode: U_GET_SEASONAL_OFFER, userToken: nil, reloadData: false) { (response) in
                Singleton.shared.seasonalOffer = response.response
                completionHandler(response.response)
                ActivityIndicator.hide()
            }
        }else {
            completionHandler(Singleton.shared.seasonalOffer)
        }
    }
    
    
    
    func hasLocPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            }
        } else {
            hasPermission = false
        }
        return hasPermission
    }
    
    func getCities(countryId: Int,completionHandler: @escaping (([Cities]) -> Void)) {
        ActivityIndicator.show(view: view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CITY + "\(countryId)" + "?lang=\( /*Singleton.shared.language ??*/ "en")", method: .get, parameter: nil, objectClass: GetCities.self, requestCode: U_GET_CITY, userToken: nil, reloadData: false) { (cityResponse) in
            
            Singleton.shared.cityData = (cityResponse.response.cities)
            let newCity =  Cities(name_en: "All", name: "All", name_ar: "", city_images: "", country_id: countryId, city_id: nil)
            Singleton.shared.cityData.insert(newCity, at: 0)
            
            if(Singleton.shared.selectedCity.name == nil){
                let city = try? JSONEncoder().encode(Singleton.shared.cityData[0])
                Singleton.shared.selectedCity = Singleton.shared.cityData[0]
                UserDefaults.standard.setValue(city, forKey: UD_SELECTED_CITY)
            }
            completionHandler((Singleton.shared.cityData))
            ActivityIndicator.hide()
        }
    }
    
    //language for both country
    func getLangByCountry(countryId: Int,completionHandler: @escaping (([LanguageResponse]) -> Void)) {
        //        if(Singleton.shared.langData.count == 0){
        //            ActivityIndicator.show(view: view)
        //            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_LANGUAGE + "\(countryId)", method: .get, parameter: nil, objectClass: GetLanguages.self, requestCode: U_GET_LANGUAGE, userToken: nil) { (response) in
        //                Singleton.shared.langData = response.country_lang ?? []
        //                completionHandler(response.country_lang ?? [])
        //                ActivityIndicator.hide()
        //            }
        //        }else {
        //            completionHandler(Singleton.shared.langData)
        //        }
        //static language for now
        let langData: [LanguageResponse] = [
            LanguageResponse(id: 359, country_id: countryId, language_id: 15, code: "en", lang_name: "English"),
            LanguageResponse(id: 358, country_id: countryId, language_id: 3, code: "ar", lang_name: "Arabic")
        ]
        Singleton.shared.langData = langData
        completionHandler(Singleton.shared.langData)
    }
    
    func getCountries(completionHandler: @escaping (([Countries]) -> Void)) {
        if(Singleton.shared.countryData.count == 0){
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_COUNTRY /*+ "?lang=\(Singleton.shared.language ?? "en")"*/, method: .get, parameter: nil, objectClass: GetCountry.self, requestCode: U_GET_COUNTRY, userToken: nil, reloadData: false) { (response) in
                Singleton.shared.countryData = (response.response.countries)
                Singleton.shared.langData = []
                Singleton.shared.cityData = []
                if(Singleton.shared.selectedCountry.id == nil){
                    Singleton.shared.selectedCountry = response.response.countries[0]
                    let country = try? JSONEncoder().encode(response.response.countries[0])
                    UserDefaults.standard.setValue(country, forKey: UD_SELECTED_COUNTRY)
                }
                completionHandler(response.response.countries)
                ActivityIndicator.hide()
            }
        }else {
            completionHandler(Singleton.shared.countryData)
        }
    }
    
    func addRemoveProductAsFavourite(view:UIView,id:Int,variantId : Int,action: Int, completionHandler: @escaping ((String) -> Void)){
        if(Singleton.shared.userDetail.id != nil){
            ActivityIndicator.show(view: view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + (action == 1 ? U_ADD_TO_WISHLIST:U_REMOVE_FROM_WISHLIST), method: .post, parameter: ["product_id":id,"variation_id": variantId], objectClass: SuccessResponse.self, requestCode: U_REMOVE_FROM_WISHLIST, userToken: nil) { (response) in
                ActivityIndicator.hide()
                
                completionHandler(response.message ?? "")
            }
        }else {
            Singleton.shared.showToast(msg: "Please login to proceed".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }
    }
    
    func addBookMarkVendor(id:Int, action: Int,completionHandler: @escaping (() -> Void)){
        if(Singleton.shared.userDetail.id != nil){
            ActivityIndicator.show(view: view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_BOOKMARK_VENODR + "\(id ?? 0)", method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_REMOVE_FROM_WISHLIST, userToken: nil) { (response) in
                ActivityIndicator.hide()
                completionHandler()
            }
        }else {
            Singleton.shared.showToast(msg: "Please login to proceed".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }
    }
    
    @objc func handleEmptyCart(){
        //        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_HANDLE_ADD_TO_BAG), object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.handleEmptyCart), name: NSNotification.Name(N_HANDLE_ADD_TO_BAG), object: nil)
        
        print("handleEmptyCart")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        
        let myVC = storyboard.instantiateViewController(withIdentifier: "ConfirmationPopupViewController") as! ConfirmationPopupViewController
        myVC.confirmationDelegate = self
        myVC.confirmationText = "Clear Cart".localizableString(loc: Singleton.shared.language ?? "en")
        myVC.detailText = "Do you want to change vendor? Changing vendor will clear this cart.".localizableString(loc: Singleton.shared.language ?? "en")
        myVC.modalPresentationStyle = .overFullScreen
        
        if let topViewController = UIApplication.shared.windows.first?.rootViewController  {
            topViewController.present(myVC, animated: true, completion: nil)
        }
    }
    
    func addToBag(controller: UIViewController,qty:Int, variationId: Int,productId:Int, completionHandler: @escaping ((GetCartResponse) -> Void)){
        
        if(Singleton.shared.userDetail.id != nil){
            ActivityIndicator.show(view:view)
            self.controller = controller
            self.param =  [
                "product_id": productId,
                "qty": qty,
                "variation_id": variationId,
                "lang" : Singleton.shared.language
            ]
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_HANDLE_ADD_TO_BAG), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.handleEmptyCart), name: NSNotification.Name(N_HANDLE_ADD_TO_BAG), object: nil)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_TO_CART , method: .post, parameter: self.param, objectClass: GetCartData.self, requestCode: U_ADD_TO_CART, userToken: nil) { (response) in
                ActivityIndicator.hide()
                if((response.status ?? 0) == 200){
                    if(response.response.profile == nil){
                        let a = response.response
                        Singleton.shared.cartData.cart = CartResponse(id: a.id, user_id: a.user_id, business_id: a.business_id, cart_id: a.cart_id, total_tax: a.total_tax, order_total: a.order_total, discount_amount: a.discount_amount, sub_total: a.sub_total, shipping_amount: a.shipping_amount, coupon_applied: a.coupon_applied, cart_items: a.cart_items)
                        K_BAG_COUNT = response.response.cart_items?.count ?? 0
                        UserDefaults.standard.setValue(K_BAG_COUNT, forKey: UD_BAG_COUNT)
                        completionHandler(Singleton.shared.cartData)
                        //                        controller.showErrorMsg(msg: "Product added successfully.", controller: self)
                        Singleton.shared.showToast(msg: "Product added successfully".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 1)
                    }else {
                        Singleton.shared.cartData = response.response
                        K_BAG_COUNT = response.response.cart?.cart_items?.count ?? 0
                        UserDefaults.standard.setValue(K_BAG_COUNT, forKey: UD_BAG_COUNT)
                        completionHandler(response.response)
                    }
                    NavigationController.manageDelegate?.manageButton(idArray: [self.param["product_id"] as! Int],isReload:true)
                }else {
                    //                    controller.showErrorMsg(msg: response.message ?? "", controller: self)
                    Singleton.shared.showToast(msg: response.message?.localizableString(loc: Singleton.shared.language ?? "en") ?? "", controller: self, type: 0)
                }
                ActivityIndicator.hide()
            }
        } else {
            //            controller.showErrorMsg(msg: "Please login to proceed", controller: self)
            Singleton.shared.showToast(msg: "Please login to proceed".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }
    }
    
    
    func guestAddToBag(controller: UIViewController,qty:Int, variationId: Int,productId:Int, completionHandler: @escaping ((GetCartResponse) -> Void)){
        var cartId = ""
        if(UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) != nil){
            cartId = UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) as! String
        }
        
        ActivityIndicator.show(view:view)
        self.controller = controller
        self.param =  [
            "product_id": productId,
            "qty": qty,
            "variation_id": variationId,
            "lang" : Singleton.shared.language,
            "cart_id":cartId,
            "currency":Singleton.shared.selectedCurrency
        ]
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_HANDLE_ADD_TO_BAG), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleEmptyCart), name: NSNotification.Name(N_HANDLE_ADD_TO_BAG), object: nil)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GUEST_ADD_CART , method: .post, parameter: self.param, objectClass: GetCartData.self, requestCode: U_GUEST_ADD_CART, userToken: nil) { (response) in
            ActivityIndicator.hide()
            if((response.status ?? 0) == 200){
                if(response.response.profile == nil){
                    let a = response.response
                    UserDefaults.standard.setValue(K_BAG_COUNT, forKey: UD_BAG_COUNT)
                    Singleton.shared.cartData.cart = CartResponse(id: a.id, user_id: a.user_id, business_id: a.business_id, cart_id: a.cart_id, total_tax: a.total_tax, order_total: a.order_total, discount_amount: a.discount_amount, sub_total: a.sub_total, shipping_amount: a.shipping_amount, coupon_applied: a.coupon_applied, cart_items: a.cart_items)
                    K_BAG_COUNT = response.response.cart_items?.count ?? 0
                    UserDefaults.standard.setValue(response.response.cart_id ?? "", forKey: UD_GUEST_CART_ID)
                    completionHandler(Singleton.shared.cartData)
                    //                        controller.showErrorMsg(msg: "Product added successfully", controller: self)
                    Singleton.shared.showToast(msg: "Product added successfully".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 1)
                } else {
                    Singleton.shared.cartData = response.response
                    UserDefaults.standard.setValue(response.response.cart_id ?? "", forKey: UD_GUEST_CART_ID)
                    K_BAG_COUNT = response.response.cart?.cart_items?.count ?? 0
                    UserDefaults.standard.setValue(K_BAG_COUNT, forKey: UD_BAG_COUNT)
                    completionHandler(response.response)
                }
            } else {
                Singleton.shared.showToast(msg: response.message?.localizableString(loc: Singleton.shared.language ?? "en") ?? "", controller: self, type: 0)
            }
            ActivityIndicator.hide()
        }
        
    }
    
    func getCartItem(view:UIView,completionHandler: @escaping ((GetCartResponse) -> Void)){
        if(Singleton.shared.userDetail.id != nil){
            if((Singleton.shared.cartData.cart?.cart_items?.count ?? 0) == 0){
                ActivityIndicator.show(view: view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CART + "?currency=\(Singleton.shared.selectedCurrency)&lang=\(Singleton.shared.language ?? "en")", method: .get, parameter: nil, objectClass: GetCartData.self, requestCode: U_GET_CART, userToken: nil) { (response) in
                    Singleton.shared.cartData = response.response
                    K_BAG_COUNT = Singleton.shared.cartData.cart?.cart_items?.count ?? 0
                    NotificationCenter.default.post(name: NSNotification.Name(N_UPDATE_BAG),object: nil)
                    completionHandler(response.response)
                    ActivityIndicator.hide()
                }
            }else{
                completionHandler(Singleton.shared.cartData)
            }
        }
    }
    
    func getWishlistItem(completionHandler: @escaping (([WishlistResponse]) -> Void)){
        if(Singleton.shared.userDetail.id != nil){
            if(Singleton.shared.wishlistResponse.count == 0){
                // ActivityIndicator.show(view: view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_WISHLIST + "?currency=\(Singleton.shared.selectedCurrency)&lang=\(Singleton.shared.language ?? "en")", method: .get, parameter: nil, objectClass: GetWishlist.self, requestCode: U_GET_WISHLIST, userToken: nil) { (response) in
                    Singleton.shared.wishlistResponse = response.response!
                    completionHandler(response.response!)
                    // ActivityIndicator.hide()
                }
            }else {
                completionHandler(Singleton.shared.wishlistResponse)
            }
        }
    }
    
    func getSavedAddress(completionHandler: @escaping (([Address]) -> Void)){
        if(Singleton.shared.userDetail.id != nil){
            ActivityIndicator.show(view: view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_ADDRESS + "?lang=\(Singleton.shared.language ?? "en")", method: .get, parameter: nil, objectClass: GetAddress.self, requestCode: U_GET_ADDRESS, userToken: nil) { (response) in
                Singleton.shared.savedAddress = response.response
                completionHandler(response.response)
                ActivityIndicator.hide()
            }
        }else {
            completionHandler(Singleton.shared.savedAddress)
        }
    }
    
    func geCurrencyData(){
        ActivityIndicator.show(view: view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CURRENCY, method: .get, parameter: nil, objectClass: GetCurrencyResponse.self, requestCode: U_GET_CURRENCY, userToken: nil, reloadData: false) { (response) in
            Singleton.shared.currency = response.response?.currency ?? Currency()
            ActivityIndicator.hide()
        }
    }
    
    func emptyCartItem(cartId: Int, navigationFromOrder:Bool){
        if(cartId == 0){
            if(self.param["product_id"] != nil){
                self.addToBag(controller: self.controller, qty: self.param["qty"] as! Int, variationId: self.param["variation_id"] as! Int, productId: self.param["product_id"] as! Int) { (response) in
                    if(navigationFromOrder){
                        if let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MyOrderViewController") as? MyOrderViewController{
                            self.controller.navigationController?.pushViewController(myVC, animated: true)
                        }
                    }else {
                        if let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ShoppingBucketViewController") as? ShoppingBucketViewController{
                            myVC.showBackButton = true
                            // myVC.singleProductId = self.param["product_id"] as! Int
                            self.controller.navigationController?.pushViewController(myVC, animated: true)
                        }
                    }
                }
            }
            return
        }
        ActivityIndicator.show(view: self.controller.view)
        
        if(Singleton.shared.userDetail.id != nil){
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_CART, method: .post, parameter: ["cart_id":cartId, "lang": (Singleton.shared.language ?? "en")], objectClass: SuccessResponse.self, requestCode: U_DELETE_CART, userToken: nil) { (response) in
                K_BAG_COUNT = 0
                UserDefaults.standard.removeObject(forKey: UD_BAG_COUNT)
                Singleton.shared.cartData = GetCartResponse()
                if(self.param["product_id"] != nil){
                    self.addToBag(controller: self.controller, qty: self.param["qty"] as! Int, variationId: self.param["variation_id"] as! Int, productId: self.param["product_id"] as! Int) { (response) in
                        if(navigationFromOrder){
                            if let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MyOrderViewController") as? MyOrderViewController{
                                self.controller.navigationController?.pushViewController(myVC, animated: true)
                            }
                        }else {
                            if let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ShoppingBucketViewController") as? ShoppingBucketViewController{
                                myVC.showBackButton = true
                                // myVC.singleProductId = self.param["product_id"] as! Int
                                self.controller.navigationController?.pushViewController(myVC, animated: true)
                            }
                        }
                    }
                }
                ActivityIndicator.hide()
            }
        }else{
            
            var cartId = ""
            if(UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) != nil){
                cartId = UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) as! String
            }
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_GUEST_CART + "\(cartId)" , method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_DELETE_GUEST_CART, userToken: nil) { (response) in
                UserDefaults.standard.removeObject(forKey: UD_BAG_COUNT)
                Singleton.shared.cartData = GetCartResponse()
                if(self.param["product_id"] != nil){
                    
                    self.guestAddToBag(controller: self.controller, qty: self.param["qty"] as! Int, variationId: self.param["variation_id"] as! Int, productId: self.param["product_id"] as! Int) { (response) in
                    }
                }
                ActivityIndicator.hide()
            }
        }
        
        
    }
    
    //these function used for set default country first time.
    func changeCountry(pos: Int){
        getCountries { (response) in
            Singleton.shared.selectedCountry = response[pos]
            let country = try? JSONEncoder().encode(Singleton.shared.selectedCountry)
            UserDefaults.standard.setValue(country, forKey: UD_SELECTED_COUNTRY)
            //            self.selectedCountry.text = val
            Singleton.shared.langData = []
            NavigationController.shared.getLangByCountry(countryId: Singleton.shared.selectedCountry.id ?? 0) { response in
                //                self.langData = response
                if(response.count > 0){
                    //set language
                    //                    Singleton.shared.language = (response[0].code ?? "")
                    //                    Singleton.shared.saveAppLanguage(withLanguage: (response[0].code ?? ""))
                    //                    UserDefaults.standard.setValue((response[0].code ?? ""), forKey: K_APP_LANGUAGE)
                    //                    Singleton.shared.selectedLanguage = (response[0].code ?? "")
                    //                    SettingsViewController.handletab?.localize(rtl: true)
                    
                    
                    //set currency
                    if(response[0].country_id == 36){
                        Singleton.shared.selectedCurrency = "bhd"
                        Singleton.shared.selectedCurrencySymbol = "BHD"
                        UserDefaults.standard.setValue("bhd", forKey: UD_SELECTED_CURRENCY)
                    }else{
                        Singleton.shared.selectedCurrency = "aed"
                        Singleton.shared.selectedCurrencySymbol = "AED"
                        UserDefaults.standard.setValue("aed", forKey: UD_SELECTED_CURRENCY)
                    }
                }
            }
        }
    }
}

extension NavigationController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let currentLocation: CLLocation? = locations[0]
        if currentLocation != nil && onUserLocationReceived != nil {
            onUserLocationReceived!(currentLocation!)
            locationManager.delegate = nil
            locationManager.stopUpdatingLocation()
        }
        if currentLocation != nil{
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(currentLocation!) { (placemarks, error) in
                if let error = error {
                    print("Reverse geocoding error: \(error.localizedDescription)")
                    return
                }
                
                guard let placemark = placemarks?.first else {
                    print("No placemarks found.")
                    return
                }
                
                if let country = placemark.country {
                    print("Country: \(country)")
                    
                    UserDefaults.standard.set(country, forKey: UD_CURRENT_COUNTRY)
                    self.locationManager.delegate = nil
                    self.locationManager.stopUpdatingLocation()
                    
                } else {
                    print("Country information not available.")
                    
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        // alertViewMessage(title: "Alert", message: "No User Found! Please Allow the Location to see vendors")
    }
}
