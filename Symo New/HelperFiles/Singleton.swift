//
//  Singleton.swift
//  FFK-KIOSK
//
//  Created by qw on 18/01/21
//

import UIKit
import LocalAuthentication
import CoreLocation
import FBSDKLoginKit
import GoogleSignIn
import SkyFloatingLabelTextField
import GTToast

class Singleton {
    fileprivate var toast: GTToastView!
    static let shared = Singleton()
    var userLocation = CLLocation()
    var userDetail = getUserDetail()
    var countryData = [Countries]()
    var langData = [LanguageResponse]()
    var cityData = [Cities]()
    var selectedCity = getCity()
    var selectedCountry = getCountry()
    var newTrendingBusiness = NewTrendingResponse()
    var allCategoryData = [CategoryResponse]()
    var selectedLanguage = "en"
    var seasonalOffer = SeasonalOffersResponse()
    var shopByCategoryData = [ShopByCategoryResponse]()
    var currency = Currency()
    var selectedCurrency = "aed"
    var selectedCurrencySymbol = "AED"
    var bookmarkedVendors = [Int]()
    var screenData = ScreenDetail()
    
    var cartData = GetCartResponse(){
        didSet{
            if let data = cartData.cart{
                K_BAG_COUNT = data.cart_items?.count ?? 0
                UserDefaults.standard.setValue(K_BAG_COUNT, forKey: UD_BAG_COUNT)
            }else{
                K_BAG_COUNT = 0
                UserDefaults.standard.setValue(K_BAG_COUNT, forKey: UD_BAG_COUNT)
            }
        }
    }
    var wishlistResponse = [WishlistResponse]()
    var savedAddress = [Address]()
    static var handleTabDelegate : HandleTabBarItems? = nil
    
    var language:String?{
        get {
            if let appLanguage = UserDefaults.standard.value(forKey: K_APP_LANGUAGE) as? String {
                Singleton.shared.selectedLanguage = appLanguage
               return appLanguage
              // return "ur"
            }
            return "en"
        } set {
            saveAppLanguage(withLanguage: newValue!)
        }
    }
    
    public static func getCity() -> Cities {
        do{
            if let cityData = UserDefaults.standard.value(forKey: UD_SELECTED_CITY) {
                if let city = try! JSONDecoder().decode(Cities.self, from: cityData as! Data) as? Cities{
                    return city
                }
            }else {
                return Cities()
            }
            
        }catch {
            print("error saving details")
        }
        return Cities()
    }
    
    public static func getCountry() -> Countries {
        do{
            if let countryData = UserDefaults.standard.value(forKey: UD_SELECTED_COUNTRY) {
                if let country = try! JSONDecoder().decode(Countries.self, from: countryData as! Data) as? Countries{
                    
                    return country
                }
            }else {
                return Countries()
            }
            
        }catch {
            print("error saving details")
        }
        return Countries()
    }
    
    func saveUserDetail(user: UserDetail) {
        do{
            let encode =  try? JSONEncoder().encode(user)
            UserDefaults.standard.setValue(encode, forKey: UD_USER_DETAIl)
        }catch {
            print("error saving details")
        }
    }
    
    public static func getUserDetail() -> UserDetail {
        if let user = UserDefaults.standard.value(forKey: UD_USER_DETAIl) {
            if let data = try! JSONDecoder().decode(UserDetail.self, from: user as! Data) as? UserDetail{
                return data
            }
            
            if let cartCount = UserDefaults.standard.value(forKey: UD_BAG_COUNT) as? Int{
                K_BAG_COUNT = cartCount
            }
            
        }else {
            return UserDetail()
        }
        return UserDetail()
    }
    
    func handleUserLogout(){
        self.userDetail = UserDetail()
        selectedLanguage = "en"
        cartData = GetCartResponse()
        K_BAG_COUNT = 0
        UserDefaults.standard.removeObject(forKey: UD_BAG_COUNT)
        UserDefaults.standard.removeObject(forKey: UD_TOKEN)
        UserDefaults.standard.removeObject(forKey: UD_USER_DETAIl)
//        UserDefaults.standard.removeObject(forKey: UD_FIRST_TIME)
        UserDefaults.standard.removeObject(forKey: UD_GUEST_CART_ID)
        LoginManager().logOut()
        GIDSignIn.sharedInstance.signOut()
        URLCache.shared.removeAllCachedResponses()
    }
    
    func showToast(msg: String?,controller:UIViewController?,type:Int?){
        // this previous message inside gttoast is added as per our need it is not a inbuilt function of the gttoast
        
//        if GTToast.previousMessage == msg {
//            return
//        }
        
        var imageView : UIImage?
        var color = UIColor()
        
        if type == 1 {
            imageView = UIImage(named: "Group 184")!
            color = UIColor(hexString: "00930F")
        } else if type == 2 {
            imageView = UIImage(named: "groupcancel")!
            color = UIColor(hexString: "F24040")
        } else if type == 0 {
            imageView = UIImage(named: "loading (1)")!
            color = UIColor(hexString: "37C1F0")
        }

        var bottom = CGFloat()
        if(UIScreen.main.bounds.height > 700) {
                    bottom = 110
                }else {
                    bottom = 70
                }
        
        let config = GTToastConfig(cornerRadius: 4, font: UIFont(name: "Lato-Regular", size: 16) ?? UIFont(), textColor: .white, textAlignment: .left, backgroundColor: color, animation: GTToastAnimation(rawValue: 2)!.animations(), displayInterval: 2, bottomMargin: bottom,imageMargins:UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5),imageAlignment: .left)
        
                toast = GTToast.create(msg ?? "",
                    config: config,
                    image: imageView)
                toast.show()
    }
    
    
    // userDetail = SearchUserResponse()
    //Save application lanaguage
    func saveAppLanguage(withLanguage appLang: String)->Void{
        UserDefaults.standard.set(appLang, forKey: K_APP_LANGUAGE)
        UserDefaults.standard.synchronize()
        changeApplicationLanguage(language: appLang)
    }
    
    //Change Application language : This WIll be used when toggle on off
    func changeApplicationLanguage(language: String) -> Void{
        UserDefaults.standard.set(language, forKey: K_APP_LANGUAGE)
        UserDefaults.standard.synchronize()
        setUIViewAccToLanguage(language: language)
    }
    
    func setUIViewAccToLanguage(language: String) {
        if language == "ar" || language == "ur"{
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        } else {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
//        Singleton.handleTabDelegate?.localize(rtl: true)
        bundle = Bundle(path: Bundle.main.path(forResource: (language == "en" ? language : "Base"), ofType: "lproj")!)
        changeAppLanguage(to: language)
    }
    
    func changeAppLanguage(to languageCode: String) {
        UserDefaults.standard.set(languageCode, forKey: "selectedLanguage")
        Bundle.setLanguage(languageCode)
        selectedLanguage = languageCode
        
    }
    
    //GET Current Language
    func getCurrentLanguage() -> String {
        var lang = ""
        if let data = UserDefaults.standard.string(forKey: K_APP_LANGUAGE) as? String {
            lang = language!
            setUIViewAccToLanguage(language: lang)
        }
        else{
            print("There is an issue")
        }
        return lang
    }
}

