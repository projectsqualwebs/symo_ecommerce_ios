//
//  ViewController.swift
//  Symo New
//
//  Created by Apple on 30/03/21
//

import UIKit
import CoreLocation


class ViewController: UIViewController, CLLocationManagerDelegate, Confirmation {
    func confirmationSelection(action: Int) {
        if(action == 1){
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        }
    }
    
    @IBOutlet weak var TagLineLabel: UILabel!
    
    lazy var locationManager = CLLocationManager()
    var timer = Timer()
    var isLanguageChangedRedirection = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NavigationController.shared.getUserCurrentLocation()
        NavigationController.shared.changeStatusBar(color: UIColor.clear)
        //        Singleton.shared.language = UserDefaults.standard.string(forKey: K_APP_LANGUAGE)
        self.TagLineLabel.text = "Discover the best local and international luxury brands.".localizableString(loc: Singleton.shared.language ?? "en") + "\n" + "Be the first to buy latest arrivals.".localizableString(loc: Singleton.shared.language ?? "en")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.showErrorMessage(_:)), name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleUserLocation), name: NSNotification.Name(N_HANDLE_LOCATION_PERMISSION), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePushNotification(_:)), name: NSNotification.Name(N_NOTIFICATION_DATA), object: nil)
        
        if(self.isLanguageChangedRedirection == false){
            NavigationController.shared.geCurrencyData()
            
            let val =  UserDefaults.standard.value(forKey: UD_SELECTED_CURRENCY) as? String
            if val == nil {
                UserDefaults.standard.setValue("aed", forKey: UD_SELECTED_CURRENCY)
                Singleton.shared.selectedCurrencySymbol = "AED"
            }else {
                Singleton.shared.selectedCurrency = val ?? ""
                
                if val == "aed" {
                    Singleton.shared.selectedCurrencySymbol = "AED"
                } else if val == "pound"{
                    Singleton.shared.selectedCurrencySymbol = "£"
                } else if val == "usd" {
                    Singleton.shared.selectedCurrencySymbol = "$"
                } else if val == "euro" {
                    Singleton.shared.selectedCurrencySymbol = "€"
                } else if val == "bhd" {
                    Singleton.shared.selectedCurrencySymbol = "BHD"
                }
            }
            
            NavigationController.shared.getCountries { (val) in
                NavigationController.shared.getCities(countryId: val[0].id ?? 0) { (val) in
                    NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_CITY), object: nil)
                }
            }
            
            self.handleRedirection()
        } else {
            self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(handleLocalizeChange), userInfo: nil, repeats: false)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if(self.isLanguageChangedRedirection == false){
            Singleton.shared.setUIViewAccToLanguage(language: Singleton.shared.language ?? "en")
            self.handleUserLocation()
            if(Singleton.shared.userDetail.id != nil){
                NavigationController.shared.getCartItem(view: self.view) { (data) in
                }
            }
        }
        
        //for setting screen details data in singleton for future use.
        let screenheight = UIScreen.main.bounds.height
        let screenWidth = UIScreen.main.bounds.width
        let window = UIApplication.shared.windows[0]
        let safeFrame = window.safeAreaLayoutGuide.layoutFrame
        let topSafeAreaHeight = safeFrame.minY
        let bottomSafeAreaHeight = window.frame.maxY - safeFrame.maxY
        let safeHeight = screenheight - topSafeAreaHeight - bottomSafeAreaHeight
        
        
        print("Screen Height : \(screenheight)")
        print("Screen Width : \(screenWidth)")
        print("top safe area space : \(topSafeAreaHeight)")
        print("Bottom safe area space : \(bottomSafeAreaHeight)")
        print("Safe Area Heght: \(safeHeight)")
        
        let screenData = ScreenDetail(screenHeight:screenheight , screenWidth: screenWidth,topSafeArea: topSafeAreaHeight,bottomSafeArea: bottomSafeAreaHeight, safeHeight: safeHeight)
        
        Singleton.shared.screenData = screenData
    }
    
    
    
    @objc func handleLocalizeChange(){
        self.timer.invalidate()
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    func handleRedirection(){
        if let firstTime = UserDefaults.standard.value(forKey: UD_FIRST_TIME) as? Bool {
            if(firstTime){
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SplashScreenViewController") as! SplashScreenViewController
                self.navigationController?.pushViewController(myVC, animated: true)
            }else {
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }else {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SplashScreenViewController") as! SplashScreenViewController
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
    @objc func handleTimer(){
        self.handleUserLocation()
    }
    
    @objc func handleUserLocation(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_HANDLE_LOCATION_PERMISSION), object: nil)
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways,.authorizedWhenInUse:
            locationManager.delegate = self
            if let loc = locationManager.location {
                Singleton.shared.userLocation = loc
                //  self.handleRedirection()
                self.timer.invalidate()
            }
        case .notDetermined:
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            self.timer.invalidate()
            self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(handleTimer), userInfo: nil, repeats: false)
        case .denied:
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationPopupViewController") as! ConfirmationPopupViewController
            myVC.confirmationDelegate = self
            myVC.secondTxt = true
            myVC.confirmationText = "Allow Location"
            myVC.detailText = "Symo need to access your location so that you can access nearby businesses and shops."
            myVC.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(myVC, animated: true, completion: nil)
        case .restricted:
            // Nothing you can do, app cannot use location services
            break
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleUserLocation), name: NSNotification.Name(N_HANDLE_LOCATION_PERMISSION), object: nil)
    }
    
    @objc func showErrorMessage(_ notif: NSNotification){
        if let val = notif.userInfo?["msg"] as? String{
            if(val != "" && val != " "){
                Singleton.shared.showToast(msg:val.localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: notif.userInfo?["type"] as? Int)
            }
        }
    }
    
    @objc func handlePushNotification(_ notif: Notification) {
        var orderData = OrderResponse()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_NOTIFICATION_DATA), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePushNotification(_:)), name: NSNotification.Name(N_NOTIFICATION_DATA), object: nil)
        if let data = notif.userInfo{
            if let requestData = data["info"] as? [String:Any]{
                if  let data = requestData["data"] as? String {
                    let stringToData = Data(data.utf8)
                    let decoder = JSONDecoder()
                    do {
                        orderData = try decoder.decode(OrderResponse.self, from: stringToData)
                    }catch {
                        print(error.localizedDescription)
                    }
                    let type = requestData["type"] as! String
                    var controller:AnyClass?
                    self.removePreviousView()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                        
                        if type == "17" {
                            if let viewControllers = self.navigationController?.viewControllers {
                                for vc in viewControllers {
                                    if vc.isKind(of: TabbarViewController.classForCoder()) {
                                        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
                                        myVC.vendorId = orderData.business_id ?? orderData.vendor_id ?? 0
                                        self.navigationController?.pushViewController(myVC, animated: true)
                                        return
                                    }
                                }
                            }else {
                                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
                                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
                                    myVC.vendorId = orderData.business_id ?? orderData.vendor_id ?? 0
                                    self.navigationController?.pushViewController(myVC, animated: true)
                                }
                            }
                        } else {
                            if let viewControllers = self.navigationController?.viewControllers {
                                for vc in viewControllers {
                                    if vc.isKind(of: TabbarViewController.classForCoder()) {
                                        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
                                        myVC.orderData = orderData
                                        myVC.notificationRedirection = true
                                        self.navigationController?.pushViewController(myVC, animated: true)
                                        return
                                    }
                                }
                            }else {
                                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
                                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
                                    myVC.orderData = orderData
                                    myVC.notificationRedirection = true
                                    self.navigationController?.pushViewController(myVC, animated: true)
                                }
                            }
                        }
                    })
                }
            }
        }
        
    }
    
    func removePreviousView(){
        navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
            if vc.isKind(of: OrderDetailViewController.self) {
                return true
            }else {
                return false
            }
        })
    }
}

