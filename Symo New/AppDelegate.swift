//
//  AppDelegate.swift
//  Symo New
//
//  Created by Apple on 30/03/21.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import Firebase
import UserNotifications
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import Firebase
import FacebookCore
import Clarity

var bundle = Bundle(path: Bundle.main.path(forResource: (Singleton.shared.language == "en" ? Singleton.shared.language : "Base"), ofType: "lproj")!)

@available(iOS 13.0, *)
@main
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {
    
    
    var window: UIWindow?
    //   var tabBarController : UITabBarController?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window?.backgroundColor = .white
        IQKeyboardManager.shared.enable = true
        // UserDefaults.standard.synchronize()
        // Bundle.main.path(forResource: "ar", ofType: "lproj")
        self.initializeFirebaseNotification(application: application)
        //        Settings.shared.appID = "541016983730135"
        //UIView.appearance().semanticContentAttribute = .forceRightToLeft
        //        Settings.shared.isAdvertiserTrackingEnabled = true
        
        GMSServices.provideAPIKey(GOOGLE_API_KEY)
        GMSPlacesClient.provideAPIKey(GOOGLE_API_KEY)
        
        URLCache.shared.removeAllCachedResponses()
        //for localization
        if let language = Singleton.shared.language{
            if language == "ar" {
                UIView.appearance().semanticContentAttribute = .forceRightToLeft
            } else {
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
            }
            bundle = Bundle(path: Bundle.main.path(forResource: (language == "en" ? language : "Base"), ofType: "lproj")!)
            Singleton.shared.changeAppLanguage(to: language)
        }
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        let clarityConfig = ClarityConfig(projectId: "n8n6t2h3zf")
        ClaritySDK.initialize(config: clarityConfig)
        return true
        
        //FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_LOCATION_PERMISSION), object: nil)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        URLCache.shared.removeAllCachedResponses()
    }
    
    
    func initializeFirebaseNotification(application: UIApplication){
        FirebaseApp.configure()
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        Messaging.messaging().isAutoInitEnabled = true
        
        Messaging.messaging().token { token, error in
            if let error = error {
                print("Error fetching FCM registration token: \(error)")
            } else if let token = token {
                print("FCM registration token: \(token)")
            }
        }
        
    }
    
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        guard let url = URLContexts.first?.url else {
            return
        }
    }
    
}



//MARK: Notification Delegate
@available(iOS 13.0, *)
extension AppDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("fcmToken : \(fcmToken ?? "")")
        UserDefaults.standard.setValue(fcmToken ?? "", forKey: UD_ACCESS_TOKEN)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.hexString
        UserDefaults.standard.setValue(deviceTokenString, forKey: UD_DEVICE_TOKEN)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        completionHandler([.alert, .badge, .sound])
    }
    
    //these will call when user tap on notification (it open app)
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let token = UserDefaults.standard.value(forKey: UD_TOKEN) as? String {
                NotificationCenter.default.post(name: NSNotification.Name(N_NOTIFICATION_DATA),object: nil,userInfo: ["info": userInfo])
            }
        }
    }
    
    //called when notification is recived
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
       
    }
}




//MARK: Deep Linking //merge
extension AppDelegate {
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            if let url = userActivity.webpageURL {
                // Handle the URL
                handleIncomingLink(url)
                return true
            }
        }
        return false
    }
    
    
    private func handleIncomingLink(_ url: URL) {
        // Parse the URL and extract the ID
        print("Incoming URL: \(url)")
        var urlString = url.absoluteString
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
            if  urlString.contains("https://gosymo.com/"){
                urlString = urlString.replacingOccurrences(of: "https://gosymo.com", with: "")
                
                
                
                
                //MARK: Store Deeplink (https://gosymo.com/user/shop/AlshamekStore/96)
                if urlString.contains("user/shop/"){
                    urlString = urlString.replacingOccurrences(of: "user/shop/", with: "")
                    //now url stroing will be (AlshamekStore/96)  now get 96 from it
                    if let lastPathComponent = urlString.components(separatedBy: "/").last {
                        // Extract "96" from the last path component
                        let id = lastPathComponent
                        print("Extracted ID: \(id)")
                        
                        let sb = UIStoryboard(name: "Main", bundle: nil)
                        if let navigationController = self.window?.rootViewController as? UINavigationController {
                            if let vc = sb.instantiateViewController(withIdentifier: "CategoryViewController") as? CategoryViewController{
                                
                                guard let vendorId  = Int(id) else{return}
                                vc.vendorId = vendorId
                                navigationController.pushViewController(vc, animated: true)
                            }
                        }
                    } else {
                        print("Failed to extract ID")
                    }
                }
                
                
                //MARK: Product DeepLink (https://gosymo.com/user/product/giftsandFlower/test)
                if urlString.contains("user/product/"){
                    urlString = urlString.replacingOccurrences(of: "user/product/", with: "")
                    //now url string will be (giftsandFlower/test)
                    let components = urlString.components(separatedBy: "/")
                    if components.count >= 2 {
                        
                        let firstPathComponent = components.first!
                        let lastPathComponent = components.last!
                        
                        let sb = UIStoryboard(name: "Main", bundle: nil)
                        if let navigationController = self.window?.rootViewController as? UINavigationController {
                            if let vc = sb.instantiateViewController(withIdentifier: "SingleProductViewController") as? SingleProductViewController{
                                
                                
                                vc.subdomain = firstPathComponent
                                vc.slug = lastPathComponent
                                navigationController.pushViewController(vc, animated: true)
                            }
                        }
                    } else {
                        print("Failed to extract ID")
                    }
                }
            }
        }
    }
}
