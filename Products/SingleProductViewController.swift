//
//  SingleProductViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 27/04/21
//

import UIKit
import Cosmos
import ImageSlideshow
import SkeletonView

protocol ManageCartButton {
    func manageButton(idArray : [Int],isReload:Bool)
}

class SingleProductViewController: UIViewController, PostReview, SelectDate ,ManageCartButton, ImageSlideshowDelegate,Confirmation {
    
    
    
    //MARK: Delegate Function
    func manageButton(idArray: [Int],isReload:Bool) {
        if isReload {
            if idArray.contains(self.selectedProduct.id ?? 0) {
                self.addToCartButton.setTitle("Go to Cart".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
            } else {
                self.addToCartButton.setTitle("Add to Cart".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
            }
            self.bagCount.text = "\(K_BAG_COUNT)"
        }
    }
    
    func selectedDate(timestamp: Date) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CheckoutViewController") as! CheckoutViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    func addProductReview() {
        self.getSingleProductDetail()
    }
    
    func confirmationSelection(action: Int) {
        if(action == 1){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            myVC.redirectBack = true
            movetoCheckout = true
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: DesignableUILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var reviewCount: UIButton!
    @IBOutlet weak var favouriteImage: UIImageView!
    @IBOutlet weak var productPrice: DesignableUILabel!
    @IBOutlet weak var bagCount: DesignableUILabel!
    @IBOutlet weak var headingLabel: DesignableUILabel!
    @IBOutlet weak var varientTable: ContentSizedTableView!
    @IBOutlet weak var variantView: UIView!
    @IBOutlet weak var addToCartButton: CustomButton!
    @IBOutlet weak var noProductLabel: DesignableUILabel!
    @IBOutlet weak var productDescription: DesignableUILabel!
    @IBOutlet weak var youMayAlsoLikeLabel: DesignableUILabel!
    @IBOutlet weak var buyNowButton: CustomButton!
    @IBOutlet weak var specificationsLabel: DesignableUILabel!
    @IBOutlet weak var similarProductsView: UIView!
    @IBOutlet weak var imageSlideShow: ImageSlideshow!
    @IBOutlet weak var storeName: UIButton!
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    
    var selectedProduct  = ProductResponse()
    var productData = SingleProductResponse()
    var subdomain = String()
    var slug = ""
    var selectedVarientId = Int()
    var isAddedToBag = false
    var isAlreadyInCart = false
    var movetoCheckout = false
    var isFirstTime = true
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        NavigationController.manageDelegate = self
        ShoppingBucketViewController.manageDelegate = self
        favouriteImage.image = #imageLiteral(resourceName: "heart_outline")
        self.noProductLabel.isHidden = true
        self.noProductLabel.text = "No Product Found".localizableString(loc: Singleton.shared.language ?? "en")
        self.youMayAlsoLikeLabel.text = "You may also like".localizableString(loc: Singleton.shared.language ?? "en")
        self.buyNowButton.setTitle("Buy now".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.specificationsLabel.text = "Specifications".localizableString(loc: Singleton.shared.language ?? "en")
        self.addToCartButton.setTitle("Add to cart".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.imageSlideShow.slideshowInterval = 0.0
        self.imageSlideShow.draggingEnabled = true
        self.imageSlideShow.pageIndicatorPosition = .init(horizontal: .center, vertical: .bottom)
        self.imageSlideShow.contentScaleMode = UIViewContentMode.scaleAspectFill
        self.imageSlideShow.delegate = self
        self.storeName.isUserInteractionEnabled = false
        self.checkProductAlreadyAvailable()
        self.getSingleProductDetail()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
        
        if(K_BAG_COUNT == 0){
            self.bagCount.isHidden = true
        }else {
            self.bagCount.text = "\(K_BAG_COUNT)"
            self.bagCount.isHidden = false
        }
        
        if (movetoCheckout){
            movetoCheckout = false
            self.buyNowAction(self)
        }
        if isFirstTime{
            showSkeleton()
            isFirstTime = false
        }
    }
    
    override func viewWillLayoutSubviews() {
        self.view.isUserInteractionEnabled = true
    }
    
    
    //MARK: Functions
    //for showing skeleton loader
    func showSkeleton(){
        self.view.isSkeletonable = true
        self.view.showAnimatedGradientSkeleton()
        collectionView.showAnimatedGradientSkeleton()
        
    }
    
    
    func hideSkeleton(){
        self.view.hideSkeleton()
        collectionView.hideSkeleton()
    }
    
    
    func checkProductAlreadyAvailable(){
        if let cartItems = Singleton.shared.cartData.cart?.cart_items {
            for cartItem in cartItems {
                if cartItem.Product?.id == self.selectedProduct.id{
                    print("cart already contain these product")
                    self.addToCartButton.setTitle("Go to Cart".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
                    isAlreadyInCart = true
                    return
                }
            }
        }
    }
    
    func getSingleProductDetail(){
        var slug = ""
        if self.slug != ""{
            //these is used for deep link and direct open single product screen
            slug = self.slug
            self.slug = ""
        }else if(self.selectedProduct.parent_product_id == 0 && self.selectedProduct.parent_product_slug == ""){
            slug = self.selectedProduct.slug ?? ""
        }else {
            slug = self.selectedProduct.parent_product_slug ?? ""
        }
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_SINGLE_PRODUCT +  "\(self.subdomain)/\(slug)" + "?currency=\(Singleton.shared.selectedCurrency)", method: .post, parameter: ["lang":Singleton.shared.language], objectClass: GetSingleProduct.self, requestCode: U_GET_SINGLE_PRODUCT, userToken: nil, reloadData: false) { (response) in
            if(response.response.count > 0){
                self.productData = response.response[0]
                self.hideSkeleton()
                
                if (self.selectedProduct.id != nil){
                    self.selectedProduct = self.productData.business_product
                }
                
                if self.productData.business_product.stock_status == 1 {
                    if !self.isAlreadyInCart {
                        self.addToCartButton.setTitle("Add to Cart".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
                    }
                    
                    self.addToCartButton.isUserInteractionEnabled = true
                    self.buyNowButton.isHidden = false
                } else {
                    Singleton.shared.showToast(msg: "Product out of stock".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                    self.addToCartButton.setTitle("Out of stock".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
                    self.addToCartButton.isUserInteractionEnabled = false
                    self.buyNowButton.isHidden = true
                }
                
                if self.productData.product_variant.count == 0 {
                    
                    var currentPrice = convertPriceInDouble(price: self.selectedProduct.price)
                    
                    let discountPrice = convertPriceInDouble(price: self.selectedProduct.discounted_price )
                    //                    let discountPrice = self.selectedProduct.discount ?? 0
                    
                    var input = [SDWebImageSource]()
                    for i in 0..<(self.productData.business_product.product_images?.count ?? 0) {
                        input.append(SDWebImageSource(urlString: U_IMAGE_BASE + (self.productData.business_product.product_images?[i].image ?? ""))!)
                    }
                    self.imageSlideShow.setImageInputs(input)
                    if (self.selectedProduct.offer_qty ?? 0) > 0 && (self.selectedProduct.offer_valid_till ?? 0) > Int(NSDate().timeIntervalSince1970) {
                        
                        self.productPrice.attributedText = self.handlePrice(price: "\(currentPrice)" , cut: "\(self.selectedProduct.offer_price ?? 0.00)")
                        
                    } else if (discountPrice != 0.00) && ((discountPrice) != (currentPrice)){
                        self.productPrice.attributedText = self.handlePrice(price: "\(currentPrice)" , cut: "\(discountPrice)")
                    } else {
                        self.productPrice.text = "\(Singleton.shared.selectedCurrencySymbol)" + " \(currentPrice.addComma)"
                    }
                    
                    self.handleFavourite()
                } else {
                    self.selectedVarientId = self.productData.product_variant[0].variation_id ?? 0
                    self.getProductVariant(index: self.productData.product_variant[0].variation_id ?? 0)
                    self.varientTable.reloadData()
                }
                self.initializeView()
                self.view.isUserInteractionEnabled = true
                NotificationCenter.default.post(name: NSNotification.Name("refresh_review_table"), object: response.response)
            }
        }
    }
    
    func getProductVariant(index : Int){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_VARIARANT_STOCK + "?currency=\(Singleton.shared.selectedCurrency)", method: .post, parameter: ["product_id": self.selectedProduct.parent_product_id == 0 ? self.selectedProduct.id ?? 0:self.selectedProduct.parent_product_id ?? 0,"lang":Singleton.shared.language ?? "","variation_id": index], objectClass: GetProductVariationResponse.self, requestCode: U_GET_VARIARANT_STOCK, userToken: nil, reloadData: false) { (response) in
            let val = response.response ?? ProductVariantResponse()
            
            if val.stock_status == 1 {
                self.favouriteImage.image = #imageLiteral(resourceName: "heart_outline")
                self.favouriteImage.changeTint(color: .white)
                //                self.selectedVarientId = index
                self.handleFavourite()
                self.varientTable.reloadData()
                
                let discountPrice =  val.discounted_price ?? 0
                //                let discountPrice = val.discount
                
                if (self.selectedProduct.offer_qty ?? 0) > 0 && (self.selectedProduct.offer_valid_till ?? 0) > Int(NSDate().timeIntervalSince1970) {
                    
                    self.productPrice.attributedText = self.handlePrice(price: "\(val.price ?? 0.00)" , cut: "\(discountPrice)")
                    
                } else if discountPrice != 0.00 && discountPrice != (val.price ?? 0.00){
                    self.productPrice.attributedText = self.handlePrice(price: "\(val.price ?? 0.00)" , cut: "\(discountPrice)")
                    
                } else {
                    self.productPrice.text = "\(Singleton.shared.selectedCurrencySymbol)" + " \(val.price?.addComma ?? "0.00")"
                }
                
                if (val.images?.count ?? 0) > 0 {
                    if((val.images?[0].image ?? "").contains("http")){
                        self.productImage.sd_setImage(with: URL(string: (val.images?[0].image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
                    }else {
                        self.productImage.sd_setImage(with: URL(string: U_IMAGE_BASE + (val.images?[0].image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
                    }
                }
                
                var input = [SDWebImageSource]()
                for i in 0..<(val.images?.count ?? 0) {
                    if((val.images?[i].image ?? "").contains("http")){
                        input.append(SDWebImageSource(urlString:(val.images?[i].image ?? ""))!)
                    } else {
                        input.append(SDWebImageSource(urlString: U_IMAGE_BASE + (val.images?[i].image ?? ""))!)
                    }
                }
                self.imageSlideShow.setImageInputs(input)
                if !self.isAlreadyInCart{
                    self.addToCartButton.setTitle("Add to Cart".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
                }
                self.addToCartButton.isUserInteractionEnabled = true
                self.buyNowButton.isHidden = false
            } else {
                Singleton.shared.showToast(msg: "Product out of stock".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                self.addToCartButton.setTitle("Out of stock".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
                self.addToCartButton.isUserInteractionEnabled = false
                self.buyNowButton.isHidden = true
            }
            ActivityIndicator.hide()
        }
    }
    
    func handleFavourite(){
        NavigationController.shared.getWishlistItem { (response) in
            for val in response{
                if(val.product_id == self.selectedProduct.id && val.variation_id == self.selectedVarientId){
                    self.favouriteImage.image = #imageLiteral(resourceName: "like")
                    self.favouriteImage.changeTint(color: .white)
                }
            }
        }
    }
    
    //initialize view
    func initializeView(){
        self.favouriteImage.tintColor = .white
        //        self.headingLabel.text = self.selectedProduct.vendor_details?.name ?? self.selectedProduct.vendorDetails?.name ?? self.selectedProduct.vendor_details?.name_en ?? ""
        self.headingLabel.text = self.productData.business_product.vendor?.name ?? self.productData.business_product.vendor?.name_en ?? ""
        if self.headingLabel.text == "" {
            self.storeName.isUserInteractionEnabled = false
        } else {
            self.storeName.isUserInteractionEnabled = true
        }
        self.productDescription.text = selectedProduct.description
        self.collectionView.reloadData()
        self.ratingView.rating = Double(self.productData.business_product.avg_rating ?? "0.00") ?? 0.00
        
        if self.productData.all_reviews.count == 0 {
            self.reviewCount.setTitle("No review found".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        } else{
            self.reviewCount.setTitle("\(self.productData.all_reviews.count)" + " reviews".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        }
        self.view.backgroundColor = .white
        if((self.selectedProduct.default_image ?? "").contains("http")){
            self.productImage.sd_setImage(with: URL(string: (self.selectedProduct.default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }else {
            self.productImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (self.selectedProduct.default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }
        self.productName.text = selectedProduct.name ?? self.productData.business_product.name ?? ""
        self.view.isUserInteractionEnabled = true
    }
    
    //show popup when user was not logged in try to checkout
    func showPopup(title: String, msg: String) {
        
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationPopupViewController") as! ConfirmationPopupViewController
        myVC.confirmationDelegate = self
        myVC.confirmationText = title.localizableString(loc: Singleton.shared.language ?? "en")
        myVC.detailText = msg.localizableString(loc: Singleton.shared.language ?? "en")
        myVC.secondButtonTitle = "Not now".localizableString(loc: Singleton.shared.language ?? "en")
        myVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(myVC, animated: true, completion: nil)
        
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        reloadBucket = true
    }
    
    @IBAction func cartAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ShoppingBucketViewController") as! ShoppingBucketViewController
        myVC.showBackButton = true
        myVC.shouldReload = self.isAddedToBag
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func addToCartAction(_ sender: Any) {
        if(self.addToCartButton.titleLabel?.text == "Go to Cart".localizableString(loc: Singleton.shared.language ?? "en")){
            self.cartAction(self)
        }else {
            if(Singleton.shared.userDetail.id != nil){ //for logged user
                NavigationController.shared.addToBag(controller: self, qty: 1, variationId: self.selectedVarientId, productId: self.selectedProduct.parent_product_id == 0 ? self.selectedProduct.id ?? 0:self.selectedProduct.parent_product_id ?? 0) {(val) in
                    self.bagCount.text = "\(K_BAG_COUNT)"
                    self.bagCount.isHidden = false
                    self.dismiss(animated: true, completion: nil)
                    Singleton.shared.showToast(msg:"Product added successfully".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 1)
                    self.isAddedToBag = true
                    self.addToCartButton.setTitle("Go to Cart".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
                }
            } else { // for guest user
                NavigationController.shared.guestAddToBag(controller: self, qty: 1, variationId: self.selectedVarientId, productId: self.selectedProduct.parent_product_id == 0 ? self.selectedProduct.id ?? 0:self.selectedProduct.parent_product_id ?? 0) {(val) in
                    self.bagCount.text = "\(K_BAG_COUNT)"
                    self.bagCount.isHidden = false
                    self.dismiss(animated: true, completion: nil)
                    Singleton.shared.showToast(msg:"Product added successfully".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 1)
                    self.isAddedToBag = true
                    self.addToCartButton.setTitle("Go to Cart".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
                }
            }
        }
    }
    
    @IBAction func singleStoreAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
        myVC.vendorId = self.selectedProduct.vendor_details?.id ?? self.selectedProduct.vendorDetails?.id ?? 0
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    // save or remove product as favourite
    @IBAction func markFavouriteAction(_ sender: Any) {
        if(Singleton.shared.userDetail.id != nil){
            if(favouriteImage.image == #imageLiteral(resourceName: "heart_outline")){
                NavigationController.shared.addRemoveProductAsFavourite(view: self.view, id: selectedProduct.id ?? 0, variantId: self.selectedVarientId, action: 1) { (val) in
                    self.favouriteImage.image = #imageLiteral(resourceName: "like")
                    self.favouriteImage.changeTint(color: .white)
                    Singleton.shared.showToast(msg: val.localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 0)
                    Singleton.shared.wishlistResponse = []
                    self.handleFavourite()
                }
            }else {
                NavigationController.shared.addRemoveProductAsFavourite(view: self.view, id: selectedProduct.id ?? 0, variantId: self.selectedVarientId, action: 2) { (val) in
                    self.favouriteImage.image = #imageLiteral(resourceName: "heart_outline")
                    self.favouriteImage.changeTint(color: .white)
                    Singleton.shared.showToast(msg: val.localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 0)
                    Singleton.shared.wishlistResponse = []
                    self.handleFavourite()
                }
            }
        } else { //if user is not logged in show alert for login
            self.showPopup(title: "Alert".localizableString(loc: Singleton.shared.language ?? "en"), msg: "Please login to proceed".localizableString(loc: Singleton.shared.language ?? "en"))
        }
    }
    
    @IBAction func bagAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ShoppingBucketViewController") as! ShoppingBucketViewController
        myVC.showBackButton = true
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func reviewAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ReviewViewController") as! ReviewViewController
        PostReviewViewController.postDelegate = self
        myVC.productData = self.productData
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func buyNowAction(_ sender: Any) {
        if(Singleton.shared.userDetail.id != nil){
            NavigationController.shared.addToBag(controller: self, qty: 1, variationId: self.selectedVarientId, productId: self.selectedProduct.parent_product_id == 0 ? self.selectedProduct.id ?? 0:self.selectedProduct.parent_product_id ?? 0) {(val) in
                K_BAG_COUNT = K_BAG_COUNT + 1
                self.bagCount.text = "\(K_BAG_COUNT)"
                
                self.bagCount.isHidden = false
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CheckoutViewController") as! CheckoutViewController
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        } else {
            self.showPopup(title: "Alert".localizableString(loc: Singleton.shared.language ?? "en"), msg: "Please login to proceed".localizableString(loc: Singleton.shared.language ?? "en"))
        }
    }
}

//MARK:  Collection View
extension SingleProductViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(self.productData.similar_products.count == 0 && !self.collectionView.sk.isSkeletonActive){
            self.similarProductsView.isHidden = true
        }else {
            self.similarProductsView.isHidden = false
        }
        return self.productData.similar_products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionCell", for: indexPath) as! CategoryCollectionCell
        let val = self.productData.similar_products[indexPath.row]
        cell.shopImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (val.default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        cell.shopName.text = val.name
        
        let currentPrice = convertPriceInDouble(price: val.price)
        var discountPrice = convertPriceInDouble(price: val.discounted_price)
        //        let discountPrice = val.discount ?? 0
        
        if (val.offer_qty ?? 0) > 0 && (val.offer_valid_till ?? 0) > Int(NSDate().timeIntervalSince1970) {
            
            cell.shopPrice.attributedText = self.handlePrice(price: "\(currentPrice)" , cut: "\(val.offer_price ?? 0.00)")
            
        } else if (discountPrice != 0.00) && (discountPrice) != (currentPrice){
            cell.shopPrice.attributedText = self.handlePrice(price: "\(currentPrice)" , cut: "\(discountPrice)")
        } else {
            cell.shopPrice.text = "\(Singleton.shared.selectedCurrencySymbol)" + " \(currentPrice.addComma)"
        }
        cell.shopRating.text = val.avg_rating ?? "0.0"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedProduct = self.productData.similar_products[indexPath.row]
        self.subdomain = self.productData.similar_products[indexPath.row].vendor_details?.subdomain ?? ""
        self.getSingleProductDetail()
    }
    
    //setting size of collection view
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 200)
    }
}

//MARK: Table View
extension SingleProductViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.productData.product_variant.count == 0){
            self.variantView.isHidden = true
        }else {
            self.variantView.isHidden = false
        }
        return self.productData.product_variant.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableCell") as! MenuTableCell
        let val = self.productData.product_variant[indexPath.row]
        cell.menuLabel.text = ""
        for i in val.VariationValues{
            if((i.attribute_text == "Color") || (i.attribute_text == "Ram") || (i.attribute_text == "Internal Storage") || (i.attribute_text == "Size")){
                let att = (i.attribute_text ?? "") + " | " + (i.value_text ?? "")
                cell.menuLabel.text = (((cell.menuLabel.text ?? "") == "") ? ((cell.menuLabel.text ?? "")):(cell.menuLabel.text ?? "") + ", ") + att
            }
        }
        if(self.selectedVarientId == val.variation_id){
            cell.backView.borderColor = K_BLUE_COLOR
            //K_PINK_COLOR
            cell.menuLabel.textColor = .black
        }else {
            cell.backView.borderColor = .lightGray
            cell.menuLabel.textColor =  .lightGray
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.getProductVariant(index: self.productData.product_variant[indexPath.row].variation_id ?? 0)
        self.selectedVarientId = self.productData.product_variant[indexPath.row].variation_id ?? 0
    }
    
}



extension SingleProductViewController: SkeletonCollectionViewDataSource {
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "CategoryCollectionCell"
    }
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell? {
        let cell = skeletonView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionCell", for: indexPath) as? CategoryCollectionCell
        return cell
    }
    
}
