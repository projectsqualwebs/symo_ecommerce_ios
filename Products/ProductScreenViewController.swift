//
//  ProductScreenViewController.swift
//  Symo New
//
//  Created by Apple on 17/04/21
//

import UIKit
import SDWebImage

protocol HandleBlurView {
    func showHideBlurView(type:Int)
}

protocol HandleShopProductFilter {
    func handleFilter(filterData:[String:Set<Int>])
}

class ProductScreenViewController: UIViewController, SortData, HandleBlurView, SelectFromPicker,HandleShopProductFilter {
    //MARK: Delegate functions
    func handleFilter(filterData: [String : Set<Int>]) {
        
        if let color = filterData["Color"]{
            self.filterParam["color"] = Array(color)
        }
        
        if let size = filterData["Size"]{
            self.filterParam["size"] = Array(size)
        }
        
        if let price = filterData["price"]{
            self.filterParam["price"] = Array(price)
        }
        
        if let rating = filterData["rating"] {
            self.filterParam["rating"] = rating
        }
        
        self.getShopProduct()
    }
    
    
    func selectedPickerData(val: String, pos: Int) {
        K_SELECTED_SORTBY = pos
        self.sortProductList(option: pos)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func showHideBlurView(type:Int) {
        if type == 1 {
            self.blurView.isHidden = true
        } else if type == 2{
            self.blurView.isHidden = false
        }
    }
    
    
    func sortShopList(option: Int) {
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if(scrollView == self.productCollectionView){
            if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height/1.2) && !isLoadingList){
                if(lastPage > currentPage){
                    self.loadMoreButton.setTitle("Load More Products", for: .normal)
                } else {
                    self.loadMoreButton.setTitle("No More Products", for: .normal)
                }
                self.loadMoreButtonHeight.constant = 50
                self.loadMoreButton.isHidden = false
                
            } else {
                self.loadMoreButtonHeight.constant = 0
                self.loadMoreButton.isHidden = true
            }
            
        }
    }
    
    func sortProductList(option: Int) {
        self.getShopProduct()
    }
    
    func loadMoreItemsForList(){
        if isMostSelling == false {
            currentPage += 1
            self.getShopProduct()
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var productCollectionView: UICollectionView!
    @IBOutlet weak var bagCount: DesignableUILabel!
    @IBOutlet weak var productListLabel: DesignableUILabel!
    @IBOutlet weak var filterLabel: DesignableUILabel!
    @IBOutlet weak var sortLabel: DesignableUILabel!
    
    @IBOutlet weak var noProductLabel: DesignableUILabel!
    @IBOutlet weak var filterStackView: UIStackView!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var sortStackView: UIStackView!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var loadMoreButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var loadMoreButton: CustomButton!
    @IBOutlet weak var storeName: DesignableUILabel!
    @IBOutlet weak var filterMainView: UIView!
    @IBOutlet weak var filterMainViewHeight: NSLayoutConstraint!
    
    var mostSellingData = [ProductResponse]()
    var productData  = [ProductResponse]()
    var subdomain = String()
    var isMostSelling = false
    var currentPage : Int = 1
    var isLoadingList : Bool = false
    var shopName = String()
    var lastPage = 1
    
    var filterParam:[String:Any] = [
        "color": [],
        "price": [],
        "size": [],
        "rating": "any"
    ]
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.blurView.addBlurEffect()
        FilterScreenViewController.shopProductDelegate = self
        self.storeName.text = self.shopName
        FilterScreenViewController.handleBlurView = self
        self.blurView.isHidden = true
        self.productListLabel.text = "Product List".localizableString(loc: Singleton.shared.language ?? "en")
        self.filterLabel.text = "Filter".localizableString(loc: Singleton.shared.language ?? "en")
        self.sortLabel.text = "Sort".localizableString(loc: Singleton.shared.language ?? "en")
        self.noProductLabel.text = "No Products found.".localizableString(loc: Singleton.shared.language ?? "en")
        currentFilterParam = [String:Set<Int>]()
        self.view.backgroundColor = .white
        self.getShopProduct()
        productType = 0
        handleTextAlignmentForArabic(view: self.view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
        if(K_BAG_COUNT == 0){
            self.bagCount.isHidden = true
        }else {
            self.bagCount.text = "\(K_BAG_COUNT)"
            self.bagCount.isHidden = false
        }
        
        self.filterButton.isHidden = self.isMostSelling ? true : false
        self.sortButton.isHidden = self.isMostSelling ? true : false
        self.sortStackView.isHidden = self.isMostSelling ? true : false
        self.filterMainView.isHidden = self.isMostSelling ? true : false
        self.filterMainViewHeight.constant = self.isMostSelling ? 0 : 50
    }
    
    //MARK: Functions
    func getShopProduct(){
        ActivityIndicator.show(view: self.view)
        let param: [String:Any] = [
            "lat":Singleton.shared.userLocation.coordinate.latitude,
            "long" : Singleton.shared.userLocation.coordinate.longitude,
            "lang": Singleton.shared.language ?? "en",
            "category" : Array(selectedCategory),// category id,
            "sub_category" : Array(selectedSubCategory),// subcategory id
            "distance": selectedDistance == 0 ? "":selectedDistance,// possible values 0-5 km -> 5, 5-10 -> 10, 10-15 -> 15, more than 16 -> 16
            "city_id": Singleton.shared.selectedCity.city_id,
            "country_id" : Singleton.shared.selectedCountry.id,
            "store_type" : storeType,
            "sort_by": K_SELECTED_SORTBY + 1,
            "offers": offerSelected == 0 ? "":offerSelected,
            "filter":[
                "color": self.filterParam["color"] ?? [],
                "price": [selectedPriceMin,selectedPriceMax],
                "size": self.filterParam["Size"] ?? [],
                "rating": selectedStar ?? 0
            ]
            
        ]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_SHOP_PRODUCT + subdomain + "?page=" + "\(self.currentPage)" + "&currency=\(Singleton.shared.selectedCurrency)", method: .post, parameter: param, objectClass: GetShopProducts.self, requestCode: U_GET_SHOP_PRODUCT, userToken: U_GET_SHOP_PRODUCT) { (response) in
            
            self.lastPage = response.response.business_products.last_page ?? 0
            
            if(self.productData.count == 0 || self.currentPage == 1 ){
                self.productData = response.response.business_products.data
                self.isLoadingList = false
            } else if response.response.business_products.data.count > 0{
                for val in response.response.business_products.data {
                    self.productData.append(val)
                }
                self.isLoadingList = false
            }
            if(response.response.business_products.data.count == 0){
                if self.currentPage > 1 {
                    self.currentPage -= 1
                }
                self.isLoadingList = false
            }
            
            self.productCollectionView.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func filterAction(_ sender: Any) {
        self.blurView.isHidden = false
        FILTER_SUBDOMAIN = self.subdomain
        K_FILTER_REDIRECTION = 2
        self.openFilter(view: 10)
    }
    
    @IBAction func sortAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        myVC.pickerData = ["Most popular","Price low to high","Price high to low"]
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func bagAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ShoppingBucketViewController") as! ShoppingBucketViewController
        myVC.showBackButton = true
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func loadMoreAction(_ sender: Any) {
        if(self.loadMoreButton.currentTitle == "Load More Products"){
            self.loadMoreButton.isHidden = true
            self.loadMoreButtonHeight.constant = 0
            self.isLoadingList = true
            self.loadMoreItemsForList()
        }
    }
}


//MARK: Collection View Delegate
extension ProductScreenViewController : UICollectionViewDelegate,UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isMostSelling == false {
            if(self.productData.count == 0){
                self.noProductLabel.isHidden = false
            }else {
                self.noProductLabel.isHidden = true
            }
        } else {
            if(self.mostSellingData.count == 0){
                self.noProductLabel.isHidden = false
            }else {
                self.noProductLabel.isHidden = true
            }
        }
        return isMostSelling ? mostSellingData.count : self.productData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionCell", for: indexPath) as! CategoryCollectionCell
        
        let val = isMostSelling ? self.mostSellingData[indexPath.item] : self.productData[indexPath.item]
        if((val.default_image ?? "").contains("http")){
            cell.shopImage.sd_setImage(with: URL(string: (val.default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }else {
            cell.shopImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (val.default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }
        cell.shopName.text = val.name
        cell.storeName.text = "by \(val.vendor_details?.name ?? "")"
        var count = Int()
        switch val.qty {
        case .string(let text):
            count = Int(text ?? "0")!
            break
        case .int(let text):
            count = Int(text)
            break
        default:
            break
        }
        
        if val.stock_status == 1 {
            if count < 10 && count > 0 {
                cell.onlyLeftView.isHidden = false
                cell.onlyLeftLabel.text = "Only \(count) left"
            } else {
                cell.onlyLeftView.isHidden = true
            }
        } else {
            cell.onlyLeftView.isHidden = false
            cell.onlyLeftLabel.text = "Out of stock"
        }
        
        cell.onlyLeftView.clipsToBounds = false
        cell.onlyLeftView.layer.masksToBounds = false
        cell.onlyLeftView.layer.cornerRadius = 4
        cell.onlyLeftView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        
        var currentPrice = convertPriceInDouble(price: val.price )
        //        var discountPrice = convertPriceInDouble(price: val.discounted_price )
        let discountPrice = val.discount ?? 0
        
        if (val.offer_qty ?? 0) > 0 && (val.offer_valid_till ?? 0) > Int(NSDate().timeIntervalSince1970) {
            
            cell.shopPrice.attributedText = self.handlePrice(price: "\(currentPrice)" , cut: "\(val.offer_price ?? 0.00)")
            
        } else if (val.discount != nil) && (discountPrice != 0.00) && ((discountPrice) != (currentPrice)){
            cell.shopPrice.attributedText = self.handlePrice(price: "\(currentPrice)" , cut: "\(discountPrice)")
        } else {
            cell.shopPrice.text = "\(Singleton.shared.selectedCurrencySymbol) \(currentPrice.addComma)"
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SingleProductViewController") as! SingleProductViewController
        myVC.selectedProduct = isMostSelling ? self.mostSellingData[indexPath.item]: self.productData[indexPath.item]
        myVC.subdomain =  isMostSelling ?  (self.mostSellingData[indexPath.item].vendor_details?.subdomain ?? "") :self.subdomain
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width/2)-15, height:(collectionView.frame.size.width/2) + 40)
        
    }
  
}
