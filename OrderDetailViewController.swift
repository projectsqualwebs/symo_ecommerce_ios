//
//  OrderDetailViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 04/05/21
//

import UIKit

protocol returnExchangeStatus {
    func reloadStatus(ind:Int)
}

class OrderDetailViewController: UIViewController, CAAnimationDelegate, Confirmation ,returnExchangeStatus{
    //MARK: Delegate Functions
    func reloadStatus(ind: Int) {
        self.orderData.status = ind
        self.orderStatus = ind
        self.productTableView.reloadData()
        showTrackAnmation()
    }
    
    func confirmationSelection(action: Int) {
        if(action == 2){
            self.dismiss(animated: true, completion: nil)
        }else {
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_CANCEL_ORDER + "\(self.orderData.id ?? 0)" + "?lang=\(Singleton.shared.language ?? "en")", method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_CANCEL_ORDER, userToken: nil) { response in
                ActivityIndicator.hide()
                self.navigationController?.popViewController(animated: true)
                Singleton.shared.showToast(msg: "Order cancelled".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
                
            }
        }
    }
    
    
    
    //MARK: IBOutlets
    @IBOutlet weak var productTableView: ContentSizedTableView!
    @IBOutlet weak var pointOne: View!
    @IBOutlet weak var lineOne: UIView!
    @IBOutlet weak var pointTwo: View!
    @IBOutlet weak var lineTwo: UIView!
    @IBOutlet weak var pointThree: View!
    @IBOutlet weak var lineThree: UIView!
    @IBOutlet weak var pointfour: View!
    @IBOutlet weak var lineFour: UIView!
    @IBOutlet weak var pointFive: View!
    
    @IBOutlet weak var itemTotal: DesignableUILabel!
    @IBOutlet weak var deliveryCharge: DesignableUILabel!
    @IBOutlet weak var orderTax: DesignableUILabel!
    @IBOutlet weak var totalAmount: DesignableUILabel!
    @IBOutlet weak var address: DesignableUILabel!
    @IBOutlet weak var billingNameLabel: DesignableUILabel!
    
    @IBOutlet weak var orderDetailsHeadingLabel: DesignableUILabel!
    @IBOutlet weak var trackYourOrdersLabel: DesignableUILabel!
    @IBOutlet weak var estimateDeliveryByLabel: DesignableUILabel!
    @IBOutlet weak var orderPlacedLabel: DesignableUILabel!
    @IBOutlet weak var orderPlacedOnLabel: DesignableUILabel!
    @IBOutlet weak var orderAcceptedLabel: DesignableUILabel!
    @IBOutlet weak var orderConfirmedByVendorLabel: DesignableUILabel!
    @IBOutlet weak var readyForDeliveryLabel: DesignableUILabel!
    @IBOutlet weak var outForDeliveryLabel: DesignableUILabel!
    
    @IBOutlet weak var downloadInvoiceButton: CustomButton!
    @IBOutlet weak var deliveredLabel: DesignableUILabel!
    @IBOutlet weak var billingDetailsLabel: DesignableUILabel!
    @IBOutlet weak var itemTotalLabel: DesignableUILabel!
    @IBOutlet weak var deliveryFeeLabel: DesignableUILabel!
    @IBOutlet weak var taxLabel: DesignableUILabel!
    @IBOutlet weak var totalAmountLAbel: DesignableUILabel!
    @IBOutlet weak var discountLocalizeLabel: DesignableUILabel!
    
    @IBOutlet weak var discountAmountLabel: DesignableUILabel!
    @IBOutlet weak var billingName: DesignableUILabel!
    @IBOutlet weak var couponCodeLabel: DesignableUILabel!
    @IBOutlet weak var couponCodeAmount: DesignableUILabel!
    @IBOutlet weak var couponCodeStackView: UIStackView!
    @IBOutlet weak var deliveryNoteLabel: DesignableUILabel!
    @IBOutlet weak var deliveryDateLabel: DesignableUILabel!
    @IBOutlet weak var trackDetailsView: UIView!
    
    var sortbyData = ["Pending", "Accepted", "Ready For Delivery", "Out For Delivery", "Delivered/Pickedup", "Return Initiated", "Return Approved", "Return Rejected", "Product Pickedup", "Refund Initiated","Refunded", "Cancelled","Order Rejected","Exchange Requested"]
    var orderData = OrderResponse()
    var orderStatus = Int()
    var orderId = Int()
    var notificationRedirection = false
    
    
    //MARK: View Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
        ReturnExchangeViewController.returnDelegate = self
        
        self.discountLocalizeLabel.text = "Discount".localizableString(loc: Singleton.shared.language ?? "en")
        self.orderDetailsHeadingLabel.text = "Order details".localizableString(loc: Singleton.shared.language ?? "en")
        self.trackYourOrdersLabel.text = "Track your orders".localizableString(loc: Singleton.shared.language ?? "en")
        self.orderPlacedLabel.text = "Order placed".localizableString(loc: Singleton.shared.language ?? "en")
        //        self.orderPlacedOnLabel.text = "Order placed on".localizableString(loc: Singleton.shared.language ?? "en")
        self.orderAcceptedLabel.text = "Order accepted".localizableString(loc: Singleton.shared.language ?? "en")
        self.orderConfirmedByVendorLabel.text = "Order confirmed by vendor".localizableString(loc: Singleton.shared.language ?? "en")
        self.readyForDeliveryLabel.text = "Ready for delivery".localizableString(loc: Singleton.shared.language ?? "en")
        self.outForDeliveryLabel.text = "Out for delivery".localizableString(loc: Singleton.shared.language ?? "en")
        self.downloadInvoiceButton.setTitle("Download invoice".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.deliveredLabel.text = "Delivered".localizableString(loc: Singleton.shared.language ?? "en")
        self.billingDetailsLabel.text = "Billling details".localizableString(loc: Singleton.shared.language ?? "en")
        self.itemTotalLabel.text = "Item total".localizableString(loc: Singleton.shared.language ?? "en")
        self.deliveryFeeLabel.text = "Delivery Fee".localizableString(loc: Singleton.shared.language ?? "en")
        self.taxLabel.text = "Tax".localizableString(loc: Singleton.shared.language ?? "en")
        self.totalAmountLAbel.text = "Total Amount".localizableString(loc: Singleton.shared.language ?? "en")
        if(self.notificationRedirection){
            self.orderStatus = self.orderData.status ?? 0
            self.orderId = self.orderData.id ?? 0
        }
        
        getOrderDetail()
        
        if orderStatus == 11 {
            self.trackDetailsView.isHidden = true
        } else {
            self.trackDetailsView.isHidden = false
        }
        handleTextAlignmentForArabic(view: self.view)
    }
    
    //MARK: Functions
    //for localization and labels setup
    func handleLabels(){
        let address = "Delivery address: ".localizableString(loc: Singleton.shared.language ?? "en") +  (orderData.order_address?.billing_address ?? "")
        let addAttribute = address.setColor(.lightGray, ofSubstring: (orderData.order_address?.billing_address ?? ""))
        self.address.attributedText = addAttribute
        
        let optionalNote = "\(self.orderData.order_notes ?? "N/A")"
        
        let note = "Note: ".localizableString(loc: Singleton.shared.language ?? "en") + optionalNote
        let noteAttr = note.setColor(.lightGray, ofSubstring: optionalNote)
        self.deliveryNoteLabel.attributedText = noteAttr
        
        
        let onText = self.orderData.created_at ?? ""
        let sliced = onText.prefix(10)
        self.orderPlacedOnLabel.text = "On " + "\(sliced)"
        
        let date = "Delivery Date: ".localizableString(loc: Singleton.shared.language ?? "en") + "\(self.convertTimestampToDate(Int(self.orderData.delivery_date ?? "0")!, to: "dd-MM-YYYY"))"
        let dateAttr = date.setColor(.lightGray, ofSubstring: "\(self.convertTimestampToDate(Int(self.orderData.delivery_date ?? "0")!, to: "dd-MM-YYYY"))")
        self.deliveryDateLabel.attributedText = dateAttr
        
        self.estimateDeliveryByLabel.text = "Estimated delivery by".localizableString(loc: Singleton.shared.language ?? "en") + " \(self.convertTimestampToDate(Int(self.orderData.delivery_date ?? "0")!, to: "MMM dd,yyyy"))"
        
        self.billingName.text = orderData.order_address?.billing_name
        self.itemTotal.text = "\(Singleton.shared.selectedCurrencySymbol) " + (orderData.currency_total?.order_subtotal ?? "0.0")
        self.orderTax.text = "\(Singleton.shared.selectedCurrencySymbol) " + (orderData.currency_total?.order_tax ?? "0.0")
        self.deliveryCharge.text = "\(Singleton.shared.selectedCurrencySymbol) " + (orderData.currency_total?.order_shipping ?? "0.0")
        self.totalAmount.text = "\(Singleton.shared.selectedCurrencySymbol) " + (orderData.currency_total?.order_total ?? "0.0")
        if self.orderData.currency_total?.coupon_discount_amount == "0.00" || self.orderData.currency_total?.coupon_discount_amount == nil {
            self.couponCodeStackView.isHidden = true
            self.discountAmountLabel.text = "- \(Singleton.shared.selectedCurrencySymbol) " + (orderData.currency_total?.discount_amount ?? "0.0")
        }else {
            self.couponCodeStackView.isHidden = false
            self.couponCodeAmount.text = "-\(Singleton.shared.selectedCurrencySymbol) " + (orderData.currency_total?.coupon_discount_amount ?? "0.0")
            self.discountAmountLabel.text = "- \(Singleton.shared.selectedCurrencySymbol) " + "\(orderData.currency_total?.discount_amount ?? "0.0")"
        }
    }
    
    //for showing order status
    func showTrackAnmation(){
        
        if self.sortbyData[self.orderStatus] == "Pending"{
            self.pointOne.backgroundColor = UIColor.red
            pointOne.addRippleAnimation(color: K_PINK_COLOR, duration: 3, repeatCount:1, rippleCount: 2, rippleDistance: 10)
        } else if self.sortbyData[self.orderStatus] == "Accepted"{
            self.pointOne.backgroundColor = UIColor.red
            self.pointTwo.backgroundColor = UIColor.red
            self.lineOne.backgroundColor = UIColor.red
            pointTwo.addRippleAnimation(color: K_PINK_COLOR, duration: 3, repeatCount:1, rippleCount: 2, rippleDistance: 10)
        } else if self.sortbyData[self.orderStatus] == "Ready For Delivery"{
            self.pointOne.backgroundColor = UIColor.red
            self.pointTwo.backgroundColor = UIColor.red
            self.pointThree.backgroundColor = UIColor.red
            self.lineOne.backgroundColor = UIColor.red
            self.lineTwo.backgroundColor = UIColor.red
            pointThree.addRippleAnimation(color: K_PINK_COLOR, duration: 3, repeatCount:1, rippleCount: 2, rippleDistance: 10)
        } else if self.sortbyData[self.orderStatus] == "Out For Delivery"{
            self.pointOne.backgroundColor = UIColor.red
            self.pointTwo.backgroundColor = UIColor.red
            self.pointThree.backgroundColor = UIColor.red
            self.pointfour.backgroundColor = UIColor.red
            self.lineOne.backgroundColor = UIColor.red
            self.lineTwo.backgroundColor = UIColor.red
            self.lineThree.backgroundColor = UIColor.red
            pointfour.addRippleAnimation(color: K_PINK_COLOR, duration: 3, repeatCount:1, rippleCount: 2, rippleDistance: 10)
        } else { //if self.orderStatus == "Delivered/Pickedup"
            self.pointOne.backgroundColor = UIColor.red
            self.pointTwo.backgroundColor = UIColor.red
            self.pointThree.backgroundColor = UIColor.red
            self.pointfour.backgroundColor = UIColor.red
            self.pointFive.backgroundColor = UIColor.red
            self.lineOne.backgroundColor = UIColor.red
            self.lineTwo.backgroundColor = UIColor.red
            self.lineThree.backgroundColor = UIColor.red
            self.lineFour.backgroundColor = UIColor.red
            pointFive.addRippleAnimation(color: K_PINK_COLOR, duration: 3, repeatCount:1, rippleCount: 2, rippleDistance: 10)
        }
    }
    
    func showAnimation(){
        let val = self.orderData.order_timeline ?? []
        for i in 0..<val.count{
            if val[i].type == 0 {
                if val[i].status == 1 {
                    self.pointOne.backgroundColor = UIColor.red
                    pointOne.addRippleAnimation(color: K_PINK_COLOR, duration: 3, repeatCount:1, rippleCount: 2, rippleDistance: 10)
                }
            } else if val[i].type == 1 {
                if val[i].status == 1 {
                    self.pointOne.backgroundColor = UIColor.red
                    self.pointTwo.backgroundColor = UIColor.red
                    self.lineOne.backgroundColor = UIColor.red
                    pointTwo.addRippleAnimation(color: K_PINK_COLOR, duration: 3, repeatCount:1, rippleCount: 2, rippleDistance: 10)
                }
            }  else if val[i].type == 2 {
                if val[i].status == 1 {
                    self.pointOne.backgroundColor = UIColor.red
                    self.pointTwo.backgroundColor = UIColor.red
                    self.pointThree.backgroundColor = UIColor.red
                    self.lineOne.backgroundColor = UIColor.red
                    self.lineTwo.backgroundColor = UIColor.red
                    pointThree.addRippleAnimation(color: K_PINK_COLOR, duration: 3, repeatCount:1, rippleCount: 2, rippleDistance: 10)
                }
            }   else if val[i].type == 3 {
                if val[i].status == 1 {
                    self.pointOne.backgroundColor = UIColor.red
                    self.pointTwo.backgroundColor = UIColor.red
                    self.pointThree.backgroundColor = UIColor.red
                    self.pointfour.backgroundColor = UIColor.red
                    self.lineOne.backgroundColor = UIColor.red
                    self.lineTwo.backgroundColor = UIColor.red
                    self.lineThree.backgroundColor = UIColor.red
                    pointfour.addRippleAnimation(color: K_PINK_COLOR, duration: 3, repeatCount:1, rippleCount: 2, rippleDistance: 10)
                }
            }    else if val[i].type == 4 {
                if val[i].status == 1 {
                    self.pointOne.backgroundColor = UIColor.red
                    self.pointTwo.backgroundColor = UIColor.red
                    self.pointThree.backgroundColor = UIColor.red
                    self.pointfour.backgroundColor = UIColor.red
                    self.pointFive.backgroundColor = UIColor.red
                    self.lineOne.backgroundColor = UIColor.red
                    self.lineTwo.backgroundColor = UIColor.red
                    self.lineThree.backgroundColor = UIColor.red
                    self.lineFour.backgroundColor = UIColor.red
                    pointFive.addRippleAnimation(color: K_PINK_COLOR, duration: 3, repeatCount:1, rippleCount: 2, rippleDistance: 10)
                }
            }
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 2) {
                // self.addLine(fromPoint: self.pointOne.center, toPoint: self.pointTwo.center)
            }
        }
    }
    
    func addLine(fromPoint start: CGPoint, toPoint end:CGPoint) {
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: start)
        linePath.addLine(to: end)
        line.path = linePath.cgPath
        line.strokeColor = UIColor.red.cgColor
        line.lineWidth = 1
        line.duration = 3
        
        line.repeatCount = .infinity
        line.lineJoin = .round
        lineOne.layer.addSublayer(line)
    }
    
    func getOrderDetail(){
        ActivityIndicator.show(view: self.view)
        let param:[String:Any] = [
            "lang": Singleton.shared.language
        ]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_SINGLE_ORDER_DETAIL + "\(orderId)" + "?currency=\(Singleton.shared.selectedCurrency)", method: .post, parameter: param, objectClass: GetSingleOrderResponse.self, requestCode: U_GET_SINGLE_ORDER_DETAIL, userToken: nil) { (response) in
            self.orderData = response.response ?? OrderResponse()
            self.handleLabels()
            self.showAnimation()
            self.productTableView.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    func saveFile(contents: [URL],dUrl:URL){
        
        for indexx in 0..<contents.count {
            if contents[indexx].lastPathComponent == dUrl.lastPathComponent {
                self.dismiss(animated: true, completion: nil)
                let activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
                self.navigationController?.present(activityViewController, animated: true, completion: nil)
            }
        }
        
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func downloadInvoiceAction(_ sender: Any) {
        var pdfUrl = String()
        
        SessionManager.shared.methodForApiCalling(url:  U_BASE + U_DOWNLOAD_PDF + "\(self.orderData.id ?? 0)" + "?lang=" + "\(Singleton.shared.language ?? "en")" + "&currency=\(Singleton.shared.selectedCurrency)", method: .post, parameter:nil, objectClass: GetPDFresponse.self, requestCode: U_DOWNLOAD_PDF, userToken: UD_TOKEN) { (response) in
            pdfUrl = response.response?.pdf_url ?? ""
            if pdfUrl != "" {
                Singleton.shared.showToast(msg: "Downloading pdf".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 1)
                if let url = URL(string: pdfUrl){
                    let fileName = String((url.lastPathComponent)) as NSString
                    
                    // Create destination URL
                    let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
                    let destinationFileUrl = documentsUrl.appendingPathComponent("\(fileName)")
                    //Create URL to the source file you want to download
                    let fileURL = url
                    let sessionConfig = URLSessionConfiguration.default
                    let session = URLSession(configuration: sessionConfig)
                    var request = URLRequest(url:fileURL)
                    request.httpMethod = "GET"
                    let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                        if let tempLocalUrl = tempLocalUrl, error == nil {
                            do {
                                try? FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                                do {
                                    let contents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                                    ActivityIndicator.hide()
                                    DispatchQueue.main.async {
                                        self.saveFile(contents: contents,dUrl: destinationFileUrl)
                                    }
                                }
                                catch (let err) {
                                    print("error: \(err)")
                                }
                            } catch (let writeError) {
                                print("Error creating a file \(destinationFileUrl) : \(writeError)")
                            }
                        } else {
                            print("Error took place while downloading a file. Error description: \(error?.localizedDescription ?? "")")
                        }
                    }
                    task.resume()
                }else {
                    print("test")
                }
            }
            ActivityIndicator.hide()
        }
        
        
    }
    
}

//MARK: TableView Delegate
extension OrderDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orderData.order_items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SingleProductViewController") as! SingleProductViewController
        myVC.selectedProduct = self.orderData.order_items?[indexPath.row].Product ?? ProductResponse()
        myVC.subdomain = self.orderData.order_items?[indexPath.row].Product?.vendor_details?.subdomain ?? ""
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableCell") as! CategoryTableCell
        let val = self.orderData.order_items?[indexPath.row]
        cell.shopName.text = val?.Product?.name
        cell.orderId.text = "Order ID: ".localizableString(loc: Singleton.shared.language ?? "en") + "#SYMO000\(self.orderData.id ?? 0)"
        
        let stamp = self.orderData.created_at ?? ""
        let sliced = stamp.prefix(10)
        cell.orderDate.text = "Placed on: ".localizableString(loc: Singleton.shared.language ?? "en") + "\(String(sliced))"
        if((val?.Product?.default_image ?? "").contains("http")){
            cell.shopImage.sd_setImage(with: URL(string: (val?.Product?.default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        } else{
            cell.shopImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (val?.Product?.default_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }
        if(self.sortbyData[self.orderStatus] != ""){
            cell.ratingText.text = "🟢 " + self.sortbyData[self.orderStatus]
        }
        var currentPrice = convertPriceInDouble(price: val?.product?.price )
        //        var discountPrice = convertPriceInDouble(price: val?.Product?.discounted_price )
        let discountPrice = val?.Product?.discount ?? 0
        
        if (val?.Product?.offer_qty ?? 0) > 0 && (val?.Product?.offer_valid_till ?? 0) > Int(NSDate().timeIntervalSince1970) {
            
            cell.shopPrice.attributedText = self.handlePrice(price: "\(currentPrice)" , cut: "\(val?.Product?.offer_price ?? 0.00)")
            
        } else if (val?.Product?.discount != nil) && (discountPrice != 0.00) && (discountPrice) != (currentPrice){
            cell.shopPrice.attributedText = self.handlePrice(price: "\(currentPrice)" , cut: "\(discountPrice)")
        } else {
            cell.shopPrice.text = "\(Singleton.shared.selectedCurrencySymbol) \(currentPrice.addComma)"
        }
        
        let variation = val?.VariationValues ?? []
        
        for i in variation{
            if((i.attribute_text == "Color") || (i.attribute_text == "Ram") || (i.attribute_text == "Internal Storage") || (i.attribute_text == "Size")){
                let att = (i.attribute_text ?? "") + " | " + (i.value_text ?? "")
                cell.shopAddress.text = (((cell.shopAddress.text ?? "") == "") ? ((cell.shopAddress.text ?? "")):(cell.shopAddress.text ?? "") + ", ") + att
            }
        }
        cell.cartItemCount.text = "\(val?.qty ?? 0)"
        cell.exchangeButton.setTitle("Exchange".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        //  cell.returnButton.setTitle("Return".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        if(self.orderData.service_preference == 0 && self.orderData.status == 4){
            if(val?.Product?.is_exchangable == 1){
                cell.returnButton.isHidden = false
                cell.returnExchangeStack.isHidden = false
            }
            if(val?.Product?.is_returnable == 1){
                cell.exchangeButton.isHidden = false
                cell.returnExchangeStack.isHidden = false
            }
        }else {
            cell.returnExchangeStack.isHidden = true
        }
        if(self.orderData.status == 0 || self.orderData.status == 1){
            cell.returnButton.isHidden = false
            cell.returnExchangeStack.isHidden = false
            cell.returnButton.setTitle("Cancel Order".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        }
        
        
        
        cell.heartButton = {
            if(cell.returnButton.titleLabel?.text == "Cancel Order".localizableString(loc: Singleton.shared.language ?? "en")){
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationPopupViewController") as!
                ConfirmationPopupViewController
                myVC.confirmationDelegate = self
                myVC.modalPresentationStyle = .overFullScreen
                myVC.confirmationText = "Cancel Order!".localizableString(loc: Singleton.shared.language ?? "en")
                myVC.detailText = "Do you really want to cancel order? Your order will be cancelled immediately".localizableString(loc: Singleton.shared.language ?? "en")
                self.navigationController?.present(myVC, animated: true, completion: nil)
            }else {
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ReturnExchangeViewController") as! ReturnExchangeViewController
                myVC.orderData = val!
                myVC.currentAction = 1
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
        
        cell.bagButton = {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ReturnExchangeViewController") as! ReturnExchangeViewController
            myVC.orderData = val!
            myVC.currentAction = 2
            self.navigationController?.pushViewController(myVC, animated: true)
        }
        
        return cell
    }
}

//0 -> pending
//1 -> order_accepted
//2 -> order_ready_for_pickup
//3 ->order_out_of_delivery
//4 -> pickedup
//5 -> refund_requested
//6 -> refund_approved
//7 -> refund_rejected
//8 -> product_picked_up
//9 -> refund_initiated
//10 -> refunded
//11 -> cancelled
//12 -> order_rejected
//13 -> exchange_requested
//14 -> exchange_approved
//15 -> exchange_rejected
//16 -> product_exchanged
