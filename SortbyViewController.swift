//
//  SortbyViewController.swift
//  Symo New
//
//  Created by Apple on 07/04/21.
//

import UIKit

protocol SortData{
    func sortProductList(option: Int)
    func sortShopList(option:Int)
}

class SortbyViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var sortbyTable: UITableView!
    @IBOutlet weak var sortByLabel: DesignableUILabel!
    @IBOutlet weak var arrangeProductListLabel: DesignableUILabel!
    
    var sortByData = ["Most popular","Price low to high","Price high to low"]
    var sortShops = ["Latest","A-Z","Z-A"]
    //  for sort by shop //1 - latest , 3 - A - Z , 4 - z to a
    var sortDelegate: SortData? = nil
    var redirectionFrom = 1
    var selectedIndex : Int?
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sortByLabel.text = "Sort by".localizableString(loc: Singleton.shared.language ?? "en")
        self.arrangeProductListLabel.text = "Arrange product list according to ".localizableString(loc: Singleton.shared.language ?? "en")
        
        if self.redirectionFrom != 1 {
            if K_SELECTED_SORTBY == 0 {
                selectedIndex = 0
            } else if K_SELECTED_SORTBY == 1 {
                selectedIndex = 1
            } else if K_SELECTED_SORTBY == 2 {
                selectedIndex = 2
            }
        }
    }
    
    //MARK: IBAction
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

//MARK: TableView Delegate
extension SortbyViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if redirectionFrom == 1{
            return sortShops.count
        } else {
            return sortByData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableCell") as! MenuTableCell
        if self.redirectionFrom == 1 {
            cell.menuImage.image = #imageLiteral(resourceName: "Ellipse 3 copy 2")
        }else if self.redirectionFrom == 2 {
            if indexPath.row == selectedIndex {
                cell.menuImage.image = #imageLiteral(resourceName: "Ellipse 3 copy")
            }else {
                cell.menuImage.image = #imageLiteral(resourceName: "Ellipse 3 copy 2")
            }
        }
        if self.redirectionFrom == 2 {
            cell.menuLabel.text = sortByData[indexPath.row].localizableString(loc: Singleton.shared.language ?? "en")
        } else {
            cell.menuLabel.text = sortShops[indexPath.row].localizableString(loc: Singleton.shared.language ?? "en")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.redirectionFrom == 1 {
            if indexPath.row == 0 {
                K_SELECTED_SORTBY = 1
            } else if indexPath.row == 1{
                K_SELECTED_SORTBY = 3
            } else if indexPath.row == 2{
                K_SELECTED_SORTBY = 4
            }
            self.sortDelegate?.sortShopList(option: K_SELECTED_SORTBY)
        } else {
            K_SELECTED_SORTBY = indexPath.row
            self.sortDelegate?.sortProductList(option: indexPath.row)
        }
        self.sortbyTable.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
}
