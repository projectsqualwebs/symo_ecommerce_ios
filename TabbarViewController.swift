//
//  TabbarViewController.swift
//  Symo New
//
//  Created by Apple on 01/04/21.
//

import UIKit
import FirebaseRemoteConfig
import GoogleMaps
import GooglePlaces

protocol HandleTabBarItems {
    func setTabBar()
    func localize(rtl:Bool)
}

var firstTimeApp = true

class TabbarViewController: UITabBarController, HandleTabBarItems ,UITabBarControllerDelegate, Confirmation{
    
    // to localize bottom tabs
    func localize(rtl:Bool){
        for i in 0..<(tabBar.items?.count ?? 0){
            tabBar.items?[i].title = self.itemNames[i].localizableString(loc: Singleton.shared.language ?? "en")
        }
    }
    
    // for changing tab bar
    func setTabBar(){
        if(Singleton.shared.userDetail.id == nil){
            let indexToRemove = 4
            if indexToRemove < self.viewControllers!.count {
                var viewControllers = self.viewControllers
                viewControllers?.remove(at: indexToRemove)
                self.viewControllers = viewControllers
            }
        }
    }
    
    
    func confirmationSelection(action: Int) {
        if(action == 1){
            self.yesButtonAction()
        }else{
            if(popupAction != 1){
                self.forceUpdate()
            }
        }
        
    }
    
    var orderedViewControllers: [UIViewController] = []
    var rtlOrderedViewControllers: [UIViewController] = []
    var itemNames = ["Home","Explore","Cart","Favourite","Settings"]
    var imageNames = ["home","grid","Vector","heart_outline","settings"]
    static var shared = TabbarViewController()
    static var reloadDelegate : reloadSearchResult? = nil
    var isWrongAligned = false
    var remoteConfig: RemoteConfig!
    private var popupAction = 1
    
    
    //MARK: View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        remoteConfig = RemoteConfig.remoteConfig()
        let remoteConfigSettings = RemoteConfigSettings()
        remoteConfigSettings.minimumFetchInterval = 0
        remoteConfig.configSettings = remoteConfigSettings
        remoteConfig.setDefaults(fromPlist: "firebaseConfigInfo")

        self.fetchConfig()
       
        self.localize(rtl: true)
        self.tabBar.tintColor = K_PINK_COLOR
        self.tabBar.unselectedItemTintColor = .lightGray
        SideMenuViewController.handleTabDelegate = self
        SettingsViewController.handletab = self
        Singleton.handleTabDelegate = self
        if let count = UserDefaults.standard.value(forKey: UD_BAG_COUNT) as? Int{
            K_BAG_COUNT = count
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleTabar(_:)), name: NSNotification.Name(N_HANDLE_TABBAR), object: nil)
        NavigationController.shared.getAllBookMarkStore()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        resetView = true
        if item.title == "Explore" {
            TabbarViewController.reloadDelegate?.reloadLocationTabel(searchText: "")
        }
    }
    
    func newViewController(controller: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: controller)
    }
    
    @objc func handleTabar(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_HANDLE_TABBAR), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleTabar), name: NSNotification.Name(N_HANDLE_TABBAR), object: nil)
        if let home = notif.userInfo?["home"] as? Bool{
            self.selectedIndex = 0
            return
        }
        
        if let settings = notif.userInfo?["settings"] as? Bool{
            self.selectedIndex = 4
            return
        }
    }
    
    // fetch remote config for update and other dynamic data.
    func fetchConfig() {
    
        var expirationDuration = 3600
        if remoteConfig.configSettings.minimumFetchInterval == 0 {
            expirationDuration = 0
        }
        remoteConfig.fetch(withExpirationDuration: TimeInterval(expirationDuration)) { (status, error) -> Void in
            if status == .success {
                print("Config fetched!")
                self.remoteConfig.activate { changed, error in
                }
                self.display()
                self.handleRemoteConfigValues()
            } else {
                print("Config not fetched")
                print("Error \(error!.localizedDescription)")
            }
            
        }
        
    }
    
    func handleRemoteConfigValues(){
        var googleApiKey = "google_api_key"
        GOOGLE_API_KEY = remoteConfig[googleApiKey].stringValue ?? "AIzaSyDEKWYKnm_ZHSH0p7FP3Y08Ol3fDiQr78E"
        print("🛜 GOOGLE_API_KEY : \(GOOGLE_API_KEY)")
        GMSServices.provideAPIKey(GOOGLE_API_KEY)
        GMSPlacesClient.provideAPIKey(GOOGLE_API_KEY)
    }
    
    func display() {
        var softUpdate =  "update_soft_ios"
        var updateRequired = "force_update_required_ios"
        var currentVersion = "force_update_current_version_ios"
        var updateUrl = "force_update_store_url_ios"
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        if(remoteConfig[updateRequired].stringValue == "true"){
            if(remoteConfig[currentVersion].stringValue == appVersion){
                return
            }else{
                if(remoteConfig[softUpdate].stringValue == "false"){
                    self.showPopup(title: "Update".localizableString(loc: Singleton.shared.language ?? "en"), msg: "New Version is available on App Store".localizableString(loc: Singleton.shared.language ?? "en"),action:2)
                }else{
                    self.showPopup(title: "Update".localizableString(loc: Singleton.shared.language ?? "en"), msg:  "New Version is available on App Store".localizableString(loc: Singleton.shared.language ?? "en"),action: 1)
                }
            }
        }else {
            return
        }
    }
    //update popup to user
    func showPopup(title: String, msg: String,action:Int?) {
        
        self.popupAction = action ?? 1
        
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationPopupViewController") as! ConfirmationPopupViewController
        myVC.confirmationDelegate = self
        myVC.confirmationText = title.localizableString(loc: Singleton.shared.language ?? "en")
        myVC.detailText = msg.localizableString(loc: Singleton.shared.language ?? "en")
        myVC.firstButtonTitle = "Update".localizableString(loc: Singleton.shared.language ?? "en")
        myVC.secondButtonTitle = "Cancel".localizableString(loc: Singleton.shared.language ?? "en")
        myVC.modalPresentationStyle = .overFullScreen
        self.present(myVC, animated: true, completion: nil)
        
    }
    
    func yesButtonAction(){
        let string = remoteConfig["force_update_store_url_ios"].stringValue!
        let url  = NSURL(string: string)//itms   https
        if UIApplication.shared.canOpenURL(url! as URL) {
            UIApplication.shared.openURL(url! as URL)
        }
    }
    
    func forceUpdate() {
        exit(0);
    }
    
    
}
