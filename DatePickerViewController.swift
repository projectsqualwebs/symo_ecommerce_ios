//
//  DatePickerViewController.swift
//  Symo New
//
//  Created by Apple on 06/04/21
//

import UIKit

protocol SelectDate {
    func selectedDate(timestamp:Date)
}

class DatePickerViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var headingLabel: DesignableUILabel!
    @IBOutlet weak var pickupButton: CustomButton!
    @IBOutlet weak var deliveryButton: CustomButton!
    
    static var shared = DatePickerViewController()
    var dateDelegate: SelectDate? = nil
    var pickerMode: UIDatePicker.Mode?
    var headingText = "Select Date".localizableString(loc: Singleton.shared.language ?? "en")
    var orderPreference:Int?
    var minDate: Date?
    var maxDate: Date?
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pickupButton.setTitle("Done".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        switch orderPreference {
        case 0:
            self.pickupButton.isHidden = true
            self.deliveryButton.isHidden = false
            break
        case 1:
            self.pickupButton.setTitle("Done".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
            self.pickupButton.isHidden = false
            self.deliveryButton.isHidden = true
            break
        case 2:
            self.pickupButton.setTitle("Done".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
            self.pickupButton.isHidden = false
            self.deliveryButton.isHidden = true
            break
        default:
            self.pickupButton.isHidden = false
            self.deliveryButton.isHidden = true
            break
        }
        
        self.headingLabel.text = headingText
        datePicker.datePickerMode = (pickerMode != nil) ? pickerMode as! UIDatePicker.Mode:.date
        if(minDate != nil){
            datePicker.minimumDate = minDate
            datePicker.date = minDate!
        }
        if(maxDate != nil){
            datePicker.maximumDate = maxDate
        }
        
    }
    
    
    //MARK: IBActions
    @IBAction func doneButton(_ sender: CustomButton) {
       // isSelfPickup = sender.tag == 1 ? true:false
        self.dateDelegate?.selectedDate(timestamp: datePicker.date)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}
