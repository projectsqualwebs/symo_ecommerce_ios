//
//  ReviewViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 29/04/21.
//

import UIKit

class ReviewViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var fiveRatingSlider: UISlider!
    @IBOutlet weak var fourRatingSlider: UISlider!
    @IBOutlet weak var threeRatingSlider: UISlider!
    @IBOutlet weak var twoRatingSlider: UISlider!
    @IBOutlet weak var oneRatingSlider: UISlider!
    @IBOutlet weak var oneRatingLabel: DesignableUILabel!
    @IBOutlet weak var twoRatingLabel: DesignableUILabel!
    @IBOutlet weak var threeRatingLabel: DesignableUILabel!
    @IBOutlet weak var fourRatingLabel: DesignableUILabel!
    @IBOutlet weak var fiveRatingLabel: DesignableUILabel!
    @IBOutlet weak var averageRating: DesignableUILabel!
    @IBOutlet weak var reviewTable: ContentSizedTableView!
    @IBOutlet weak var totalReviewRating: DesignableUILabel!
    @IBOutlet weak var noReviewLabel: DesignableUILabel!
    @IBOutlet weak var reviewAndRatingeadingLabel: DesignableUILabel!
    @IBOutlet weak var customerRatings: DesignableUILabel!
    @IBOutlet weak var postAReviewButton: CustomButton!
    
    var productData = SingleProductResponse()
    
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.reviewAndRatingeadingLabel.text = "Reviews & Ratings".localizableString(loc: Singleton.shared.language ?? "en")
        self.customerRatings.text = "Customer Ratings".localizableString(loc: Singleton.shared.language ?? "en")
        self.postAReviewButton.setTitle("Post a review".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        
        self.view.backgroundColor = .white
        self.initializeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
    }
    
    
    //MARK: Initialize View
    func initializeView(){
        
        self.fiveRatingSlider.minimumTrackTintColor = ((Float(self.productData.starRatings.fivestar ?? 0)) == 0.0) ? UIColor.init(hexString: "F2F4F8") : UIColor.init(hexString: "F2CF1F")
        self.fourRatingSlider.minimumTrackTintColor = ((Float(self.productData.starRatings.fourstar ?? 0)) == 0.0) ? UIColor.init(hexString: "F2F4F8") : UIColor.init(hexString: "F2CF1F")
        self.threeRatingSlider.minimumTrackTintColor = ((Float(self.productData.starRatings.threestar ?? 0)) == 0.0) ? UIColor.init(hexString: "F2F4F8") : UIColor.init(hexString: "F2CF1F")
        self.twoRatingSlider.minimumTrackTintColor = ((Float(self.productData.starRatings.twostar ?? 0)) == 0.0) ? UIColor.init(hexString: "F2F4F8") : UIColor.init(hexString: "F2CF1F")
        self.oneRatingSlider.minimumTrackTintColor = ((Float(self.productData.starRatings.onestar ?? 0)) == 0.0) ? UIColor.init(hexString: "F2F4F8") : UIColor.init(hexString: "F2CF1F")
        
        self.fiveRatingSlider.maximumTrackTintColor = ((Float(self.productData.starRatings.fivestar ?? 0)) == 5.0) ? UIColor.init(hexString: "F2CF1F") : UIColor.init(hexString: "F2F4F8")
        self.fourRatingSlider.maximumTrackTintColor = ((Float(self.productData.starRatings.fourstar ?? 0)) == 5.0) ? UIColor.init(hexString: "F2CF1F") : UIColor.init(hexString: "F2F4F8")
        self.threeRatingSlider.maximumTrackTintColor = ((Float(self.productData.starRatings.threestar ?? 0)) == 5.0) ? UIColor.init(hexString: "F2CF1F") : UIColor.init(hexString: "F2F4F8")
        self.twoRatingSlider.maximumTrackTintColor = ((Float(self.productData.starRatings.twostar ?? 0)) == 5.0) ? UIColor.init(hexString: "F2CF1F") : UIColor.init(hexString: "F2F4F8")
        self.oneRatingSlider.maximumTrackTintColor = ((Float(self.productData.starRatings.onestar ?? 0)) == 5.0) ? UIColor.init(hexString: "F2CF1F") : UIColor.init(hexString: "F2F4F8")
        
        if(self.productData.all_reviews.count > 0){
            self.fiveRatingSlider.maximumValue = Float(self.productData.all_reviews.count)
            self.fourRatingSlider.maximumValue = Float(self.productData.all_reviews.count)
            self.threeRatingSlider.maximumValue = Float(self.productData.all_reviews.count)
            self.twoRatingSlider.maximumValue = Float(self.productData.all_reviews.count)
            self.oneRatingSlider.maximumValue = Float(self.productData.all_reviews.count)
            
            self.fiveRatingSlider.value = Float((self.productData.starRatings.fivestar ?? 0)/self.productData.all_reviews.count)
            self.fourRatingSlider.value = Float((self.productData.starRatings.fourstar ?? 0)/self.productData.all_reviews.count)
            self.threeRatingSlider.value = Float((self.productData.starRatings.threestar ?? 0)/self.productData.all_reviews.count)
            self.twoRatingSlider.value = Float((self.productData.starRatings.twostar ?? 0)/self.productData.all_reviews.count)
            self.oneRatingSlider.value = Float((self.productData.starRatings.onestar ?? 0)/self.productData.all_reviews.count)
            self.totalReviewRating.text = "\(self.productData.all_reviews.count ) " + "review and rating".localizableString(loc: Singleton.shared.language ?? "en")
        }else {
            self.totalReviewRating.text = "No review(s) and rating(s)".localizableString(loc: Singleton.shared.language ?? "en")
        }
        self.fiveRatingLabel.text = "\(self.productData.starRatings.fivestar ?? 0)"
        self.fourRatingLabel.text = "\(self.productData.starRatings.fourstar ?? 0)"
        self.threeRatingLabel.text = "\(self.productData.starRatings.threestar ?? 0)"
        self.twoRatingLabel.text = "\(self.productData.starRatings.twostar ?? 0)"
        self.oneRatingLabel.text = "\(self.productData.starRatings.onestar ?? 0)"
        switch self.productData.starRatings.avgRating?.rating {
        case .string(let text):
            let val : Double? = Double(text)
            self.averageRating.text = String(format: "%.1f", val ?? 0.0)
            break
        case .double(let text):
            self.averageRating.text = "\(text)"
            break
        case .int(let text):
            self.averageRating.text = "\(text)"
            break
        default:
            break
        }
        self.reviewTable.reloadData()
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func postReviewAction(_ sender: Any) {
        let myVC =  self.storyboard?.instantiateViewController(withIdentifier: "PostReviewViewController") as! PostReviewViewController
        myVC.productId = self.productData.business_product.id ?? 0
        self.navigationController?.pushViewController(myVC, animated: true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshTable(_:)), name: NSNotification.Name("refresh_review_table"), object: nil)
    }
    
    @objc func refreshTable(_ notif: Notification){
        if let product = notif.object as? SingleProductResponse{
            self.productData = product
            self.reviewTable.reloadData()
        }
    }
}

//MARK: Table Delegate
extension ReviewViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.productData.all_reviews.count == 0){
            self.noReviewLabel.isHidden = false
        }else {
            self.noReviewLabel.isHidden = true
        }
        return self.productData.all_reviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableCell") as! CategoryTableCell
        let val = self.productData.all_reviews[indexPath.row]
        if((val.user?.avatar ?? "").contains("http")){
            cell.shopImage.sd_setImage(with: URL(string: (val.user?.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }else {
            cell.shopImage.sd_setImage(with: URL(string: U_IMAGE_STORE_BASE + (val.user?.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }
        cell.shopName.text = val.user?.name
        let sub = (val.created_at ?? "").prefix(10)
        cell.shopPrice.text = "\(sub)"
        
        //for handling data type
        switch val.rating {
        case .string(let text):
            cell.ratingView.rating = Double(text) ?? 0
            break
        case .double(let text):
            cell.ratingView.rating = Double(text)
            break
        case .int(let text):
            cell.ratingView.rating = Double(text)
            break
        default:
            break
        }
        cell.shopAddress.text = val.comment
        return cell
    }
    
}
