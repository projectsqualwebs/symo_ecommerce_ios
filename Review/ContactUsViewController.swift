//
//  ContactUsViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 28/02/22.
//

import UIKit
import MapKit
import SkyFloatingLabelTextField
import GoogleMaps

class ContactUsViewController: UIViewController, SelectFromPicker ,UITextViewDelegate{
    
    //MARK: Delegate Functions
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == self.messageTextView {
            if self.messageTextView.text == "Message" {
                self.messageTextView.text = ""
                self.messageTextView.textColor = UIColor.black
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == self.messageTextView {
            if self.messageTextView.text == "" {
                self.messageTextView.text = "Message"
                self.messageTextView.textColor = UIColor.lightGray
            }
        }
    }
    
    func selectedPickerData(val: String, pos: Int) {
        // 1 for country field
        if(self.currentPicker == 1){
            Singleton.shared.cityData = []
            Singleton.shared.langData = []
            self.cityData = []
            ActivityIndicator.show(view: self.view)
            Singleton.shared.selectedCountry = self.countryData[pos]
            self.contactTextField.text = "+\(self.countryData[pos].country_code ?? "")"
            let country = try? JSONEncoder().encode(Singleton.shared.selectedCountry)
            UserDefaults.standard.setValue(country, forKey: UD_SELECTED_COUNTRY)
            self.countryTextField.text = val
            self.cityTextField.text = ""
            self.selectedCountry = self.countryData[pos]
            ActivityIndicator.show(view: self.view)
            NavigationController.shared.getCities(countryId: self.countryData[pos].id ?? 0) { (val) in
                self.view.isUserInteractionEnabled = true
                self.cityData = val
                if(self.cityData.count > 0){
                    self.cityTextField.text = self.cityData[0].name
                    self.selectedcity = self.cityData[0]
                }
                ActivityIndicator.hide()
            }
        } else if(self.currentPicker == 2) { //2 for city field
            self.view.isUserInteractionEnabled = true
            self.cityTextField.text = val
            self.selectedcity = self.cityData[pos]
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
            self.view.isUserInteractionEnabled = true // enabled user interaction which was by defauklt off
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var fullNameTextField: DesignableUITextField!
    @IBOutlet weak var emailTextField: DesignableUITextField!
    @IBOutlet weak var contactTextField: DesignableUITextField!
    @IBOutlet weak var countryCode: UITextField!
    @IBOutlet weak var titleTextField: DesignableUITextField!
    @IBOutlet weak var countryTextField: DesignableUITextField!
    @IBOutlet weak var cityTextField: DesignableUITextField!
    @IBOutlet weak var locationLabel: DesignableUILabel!
    @IBOutlet weak var phoneNumberLabel: DesignableUILabel!
    @IBOutlet weak var emailLabel: DesignableUILabel!
    @IBOutlet weak var messageText: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var contactData = ContactDetails()
    var currentPicker = Int()
    var countryData = [Countries]()
    var cityData = [Cities]()
    var selectedcity = Cities()
    var selectedCountry = Countries()
    var locationManager: CLLocationManager!
    var userAnnotation = MKPointAnnotation()
    var isRedirectionFromSideBar = false
    
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getContactDetail()
        self.messageTextView.delegate = self
        self.mainView.isUserInteractionEnabled = true
        self.scrollView.isUserInteractionEnabled = true
        NavigationController.shared.getCountries { (response) in
            self.countryData = response
            Singleton.shared.selectedCountry = response.count > 0 ? response[0]:Countries()
            if(self.countryData.count > 0){
                NavigationController.shared.getCities(countryId: self.countryData[0].id ?? 0) { (response) in
                    self.cityData = response
                }
            }
        }
        
        fullNameTextField.placeholder = "Name".localizableString(loc: Singleton.shared.language ?? "en")
        emailTextField.placeholder = "Email Address".localizableString(loc: Singleton.shared.language ?? "en")
        titleTextField.placeholder = "Title".localizableString(loc: Singleton.shared.language ?? "en")
        countryTextField.placeholder = "Country".localizableString(loc: Singleton.shared.language ?? "en")
        cityTextField.placeholder = "City".localizableString(loc: Singleton.shared.language ?? "en")
        contactTextField.placeholder = "Contact Number".localizableString(loc: Singleton.shared.language ?? "en")
        handleTextAlignmentForArabic(view: self.view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.changeStatusBar(color: K_YELLOW_COLOR)
        
    }
    
    //MARK: Functions
    //getting all details about contact information.
    func getContactDetail(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CONTACT_DETAILS , method: .post, parameter: ["country_id" : Singleton.shared.selectedCountry.id ?? 0], objectClass: GetContactDetailsResponse.self, requestCode: U_GET_CONTACT_DETAILS, userToken: nil) { (response) in
            self.contactData = response.response ?? ContactDetails()
            self.initializeView()
            self.mainView.isUserInteractionEnabled = true
            self.scrollView.isUserInteractionEnabled = true
            let userCoordinate = CLLocationCoordinate2D(latitude: Double(self.contactData.latitude ?? "0")!, longitude: Double(self.contactData.longitude ?? "0")!)
            self.markPinOnMap(coordinate: userCoordinate)
        }
    }
    
    //submit query by user , check validation then call API
    func submitQuery(){
        if (self.fullNameTextField.text!.isEmpty) {
            Singleton.shared.showToast(msg: "Enter your name", controller: self, type: 2)
        } else if (self.emailTextField.text!.isEmpty) {
            Singleton.shared.showToast(msg: "Enter your email", controller: self, type: 2)
        }  else if (self.contactTextField.text!.isEmpty) {
            Singleton.shared.showToast(msg: "Enter your contact number", controller: self, type: 2)
        }  else if (self.titleTextField.text!.isEmpty) {
            Singleton.shared.showToast(msg: "Enter title", controller: self, type: 2)
        }  else if (self.countryTextField.text!.isEmpty) {
            Singleton.shared.showToast(msg: "Select country", controller: self, type: 2)
        }   else if (self.cityTextField.text!.isEmpty) {
            Singleton.shared.showToast(msg: "Select city", controller: self, type: 2)
        }   else if (self.messageTextView.text!.isEmpty) || self.messageTextView.text == "Message" || self.messageTextView.text == "Enter Your Message" {
            Singleton.shared.showToast(msg: "Enter message", controller: self, type: 2)
        } else {
            let param = [
                "name": self.fullNameTextField.text,
                "email": self.emailTextField.text,
                "phone" : self.contactTextField.text,
                "title" : self.titleTextField.text,
                "msg": self.messageTextView.text,
                "country_id": self.selectedCountry.id,
                "city_id": self.selectedcity.city_id
            ] as [String : Any]
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_POST_QUERY , method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_POST_QUERY, userToken: nil) { (response) in
                Singleton.shared.showToast(msg: response.message ?? "", controller: self, type: 0)
                if self.isRedirectionFromSideBar == true {
                    self.menu()
                }
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func initializeView(){
        self.locationLabel.text = self.contactData.address ?? ""
        self.phoneNumberLabel.text = self.contactData.phone ?? ""
        self.emailLabel.text = self.contactData.email ?? ""
        self.mainView.isUserInteractionEnabled = true
        self.scrollView.isUserInteractionEnabled = true
    }
    
    
    //MARK: IBAction
    //open PickerViewController on country Tap
    @IBAction func countryAction(_ sender: Any) {
        self.currentPicker = 1
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.headingLabel = "Select Country"
        myVC.pickerDelegate = self
        for val in self.countryData {
            myVC.pickerData.append(val.name ?? val.name_ar ?? "")
        }
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func submitAction(_ sender: Any) {
        self.submitQuery()
    }
    
    // open PickerViewController on city tap
    @IBAction func cityAction(_ sender: Any) {
        self.currentPicker = 2
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.headingLabel = "Select City"
        myVC.pickerDelegate = self
        for val in self.cityData {
            myVC.pickerData.append(val.name ?? "")
        }
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        if isRedirectionFromSideBar == true {
            self.menu()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func facebookAction(_ sender: Any) {
        self.openUrl(urlStr: self.contactData.facebook ?? "")
    }
    
    @IBAction func whatsappAction(_ sender: Any) {
        let urlWhats = "whatsapp://send?phone=\(self.contactData.whatsapp ?? "")"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL){
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(whatsappURL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(whatsappURL)
                    }
                }
                else {
                    Singleton.shared.showToast(msg: "Install Whatsapp", controller: self, type: 2)
                }
            }
        }
        
    }
    
    @IBAction func instaAction(_ sender: Any) {
        self.openUrl(urlStr: self.contactData.instagram ?? "")
    }
    
    @IBAction func linkedAction(_ sender: Any) {
        self.openUrl(urlStr: "https://www.linkedin.com/in/symo-uae-40b8761a1/")
    }
    
    @IBAction func twitterAction(_ sender: Any) {
        self.openUrl(urlStr: self.contactData.twitter ?? "")
    }
}

//MARK: Map View
extension ContactUsViewController: GMSMapViewDelegate {
    func markPinOnMap(coordinate: CLLocationCoordinate2D) {
        let position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
        self.mapView.animate(to: GMSCameraPosition(latitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 15))
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        let lat = coordinate.latitude
        let long = coordinate.longitude
        let marker = GMSMarker()
        marker.position.longitude =  coordinate.longitude
        marker.position.latitude =  coordinate.latitude
        marker.map = self.mapView
        marker.icon = GMSMarker.markerImage(with: .blue)
        self.mapView?.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 15.0)
    }
}
