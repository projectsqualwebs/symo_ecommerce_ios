//
//  PostReviewViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 29/04/21.
//

import UIKit
import PhotosUI
import Cosmos

protocol PostReview {
    func addProductReview()
}

class PostReviewViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var imageCollection: UICollectionView!
    @IBOutlet weak var starRating: DesignableUILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var descriptionView: UITextView!
    @IBOutlet weak var postReviewHeadingLabel: DesignableUILabel!
    @IBOutlet weak var submitButton: CustomButton!
    @IBOutlet weak var rateThisProductLabel: DesignableUILabel!
    @IBOutlet weak var putRatingAsPerQualityLabel: DesignableUILabel!
    @IBOutlet weak var pleaseShareOpinionlabel: DesignableUILabel!
    @IBOutlet weak var uploadPhotoLabel: DesignableUILabel!
    @IBOutlet weak var reviewTable: ContentSizedTableView!
    @IBOutlet weak var imageCollectionHideView: UIView!
    @IBOutlet weak var noReviewLabel: UILabel!
    
    var imageData = [String]()
    static var postDelegate: PostReview?
    let picker = UIImagePickerController()
    var imageName = ""
    var productId = Int()
    var redirection = 1
    var reviewData = [Reviews]()
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.postReviewHeadingLabel.text = "Post Review".localizableString(loc: Singleton.shared.language ?? "en")
        self.submitButton.setTitle("Submit".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.rateThisProductLabel.text = "Rate this product".localizableString(loc: Singleton.shared.language ?? "en")
        self.putRatingAsPerQualityLabel.text = "Put rating as per quality and satisfaction".localizableString(loc: Singleton.shared.language ?? "en")
        self.pleaseShareOpinionlabel.text = "Please share you opinion this product".localizableString(loc: Singleton.shared.language ?? "en")
        self.noReviewLabel.text = "No Review Found".localizableString(loc: Singleton.shared.language ?? "en")
        self.picker.delegate = self
        ratingView.didFinishTouchingCosmos = {
            rating in
            self.starRating.text = "\(rating)"
        }
        
        if self.redirection == 1 {
            self.uploadPhotoLabel.text = "Upload photos/videos (optional)".localizableString(loc: Singleton.shared.language ?? "en")
            self.uploadPhotoLabel.isHidden = true
            self.reviewTable.isHidden = true
            self.imageCollectionHideView.isHidden = true
            self.noReviewLabel.isHidden = true
        } else {
            self.uploadPhotoLabel.text = "Reviews".localizableString(loc: Singleton.shared.language ?? "en")
            self.reviewTable.isHidden = false
            self.uploadPhotoLabel.isHidden = false
            self.imageCollectionHideView.isHidden = true
            self.noReviewLabel.isHidden = self.reviewData.count > 0 ? true : false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
    }
    
    //MARK: IBAction
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func submitAction(_ sender: Any) {
        if(self.descriptionView.text.isEmpty){
            Singleton.shared.showToast(msg: "Enter description".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else {
            if redirection == 1 {
                ActivityIndicator.show(view: self.view)
                let param:[String:Any]=[
                    "product_id" : self.productId,
                    "rating" : self.starRating.text ?? "",
                    "comment": self.descriptionView.text,
                    "review_imgs": "",
                ]
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_SUBMIT_REVIEW, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_SUBMIT_REVIEW, userToken: nil) { (response) in
                    PostReviewViewController.postDelegate?.addProductReview()
                    Singleton.shared.showToast(msg:(response.message ?? "").localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 0)
                    self.navigationController?.popViewController(animated: true)
                }
            } else {
                ActivityIndicator.show(view: self.view)
                let param:[String:Any]=[
                    "vendor_id" : self.productId,
                    "description" : self.descriptionView.text ?? "",
                    "rate" : Double(self.starRating.text ?? "0") ?? 0.0,
                ]
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_POST_SHOP_REVIEW, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_POST_SHOP_REVIEW, userToken: nil) { (response) in
                    PostReviewViewController.postDelegate?.addProductReview()
                    Singleton.shared.showToast(msg:(response.message ?? "").localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 0)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}

//MARK: Table Delegate
extension PostReviewViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.reviewData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableCell") as! CategoryTableCell
        if((self.reviewData[indexPath.row].avatar ?? "").contains("http")){
            cell.shopImage.sd_setImage(with: URL(string: (self.reviewData[indexPath.row].avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }else {
            cell.shopImage.sd_setImage(with: URL(string: U_IMAGE_STORE_BASE + (self.reviewData[indexPath.row].avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }
        cell.shopName.text = self.reviewData[indexPath.row].name
        let sub = (self.reviewData[indexPath.row].created_at ?? "").prefix(10)
        cell.shopPrice.text = "\(sub)"
        
        switch self.reviewData[indexPath.row].rating {
        case .string(let text):
            cell.ratingView.rating = Double(text) ?? 0
            break
        case .double(let text):
            break
            cell.ratingView.rating = Double(text)
        case .int(let text):
            
            break
        default:
            break
        }
        cell.shopAddress.text = self.reviewData[indexPath.row].review ?? ""
        return cell
    }
    
    
}

extension PostReviewViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate  {
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else { return }
        
        if let selectedImageName = ((info[UIImagePickerController.InfoKey.referenceURL] as? NSURL)?.lastPathComponent) {
            
            self.imageName = selectedImageName
        }else {
            self.imageName = "image.jpg"
        }
        let imageData:NSData = selectedImage.pngData()! as NSData
        
        //save in user default
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        if let cropImage = selectedImage as? UIImage {
            let data = cropImage.jpegData(compressionQuality: 0.2) as Data?
            let params = [
                "file": data!,
            ] as [String : Any]
            // HTTP Request for upload Picture
            ActivityIndicator.show(view: self.view)
            var url = String()
            
            SessionManager.shared.makeMultipartRequest(url: U_BASE + U_REVIEW_IMAGE_UPLOAD, fileData: data!, param: params, fileName: imageName) { (path) in
                print(path)
                self.imageData.append((path as! UploadImageResponse).img_src ?? "")
                self.imageCollection.reloadData()
                ActivityIndicator.hide()
            }
            picker.dismiss(animated: true)
        }
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
        else
        {
            
            
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationPopupViewController") as! ConfirmationPopupViewController
            myVC.confirmationText = "Warning".localizableString(loc: Singleton.shared.language ?? "en")
            myVC.detailText = "You don't have camera".localizableString(loc: Singleton.shared.language ?? "en")
            myVC.secondTxt = false
            myVC.modalPresentationStyle = .overFullScreen
            self.present(myVC, animated: true, completion: nil)
            
        }
    }
    
    func openGallary()
    {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
    }
    
    
}

extension PostReviewViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageData.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollection", for: indexPath) as! ImageCollection
        cell.addButton={
            let alert = UIAlertController(title: "Choose Image".localizableString(loc: Singleton.shared.language ?? "en"), message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Camera".localizableString(loc: Singleton.shared.language ?? "en"), style: .default, handler: { _ in
                self.openCamera()
            }))
            
            alert.addAction(UIAlertAction(title: "Gallery".localizableString(loc: Singleton.shared.language ?? "en"), style: .default, handler: { _ in
                self.openGallary()
            }))
            
            alert.addAction(UIAlertAction.init(title: "Cancel".localizableString(loc: Singleton.shared.language ?? "en"), style: .cancel, handler: nil))
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = self.view.bounds
            }
            self.present(alert, animated: true, completion: nil)
        }
        
        if(self.imageData.count == 0){
            cell.iamgeView.isHidden = true
        }else {
            if((indexPath.row == self.imageData.count)){
                cell.iamgeView.isHidden = true
            }else {
                if(self.imageData[indexPath.row].contains("http")){
                    cell.iamgeView.sd_setImage(with: URL(string: (self.imageData[indexPath.row] ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
                    
                }else {
                    cell.iamgeView.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (self.imageData[indexPath.row] ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
                }
                cell.addView.isHidden = true
                cell.iamgeView.isHidden = false
            }
        }
        return cell
    }
}

class ImageCollection: UICollectionViewCell{
    //MARK: IBOUtlets
    @IBOutlet weak var iamgeView: UIImageView!
    @IBOutlet weak var addView: UIView!
    
    var addButton:(()-> Void)? = nil
    
    
    //MARK: IBActions
    @IBAction func addAction(_ sender: Any) {
        if let addButton = self.addButton{
            addButton()
        }
    }
    
}
