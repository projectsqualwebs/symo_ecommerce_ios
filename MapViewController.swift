//
//  MapViewController.swift
//  Symo New
//
//  Created by Apple on 19/04/21.
//

import UIKit
import GooglePlaces
import GoogleMapsUtils
import GoogleMaps
import MapKit

protocol reloadSearchResult {
    func reloadLocationTabel(searchText:String)
}

class MapViewController: UIViewController ,reloadSearchResult , HandleBlurView {
    //Delegate methods
    func showHideBlurView(type:Int) {
        self.view.isUserInteractionEnabled = true
    }
    
    func reloadLocationTabel(searchText:String) {
        self.tableViewHeight.constant = self.view.frame.height - 100
        self.apiCallForVendorsPin(text: searchText)
        self.view.isUserInteractionEnabled = true
    }
    
    // for loading more data
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView == self.vendorTable){
            if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isLoadingList){
                self.isLoadingList = true
                self.loadMoreItemsForList()
            }
        }
    }
    
    func loadMoreItemsForList(){
        currentPage += 1
        self.apiCallForVendorsPin(text: "")
    }
    
    
    //MARK: IBOutlets
    @IBOutlet weak var categoryName: UITextField!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var addressField: UITextField!
    @IBOutlet weak var vendorTable: UITableView!
    @IBOutlet weak var nearByLabel: DesignableUILabel!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var backButtonView: UIView!
    @IBOutlet weak var blurView: UIView!
    
    
    //My Location
    var location: CLLocation!
    lazy var geocoder = CLGeocoder()
    var marker = GMSMarker()
    private var clusterManager: GMUClusterManager!
    var isNavigationFromLocation = false
    var storeLocation : CLLocation!
    var storeData = [StoreData]()
    var lat : Double?
    var long : Double?
    var isNearBy: Bool = true
    var selectedMarkerIndex = 0
    var currentPage : Int = 1
    var isLoadingList : Bool = false
    var selectedMapStore : Int?
    var storeName : String?
    static var resetDelegate : reloadSearchResult? = nil
    var isRedirectionFromSideBar = false
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        SearchVendorViewController.showMapBlurView = self
        self.blurView.isHidden = true
        FilterScreenViewController.handleBlurView = self
        self.view.isUserInteractionEnabled = true
        SearchVendorViewController.searchDelegate = self
        self.nearByLabel.text = "Near by".localizableString(loc: Singleton.shared.language ?? "en")
        self.categoryName.placeholder = "Search category".localizableString(loc: Singleton.shared.language ?? "en")
        self.addressField.placeholder = "Search by address".localizableString(loc: Singleton.shared.language ?? "en")
        self.mapView.settings.myLocationButton = true
        //This will get the Users Current Location
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleFilter(_:)), name: NSNotification.Name(N_FILTER_PRODUCT), object: nil)
        NavigationController.shared.getUserCurrentLocation()
        vendorTable.estimatedRowHeight = 60
        vendorTable.rowHeight = UITableView.automaticDimension
        mapView.delegate = self
        if(location == nil){
            NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_LOCATION_PERMISSION), object: nil)
        }
        self.backButtonView.isHidden = self.isNavigationFromLocation == true ? false : true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.isUserInteractionEnabled = true
        MapViewController.resetDelegate?.reloadLocationTabel(searchText: "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.isUserInteractionEnabled = true
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
        nearMe = nil
        selectedCategory = Set<Int>()
        selectedSubCategory = Set<Int>()
        selectedCategory = Set<Int>()
        selectedSubCategory = Set<Int>()
        selectedPriceMin = 0
        selectedPriceMax = 10000
        selectedDistance = Int()
        selectedStar = nil
        storeType = nil
        currentFilterParam = [String:Set<Int>]()
        if isNearBy {
            setUpCluster()
        }
        self.tableViewHeight.constant = 140
        currentPage = 1
        if self.isNavigationFromLocation {
            let position = CLLocationCoordinate2D(latitude: storeLocation.coordinate.longitude ?? 0.0, longitude: storeLocation.coordinate.longitude ?? 0.0)
            self.mapView.animate(to: GMSCameraPosition(latitude: storeLocation.coordinate.latitude ?? 0.0, longitude: storeLocation.coordinate.longitude ?? 0.0, zoom: 15))
            self.generateClusterItems()
            self.tableViewHeight.constant = 140
        } else {
            NavigationController.shared.registerEventListenerForLocation { (location) in
                self.initMap(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 15, googleMapView: self.mapView)
                self.view.isUserInteractionEnabled = true
                self.location = location
                if self.isNearBy {
                    //API call vendor pins
                    self.apiCallForVendorsPin(text: "")
                } else {
                    self.markPinOnMap(coordinate: location.coordinate)
                    self.getAddressFromCoordinates(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                }
            }
        }
        
    }
    
    //MARK: Functions
    @objc func handleFilter(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_FILTER_PRODUCT), object: nil)
        currentPage = 1
        self.tableViewHeight.constant = self.view.frame.height - 100
        self.apiCallForVendorsPin(text: "")
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleFilter(_:)), name: NSNotification.Name(N_FILTER_PRODUCT), object: nil)
        self.view.isUserInteractionEnabled = true
    }
    
    func apiCallForVendorsPin(text: String){
        if(location == nil){
            self.storeData = []
            self.vendorTable.reloadData()
            NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_LOCATION_PERMISSION), object: nil)
        }else{
//            ActivityIndicator.show(view: self.view)
            let param:[String:Any] = [
                "lat":   location.coordinate.latitude,
                "long" : location.coordinate.longitude,
                "lang": Singleton.shared.language ?? "en",
                "keyword" : text, // search with the shop name
                "category" : Array(selectedCategory),// category id,
                "sub_category" : Array(selectedSubCategory),// subcategory id
                "distance": selectedDistance == 0 ? "":selectedDistance,// possible values 0-5 km -> 5, 5-10 -> 10, 10-15 -> 15, more than 16 -> 16
                "city_id": Singleton.shared.selectedCity.city_id,
                "country_id" : Singleton.shared.selectedCountry.id,
                "nearby_me" : nearMe,
                "store_type" : storeType,
                "sort_by" : K_SELECTED_SORTBY,
                "filter":[
                    "color": [],
                    "price": [],
                    "size": [],
                    "rating": selectedStar ?? 0
                ]
            ]
            
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_SEARCH_STORES + "\(self.currentPage)", method: .post, parameter: param, objectClass: GetStore.self, requestCode: U_SEARCH_STORES, userToken: nil, reloadData: false) { response in
                ActivityIndicator.hide()
                if(self.storeData.count == 0 || self.currentPage == 1 ){
                    self.storeData = response.response.data
                    self.isLoadingList = false
                } else if response.response.data.count > 0{
                    for val in response.response.data {
                        self.storeData.append(val)
                    }
                    self.isLoadingList = false
                }
                if(response.response.data.count == 0){
                    if self.currentPage > 1 {
                        self.currentPage -= 1
                    }
                    self.isLoadingList = false
                }
                
                
                self.clusterManager.clearItems()
                
                // DispatchQueue.main.async {
                if response.response.data.count > 0 {
                    // Generate and add random items to the cluster manager.
                    self.generateClusterItems()
                }
                else{
                    self.mapView.clear()
                }
                self.vendorTable.reloadData()
                // }
                self.view.isUserInteractionEnabled = true
            }
        }
    }
    
    func setUpCluster() {
        // Set up the cluster manager with the supplied icon generator and
        // renderer.
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView,
                                                 clusterIconGenerator: iconGenerator)
        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm,
                                           renderer: renderer)
        
        // Call cluster() after items have been added to perform the clustering
        // and rendering on map.
        clusterManager.cluster()
    }
    
    /// Randomly generates cluster items within some extent of the camera and
    /// adds them to the cluster manager.
    private func generateClusterItems() {
        mapView.clear()
        clusterManager.clearItems()
        let extent = 0.001
        if storeLocation != nil {
            let lat = (storeLocation.coordinate.latitude ?? 0.0) + extent * randomScale()
            let lng = (storeLocation.coordinate.longitude ?? 0.0) + extent * randomScale()
            let name = storeName ?? ""
            let item = POIItem(position: CLLocationCoordinate2DMake(lat, lng), name: name ?? "No Name".localizableString(loc: Singleton.shared.language ?? "en"))
            clusterManager.add(item)
        } else {
            for vendorLocation in self.storeData {
                let lat = Double(vendorLocation.latitude ?? "0")! + extent * randomScale()
                let lng = Double(vendorLocation.longitude ?? "0")! + extent * randomScale()
                let name = vendorLocation.name
                let item = POIItem(position: CLLocationCoordinate2DMake(lat, lng), name: name ?? "No Name".localizableString(loc: Singleton.shared.language ?? "en"))
                clusterManager.add(item)
            }
        }
        clusterManager.cluster()
    }
    
    /// Returns a random value between -1.0 and 1.0.
    private func randomScale() -> Double {
        return Double(arc4random()) / Double(UINT32_MAX) * 2.0 - 1.0
    }
    
    //MARK: IBActions
    @IBAction func categoryAction(_ sender: Any) {
        //        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        //        myVC.pickerDelegate = self
        //        for val in self.categoryData{
        //            myVC.pickerData.append(val.category_name ?? "")
        //        }
        //        myVC.headingLabel = "Select Category"
        //        if(self.categoryData.count > 0){
        //         self.navigationController?.present(myVC, animated: true, completion: nil)
        //        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        if isRedirectionFromSideBar == true {
            self.menu()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addressAction(_ sender: Any) {
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        self.present(acController, animated: true, completion: nil)
    }
    
    
    @IBAction func clearAction(_ sender: Any) {
        self.addressField.text = ""
    }
    
    @IBAction func tabViewHeightAction(_ sender: Any) {
        if(self.tableViewHeight.constant == 140){
            self.tableViewHeight.constant = self.view.frame.height - 100
        }else {
            self.tableViewHeight.constant = 140
        }
    }
    
}

//MARK: Map view
extension MapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        if !isNearBy {
            getAddressFromCoordinates(latitude: coordinate.latitude, longitude: coordinate.longitude)
            markPinOnMap(coordinate: coordinate)
        }
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let poiItem = marker.userData as? POIItem {
            marker.title = poiItem.name
            var vendorDetail = StoreData()
            vendorDetail = self.storeData.filter{
                $0.name == marker.title
            }[0]
            let alert = UIAlertController(title: "Select Option".localizableString(loc: Singleton.shared.language ?? "en"), message: nil, preferredStyle: .actionSheet)
            let option1 = UIAlertAction(title: "Google Map".localizableString(loc: Singleton.shared.language ?? "en"), style: .default) { (action) in
                
                if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
                    let name = vendorDetail.name?.replacingOccurrences(of: " ", with: "+")
                    let latLng = "\(vendorDetail.latitude ?? "0"),\(vendorDetail.latitude ?? "0")"
                    let url = "comgooglemaps://?q=\(latLng)&center=\(latLng)"
                    UIApplication.shared.openURL(NSURL(string: url)! as URL)
                } else {
                    let appStore = URL(string: "https://itunes.apple.com/us/app/google-maps-transit-food/id585027354?mt=8#")
                    UIApplication.shared.openURL(appStore!)
                }
            }
            let option2 = UIAlertAction(title: "Apple Map".localizableString(loc: Singleton.shared.language ?? "en"), style: .default) { (action) in
                let latitude:CLLocationDegrees =  Double(vendorDetail.latitude ?? "0")!
                let longitude:CLLocationDegrees =  Double(vendorDetail.longitude ?? "0")!
                let regionDistance:CLLocationDistance = 10000
                let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
                let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
                let options = [
                    MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                    MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
                ]
                let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
                let mapItem = MKMapItem(placemark: placemark)
                mapItem.name = "\(vendorDetail.name ?? "")"
                mapItem.openInMaps(launchOptions: options)
            }
            let option3 = UIAlertAction(title: "Business Profile".localizableString(loc: Singleton.shared.language ?? "en"), style: .default) { (action) in
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
                myVC.vendorId = vendorDetail.id ?? 0
                resetView = false
                self.navigationController?.pushViewController(myVC, animated: true)
            }
            
            let option4 = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
            alert.addAction(option1)
            alert.addAction(option2)
            alert.addAction(option3)
            alert.addAction(option4)
            self.present(alert, animated: true, completion: nil)
        }
        return false
    }
    
    //For making annotation
    func markPinOnMap(coordinate: CLLocationCoordinate2D) {
        self.location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        self.lat = coordinate.latitude
        self.long = coordinate.longitude
        self.marker.position.longitude =  coordinate.longitude
        self.marker.position.latitude =  coordinate.latitude
        self.marker.map = self.mapView
        self.marker.icon = GMSMarker.markerImage(with: .blue)
        self.mapView?.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 15.0)
    }
    
    //for getting address details from coordinates
    func getAddressFromCoordinates(latitude: CLLocationDegrees, longitude: CLLocationDegrees)
    {
        let location = CLLocation(latitude: latitude, longitude: longitude)
        geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            let addressDict = String(describing: (placemarks?[0].addressDictionary as Any))
            if placeMark != nil {
                if addressDict != "nil" {
                    let address = placeMark.addressDictionary?["FormattedAddressLines"] as? AnyObject
                    let array_address = address as! NSArray
                    let city = String(describing: (placeMark.addressDictionary!["City"] as AnyObject))
                    let subAdministrativeArea = String(describing: (placeMark.addressDictionary!["SubAdministrativeArea"] as AnyObject))
                    let state = String(describing: (placeMark.addressDictionary!["State"] as AnyObject))
                    for var index in 0..<array_address.count
                    {
                        if index == 0 {
                            self.addressField.text = String(describing: array_address[index])
                        } else {
                            self.addressField.text = self.addressField.text! + ", " + String(describing: array_address[index])
                        }
                    }
                }
                else {
                    self.addressField.text = ""
                }
            } else {
                self.addressField.text = ""
            }
        }
    }
    
}

//MARK: Table View Delegate
extension MapViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.storeData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressTableCell") as! AddressTableCell
        let val = self.storeData[indexPath.row]
        cell.addressType.text = val.name
        //   cell.street.text = val.address
        cell.street.text = "\(Int(val.distance ?? 0)) " + "kms".localizableString(loc: Singleton.shared.language ?? "en")
        cell.address.text = "\(val.ratings_count ?? 0) " + "reviews".localizableString(loc: Singleton.shared.language ?? "en")
        if((val.shop_image ?? "").contains("http")){
            cell.storeImage.sd_setImage(with: URL(string: (val.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }else {
            cell.storeImage.sd_setImage(with: URL(string: U_PRODUCT_IMAGE_BASE + (val.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }
        let open = self.convertTimestampToDate(val.open_time ?? 0, to: "h: mm a")
        let close = self.convertTimestampToDate(val.close_time ?? 0, to: "h: mm a")
        //  cell.address.text = "Timings ".localizableString(loc: Singleton.shared.language ?? "en") + "| \(open) - \(close)"
        cell.editLocalizeButton.setTitle("View".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        if selectedMapStore == indexPath.row {
            cell.editLocalizeButton.isHidden = false
        } else {
            cell.editLocalizeButton.isHidden = true
        }
        
        cell.editButton = {
            self.selectedMapStore = nil
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
            myVC.vendorId = self.storeData[indexPath.row].id ?? 0
            var vendorDetail = StoreData()
            if let poiItem = self.marker.userData as? POIItem {
                self.marker.title = poiItem.name
                vendorDetail = self.storeData.filter{
                    $0.name == self.marker.title
                }[0]
            }
            resetView = false
            self.navigationController?.pushViewController(myVC, animated: true)
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(self.storeData.count > indexPath.row){
            self.selectedMapStore = indexPath.row
            let val = self.storeData[indexPath.row]
            let position = CLLocationCoordinate2D(latitude: Double(val.latitude ?? "0")!, longitude: Double(val.longitude ?? "0")!)
            self.mapView.animate(to: GMSCameraPosition(latitude: Double(val.latitude ?? "0")!, longitude: Double(val.longitude ?? "0")!, zoom: 15))
            self.generateClusterItems()
            self.vendorTable.reloadData()
        }
    }
    
}


//MARK: MAP Auto complete
extension MapViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        self.addressField.text = place.formattedAddress
        
        // API CALL FOR MAP when user update location
        location = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        
        // This will update the user Current Location
        
        dismiss(animated: true, completion: {
            if self.isNearBy {
                self.initMap(withLatitude: self.location.coordinate.latitude, longitude: self.location.coordinate.longitude, zoom: 15, googleMapView: self.mapView)
                //DispatchQueue.global(qos: .userInteractive).async {
                self.apiCallForVendorsPin(text: "")
                //}
            } else {
                self.markPinOnMap(coordinate: place.coordinate)
            }
        })
    }
    
    func initMap(withLatitude lat: CLLocationDegrees, longitude lng: CLLocationDegrees, zoom: Int , googleMapView: GMSMapView) {
        var myLocationMarker: GMSMarker?
        defer {
        }
        do {
            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: Float(zoom))
            googleMapView.isMyLocationEnabled = true
            googleMapView.camera = camera
            // Creates a marker in the center of the map.
            myLocationMarker?.map = googleMapView
        } catch let ex {
            
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: \(error)")
        dismiss(animated: true, completion: nil)
    }
    
    // User cancelled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        dismiss(animated: true, completion: nil)
    }
}


class POIItem: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var name: String!
    
    init(position: CLLocationCoordinate2D, name: String) {
        self.position = position
        self.name = name
    }
}

