//
//  SelectCategoryViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 25/12/21.
//

import UIKit
import Foundation

protocol SelectedCategories {
    func reloadSearchByCollection()
}

class SelectCategoryViewController: UIViewController {
    
    //MARK: IBOUTLETS
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: PROPERTIES
    static var categoryDelegate: SelectedCategories? = nil
    var subcategories = [ShopByCategoryResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
    }
    
    //MARK: IBActions
    @IBAction func doneBtnPressed(_ sender: Any) {
        let categoriesSelected = try? JSONEncoder().encode(self.subcategories)
        UserDefaults.standard.set(categoriesSelected, forKey: UD_SELECTED_CATEGORIES)
        SelectCategoryViewController.categoryDelegate?.reloadSearchByCollection()
        dismiss(animated: true, completion: nil)
    }
    
    
}

//MARK: TABLE View Delegate
extension SelectCategoryViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subcategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ShipCategoriesCell", for: indexPath) as! ShipCategoriesCell
        
        let val = self.subcategories[indexPath.row]
        cell.lblCategories.text = val.name
        if val.isSelected == true{
            cell.cellImage.image = #imageLiteral(resourceName: "GreyTick")
        } else {
            cell.cellImage.image = UIImage(named: "GreyBox")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ShipCategoriesCell
        if(cell.cellImage.image == #imageLiteral(resourceName: "GreyTick")){
            cell.cellImage.image = #imageLiteral(resourceName: "GreyBox")
            self.subcategories[indexPath.row].isSelected = false
        }else{
            cell.cellImage.image = #imageLiteral(resourceName: "GreyTick")
            self.subcategories[indexPath.row].isSelected = true
            
        }
    }
}

//MARK: Table cell
class ShipCategoriesCell: UITableViewCell{
    @IBOutlet weak var lblCategories: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    
    var deleteButton:(()-> Void)? = nil
    
    @IBAction func deleteAction(_ sender: Any) {
        if let deleteButton = self.deleteButton{
            deleteButton()
        }
    }
}
