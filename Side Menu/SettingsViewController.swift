//
//  SettingsViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 10/06/21
//

import UIKit

class SettingsViewController: UIViewController, SelectFromPicker , Confirmation{
    
    //MARK: Picker Delegate function
    func selectedPickerData(val: String, pos: Int) {
        if(self.currentPicker == 1){
            
            if Singleton.shared.selectedCountry.id == self.countryData[pos].id{
                return
            }else{
                changeCountry(val: val, pos: pos)
//                //checking cart data, if cart empty
//                if Singleton.shared.cartData.cart?.cart_items == nil{
//                    changeCountry(val: val, pos: pos)
//                }else{
//                    self.showClearPopup(title: "Clear Cart".localizableString(loc: Singleton.shared.language ?? "en"), msg: "Changing your country will clear your cart. Are you sure you want to proceed?".localizableString(loc: Singleton.shared.language ?? "en") , val: val, pos: pos)
//                }
            }
            
        } else if(self.currentPicker == 2) {
            self.view.isUserInteractionEnabled = true
            self.selectedCity.text = val/*.localizableString(loc: Singleton.shared.language ?? "en")*/
            let city = try? JSONEncoder().encode(self.cityData[pos])
            Singleton.shared.selectedCity = self.cityData[pos]
            UserDefaults.standard.setValue(city, forKey: UD_SELECTED_CITY)
            NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_CITY), object: nil, userInfo: ["callApi": "yes"])
        }else if(self.currentPicker == 3){
            self.view.isUserInteractionEnabled = true
            if Singleton.shared.language?.lowercased() == self.langData[pos].code?.lowercased(){
                return
            }
            self.selectedLanguage.text = (self.langData[pos].code ?? "").uppercased()
            Singleton.shared.language = (self.langData[pos].code ?? "")
            Singleton.shared.selectedLanguage = (self.langData[pos].code ?? "")
            Singleton.shared.saveAppLanguage(withLanguage: (self.langData[pos].code ?? ""))
            UserDefaults.standard.setValue((self.langData[pos].code ?? ""), forKey: K_APP_LANGUAGE)
            SettingsViewController.handletab?.localize(rtl: true)
            NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_CITY), object: nil, userInfo: ["callApi": "yes"])
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            myVC.isLanguageChangedRedirection = true
            self.navigationController?.pushViewController(myVC, animated: true)
        }else if(self.currentPicker == 4){
            self.view.isUserInteractionEnabled = true
            self.selectedCurrency.text = val.uppercased()
            Singleton.shared.selectedCurrency = val.lowercased()
            if val.lowercased() == "aed" {
                Singleton.shared.selectedCurrencySymbol = "AED"
            } else if val.lowercased() == "pound"{
                Singleton.shared.selectedCurrencySymbol = "£"
            } else if val.lowercased() == "usd" {
                Singleton.shared.selectedCurrencySymbol = "$"
            } else if val.lowercased() == "euro" {
                Singleton.shared.selectedCurrencySymbol = "€"
            }else if val.lowercased() == "bhd" {
                Singleton.shared.selectedCurrencySymbol = "BHD"
            }
            UserDefaults.standard.setValue(val.lowercased(), forKey: UD_SELECTED_CURRENCY)
            NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_CITY), object: nil, userInfo: ["callApi": "yes"])
        }
        
        self.view.isUserInteractionEnabled = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
            self.view.isUserInteractionEnabled = true
        }
    }
    
    
    
    func confirmationSelection(action: Int) {
        
        let val = self.selectedVar
        let pos = self.selectedPos
        if(action == 1){
            ActivityIndicator.show(view: self.view)
            if(Singleton.shared.userDetail.id != nil){
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_CART, method: .post, parameter: ["cart_id":Singleton.shared.cartData.cart?.id ?? 0, "lang": Singleton.shared.language ?? "en"], objectClass: SuccessResponse.self, requestCode: U_DELETE_CART, userToken: nil) { (response) in
                    UserDefaults.standard.removeObject(forKey: UD_BAG_COUNT)
                    K_BAG_COUNT = 0
                    self.changeCountry(val: val, pos: pos)
                    Singleton.shared.cartData = GetCartResponse()
                    ActivityIndicator.hide()
                }
            } else {
                var cartId = ""
                if(UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) != nil){
                    cartId = UserDefaults.standard.value(forKey: UD_GUEST_CART_ID) as! String
                }
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_GUEST_CART + "\(cartId)" , method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_DELETE_GUEST_CART, userToken: nil) { (response) in
                    UserDefaults.standard.removeObject(forKey: UD_BAG_COUNT)
                    K_BAG_COUNT = 0
                    self.changeCountry(val: val, pos: pos)
                    Singleton.shared.cartData = GetCartResponse()
                    ActivityIndicator.hide()
                }
            }
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var selectedCountry: DesignableUILabel!
    @IBOutlet weak var selectedCity: DesignableUILabel!
    @IBOutlet weak var selectedLanguage: DesignableUILabel!
    @IBOutlet weak var selectedCurrency: DesignableUILabel!
    
    @IBOutlet weak var settingsHeadingLabel: DesignableUILabel!
    @IBOutlet weak var selectedCountryLabel: DesignableUILabel!
    @IBOutlet weak var selectedCityLabel: DesignableUILabel!
    @IBOutlet weak var selectedLanguageLabel: DesignableUILabel!
    @IBOutlet weak var selectedCurrencyLabel: DesignableUILabel!
    
    
    var langData = [LanguageResponse]()
    var countryData = [Countries]()
    var cityData = [Cities]()
    var currentPicker = 1
    static var handletab : HandleTabBarItems? = nil
    var selectedVar = ""
    var selectedPos = 0
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.isUserInteractionEnabled = true
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
        self.selectedCountry.text = Singleton.shared.selectedCountry.name ?? Singleton.shared.selectedCountry.name_en
        self.selectedCity.text = Singleton.shared.selectedCity.name/*?.localizableString(loc: Singleton.shared.language ?? "en")*/
        self.selectedLanguage.text = Singleton.shared.selectedLanguage.uppercased()
        self.selectedCurrency.text = Singleton.shared.selectedCurrency.uppercased()
    }
    
    func localize(){
        self.settingsHeadingLabel.text = "Settings".localizableString(loc: Singleton.shared.language ?? "en")
        self.selectedCountryLabel.text = "Select Country".localizableString(loc: Singleton.shared.language ?? "en")
        self.selectedCityLabel.text = "Select City".localizableString(loc: Singleton.shared.language ?? "en")
        self.selectedLanguageLabel.text = "Select Language".localizableString(loc: Singleton.shared.language ?? "en")
        self.selectedCurrencyLabel.text = "Select Currency".localizableString(loc: Singleton.shared.language ?? "en")
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.menu()
    }
    
    @IBAction func countryAction(_ sender: UIButton) {
        self.currentPicker = sender.tag
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        myVC.showInEnglishOnly = true
        if(currentPicker == 1){
            myVC.headingLabel = "Select Country"
            for val in self.countryData {
                myVC.pickerData.append(val.name ?? val.name_en ?? "")
            }
        }else if(self.currentPicker == 2){
            myVC.headingLabel = "Select City"
            for val in self.cityData {
                myVC.pickerData.append(val.name ?? "")
            }
        }else if(self.currentPicker == 3){
            myVC.headingLabel = "Select Language"
            myVC.pickerData = self.langData.map{ val in
                return val.lang_name ?? ""
            }
        }else if self.currentPicker == 4 {
            myVC.headingLabel = "Select Currency"
            myVC.pickerData = []
            myVC.pickerData.append("aed".uppercased())
            myVC.pickerData.append("usd".uppercased())
            myVC.pickerData.append("pound".uppercased())
            myVC.pickerData.append("euro".uppercased())
            myVC.pickerData.append("bhd".uppercased())
        }
        
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }else{
            getData()
        }
    }
    
    func getData(){
        localize()
        NavigationController.shared.getLangByCountry(countryId: Singleton.shared.selectedCountry.id ?? 0) { response in
            self.langData = response
        }
        
        NavigationController.shared.getCountries { (response) in
            self.countryData = response
            
            // Retrieve the country object from UserDefaults
            if let encodedData = UserDefaults.standard.data(forKey: UD_SELECTED_COUNTRY),
               let storedCountry = try? JSONDecoder().decode(Countries.self, from: encodedData) {
                Singleton.shared.selectedCountry = storedCountry
            } else {
                Singleton.shared.selectedCountry = response.count > 0 ? response[0]:Countries()
            }
            
            if(self.countryData.count > 0){
                NavigationController.shared.getCities(countryId: Singleton.shared.selectedCountry.id ?? 0) { (response) in
                    for i in response {
                        self.cityData.append(i)
                    }
                }
            }
        }
    }
    
    //change default country for user
    func changeCountry(val: String, pos: Int){
        Singleton.shared.cityData = []
        Singleton.shared.langData = []
        self.cityData = []
        ActivityIndicator.show(view: self.view)
        Singleton.shared.selectedCountry = self.countryData[pos]
        let country = try? JSONEncoder().encode(Singleton.shared.selectedCountry)
        UserDefaults.standard.setValue(country, forKey: UD_SELECTED_COUNTRY)
        self.selectedCountry.text = val
        NavigationController.shared.getLangByCountry(countryId: Singleton.shared.selectedCountry.id ?? 0) { response in
            self.langData = response
            if(response.count > 0){
                
                //for changing currency on country change
                if(response[0].country_id == 36){
                    Singleton.shared.selectedCurrency = "bhd"
                    Singleton.shared.selectedCurrencySymbol = "BHD"
                    UserDefaults.standard.setValue("bhd", forKey: UD_SELECTED_CURRENCY)
                    self.selectedCurrency.text = "BHD"
                }else{
                    Singleton.shared.selectedCurrency = "aed"
                    Singleton.shared.selectedCurrencySymbol = "AED"
                    UserDefaults.standard.setValue("aed", forKey: UD_SELECTED_CURRENCY)
                    self.selectedCurrency.text = "AED"
                }
                
                // fetch bookmark store of these country..
                NavigationController.shared.getAllBookMarkStore()
                
                
                ActivityIndicator.show(view: self.view)
                NavigationController.shared.getCities(countryId: self.countryData[pos].id ?? 0) { (val) in
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
                        self.view.isUserInteractionEnabled = true
                    }
                    
                    for i in val {
                        self.cityData.append(i)
                    }
                    if(self.cityData.count > 0){
                        Singleton.shared.selectedCity = self.cityData[0]
                        self.selectedCity.text = self.cityData[0].name/*?.localizableString(loc: Singleton.shared.language ?? "en")*/
                        let encodedCity = try? JSONEncoder().encode(Singleton.shared.selectedCity)
                        UserDefaults.standard.setValue(encodedCity, forKey: UD_SELECTED_CITY)
                    }
                    ActivityIndicator.hide()
                    NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_CITY), object: nil, userInfo: ["callApi": "yes"])
                }
            }
        }
        
        let encode = try? JSONEncoder().encode(self.countryData[pos])
        UserDefaults.standard.setValue(encode, forKey: UD_SELECTED_COUNTRY)
    }
    
    //Warning popup before change country when there is product in cart
    func showClearPopup(title: String, msg: String, val: String, pos: Int) {
        
        
        self.selectedVar = val
        self.selectedPos = pos
        
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationPopupViewController") as! ConfirmationPopupViewController
        myVC.confirmationDelegate = self
        myVC.confirmationText = title.localizableString(loc: Singleton.shared.language ?? "en")
        myVC.detailText = msg.localizableString(loc: Singleton.shared.language ?? "en")
        myVC.modalPresentationStyle = .overFullScreen
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
            self.present(myVC, animated: true, completion: nil)
        }
    }
}
