//
//  DealsViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 09/03/22.
//

import UIKit
import GoogleMaps
import GooglePlaces
import GoogleMapsUtils
import MapKit

class DealsViewController: UIViewController,UITextFieldDelegate, SortData ,HandleBlurView, SelectFromPicker{
    //MARK: Delegate functions
    //picker view delegate
    func selectedPickerData(val: String, pos: Int) {
        if pos == 0 {
            K_SELECTED_SORTBY = 1
        } else if pos == 1{
            K_SELECTED_SORTBY = 3
        } else if pos == 2{
            K_SELECTED_SORTBY = 4
        }
        self.sortShopList(option: K_SELECTED_SORTBY)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func showHideBlurView(type:Int) {
        if type == 1 {
            self.blurView.isHidden = true
        } else if type == 2{
            self.blurView.isHidden = false
        }
    }
    
    
    
    func sortShopList(option: Int) {
        self.searchField.text = ""
        self.getDeals(text: "")
    }
    
    func sortProductList(option: Int) {
    }
    
    //textfield delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text =  (self.searchField.text ?? "") + string
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                if text.count == 1 {
                    self.getDeals(text: "")
                }
            }
        }
        self.dealsCollectionView.reloadData()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if self.searchField.text!.isEmpty {
            self.getDeals(text: "")
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView == self.dealsCollectionView){
            if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isLoadingList){
                self.isLoadingList = true
                self.loadMoreItemsForList()
            }
        }
    }
    
    func loadMoreItemsForList(){
        self.currentPage += 1
        self.getDeals(text: "")
    }
    
    //MARK: Outlets
    @IBOutlet weak var searchField: DesignableUITextField!
    @IBOutlet weak var dealsCollectionView: UICollectionView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var offerPopupDescription: UILabel!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var noDealsFoundLabel: UILabel!
    
    var location: CLLocation!
    lazy var geocoder = CLGeocoder()
    var marker = GMSMarker()
    private var clusterManager: GMUClusterManager!
    var isNavigationFromLocation = false
    var storeLocation : CLLocation!
    var storeData = [StoreData]()
    var lat : Double?
    var long : Double?
    var isNearBy: Bool = true
    var selectedMarkerIndex = 0
    var currentPage : Int = 1
    var isLoadingList : Bool = false
    var selectedMapStore : Int?
    var storeName : String?
    static var resetDelegate : reloadSearchResult? = nil
    var isRedirectionFromSideBar = false
    var sortType = String()
    
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        handleTextAlignmentForArabic(view: self.view)
        FilterScreenViewController.handleBlurView = self
        self.blurView.addBlurEffect()
        self.searchField.delegate = self
        self.mapView.settings.myLocationButton = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleFilter(_:)), name: NSNotification.Name(N_FILTER_PRODUCT), object: nil)
        NavigationController.shared.getUserCurrentLocation()
        mapView.delegate = self
        if(location == nil){
            NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_LOCATION_PERMISSION), object: nil)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleFilter(_:)), name: NSNotification.Name(N_FILTER_PRODUCT), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
        nearMe = nil
        selectedCategory = Set<Int>()
        selectedSubCategory = Set<Int>()
        selectedCategory = Set<Int>()
        selectedSubCategory = Set<Int>()
        selectedPriceMin = 0
        selectedPriceMax = 10000
        selectedDistance = Int()
        selectedStar = nil
        storeType = nil
        currentFilterParam = [String:Set<Int>]()
        if isNearBy {
            setUpCluster()
        }
        /*Get Location For User*/
        self.collectionViewHeight.constant = 330
        currentPage = 1
        if self.isNavigationFromLocation {
            let position = CLLocationCoordinate2D(latitude: storeLocation.coordinate.longitude ?? 0.0, longitude: storeLocation.coordinate.longitude ?? 0.0)
            self.mapView.animate(to: GMSCameraPosition(latitude: storeLocation.coordinate.latitude ?? 0.0, longitude: storeLocation.coordinate.longitude ?? 0.0, zoom: 15))
            self.generateClusterItems()
            self.collectionViewHeight.constant = 330
        } else {
            NavigationController.shared.registerEventListenerForLocation { (location) in
                self.initMap(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 15, googleMapView: self.mapView)
                
                self.location = location
                if self.isNearBy {
                    //API call vendor pins
                    self.getDeals(text: "")
                } else {
                    self.markPinOnMap(coordinate: location.coordinate)
                }
            }
        }
        
    }
    
    //MARK: Functions
    @objc func handleFilter(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_FILTER_PRODUCT), object: nil)
        currentPage = 1
        self.collectionViewHeight.constant = self.view.frame.height - 180
        self.getDeals(text: "")
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleFilter(_:)), name: NSNotification.Name(N_FILTER_PRODUCT), object: nil)
    }
    
    func getDeals(text: String){
        if(location == nil){
            self.storeData = []
            self.dealsCollectionView.reloadData()
            NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_LOCATION_PERMISSION), object: nil)
        }else{
            ActivityIndicator.show(view: self.view)
            let param:[String:Any] = [
                "country_id": Singleton.shared.selectedCountry.id,
                "currency" : Singleton.shared.selectedCurrency,
                "sort" : self.sortType,
                "lat":   location.coordinate.latitude,
                "long" : location.coordinate.longitude,
                "lang": Singleton.shared.language ?? "en",
                "keyword" : text, // search with the shop name
                "category" : Array(selectedCategory),// category id,
                "sub_category" : Array(selectedSubCategory),// subcategory id
                "distance": selectedDistance == 0 ? "":selectedDistance,// possible values 0-5 km -> 5, 
                "city_id": Singleton.shared.selectedCity.city_id,
                "store_type" : storeType,
                "nearby_me" : nearMe,
                "filter":[
                    "color": [],
                    "price": [],
                    "size": [],
                    "rating": selectedStar ?? 0
                ]
            ]
            
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_DEALS + "?lang=\(Singleton.shared.language ?? "en")&lat=\(Singleton.shared.userLocation.coordinate.latitude)&long=\(Singleton.shared.userLocation.coordinate.longitude)&city_id=\(Singleton.shared.selectedCity.city_id)", method: .post, parameter: param, objectClass: GetDealsResponse.self, requestCode: U_GET_DEALS, userToken: nil) { response in
                
                self.storeData = response.response?.offers?.data ?? []
                
                self.clusterManager.clearItems()
                if (response.response?.deals?.count ?? 0) > 0 {
                    self.generateClusterItems()
                }
                else{
                    self.mapView.clear()
                }
                self.dealsCollectionView.reloadData()
            }
        }
    }
    
    func setUpCluster() {
        // Set up the cluster manager with the supplied icon generator and
        // renderer.
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView,
                                                 clusterIconGenerator: iconGenerator)
        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm,
                                           renderer: renderer)
        
        // Call cluster() after items have been added to perform the clustering
        // and rendering on map.
        clusterManager.cluster()
    }
    
    /// Randomly generates cluster items within some extent of the camera and
    /// adds them to the cluster manager.
    private func generateClusterItems() {
        mapView.clear()
        clusterManager.clearItems()
        let extent = 0.001
        if storeLocation != nil {
            let lat = (storeLocation.coordinate.latitude ?? 0.0) + extent * randomScale()
            let lng = (storeLocation.coordinate.longitude ?? 0.0) + extent * randomScale()
            let name = storeName ?? ""
            let item = POIItem(position: CLLocationCoordinate2DMake(lat, lng), name: name ?? "No Name".localizableString(loc: Singleton.shared.language ?? "en"))
            clusterManager.add(item)
        } else {
            for vendorLocation in self.storeData {
                let lat = Double(vendorLocation.latitude ?? "0")! + extent * randomScale()
                let lng = Double(vendorLocation.longitude ?? "0")! + extent * randomScale()
                let name = vendorLocation.name
                let item = POIItem(position: CLLocationCoordinate2DMake(lat, lng), name: name ?? "No Name".localizableString(loc: Singleton.shared.language ?? "en"))
                clusterManager.add(item)
            }
        }
        clusterManager.cluster()
    }
    
    /// Returns a random value between -1.0 and 1.0.
    private func randomScale() -> Double {
        return Double(arc4random()) / Double(UINT32_MAX) * 2.0 - 1.0
    }
    
    //MARK: IBActions
    @IBAction func filterAction(_ sender: Any) {
        self.blurView.isHidden = false
        self.openFilter(view: 4)
    }
    
    
    
    @IBAction func backAction(_ sender: Any) {
        if isRedirectionFromSideBar == true {
            self.menu()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func searchAction(_ sender: Any) {
        if self.searchField.text != "" {
            self.getDeals(text: self.searchField.text ?? "")
        }
    }
    
    @IBAction func tabViewHeightAction(_ sender: Any) {
        if(self.collectionViewHeight.constant == 330){
            self.collectionViewHeight.constant = self.view.frame.height - 180
        }else {
            self.collectionViewHeight.constant = 330
        }
    }
    
    @IBAction func sortAction(_ sender: Any) {
//        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SortbyViewController") as! SortbyViewController
//        myVC.sortDelegate = self
//        myVC.redirectionFrom = 1
//        self.navigationController?.present(myVC, animated: true, completion: nil)
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        myVC.pickerData = ["Latest","A-Z","Z-A"]
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func closePopupAction(_ sender: Any) {
        self.popupView.isHidden = true
    }
}


//MARK: Collection View
extension DealsViewController : UICollectionViewDelegate,UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.noDealsFoundLabel.isHidden = self.storeData.count > 0 ? true : false
        return self.storeData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionCell", for: indexPath) as! CategoryCollectionCell
        let val = self.storeData[indexPath.item]
        cell.shopName.text = val.name ?? ""
        if((val.avatar ?? "").contains("http")){
            cell.shopImage.sd_setImage(with: URL(string: (val.offer_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }else {
            cell.shopImage.sd_setImage(with: URL(string: U_PRODUCT_IMAGE_BASE + (val.offer_image ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }
        cell.shopDistance.text = (val.offer_discount ?? "") + "%"
        cell.shopPrice.text = "\(self.convertTimestampToDate(val.offer_start_date ?? 0, to: "dd MMM,YYYY"))"
        cell.shopRating.text = "\(self.convertTimestampToDate(val.offer_end_date ?? 0, to: "dd MMM,YYYY"))"
        
        cell.favouriteButton = {
            self.offerPopupDescription.text = val.offer_description ?? ""
            self.popupView.isHidden = false
        }
        
        if selectedMapStore == indexPath.row {
            cell.viewButton.isHidden = false
        } else {
            cell.viewButton.isHidden = true
        }
        
        cell.view = {
            self.selectedMapStore = nil
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
            myVC.vendorId = self.storeData[indexPath.row].id ?? 0
            self.navigationController?.pushViewController(myVC, animated: true)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedMapStore = indexPath.row
        let val = self.storeData[indexPath.row]
        let position = CLLocationCoordinate2D(latitude: Double(val.latitude ?? "0")!, longitude: Double(val.longitude ?? "0")!)
        self.mapView.animate(to: GMSCameraPosition(latitude: Double(val.latitude ?? "0")!, longitude: Double(val.longitude ?? "0")!, zoom: 15))
        self.generateClusterItems()
        self.dealsCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width/2)-15, height:310)
    }
}

//MARK: Map View Delegate Methods

extension DealsViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        if !isNearBy {
            markPinOnMap(coordinate: coordinate)
        }
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let poiItem = marker.userData as? POIItem {
            marker.title = poiItem.name
            var vendorDetail = StoreData()
            vendorDetail = self.storeData.filter{
                $0.name == marker.title
            }[0]
            let alert = UIAlertController(title: "Select Option".localizableString(loc: Singleton.shared.language ?? "en"), message: nil, preferredStyle: .actionSheet)
            let option1 = UIAlertAction(title: "Google Map".localizableString(loc: Singleton.shared.language ?? "en"), style: .default) { (action) in
                if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
                    let name = vendorDetail.name?.replacingOccurrences(of: " ", with: "+")
                    let latLng = "\(vendorDetail.latitude ?? "0"),\(vendorDetail.latitude ?? "0")"
                    let url = "comgooglemaps://?q=\(latLng)&center=\(latLng)"
                    UIApplication.shared.openURL(NSURL(string: url)! as URL)
                } else {
                    let appStore = URL(string: "https://itunes.apple.com/us/app/google-maps-navigation-transit/id585027354?mt=8#")
                    UIApplication.shared.openURL(appStore!)
                }
            }
            let option2 = UIAlertAction(title: "Apple Map".localizableString(loc: Singleton.shared.language ?? "en"), style: .default) { (action) in
                let latitude:CLLocationDegrees =  Double(vendorDetail.latitude ?? "0")!
                let longitude:CLLocationDegrees =  Double(vendorDetail.longitude ?? "0")!
                let regionDistance:CLLocationDistance = 10000
                let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
                let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
                let options = [
                    MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                    MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
                ]
                let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
                let mapItem = MKMapItem(placemark: placemark)
                mapItem.name = "\(vendorDetail.name ?? "")"
                mapItem.openInMaps(launchOptions: options)
            }
            let option3 = UIAlertAction(title: "Business Profile".localizableString(loc: Singleton.shared.language ?? "en"), style: .default) { (action) in
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
                myVC.vendorId = vendorDetail.id ?? 0
                self.navigationController?.pushViewController(myVC, animated: true)
            }
            
            let option4 = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
            alert.addAction(option1)
            alert.addAction(option2)
            alert.addAction(option3)
            alert.addAction(option4)
            self.present(alert, animated: true, completion: nil)
        }
        return false
    }
    
    
    func markPinOnMap(coordinate: CLLocationCoordinate2D) {
        self.location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        self.lat = coordinate.latitude
        self.long = coordinate.longitude
        self.marker.position.longitude =  coordinate.longitude
        self.marker.position.latitude =  coordinate.latitude
        self.marker.map = self.mapView
        self.marker.icon = GMSMarker.markerImage(with: .blue)
        self.mapView?.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 15.0)
    }
}


extension DealsViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        
    }
    
    
    func initMap(withLatitude lat: CLLocationDegrees, longitude lng: CLLocationDegrees, zoom: Int , googleMapView: GMSMapView) {
        var myLocationMarker: GMSMarker?
        defer {
        }
        do {
            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: Float(zoom))
            googleMapView.isMyLocationEnabled = true
            googleMapView.camera = camera
            myLocationMarker?.map = googleMapView
        } catch let ex {
            
        }
    }
}
