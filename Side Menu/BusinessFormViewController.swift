//
//  BusinessFormViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 11/11/21.
//

import UIKit
import SkyFloatingLabelTextField
import BEMCheckBox
import FirebaseMessaging

class BusinessFormController: UIViewController,SelectFromPicker, UITextFieldDelegate {
    
    //MARK: Picker Delegate
    func selectedPickerData(val: String, pos: Int) {
        if(self.currentPicker == 1){
            self.selectedCityIndex = pos
            self.cityName.text = val
            cityBool = true
        }else if(self.currentPicker==2){
            self.selectedCountryCode = self.countryModel[pos].country_code ?? "0"
            self.countryCode.text = "+" + self.selectedCountryCode
            self.selectedCoutryIndex = pos
            self.cityName.text = ""
            self.country.text = val
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
            self.view.isUserInteractionEnabled = true
        }
    }
    
    //MARK: IBOutlet
    //  @IBOutlet weak var labelInterested: UILabel!
    @IBOutlet weak var name: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var nameLine: UIView!
    @IBOutlet weak var emailAddress: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var emailLine: UIView!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var password: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var confirmPassword: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var confirmPasswordLine: UIView!
    @IBOutlet weak var phoneLine: UIView!
    @IBOutlet weak var passwordLine: UIView!
    @IBOutlet weak var cityLine: UIView!
    @IBOutlet weak var cityName: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var countryCode: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var cityDrop: UIButton!
    @IBOutlet weak var buttonSignup: CustomButton!
    @IBOutlet weak var checkBox: BEMCheckBox!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var country: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewConfirmPassword: UIView!
    @IBOutlet weak var viewFirstName: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewCity: UIView!
    @IBOutlet weak var viewForNumber: UIView!
    
    @IBOutlet weak var startBusiness: UILabel!
    @IBOutlet weak var labelInserted: UILabel!
    @IBOutlet weak var labelSignUp: UILabel!
    @IBOutlet weak var viewChangeNumber: UIView!
    @IBOutlet weak var becomeVendorHeadingLabel: DesignableUILabel!
    @IBOutlet weak var changePhoneNumberButton: UIButton!
    @IBOutlet weak var pleaseSubmitFormLabel: UILabel!
    
    var textFields: [SkyFloatingLabelTextFieldWithIcon] = []
    let borderColor: UIColor = UIColor(red: 52 / 255, green: 192 / 255, blue: 240 / 255, alpha: 1.0)
    var currentPicker = 1
    var isTermsSelected = false
    
    //Facebook Mobile Verification
    
    
    //Picker For Cities
    var pickerData: [String] = [String]()
    var selectedCityIndex = 0
    var selectedCoutryIndex = 0
    var selectedCountryCode = String()
    
    //Data From SessionManger
    var language = ""
    
    //Category List
    var categoryBool = false
    var categoryIds = [Int]()
    var selectedCategory = 0
    
    //City Cell
    var cityModel = [Cities]()
    var countryModel = [Countries]()
    var selectedCell : IndexPath!
    var cityIds = [Int]()
    var cityCount = 0
    var selectedCityId = 0
    var cityBool = false
    var vendorType: String?
    var vendorId: Int?
    var userData = Singleton.shared.userDetail
    var isNewUser = true
    var isRedirectionFromSideBar = false
    var userDetailResponse = UserDetail()
    
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        handleTextAlignmentForArabic(view: self.view)
        NavigationController.shared.changeStatusBar(color: K_PINK_COLOR)
        self.phoneNumber.delegate = self
        self.name.placeholder = "Full name".localizableString(loc: Singleton.shared.language ?? "en")
        self.emailAddress.placeholder = "Email Address".localizableString(loc: Singleton.shared.language ?? "en")
        self.phoneNumber.placeholder = "Phone number".localizableString(loc: Singleton.shared.language ?? "en")
        self.password.placeholder = "Password".localizableString(loc: Singleton.shared.language ?? "en")
        self.confirmPassword.placeholder = "Confirm password".localizableString(loc: Singleton.shared.language ?? "en")
        self.cityName.placeholder = "City".localizableString(loc: Singleton.shared.language ?? "en")
        self.buttonSignup.setTitle("Submit".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.country.placeholder = "Country".localizableString(loc: Singleton.shared.language ?? "en")
        self.becomeVendorHeadingLabel.text = "Become Vendor".localizableString(loc: Singleton.shared.language ?? "en")
        self.startBusiness.text = "Start your business with Symo & reach customers across!!".localizableString(loc: Singleton.shared.language ?? "en")
        self.changePhoneNumberButton.setTitle("Change Phone Number".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.pleaseSubmitFormLabel.text = "Please submit the form. A representative from SYMO should get in touch with you shortly.".localizableString(loc: Singleton.shared.language ?? "en")
        
        name.iconType = .image
        emailAddress.iconType = .image
        password.iconType = .image
        confirmPassword.iconType = .image
        cityName.iconType = .image
        countryCode.iconType = .image
        country.iconType = .image
        
        NavigationController.shared.getCountries { response in
            self.countryModel = response
        }
        labelSignUp.text = labelSignUp.text?.localized
        underLineWord()
        startBusiness.text = startBusiness.text?.localized
        
        popUpView.isHidden = true
        
        language = Singleton.shared.language ?? "en"
        
        
        //Setting up BEMCheckBox
        checkBox.boxType = .square
        checkBox.onCheckColor = borderColor
        checkBox.onTintColor = borderColor
        
        self.countryCode.placeholder = "+971"
        self.countryCode.selectedTitle = ""
        self.countryCode.title = ""
        self.countryCode.selectedLineColor = .clear
        self.countryCode.lineColor = .clear
        phoneLine.backgroundColor = .black
        
        if (self.userData != nil) {
            if(self.userData.user_type == 3){
                self.viewPassword.isHidden = true
                self.viewConfirmPassword.isHidden = true
                self.viewFirstName.isHidden = true
                self.viewEmail.isHidden = true
                self.viewCity.isHidden = true
                self.viewForNumber.isUserInteractionEnabled = false
                self.buttonSignup.setTitle("Submit".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
                
                self.labelInserted.isHidden = false
                self.startBusiness.isHidden = true
                self.phoneNumber.text = userData.phone
            }
        }else{
            self.viewPassword.isHidden = false
            self.viewConfirmPassword.isHidden = false
            self.viewFirstName.isHidden = false
            self.viewEmail.isHidden = false
            self.viewCity.isHidden = false
            self.buttonSignup.setTitle("Sign up".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
            self.viewForNumber.isUserInteractionEnabled = true
            
            self.labelInserted.isHidden = true
            self.startBusiness.isHidden = false
        }
    }
    
    func underLineWord(){
        let linkTextWithColor = "terms and conditions".localizableString(loc: Singleton.shared.language ?? "en")
        let range = ("By signing up, you agree to our terms and conditions".localizableString(loc: Singleton.shared.language ?? "en") as NSString).range(of: linkTextWithColor)
        let attributedString = NSMutableAttributedString(string: "By signing up, you agree to our terms and conditions".localizableString(loc: Singleton.shared.language ?? "en"))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.blue , range: range)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: range)
        labelSignUp.attributedText = attributedString
    }
    
    
    //MARK: IBActions
    
    @IBAction func changeNumber(_ sender: Any) {
        if(self.viewForNumber.isUserInteractionEnabled == false){
            self.viewForNumber.isUserInteractionEnabled = true
        }else {
            self.viewForNumber.isUserInteractionEnabled = false
        }
    }
    
    @IBAction func selectCity(_ sender: Any) {
        self.currentPicker = 1
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.modalPresentationStyle = .overFullScreen
        if(self.country.text!.isEmpty){
            Singleton.shared.showToast(msg: "Please select Country".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else {
            Singleton.shared.cityData = []
            NavigationController.shared.getCities(countryId: self.countryModel[self.selectedCoutryIndex].id ?? 7) { (cities) in
                
                self.cityModel = cities
                myVC.pickerData = []
                
                if (self.cityModel.count > 0){
                    for val in self.cityModel {
                        myVC.pickerData.append(val.name ?? "")
                    }
                    myVC.pickerDelegate = self
                    self.navigationController?.present(myVC, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func selectCountry(_ sender: Any) {
        self.currentPicker = 2
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.modalPresentationStyle = .overFullScreen
        if self.countryModel.count > 0{
            for val in self.countryModel {
                myVC.pickerData.append(val.name ?? val.name_en ?? "")
            }
            myVC.pickerDelegate = self
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    // form submit validation
    @IBAction func submitForm(_ sender: UIButton) {
        if self.name.text == "" {
            Singleton.shared.showToast(msg: "Enter full name".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        } else if self.emailAddress.text == ""{
            Singleton.shared.showToast(msg: "Enter your email address".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        } else if self.password.text == ""{
            Singleton.shared.showToast(msg: "Enter password".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        } else if self.confirmPassword.text == ""{
            Singleton.shared.showToast(msg: "Re-enter your password".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        } else if self.country.text == ""{
            Singleton.shared.showToast(msg: "Select Country".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }  else if self.cityName.text == ""{
            Singleton.shared.showToast(msg: "Select City".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        }else if self.phoneNumber.text == ""{
            Singleton.shared.showToast(msg: "Enter phone number".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        } else if (phoneNumber.text?.count)! < 6 {
            Singleton.shared.showToast(msg: "Enter valid phone number".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        } else if self.password.text != self.confirmPassword.text{
            Singleton.shared.showToast(msg: "Enter correct password".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        } else if self.isTermsSelected == false{
            Singleton.shared.showToast(msg: "Please check Terms and conditions".localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 2)
        } else {
            self.apiCallForSubmitForm()
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        if isRedirectionFromSideBar == true {
            self.menu()
        }
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func checkBoxAction(_ sender: Any) {
        if self.isTermsSelected == false {
            self.isTermsSelected = true
        } else {
            self.isTermsSelected = false
        }
    }
    
    
    // for submit form
    func apiCallForSubmitForm(){
        let parameters: [String: Any] = ["name": self.name.text!,
                                         "email": self.emailAddress.text!,
                                         "phone": self.phoneNumber.text!,
                                         "country_id": self.countryModel[self.selectedCoutryIndex].id,
                                         "password": self.password.text!,
                                         "city_id": self.cityModel[self.selectedCityIndex].city_id]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_BECOME_VENDOR, method: .post, parameter: parameters, objectClass: LoginResponse.self, requestCode: U_BECOME_VENDOR, userToken: nil) { response in
            self.userDetailResponse = response.response
            self.userDetailResponse.user_type = 2
            self.userDetailResponse.role_id = 2
            Messaging.messaging().subscribe(toTopic: "VENDOR")
            self.saveUserData(userDetail: self.userDetailResponse)
            UserDefaults.standard.setValue(self.userDetailResponse.access_token!, forKey: UD_TOKEN)
            self.navigationController?.popViewController(animated: true)
            NavigationController.shared.getAllCategory(view: self.view) { category in
                self.name.text = ""
                self.password.text = ""
                self.emailAddress.text = ""
                self.phoneNumber.text = ""
                ActivityIndicator.hide()
            }
        }
    }
    
    func showPopup(title: String, msg: String) {
        
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationPopupViewController") as! ConfirmationPopupViewController
        myVC.confirmationText = title.localizableString(loc: Singleton.shared.language ?? "en")
        myVC.detailText = msg.localizableString(loc: Singleton.shared.language ?? "en")
        myVC.firstButtonTitle = "Verify Now".localizableString(loc: Singleton.shared.language ?? "en")
        myVC.secondButtonTitle = "Ok".localizableString(loc: Singleton.shared.language ?? "en")
        myVC.modalPresentationStyle = .overFullScreen
        self.present(myVC, animated: true, completion: nil)
        
    }
    
    @IBAction func verifyAccount(_ sender: Any) {
        self.popUpView.isHidden = true
        self.apiCallForSubmitForm()
    }
    
    @IBAction func emailVerifyAction(_ sender: Any) {
        self.popUpView.isHidden = true
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func termsAndConditionAction(_ sender: Any) {
        self.openUrl(urlStr: U_TERMS_CONDITION)
    }
    
    func apiCallForCategory(sender : UIButton){
        NavigationController.shared.getAllCategory(view: self.view) { category in
            if category.count > 0{
                self.pickerData.removeAll()
                self.categoryIds.removeAll()
                
                for i in 0..<category.count{
                    self.pickerData.append((category[i].category_name?.camelcaseString)!)
                    self.categoryIds.append(category[i].category_id!)
                }
                self.showCategoryDropDown(categoryButton: sender)
            }
        }
    }
    
    //SHOW Country List on Pop UP
    func apiCallForShowCountryList(sender : UIButton){
        NavigationController.shared.getCities(countryId: self.userData.country_id ?? 7) { (cities) in
            self.cityModel = cities
            self.handlePickerData(sender: sender)
        }
    }
    
    func handlePickerData(sender: UIButton) {
        if cityModel.count > 0{
            self.cityCount = cityModel.count
            self.pickerData.removeAll()
            self.cityIds.removeAll()
            
            for i in 0...cityModel.count-1{
                if self.language == "en" {
                    self.pickerData.append((cityModel[i].name_en?.camelcaseString)!)
                } else {
                    self.pickerData.append(cityModel[i].name!)
                }
                self.cityIds.append(cityModel[i].city_id!)
            }
            self.showCityDropDown(cityButton: sender)
        }
    }
    
    func showCategoryDropDown(categoryButton : UIButton){
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerData = Singleton.shared.allCategoryData.map{
            val in
            return val.name ?? ""
        }
        myVC.pickerDelegate = self
        myVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
    func showCityDropDown(cityButton : UIButton) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerData = Singleton.shared.cityData.map{
            val in
            return val.name ?? ""
        }
        myVC.pickerDelegate = self
        myVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
    
    func refreshFields(){
        name.text = ""
        password.text = ""
        emailAddress.text = ""
        phoneNumber.text = ""
        navigationController?.popViewController(animated: true)
    }
}

extension BusinessFormController {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 0 {
            nameLine.backgroundColor = borderColor
        } else if textField.tag == 1 {
            emailLine.backgroundColor = borderColor
        } else if textField.tag == 2 {
            passwordLine.backgroundColor = borderColor
        } else if textField.tag == 3 {
            confirmPasswordLine.backgroundColor = borderColor
        } else if textField.tag == 4 {
            phoneLine.backgroundColor = borderColor
        } else if textField.tag == 5 {
            cityLine.backgroundColor = borderColor
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 0 {
            nameLine.backgroundColor = UIColor.lightGray
        } else if textField.tag == 1 {
            emailLine.backgroundColor = UIColor.lightGray
        } else if textField.tag == 2 {
            passwordLine.backgroundColor = UIColor.lightGray
        } else if textField.tag == 3 {
            confirmPasswordLine.backgroundColor = UIColor.lightGray
        } else if textField.tag == 4 && (textField.text?.isEmpty)! {
            phoneLine.backgroundColor = UIColor.lightGray
        } else if textField.tag == 5 {
            cityLine.backgroundColor = UIColor.lightGray
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneNumber {
            let ACCEPTABLE_CHARACTERS = "0123456789"
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        } else {
            
            if textField.tag == 4 && range.location >= 9 {
                return false
            }else if textField.tag == 8 && range.location >= 4 {
                return false
            }
        }
        return true
    }
}


extension BusinessFormController {
    func saveUserData(userDetail: UserDetail){
        Singleton.shared.saveUserDetail(user: userDetail)
    }
}
