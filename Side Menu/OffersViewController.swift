//
//  OffersViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 05/05/21.
//

import UIKit
import CarbonKit


class OffersViewController: UIViewController,CarbonTabSwipeNavigationDelegate{
    
    //MARK: Delegate Functions
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PromotionViewController") as! PromotionViewController
        return myVC
        
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt) {
        promotionIndex = Int(index)
    }
    
    
    
    //MARK: IBOutlets
    @IBOutlet weak var carbonView: UIView!
    @IBOutlet weak var offersHeadingLabel: DesignableUILabel!
    
    var selectedIndex = 0
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
        self.offersHeadingLabel.text = "Offers".localizableString(loc: Singleton.shared.language ?? "en")
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: ["Products", "Services"], delegate: self)
        carbonTabSwipeNavigation.setTabExtraWidth(40)
        carbonTabSwipeNavigation.setNormalColor(.gray)
        carbonTabSwipeNavigation.setSelectedColor(.black)
        carbonTabSwipeNavigation.setIndicatorColor(.black)
        carbonTabSwipeNavigation.setTabBarHeight(50)
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: self.carbonView)
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
