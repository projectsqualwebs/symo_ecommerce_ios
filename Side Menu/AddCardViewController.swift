//
//  AddCardViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 05/05/21.
//

import UIKit
import SkyFloatingLabelTextField

protocol  AddNewCard {
    func refreshCardTable()
}

class AddCardViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: IBOutlets
    @IBOutlet weak var cardholderName: SkyFloatingLabelTextField!
    @IBOutlet weak var cardNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var monthField: SkyFloatingLabelTextField!
    @IBOutlet weak var cvvField: SkyFloatingLabelTextField!
    @IBOutlet weak var saveCreditDetailsButton: CustomButton!
    @IBOutlet weak var addNewCardLabel: DesignableUILabel!
    
    var cardDelegate: AddNewCard? = nil
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
        self.cardholderName.placeholder = "Card holder's name".localizableString(loc: Singleton.shared.language ?? "en")
        self.cardNumber.placeholder = "Card number".localizableString(loc: Singleton.shared.language ?? "en")
        self.monthField.placeholder = "Expiry(MM/YY)".localizableString(loc: Singleton.shared.language ?? "en")
        self.cvvField.placeholder = "CVV".localizableString(loc: Singleton.shared.language ?? "en")
        self.saveCreditDetailsButton.setTitle("Save credit details".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.addNewCardLabel.text = "Add new Card".localizableString(loc: Singleton.shared.language ?? "en")
        self.cardNumber.delegate = self
        self.monthField.delegate = self
        self.cvvField.delegate = self
        handleTextAlignmentForArabic(view: self.view)
    }
    
    //MARK: IBActions
    @IBAction func saveAction(_ sender: Any) {
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: TextField text change delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true
            
        }
        
        if(textField == cardNumber){
            let currentCharacterCount = self.cardNumber.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            self.cardNumber.text = self.cardNumber.text?.applyPatternOnNumbers(pattern: "####  ####  ####  ####", replacmentCharacter: "#")
            return newLength <= 22
        }else if(textField == monthField) {
            let currentCharacterCount = self.monthField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            if(newLength == 3){
                self.monthField.text = (self.monthField.text ?? "") + "/"
            }
            return newLength <= 5
        }else if(textField == monthField) {
            let currentCharacterCount = self.cvvField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 3
        }else {
            return true
        }
    }
    
}
