//
//  PromotionViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 11/06/21.
//


import UIKit
import SDWebImage

var promotionIndex = 0
class PromotionViewController: UIViewController {
    
    //MARK: IBOutlet
    @IBOutlet weak var noPromolabel: UILabel!
    @IBOutlet weak var offersCollectionView: UICollectionView!
    
    var promotionArray = [4,5]
    
    
    var color = UIColor(red: 233 / 255, green: 46 / 255, blue: 100 / 255, alpha: 1.0)
    var colorBlue = UIColor(red: 81 / 255, green: 201 / 255, blue: 242 / 255, alpha: 1.0)
    
    //Data From SessionManger
    var userData: UserDetail!
    var language = ""
    
    static var observerAdded: Bool = false
    static var selectedTitle: String = "Offers".localizableString(loc: Singleton.shared.language ?? "en")
    
    var offers = [ProductResponse]()
    var seasonalOffers = [ProductResponse]()
    var offersCount = 0
    var categoryId = 0
    var selectedVendorIndex = 0
    
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        userData = Singleton.shared.userDetail
        language = Singleton.shared.language ?? "en"
        
        //checking notification observer added or not. If added then it will not add again
        if !(PromotionViewController.observerAdded) {
            NotificationCenter.default.addObserver(self, selector: #selector(getTitle(_:)), name: NSNotification.Name("title"), object: nil)
            PromotionViewController.observerAdded = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //DispatchQueue.global(qos: .userInteractive).async {
        self.apiCallForPromotions()
        // }
    }
    
    //MARK: Functions
    @objc func getTitle(_ notification: NSNotification) {
        if let object = notification.object as? String {
            
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name("title"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(getTitle(_:)), name: NSNotification.Name("title"), object: nil)
            PromotionViewController.selectedTitle = object
            self.apiCallForPromotions()
        }
    }
    
    @objc func apiCallForPromotions() {
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_OFFERS_CATEGORY + "?lang=\(Singleton.shared.language ?? "en")&currency=\(Singleton.shared.selectedCurrency ?? "usd")", method: .post    , parameter: ["type":promotionIndex+1], objectClass: GetOffers.self, requestCode: U_GET_OFFERS_CATEGORY, userToken: nil) { (offers) in
            self.offers = [ProductResponse]()
            self.seasonalOffers = [ProductResponse]()
            if(promotionIndex == 0){
                self.offers = offers.response.offer_services.data
            }else {
                self.seasonalOffers = offers.response.offer_services.data
            }
            if (self.offers.count > 0 && PromotionViewController.selectedTitle == "Offers".localizableString(loc: Singleton.shared.language ?? "en")) || (self.seasonalOffers.count > 0 && PromotionViewController.selectedTitle == "Seasonal Offers".localizableString(loc: Singleton.shared.language ?? "en")) {
                self.noPromolabel.isHidden = true
                self.offersCollectionView.isHidden = false
                self.offersCollectionView.reloadData()
            } else {
                self.offersCollectionView.isHidden = true
                self.noDataFound()
            }
        }
    }
    
    /*--------------------- NO Data FOUND LABEL ---------------------*/
    func noDataFound(){
        let preferredLanguage : String = Bundle.main.preferredLocalizations.first!
        noPromolabel.isHidden = false
        noPromolabel.text = "NO OFFERS FOUND !".localizableString(loc: Singleton.shared.language ?? "en")
        noPromolabel.shadowColor = UIColor.gray
        noPromolabel.shadowOffset.width = 1
        noPromolabel.shadowOffset.height = 2
        noPromolabel.textAlignment = .center
    }
}

//MARK: Collection View
extension PromotionViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if PromotionViewController.selectedTitle == "Offers".localizableString(loc: Singleton.shared.language ?? "en") {
            return offers.count
        } else {
            return seasonalOffers.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OfferCell", for: indexPath) as! OfferCell
        
        if PromotionViewController.selectedTitle == "Offers".localizableString(loc: Singleton.shared.language ?? "en") {
            cell.labelDescription.isHidden = false
            cell.labelName.isHidden = false
            cell.labelEndDate.isHidden = false
            cell.offerImage.sd_setImage(with: URL(string: offers[indexPath.row].default_image ?? ""), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            
            if offers[indexPath.row].description != nil{
                cell.labelDescription.text = offers[indexPath.row].description!
            }
            
            if let discount = offers[indexPath.row].discount {
                cell.labelName.text = "\(discount)" + " on ".localizableString(loc: Singleton.shared.language ?? "en") + "\(offers[indexPath.row].name ?? "")"
            } else {
                cell.labelName.text = offers[indexPath.row].name
            }
            
        } else {
            if let url = URL(string: (seasonalOffers[indexPath.row].default_image)!) {
                cell.offerImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }
            cell.labelDescription.isHidden = true
            cell.labelName.isHidden = true
            cell.labelEndDate.isHidden = true
        }
        cell.offerImage.layer.cornerRadius = 8
        cell.offerImage.layer.masksToBounds = false
        cell.offerImage.clipsToBounds = true
        cell.opacityView.layer.cornerRadius = 8
        cell.opacityView.layer.masksToBounds = false
        cell.opacityView.clipsToBounds = true
        
        return cell
    }
}
extension PromotionViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    // for setting size of collection view single cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var numberOfOffersInRow: CGFloat?
        if UIDevice.current.userInterfaceIdiom == .phone {
            numberOfOffersInRow = 2
        } else {
            numberOfOffersInRow = 4
        }
        let cellheight : CGFloat = (((self.view.frame.size.width/numberOfOffersInRow!)-15)/2)*3
        return CGSize(width: (self.view.frame.size.width/numberOfOffersInRow!)-15, height: cellheight)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedVendorIndex = indexPath.row
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
        myVC.vendorId = offers[indexPath.row].vendor_details?.vendor_id ?? 0
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}
extension UICollectionView {
    func reloadData(_ completion: @escaping () -> Void) {
        reloadData()
        DispatchQueue.main.async { completion() }
    }
}


//MARK: Collection Cell

class OfferCell: UICollectionViewCell {
    
    @IBOutlet weak var offerImage: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelOffer: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var textViewDescription: UITextView!
    @IBOutlet weak var labelEndDate: UILabel!
    @IBOutlet weak var opacityView: UIView!
    
    var shareOffer: (() -> Void)? = nil
    var readMore: (() -> Void)? = nil
    
    @IBOutlet weak var shareButton: UIButton!
    
    //Cell2
    @IBOutlet weak var labelNoMoreOffers: UILabel!
    
    //MARK: Action
    @IBAction func shareOffer(_ sender: UIButton) {
        if let shareOffer = shareOffer {
            shareOffer()
        }
    }
    
    @IBAction func readMore(_ sender: UIButton) {
        if let readMore = readMore {
            readMore()
        }
    }
}
