//
//  BookMarkedStoresViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 11/03/22.
//

import UIKit

class BookMarkedStoresViewController: UIViewController ,UITextFieldDelegate{
    
    //MARK: Textfield Delegate Method
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text =  (self.searchField.text ?? "") + string
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                if text.count == 1 {
                    self.searchData = self.storeData
                    self.bookMarkTableView.reloadData()
                }
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            getShopData()
        }
    }
    
    //MARK: IBoutlets
    @IBOutlet weak var searchField: DesignableUITextField!
    @IBOutlet weak var bookMarkTableView: ContentSizedTableView!
    @IBOutlet weak var noDataLabel: UILabel!
    
    var isRedirectionFromSideBar = false
    var storeData = [StoreData]()
    var searchData = [StoreData]()
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        handleTextAlignmentForArabic(view: self.view)
        self.searchField.delegate = self
        self.bookMarkTableView.estimatedRowHeight = 350
        self.bookMarkTableView.rowHeight = UITableView.automaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
        selectedCategory = Set<Int>()
        selectedSubCategory = Set<Int>()
        selectedPriceMin = 0
        selectedPriceMax = 10000
        selectedDistance = Int()
        offerSelected = 0
        selectedStar = nil
        nearMe = nil
        K_SELECTED_SORTBY = 0
        FILTER_SUBDOMAIN = ""
        currentFilterParam = [String:Set<Int>]()
        storeType = nil
        if dataType == nil {
            productType = nil
        }
        self.getShopData()
    }
    
    //MARK: Functions
    func getShopData(){
        ActivityIndicator.show(view: self.view)
        let param:[String:Any] = [
            "lat":Singleton.shared.userLocation.coordinate.latitude,
            "long" : Singleton.shared.userLocation.coordinate.longitude,
            "lang": Singleton.shared.language ?? "en",
            "category" : Array(selectedCategory),// category id,
            "sub_category" : Array(selectedSubCategory),// subcategory id
            "keyword" : "", // search with the shop name
            "distance": selectedDistance == 0 ? "":selectedDistance,// possible values 0-5 km -> 5, 5-10 -> 10, 10-15 -> 15, more than 16 -> 16
            "city_id": Singleton.shared.selectedCity.city_id,
            "country_id" : Singleton.shared.selectedCountry.id,
            "store_type" : storeType,
            "nearby_me" : nearMe,
            "filter":[
                "color": Array(currentFilterParam["Color"] ?? []),
                "price": [selectedPriceMin,selectedPriceMax],
                "size": Array(currentFilterParam["Size"] ?? []),
                "rating": selectedStar ?? 0
            ]
        ]
        //fetching the bookmark shops
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_BOOKMARK_SHOPS, method: .post, parameter: param, objectClass: GetStore.self, requestCode: U_GET_BOOKMARK_SHOPS, userToken: nil) { response in
            self.storeData = response.response.data
            self.searchData = response.response.data
            self.view.isUserInteractionEnabled = true
            self.bookMarkTableView.reloadData()
            
            //set book mark vendors in singleton.
            Singleton.shared.bookmarkedVendors = []
            for store in response.response.data{
                if  !Singleton.shared.bookmarkedVendors.contains(store.id ?? 0){
                    Singleton.shared.bookmarkedVendors.append(store.id ?? 0)
                }
            }
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        if self.isRedirectionFromSideBar == true {
            self.menu()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        let text = (self.searchField.text!.trimmingCharacters(in: .whitespacesAndNewlines)).lowercased()
        if searchField.text != "" {
            self.searchData = []
            for val in self.storeData {
                let i = ((val.name ?? "").trimmingCharacters(in: .whitespacesAndNewlines)).lowercased()
                if i.contains(text){
                    self.searchData.append(val)
                }
            }
            self.bookMarkTableView.reloadData()
        } else {
            self.searchData = self.storeData
            self.bookMarkTableView.reloadData()
        }
    }
    
    @IBAction func filterAction(_ sender: Any) {
        self.openFilter(view: 3)
    }
    
}

//MARK: TableView Delegate
extension BookMarkedStoresViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.noDataLabel.text = "No Stores Found".localizableString(loc: Singleton.shared.language ?? "en")
        if(self.searchData.count == 0){
            self.noDataLabel.isHidden = false
        }else {
            self.noDataLabel.isHidden = true
        }
        return self.searchData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableCell") as! CategoryTableCell
        let val = self.searchData[indexPath.row]
        if((val.shop_image ?? val.avatar ?? "").contains("http")){
            cell.shopImage.sd_setImage(with: URL(string: (val.shop_image ?? val.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }else {
            cell.shopImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (val.shop_image ?? val.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
        }
        cell.deliveryLocalizeLabel.text = "Delivery".localizableString(loc: Singleton.shared.language ?? "en")
        cell.pickupLocalizeLabel.text = "Pickup".localizableString(loc: Singleton.shared.language ?? "en")
        cell.codLocalizeLabel.text = "COD".localizableString(loc: Singleton.shared.language ?? "en")
        cell.oneDayDeliveryLocalize.text = "One day delivery".localizableString(loc: Singleton.shared.language ?? "en")
        cell.scheduleDeliveryLocalize.text = "Schedule delivery".localizableString(loc: Singleton.shared.language ?? "en")
        cell.viewButton.setTitle("View".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        cell.freeDeliveryStack.isHidden = false
        cell.pickupStack.isHidden = val.pickup_delivery_setting == 1 ? false: true
        cell.codStack.isHidden = val.cod_method == 1 ? false : true
        cell.scheduleDeliveryStack.isHidden = val.schedule_delivery == 1 ? false : true
        cell.oneDayDelivery.isHidden = val.one_day_delivery == 1 ? false : true
        cell.shopName.text = val.name
        cell.shopAddress.text = val.address
        cell.shopDistance.text = "\(String(format:"%.2f", val.distance ?? 0))" + " kms".localizableString(loc: Singleton.shared.language ?? "en")
//        "\(Int(val.distance ?? 0)) "
        cell.totalReview.text = "\(val.ratings_count ?? 0) " + "reviews".localizableString(loc: Singleton.shared.language ?? "en")
        cell.ratingView.rating = Double(val.ratings ?? "0")!
        cell.bagButton = {
            resetView = false
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
            myVC.vendorId = self.searchData[indexPath.row].id ?? 0
            self.navigationController?.pushViewController(myVC, animated: true)
        }
        
        // for removeing from book mark
        cell.heartButton = {
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_BOOKMARK_VENODR, method: .post,
                                                      parameter: ["vendor_id": self.searchData[indexPath.row].id ?? 0], objectClass: SuccessResponse.self, requestCode: U_BOOKMARK_VENODR, userToken: UD_TOKEN) { (response) in
                self.getShopData()
                Singleton.shared.showToast(msg: response.message?.localizableString(loc: Singleton.shared.language ?? "en") ?? "", controller: self, type: 0)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        resetView = false
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
        myVC.vendorId = self.searchData[indexPath.row].id ?? 0
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}

