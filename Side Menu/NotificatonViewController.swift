//
//  NotificatonViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 05/05/21.
//

import UIKit

class NotificatonViewController: UIViewController {
    
    //MARK: IBOutlets
    
    @IBOutlet weak var notificationTable: UITableView!
    @IBOutlet weak var notificationsHeadingLabel: DesignableUILabel!
    @IBOutlet weak var noDataDoundLabel: DesignableUILabel!
    
    
    var notificationData = [NotificationData]()
    var currentPage : Int = 1
    var isLoadingList : Bool = false
    var isRedirectionFromSideBar = false
    var vendorId : Int? = nil
    
    //For manage UI as per scroll
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView == self.notificationTable){
            if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height) && !isLoadingList){
                self.isLoadingList = true
                self.loadProductItemsForList()
            }
        }
    }
    
    func loadProductItemsForList(){
        currentPage += 1
        self.getNotificationData()
    }
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
        self.notificationsHeadingLabel.text = "Notifications".localizableString(loc: Singleton.shared.language ?? "en")
        self.notificationTable.estimatedRowHeight = 90
        self.notificationTable.rowHeight = UITableView.automaticDimension
        self.getNotificationData()
    }
    
    // fetching notification data from server.
    func getNotificationData(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_NOTIFICATION + "?page=\(currentPage)&lang=\(Singleton.shared.language ?? "en")" , method: .post, parameter: [ "vendor_id": vendorId], objectClass: GetNotificationResponse.self, requestCode: U_GET_NOTIFICATION, userToken: nil) { response in
            
            if(self.notificationData.count == 0 || self.currentPage == 1 ){
                self.notificationData = response.response?.data ?? []
                self.isLoadingList = false
            } else if (response.response?.data?.count ?? 0) > 0{
                for val in (response.response?.data  ?? []){
                    self.notificationData.append(val)
                }
                self.isLoadingList = false
            }
            if((response.response?.data?.count ?? 0) == 0){
                if self.currentPage > 1 {
                    self.currentPage -= 1
                }
                self.isLoadingList = false
            }
            
            self.notificationTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        if isRedirectionFromSideBar == true {
            self.menu()
        }
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: Table View Delegate
extension NotificatonViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.noDataDoundLabel.isHidden = self.notificationData.count > 0 ? true : false
        self.notificationTable.isUserInteractionEnabled =  self.notificationData.count > 0 ? true : false
        return self.notificationData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableCell") as! CategoryTableCell
        let val = notificationData[indexPath.row]
        cell.shopName.text = ""
        cell.shopPrice.text = val.message ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
        myVC.orderId = notificationData[indexPath.row].order_id ?? 0
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
}
