//
//  SideMenuViewController.swift
//  Symo New
//
//  Created by Apple on 03/04/21.
//

import UIKit
import SDWebImage
import FBSDKCoreKit

class SideMenuViewController: UIViewController, Confirmation {
    func confirmationSelection(action: Int) {
        if(action == 1){
            SideMenuViewController.handleTabDelegate?.setTabBar()
            Singleton.shared.handleUserLogout()
            K_BAG_COUNT = 0
            
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            self.navigationController?.pushViewController(myVC, animated: false)
        }
    }
    
    
    //MARK: IBOutlets
    @IBOutlet weak var signOutButton: CustomButton!
    @IBOutlet weak var menuTable: UITableView!
    @IBOutlet weak var loginView: View!
    @IBOutlet weak var username: DesignableUILabel!
    @IBOutlet weak var profileName: DesignableUILabel!
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    
    // "Payments"
    // side menu options as per logged in non logged in
    let loggedIn = ["List your Business","My Orders", "Shipping Address", "My Favourites", "Notifications","Bookmarks" ,"Deals", "Share", "Settings", "Contact Us", "FAQ's", "Privacy Policy", "Disclaimer", "Terms & Condition"]
    let nonLoggedIn = ["Become a Vendor","Share", "Login", "Sign Up", "Contact Us", "FAQ's", "Privacy Policy", "Disclaimer", "Terms & Condition"]
    static var handleTabDelegate : HandleTabBarItems? = nil
    
    
    //MARK: ViewCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue]
        let underlineAttributedString = NSAttributedString(string: "Log out".localizableString(loc: Singleton.shared.language ?? "en"), attributes: underlineAttribute)
        self.signOutButton.setAttributedTitle(underlineAttributedString, for: .normal)
        self.view.backgroundColor = .white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
        self.view.isUserInteractionEnabled = true
        self.menuTable.reloadData()
        if(Singleton.shared.userDetail.id != nil){
            self.imageWidth.constant = 0
            self.loginView.isHidden = false
            self.username.text = "Hi, ".localizableString(loc: Singleton.shared.language ?? "en") + "\(Singleton.shared.userDetail.name ?? "")"
            if((Singleton.shared.userDetail.avatar ?? "").contains("http")){
                self.userImage.sd_setImage(with: URL(string: Singleton.shared.userDetail.avatar ?? ""), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }else {
                self.userImage.sd_setImage(with: URL(string: U_IMAGE_STORE_BASE + (Singleton.shared.userDetail.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "symoPlaceholder"))
            }
            
            self.profileName.text = Singleton.shared.userDetail.name
        }else {
            self.imageWidth.constant = 45
            self.loginView.isHidden = true
            self.username.text = "Hi, Guest".localizableString(loc: Singleton.shared.language ?? "en")
        }
        
        addTableFooter()
    }
    
    
    //MARK: Functions
    
    //for showing footer below table which display logo and version
    func addTableFooter(){
        var tableHeight : CGFloat = CGFloat(50 * (Singleton.shared.userDetail.id != nil ? loggedIn.count : nonLoggedIn.count))
        
        var viewHeight = (Singleton.shared.screenData.safeHeight ?? 0) - tableHeight  - 70
        
        if viewHeight < 100 {
            viewHeight = 120
        }
        
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: menuTable.frame.width, height: viewHeight))
        customView.backgroundColor = UIColor.clear
        
        // Add an image view with a specified height and centered horizontally
        let imageView = UIImageView(frame: CGRect(x: 0, y: viewHeight - 100, width: customView.frame.width, height: 45))
        imageView.image = UIImage(named: "logo_new")
        imageView.contentMode = .scaleAspectFit
        customView.addSubview(imageView)
        
        // Add a label for the app name "SYMO"
        let nameLabel = UILabel(frame: CGRect(x: 0, y: viewHeight - 60, width: customView.frame.width, height: 30))
        nameLabel.text = "Symo"
        nameLabel.font = UIFont(name:"Lato-SemiBold", size:22)
        nameLabel.textAlignment = .center
        customView.addSubview(nameLabel)
        
        // Add a label for the app version with light gray color, system font, and size 15
        let versionLabel = UILabel(frame: CGRect(x: 0, y:  viewHeight - 30, width: customView.frame.width, height: 20))
        versionLabel.text = "Version : ---"
        if let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            print("App Version:", appVersion)
            versionLabel.text = "Version".localizableString(loc: Singleton.shared.language ?? "en") + " : \(appVersion)"
        }
        versionLabel.font = UIFont(name:"Lato-Regular", size:14)
        versionLabel.textAlignment = .center
        versionLabel.textColor = UIColor.lightGray
        customView.addSubview(versionLabel)
        
        
        menuTable.tableFooterView = customView
    }
    
    
    //MARK: IBActions
    @IBAction func signoutAction(_ sender: Any) {
        
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationPopupViewController") as! ConfirmationPopupViewController
        myVC.confirmationDelegate = self
        myVC.confirmationText = "Are You Sure?".localizableString(loc: Singleton.shared.language ?? "en")
        myVC.detailText = "You really want to logout?".localizableString(loc: Singleton.shared.language ?? "en")
        myVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(myVC, animated: true, completion: nil)
        
    }
    
    @IBAction func profileAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        myVC.isRedirectionFromSideBar = true
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}


//MARK: Table View
extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(Singleton.shared.userDetail.id != nil){
            return loggedIn.count
        }else {
            return nonLoggedIn.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableCell", for: indexPath) as! MenuTableCell
        cell.menuLabel.textColor = UIColor.black
        if(Singleton.shared.userDetail.id != nil){
            cell.menuLabel.text  = self.loggedIn[indexPath.row].localizableString(loc: Singleton.shared.language ?? "en")
        }else {
            cell.menuLabel.text  = self.nonLoggedIn[indexPath.row].localizableString(loc: Singleton.shared.language ?? "en")
        }
        
        switch Singleton.shared.userDetail.id != nil ? self.loggedIn[indexPath.row]:self.nonLoggedIn[indexPath.row] {
        case "Registered as vendor":
            cell.icons.image = UIImage(named: "shop-1")
            break
        case "List your Business":
            cell.icons.image = UIImage(named: "shop-1")
            break
        case "Become a Vendor":
            cell.icons.image = UIImage(named: "shop-1")
            break
        case "Share":
            cell.icons.image = UIImage(named: "share-1")
            break
        case "Login":
            cell.icons.image = UIImage(named: "login")
            break
        case "Sign Up":
            cell.icons.image = UIImage(named: "signup-1")
            break
        case "Deals":
            cell.icons.image = UIImage(named: "explore")
            break
        case "Contact Us":
            cell.icons.image = UIImage(named: "phone-call-2")
            break
        case "FAQ's":
            cell.icons.image = UIImage(named: "faq")
            break
        case "Privacy Policy":
            cell.icons.image = UIImage(named: "privacy")
            break
        case "Terms & Condition":
            cell.icons.image = UIImage(named: "terms")
            break
        case "Disclaimer":
            cell.icons.image = UIImage(named: "disclaim")
            break
        case "My Orders":
            cell.icons.image = UIImage(named: "Rectangle 6")
            break
        case "My Favourites":
            cell.icons.image = UIImage(named: "heart_outline")
            break
        case "Notifications":
            cell.icons.image = UIImage(named: "bell-1")
            break
        case "Payments":
            cell.icons.image = UIImage(named: "explore")
            break
        case "Deals":
            cell.icons.image = UIImage(named: "blue_price")
            break
        case "Settings":
            cell.icons.image = UIImage(named: "setting")
            break
        case "Shipping Address":
            cell.icons.image = UIImage(named: "point_out")
            break
        case "Bookmarks":
            cell.icons.image = UIImage(named: "bookmark")
            break
        default:
            cell.icons.image = UIImage(named: "")
            break
        }
        
        return cell
    }
    
    //select action as per side menu name
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        AppEvents.shared.logEvent(AppEvents.Name(rawValue: "AppEventName"), parameters: [AppEvents.ParameterName.init("Key"):"Value"])
        
        switch Singleton.shared.userDetail.id != nil ? self.loggedIn[indexPath.row]:self.nonLoggedIn[indexPath.row] {
        case "List your Business":
            if(Singleton.shared.userDetail.role_id != 2){
                ActivityIndicator.show(view: self.view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_BECOME_VENDOR_EXISTING_USER, method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_BECOME_VENDOR_EXISTING_USER, userToken: nil) { response in
                    Singleton.shared.showToast(msg: (response.message ?? "").localizableString(loc: Singleton.shared.language ?? "en"), controller: self, type: 0)
                    Singleton.shared.userDetail.role_id = 2
                    Singleton.shared.saveUserDetail(user: Singleton.shared.userDetail)
                    ActivityIndicator.hide()
                }
            }else{
                self.openUrl(urlStr:"https://gosymo.com/user/register")
            }
            break
        case "Become a Vendor":
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "BusinessFormController") as! BusinessFormController
            myVC.isRedirectionFromSideBar = true
            self.navigationController?.pushViewController(myVC, animated: true)
            break
        case "Registered as vendor":
            self.openUrl(urlStr:"https://gosymo.com/user/register")
            break
        case "Share":
            self.share(self, K_SHARE_APP, nil, nil)
            break
        case "Login":
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            myVC.isRedirectionFromSideBar = true
            self.navigationController?.pushViewController(myVC, animated: true)
            break
        case "Sign Up":
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
            myVC.isRedirectionFromSideBar = true
            self.navigationController?.pushViewController(myVC, animated: true)
            break
        case "Deals":
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DealsViewController") as! DealsViewController
            myVC.isRedirectionFromSideBar = true
            self.navigationController?.pushViewController(myVC, animated: true)
            break
        case "Contact Us":
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            myVC.isRedirectionFromSideBar = true
            self.navigationController?.pushViewController(myVC, animated: true)
            break
        case "FAQ's":
            self.openUrl(urlStr:U_URL_FAQ)
            break
        case "Privacy Policy":
            self.openUrl(urlStr:U_PRIVACY_POLICY)
            break
        case "Terms & Condition":
            self.openUrl(urlStr:U_TERMS_CONDITION)
            break
        case "Disclaimer":
            self.openUrl(urlStr:U_DISCLAIMER)
            break
        case "My Orders":
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MyOrderViewController") as! MyOrderViewController
            myVC.isRedirectionFromSideBar = true
            self.navigationController?.pushViewController(myVC, animated: true)
            break
        case "My Favourites":
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "FavouritesViewController") as! FavouritesViewController
            myVC.isRedirectionFromSideBar = true
            myVC.isBackButton = true
            self.navigationController?.pushViewController(myVC, animated: true)
            break
            
        case "Notifications":
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotificatonViewController") as! NotificatonViewController
            myVC.isRedirectionFromSideBar = true
            self.navigationController?.pushViewController(myVC, animated: true)
            break
            
        case "Payments":
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
            myVC.isRedirectionFromSideBar = true
            self.navigationController?.pushViewController(myVC, animated: true)
            break
        case "Settings":
            NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_TABBAR), object: nil,userInfo: ["settings":true])
            self.dismiss(animated: true, completion: nil)
            break
        case "Shipping Address":
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddressViewController") as! AddressViewController
            myVC.isRedirectionFromSideBar = true
            self.navigationController?.pushViewController(myVC, animated: true)
            break
        case "Bookmarks":
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "BookMarkedStoresViewController") as! BookMarkedStoresViewController
            myVC.isRedirectionFromSideBar = true
            self.navigationController?.pushViewController(myVC, animated: true)
            break
        default:
            break
        }
    }
}


//MARK: Table Cell
class MenuTableCell: UITableViewCell {
    //MARK: IBoutlets
    @IBOutlet weak var icons: ImageView!
    @IBOutlet weak var menuLabel: DesignableUILabel!
    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var backView: View!
}


class MenuFooterTableCell : UITableViewCell{
    
    @IBOutlet weak var versionLabel: UILabel! //Version : 1.0.5
    
}
