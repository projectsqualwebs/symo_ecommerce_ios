//
//  PaymentViewController.swift
//  Symo New
//
//  Created by Sagar Pandit on 05/05/21.
//

import UIKit


class PaymentViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var transactionTable: UITableView!
    @IBOutlet weak var paymentHeadingLabel: DesignableUILabel!
    @IBOutlet weak var searchTransactionField: UITextField!
    @IBOutlet weak var addNewPaymentCardButton: CustomButton!
    
    let myCardData = [String]()
    var isRedirectionFromSideBar = false
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationController.shared.changeStatusBar(color: K_STATUS_BAR_PINK)
        self.paymentHeadingLabel.text = "Payments".localizableString(loc: Singleton.shared.language ?? "en")
        self.searchTransactionField.placeholder = "Search transactions".localizableString(loc: Singleton.shared.language ?? "en")
        self.addNewPaymentCardButton.setTitle("Add new payment card".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.transactionTable.estimatedRowHeight = 90
        self.transactionTable.rowHeight = UITableView.automaticDimension
        handleTextAlignmentForArabic(view: self.view)
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        if isRedirectionFromSideBar == true {
            self.menu()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addnewCardAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}

//MARK: Table Delegate
extension PaymentViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableCell") as! CategoryTableCell
        return cell
    }
    
}

