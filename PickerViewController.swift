//
//  PickerViewController.swift
//  Symo New
//
//  Created by Apple on 06/04/21
//

import UIKit

protocol SelectFromPicker {
    func selectedPickerData(val:String, pos: Int)
}

class PickerViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
  
    //MARK: IBOutlets
    @IBOutlet weak var pickerHeading: DesignableUILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var okButton: CustomButton!
    
    var pickerDelegate: SelectFromPicker? = nil
    var pickerData = [String]()
    var selectedVal = String()
    var selectedIndex = Int()
    var headingLabel = String()
    var showInEnglishOnly = false
    
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pickerView.delegate = self
        self.pickerView.reloadAllComponents()
        self.okButton.setTitle("OK".localizableString(loc: Singleton.shared.language ?? "en"), for: .normal)
        self.pickerHeading.text = self.headingLabel.localizableString(loc: Singleton.shared.language ?? "en")
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerData.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.pickerData[row].localizableString(loc: showInEnglishOnly ? "en" : Singleton.shared.language ?? "en")
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedVal = self.pickerData[row]
        self.selectedIndex = row
        
    }
    
    
    
    //MARK: IBActions
    @IBAction func okAction(_ sender: Any) {
        let val = self.pickerView.selectedRow(inComponent: 0)
        
        self.pickerDelegate?.selectedPickerData(val: self.pickerData[val], pos: val)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
